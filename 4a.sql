-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 17 2020 г., 10:38
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `4a`
--
CREATE DATABASE IF NOT EXISTS `4a` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `4a`;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 0,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'WuFD0U9EnzwLT2JA9Z85Yg6ZyTiY12vAorNF3XHlGCGMHUIYmNz8fgX6PXAdbb6UV0CndSAaTfAwCY4WV1oWUfTztQBmIaBm2INUZfsCn0WcrDD0eaWvBmqQ77g8QJpJ86VoC1dhqfDpjKnaXQ0jGrJBXZiW76AER7FGZNcxHb3uiaLcHVNrdXoA7whfePMMi8GBu1Y6g1QKZkdHYLIu7hMZHvE6dCMgjzoJoIauNQAKXwYZYHZdtUVWq9HfzerB', 1, '2019-05-16 14:35:22', '2019-05-16 14:35:22');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 1, '192.168.11.1'),
(2, 1, '192.168.99.1'),
(3, 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(130, 1, '3c9b1e2ee2728e1db212a3f874', '127.0.0.1', '2020-07-22 11:37:11', '2020-07-22 11:37:11'),
(129, 1, 'c8c36ca4c2cf098fee0cf6d757', '127.0.0.1', '2020-07-13 16:33:55', '2020-07-13 16:33:55'),
(128, 1, '9d3565a1ff120d2637424221bf', '127.0.0.1', '2020-07-06 17:05:09', '2020-07-06 17:05:09'),
(127, 1, 'ae9d3768b7179ede037f000587', '127.0.0.1', '2020-06-16 18:22:50', '2020-06-16 18:22:50'),
(131, 1, '4a3f768885267354a118ed13b8', '127.0.0.1', '2020-09-21 12:14:54', '2020-09-21 12:14:54'),
(132, 1, '9357d5b339b03fbe444bbea226', '127.0.0.1', '2020-10-22 19:52:03', '2020-10-22 19:52:03'),
(133, 1, 'b63e5fd325372ccbb55f16675d', '127.0.0.1', '2020-11-12 11:56:33', '2020-11-12 11:56:33'),
(134, 1, '443aa0bca15c462321801426d1', '127.0.0.1', '2020-11-25 18:07:22', '2020-11-25 18:07:22'),
(135, 1, '2a5297158397fe750b72b5721a', '127.0.0.1', '2020-12-06 17:32:41', '2020-12-06 17:32:41'),
(136, 1, '1c0abdf14b1321280d4229f86a', '127.0.0.1', '2020-12-07 17:42:47', '2020-12-07 17:42:47'),
(137, 1, '72afdb9f727038240877d805c4', '127.0.0.1', '2020-12-10 00:41:43', '2020-12-10 00:41:43'),
(138, 1, '85dd0cda033fce33e3106b1cfc', '127.0.0.1', '2020-12-10 20:20:24', '2020-12-10 20:20:24');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article`
--

CREATE TABLE `oc_article` (
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_available` date NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `article_review` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT 0,
  `gstatus` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_description`
--

CREATE TABLE `oc_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_image`
--

CREATE TABLE `oc_article_image` (
  `article_image_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related`
--

CREATE TABLE `oc_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_mn`
--

CREATE TABLE `oc_article_related_mn` (
  `article_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_product`
--

CREATE TABLE `oc_article_related_product` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_article_related_product`
--

INSERT INTO `oc_article_related_product` (`article_id`, `product_id`) VALUES
(30, 123),
(31, 123),
(43, 123),
(45, 123);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_related_wb`
--

CREATE TABLE `oc_article_related_wb` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_blog_category`
--

CREATE TABLE `oc_article_to_blog_category` (
  `article_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `main_blog_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_download`
--

CREATE TABLE `oc_article_to_download` (
  `article_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_layout`
--

CREATE TABLE `oc_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_article_to_store`
--

CREATE TABLE `oc_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `space_between` int(10) NOT NULL DEFAULT 0,
  `slides_per_view` int(10) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category`
--

CREATE TABLE `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_description`
--

CREATE TABLE `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_path`
--

CREATE TABLE `oc_blog_category_path` (
  `blog_category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_layout`
--

CREATE TABLE `oc_blog_category_to_layout` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_blog_category_to_store`
--

CREATE TABLE `oc_blog_category_to_store` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Армения', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Азербайджан', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Белоруссия (Беларусь)', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Грузия', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Казахстан', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Киргизия (Кыргызстан)', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Молдова', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Российская Федерация', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Таджикистан', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Туркменистан', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Украина', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Узбекистан', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Рубль', 'RUB', '', ' ₽', '2', 1.00000000, 1, '2020-12-10 10:20:24'),
(2, 'US Dollar', 'USD', '$', '', '2', 0.01697793, 0, '2019-05-16 14:48:35'),
(3, 'Euro', 'EUR', '', '€', '2', 0.01476363, 0, '2019-05-16 14:48:31'),
(4, 'Гривна', 'UAH', '', 'грн.', '2', 0.44016022, 0, '2019-05-16 14:48:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text DEFAULT NULL,
  `wishlist` text DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `address_id` int(11) NOT NULL DEFAULT 0,
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_activity`
--

INSERT INTO `oc_customer_activity` (`customer_activity_id`, `customer_id`, `key`, `data`, `ip`, `date_added`) VALUES
(1, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":42}', '192.168.11.1', '2019-08-05 20:20:00'),
(2, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":43}', '192.168.11.1', '2019-08-05 20:42:43'),
(3, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":45}', '192.168.11.1', '2019-08-05 21:50:48'),
(4, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":46}', '192.168.11.1', '2019-08-05 22:06:05'),
(5, 0, 'order_guest', '{\"name\":\" \",\"order_id\":47}', '192.168.11.1', '2019-08-06 11:07:32'),
(6, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u0440\\u0435\\u043d\\u043a\\u0430\\u043d \\u0412 \\u0421 \",\"order_id\":48}', '192.168.11.1', '2019-08-06 11:47:55'),
(7, 0, 'order_guest', '{\"name\":\"Qwe \",\"order_id\":49}', '192.168.11.1', '2019-08-06 11:49:44'),
(8, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":50}', '192.168.11.1', '2019-08-06 14:43:48'),
(9, 0, 'order_guest', '{\"name\":\" \",\"order_id\":51}', '192.168.11.1', '2019-08-06 16:56:50'),
(10, 0, 'order_guest', '{\"name\":\" \",\"order_id\":52}', '192.168.11.1', '2019-08-06 17:02:49'),
(11, 0, 'order_guest', '{\"name\":\" \",\"order_id\":53}', '192.168.11.1', '2019-08-06 18:08:36'),
(12, 0, 'order_guest', '{\"name\":\" \",\"order_id\":54}', '192.168.11.1', '2019-08-06 18:09:46'),
(13, 0, 'order_guest', '{\"name\":\"wqeqw \",\"order_id\":55}', '192.168.11.1', '2019-08-06 18:11:54'),
(14, 0, 'order_guest', '{\"name\":\" \",\"order_id\":57}', '192.168.11.1', '2019-08-06 18:54:54'),
(15, 0, 'order_guest', '{\"name\":\" \",\"order_id\":58}', '192.168.11.1', '2019-08-07 11:27:38'),
(16, 0, 'order_guest', '{\"name\":\" \",\"order_id\":60}', '192.168.11.1', '2019-08-07 13:02:14'),
(17, 0, 'order_guest', '{\"name\":\" \",\"order_id\":61}', '192.168.11.1', '2019-08-07 13:03:50'),
(18, 0, 'order_guest', '{\"name\":\" \",\"order_id\":75}', '192.168.11.1', '2019-08-07 14:19:56'),
(19, 0, 'order_guest', '{\"name\":\" \",\"order_id\":78}', '192.168.11.1', '2019-08-07 14:45:28'),
(20, 0, 'order_guest', '{\"name\":\" \",\"order_id\":79}', '192.168.11.1', '2019-08-07 15:39:12'),
(21, 0, 'order_guest', '{\"name\":\" \",\"order_id\":80}', '192.168.11.1', '2019-08-07 15:44:45'),
(22, 0, 'order_guest', '{\"name\":\" \",\"order_id\":82}', '192.168.11.1', '2019-08-07 17:33:16'),
(23, 0, 'order_guest', '{\"name\":\" \",\"order_id\":83}', '192.168.11.1', '2019-08-07 17:44:12'),
(24, 0, 'order_guest', '{\"name\":\" \",\"order_id\":84}', '192.168.11.1', '2019-08-07 17:48:00'),
(25, 0, 'order_guest', '{\"name\":\" \",\"order_id\":85}', '192.168.11.1', '2019-08-07 19:05:54'),
(26, 0, 'order_guest', '{\"name\":\" \",\"order_id\":86}', '192.168.11.1', '2019-08-07 19:09:00'),
(27, 0, 'order_guest', '{\"name\":\" \",\"order_id\":87}', '192.168.11.1', '2019-08-07 19:16:01'),
(28, 0, 'order_guest', '{\"name\":\" \",\"order_id\":88}', '192.168.11.1', '2019-08-07 19:38:26'),
(29, 0, 'order_guest', '{\"name\":\" \",\"order_id\":89}', '192.168.11.1', '2019-08-07 19:41:17'),
(30, 0, 'order_guest', '{\"name\":\" \",\"order_id\":90}', '192.168.11.1', '2019-08-07 19:48:08'),
(31, 0, 'order_guest', '{\"name\":\" \",\"order_id\":91}', '192.168.11.1', '2019-08-07 19:50:20'),
(32, 0, 'order_guest', '{\"name\":\" \",\"order_id\":92}', '192.168.11.1', '2019-08-07 19:56:14'),
(33, 0, 'order_guest', '{\"name\":\" \",\"order_id\":93}', '192.168.11.1', '2019-08-08 10:10:46'),
(34, 0, 'order_guest', '{\"name\":\" \",\"order_id\":94}', '192.168.11.1', '2019-08-08 14:06:55'),
(35, 0, 'order_guest', '{\"name\":\" \",\"order_id\":95}', '192.168.11.1', '2019-08-08 14:09:14'),
(36, 0, 'order_guest', '{\"name\":\" \",\"order_id\":96}', '192.168.11.1', '2019-08-08 15:02:31'),
(37, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":97}', '192.168.11.1', '2019-08-08 15:08:28'),
(38, 0, 'order_guest', '{\"name\":\" \",\"order_id\":98}', '192.168.11.1', '2019-08-08 15:15:13'),
(39, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":99}', '192.168.11.1', '2019-08-08 15:15:48'),
(40, 0, 'order_guest', '{\"name\":\" \",\"order_id\":100}', '192.168.11.1', '2019-08-09 13:22:45'),
(41, 0, 'order_guest', '{\"name\":\" \",\"order_id\":101}', '192.168.11.1', '2019-08-11 17:20:18'),
(42, 0, 'order_guest', '{\"name\":\" \",\"order_id\":103}', '192.168.11.1', '2019-08-16 12:32:10'),
(62, 0, 'order_guest', '{\"name\":\" \",\"order_id\":111}', '192.168.11.1', '2019-09-07 13:02:44'),
(61, 0, 'order_guest', '{\"name\":\" \",\"order_id\":110}', '192.168.11.1', '2019-09-06 19:47:02'),
(60, 0, 'order_guest', '{\"name\":\"Daniil \",\"order_id\":109}', '192.168.11.1', '2019-09-06 19:19:50'),
(59, 0, 'order_guest', '{\"name\":\" \",\"order_id\":108}', '192.168.11.1', '2019-09-02 16:47:56'),
(58, 0, 'order_guest', '{\"name\":\" \",\"order_id\":107}', '192.168.11.1', '2019-08-21 19:23:56'),
(57, 0, 'order_guest', '{\"name\":\" \",\"order_id\":106}', '192.168.11.1', '2019-08-20 14:10:59'),
(67, 0, 'order_guest', '{\"name\":\" \",\"order_id\":113}', '192.168.11.1', '2019-09-10 21:22:14'),
(68, 0, 'order_guest', '{\"name\":\" \",\"order_id\":114}', '192.168.11.1', '2019-09-11 12:03:32'),
(69, 0, 'order_guest', '{\"name\":\" \",\"order_id\":115}', '192.168.11.1', '2019-09-16 15:03:23'),
(71, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u043c\\u0435\\u0448\\u0438\\u043d \\u0410\\u043b\\u0435\\u043a\\u0441\\u0435\\u0439 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u0438\\u0447  \",\"order_id\":116}', '192.168.11.1', '2019-09-16 21:16:22'),
(72, 0, 'order_guest', '{\"name\":\" \",\"order_id\":117}', '192.168.11.1', '2019-09-18 15:26:42'),
(73, 0, 'order_guest', '{\"name\":\"\\u0414\\u043c\\u0438\\u0442\\u0440\\u0438\\u0439 \",\"order_id\":118}', '192.168.11.1', '2019-09-18 15:38:45'),
(74, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":119}', '192.168.11.1', '2019-09-18 15:44:49'),
(75, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":120}', '192.168.11.1', '2019-09-18 15:54:52'),
(76, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":121}', '192.168.11.1', '2019-09-18 16:11:23'),
(77, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":122}', '192.168.11.1', '2019-09-18 16:14:25'),
(78, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":123}', '192.168.11.1', '2019-09-18 16:15:34'),
(79, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":124}', '192.168.11.1', '2019-09-18 16:17:43'),
(80, 0, 'order_guest', '{\"name\":\"Dmitry Chernyavsky \",\"order_id\":125}', '192.168.11.1', '2019-09-18 16:21:53'),
(82, 0, 'order_guest', '{\"name\":\"\\u041a\\u043b\\u0438\\u043c\\u043a\\u043e\\u0432\\u0438\\u0447 \\u041d\\u0438\\u043d\\u0430 \",\"order_id\":127}', '192.168.11.1', '2019-09-24 17:32:33'),
(83, 0, 'order_guest', '{\"name\":\" \",\"order_id\":128}', '192.168.11.1', '2019-10-01 09:25:50'),
(85, 0, 'order_guest', '{\"name\":\"\\u041a\\u043b\\u0438\\u043c\\u043a\\u043e\\u0432\\u0438\\u0447 \\u041c\\u0438\\u0445\\u0430\\u0438\\u043b  \",\"order_id\":129}', '192.168.11.1', '2019-10-03 21:35:05'),
(86, 0, 'order_guest', '{\"name\":\" \",\"order_id\":130}', '192.168.99.1', '2019-10-22 15:42:00'),
(87, 0, 'order_guest', '{\"name\":\"\\u041f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u043e \",\"order_id\":131}', '192.168.99.1', '2019-10-29 18:50:46'),
(88, 0, 'order_guest', '{\"name\":\"Dmitry \",\"order_id\":132}', '192.168.99.1', '2019-10-31 18:39:01'),
(91, 0, 'order_guest', '{\"name\":\"\\u0422\\u043e\\u0440\\u043e\\u0441\\u044f\\u043d \\u0420\\u0430\\u0444\\u0430\\u044d\\u043b\\u043b\\u0430 \",\"order_id\":133}', '192.168.99.1', '2019-11-27 16:39:07'),
(94, 0, 'order_guest', '{\"name\":\"\\u0421\\u0432\\u0435\\u0442\\u043b\\u0430\\u043d\\u0430 \\u0415\\u0440\\u043e\\u0445\\u0438\\u043d\\u0430 \",\"order_id\":134}', '192.168.99.1', '2019-12-06 11:41:32'),
(97, 0, 'order_guest', '{\"name\":\" \",\"order_id\":135}', '192.168.99.1', '2019-12-18 14:18:17'),
(99, 0, 'order_guest', '{\"name\":\"\\u0428\\u0435\\u0441\\u0442\\u0430\\u043a\\u043e\\u0432\\u0430 \\u041d\\u0438\\u043d\\u0430 \",\"order_id\":136}', '192.168.99.1', '2019-12-26 16:52:03'),
(102, 0, 'order_guest', '{\"name\":\" \",\"order_id\":137}', '192.168.99.1', '2020-01-07 19:05:37'),
(104, 0, 'order_guest', '{\"name\":\"\\u0418\\u041f \\u041c\\u0430\\u0441\\u043b\\u0430\\u043a\\u043e\\u0432\\u0430 \\u0418. \\u0414.  \",\"order_id\":138}', '192.168.99.1', '2020-01-10 10:01:51'),
(105, 0, 'order_guest', '{\"name\":\" \",\"order_id\":139}', '192.168.99.1', '2020-01-10 12:55:36'),
(117, 0, 'order_guest', '{\"name\":\" \",\"order_id\":140}', '192.168.99.1', '2020-01-20 16:29:27'),
(118, 0, 'order_guest', '{\"name\":\" \",\"order_id\":141}', '192.168.99.1', '2020-01-22 15:16:22'),
(120, 0, 'order_guest', '{\"name\":\" \",\"order_id\":142}', '192.168.99.1', '2020-01-29 14:39:26'),
(121, 0, 'order_guest', '{\"name\":\" \",\"order_id\":143}', '192.168.99.1', '2020-02-18 16:36:49'),
(122, 0, 'order_guest', '{\"name\":\" \",\"order_id\":144}', '192.168.99.1', '2020-02-19 20:37:29'),
(123, 0, 'order_guest', '{\"name\":\" \",\"order_id\":145}', '192.168.99.1', '2020-02-20 18:51:57'),
(124, 0, 'order_guest', '{\"name\":\"\\u041c\\u0430\\u043c\\u0435\\u0448\\u0438\\u043d \\u0410. \\u0410.  \",\"order_id\":146}', '192.168.99.1', '2020-02-21 11:15:18'),
(125, 0, 'order_guest', '{\"name\":\" \",\"order_id\":147}', '192.168.99.1', '2020-03-02 14:17:16'),
(126, 0, 'order_guest', '{\"name\":\" \",\"order_id\":148}', '192.168.99.1', '2020-03-02 14:22:01'),
(128, 0, 'order_guest', '{\"name\":\"\\u041f\\u043e\\u0441\\u0442\\u043e\\u043b\\u0430\\u043a\\u044f\\u043d \\u0412\\u0430\\u0440\\u0434\\u0443\\u0438 \",\"order_id\":149}', '192.168.99.1', '2020-03-10 10:29:06'),
(129, 0, 'order_guest', '{\"name\":\" \",\"order_id\":150}', '192.168.99.1', '2020-03-16 15:33:06'),
(130, 0, 'order_guest', '{\"name\":\"\\u0410\\u0440\\u0442\\u0435\\u043c \\u0410\\u043d\\u0442\\u043e\\u0448\\u0438\\u043a \",\"order_id\":151}', '192.168.99.1', '2020-03-25 11:53:20'),
(131, 0, 'order_guest', '{\"name\":\" \",\"order_id\":152}', '127.0.0.1', '2020-04-02 23:08:26'),
(132, 0, 'order_guest', '{\"name\":\" \",\"order_id\":153}', '192.168.99.1', '2020-04-03 12:28:42');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT 0.00,
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1),
(4, 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Физическое лицо', 'Физическое лицо'),
(4, 1, 'Юридическое лицо', 'Юридическое лицо');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(1, 'nata.quality1980.rozovskaya@mail.ru', '192.168.11.1', 1, '2019-09-11 18:40:33', '2019-09-11 18:40:33'),
(2, '666@4ait.ru', '192.168.11.1', 2, '2019-09-18 02:47:51', '2019-09-18 02:48:02'),
(3, 'dos@4ait.ru', '192.168.11.1', 3, '2019-09-18 02:48:20', '2019-09-18 02:49:40');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_online`
--

INSERT INTO `oc_customer_online` (`ip`, `customer_id`, `url`, `referer`, `date_added`) VALUES
('127.0.0.1', 0, 'http://4a/call', 'http://4a/zayavka', '2020-12-13 02:30:13');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `order_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `dadata_field` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field`
--

INSERT INTO `oc_custom_field` (`custom_field_id`, `type`, `value`, `validation`, `location`, `status`, `sort_order`, `dadata_field`) VALUES
(1, 'text', '', '', 'address', 1, 10, 'PARTY');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field_customer_group`
--

INSERT INTO `oc_custom_field_customer_group` (`custom_field_id`, `customer_group_id`, `required`) VALUES
(1, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_custom_field_description`
--

INSERT INTO `oc_custom_field_description` (`custom_field_id`, `language_id`, `name`) VALUES
(1, 1, 'Компания');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_email_template`
--

CREATE TABLE `oc_email_template` (
  `email_template_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  `background` varchar(7) COLLATE utf8_bin NOT NULL,
  `body` varchar(7) COLLATE utf8_bin NOT NULL,
  `heading` varchar(7) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `store_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_email_template`
--

INSERT INTO `oc_email_template` (`email_template_id`, `name`, `type`, `background`, `body`, `heading`, `image`, `store_id`, `priority`, `date_start`, `date_end`, `date_added`) VALUES
(1, 'Регистрация аккаунта', 'register', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:26'),
(2, 'Регистрация партнера', 'affiliate', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:31'),
(3, 'Обратная связь', 'contact', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:36'),
(4, 'Восстановление пароля', 'forgotten', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:40'),
(5, 'Оформление заказа', 'order', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:48'),
(7, 'Изменение статуса заказа', '2', '#333333', '#eeeeee', '#19bef0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-04-18 13:07:54'),
(8, 'Добавление бонусов', 'reward', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 15:51:00'),
(9, 'Подтверждение аккаунта', 'account_approve', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:06:54'),
(10, 'Кредит магазина', 'account_transaction', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:08:35'),
(11, 'Подтверждение аккаунта', 'affiliate_approve', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:10:38'),
(12, 'Комиссия партнера', 'affiliate_transaction', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43'),
(13, 'Изменение статуса возврата', 'return_3', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43'),
(14, 'Подарочный сертификат', 'gift_voucher', '#333333', '#eeeeee', '#19bdf0', '', 0, 0, '2013-04-16', '2025-01-01', '2013-05-30 20:12:43');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_email_template_description`
--

CREATE TABLE `oc_email_template_description` (
  `email_template_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_email_template_description`
--

INSERT INTO `oc_email_template_description` (`email_template_id`, `language_id`, `subject`, `message`) VALUES
(1, 1, 'Welcome to 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Welcome {firstname} and thank you for registering at 4paper - магазин чековых лент и этикеток!&lt;/p&gt;\r\n\r\n&lt;p&gt;Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;raquo; Login:&amp;nbsp;&lt;a href=&quot;https://4paper.ru/index.php?route=account/login&quot;&gt;https://4paper.ru/index.php?route=account/login&lt;/a&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br /&gt;\r\n&lt;span style=&quot;line-height: 1.6em;&quot;&gt;&amp;raquo; Password: {password}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(2, 1, 'Welcome to 4paper - магазин чековых лент и этикеток\'s Affiliate Program', '&lt;p&gt;Welcome {firstname} and thank you for joining 4paper - магазин чековых лент и этикеток\'s Affiliate Program.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;https://4paper.ru/index.php?route=affiliate/login&quot;&gt;https://4paper.ru/index.php?route=affiliate/login&lt;/a&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px; line-height: 1.6em;&quot;&gt;&amp;raquo; Password: {password}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(3, 1, 'Thank you for contacting 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;Thank you for contacting us. We will get back to you as soon as possible.&lt;/p&gt;\r\n\r\n&lt;p&gt;Below is what you had sent to us:&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;{enquiry}&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;If you had entered anything wrongly, feel free to fill in another contact form.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(4, 1, 'Восстановление пароля', '&lt;p&gt;Привет {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;Вы запросили восстановление пароля. Для того, чтобы сбросить пароль перейдите по ссылке:&lt;/p&gt;\r\n\r\n&lt;span style=&quot;font-size: 13px; line-height: 1.6em;&quot;&gt;&amp;raquo; Ссылка: {restore_link}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Если вы не запрашивали восстановение пароля - проигнорируйте это письмо.&lt;/p&gt;\r\n\r\n&lt;p&gt;С найлучшими пожеланиями,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(5, 1, 'Your Order at 4paper - магазин чековых лент и этикеток', '&lt;p&gt;Thank you for your interest in our products. Your order has been received and will be processed once payment has been confirmed.&lt;/p&gt;\r\n\r\n&lt;table style=&quot;border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;&quot;&gt;\r\n	&lt;thead&gt;\r\n		&lt;tr&gt;\r\n			&lt;td colspan=&quot;2&quot; style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Order Details&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/thead&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;&lt;b&gt;Order ID:&lt;/b&gt; {order_id}&lt;br /&gt;\r\n			&lt;b&gt;Date Added:&lt;/b&gt; {date_added}&lt;br /&gt;\r\n			&lt;b&gt;Payment Method:&lt;/b&gt; {payment_method}&lt;br /&gt;\r\n			&lt;b&gt;Shipping Method:&lt;/b&gt; {shipping_method}&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;&lt;b&gt;Email:&lt;/b&gt; {email}&lt;br /&gt;\r\n			&lt;b&gt;Telephone:&lt;/b&gt; {telephone}&lt;br /&gt;\r\n			&lt;b&gt;IP Address:&lt;/b&gt; {ip}&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;p&gt;{comment_table}&lt;/p&gt;\r\n\r\n&lt;table style=&quot;border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;&quot;&gt;\r\n	&lt;thead&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Payment Address:&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #cccccc; font-weight: bold; text-align: left; padding: 7px; color: #222222;&quot;&gt;Shipping Address:&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/thead&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff;&quot;&gt;{payment_address}&lt;/td&gt;\r\n			&lt;td style=&quot;font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;background:#ffffff&quot;&gt;{shipping_address}&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;p&gt;{product_table}&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(7, 1, 'Processing your Order #{order_id}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;We are currently processing your order. Feel free drop us an email if you have any queries.&lt;/p&gt;\r\n\r\n&lt;p&gt;{comment}&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n'),
(8, 1, 'Reward Points', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;You have received {points} reward points! You may now spend it in our store.&lt;/p&gt;\r\n\r\n&lt;p&gt;You currently have a total of {total_points} reward points.&lt;/p&gt;\r\n\r\n&lt;p&gt;Best regards,&lt;br /&gt;\r\n4paper - магазин чековых лент и этикеток&lt;/p&gt;\r\n\r\n'),
(9, 1, 'Account Approved', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;Your account has now been approved and you can log in by using your email address and password by visiting our website or at the following URL:&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;https://4paper.ru/index.php?route=account/login&quot; style=&quot;font-size: 13px;&quot;&gt;https://4paper.ru/index.php?route=account/login&lt;/a&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&lt;span style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Email: {email}&lt;/span&gt;&lt;br style=&quot;font-size: 13px;&quot; /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.&lt;/p&gt;\r\n\r\n'),
(10, 1, 'Store Credits', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have received {credits} store credits! You may now spend it in our store.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You currently have a total of {total_credits} store credits.&lt;/p&gt;\r\n\r\n'),
(11, 1, 'Affiliate Approved', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Welcome {firstname} and thank you for joining 4paper - магазин чековых лент и этикеток\'s Affiliate Program.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Your account has been approved.&amp;nbsp;You can now log in by using your email address and password by visiting our website or at the following URL:&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;&amp;raquo; Login:&amp;nbsp;&lt;a href=&quot;https://4paper.ru/index.php?route=affiliate/login&quot;&gt;https://4paper.ru/index.php?route=affiliate/login&lt;/a&gt;&lt;br /&gt;\r\n&amp;raquo; Email: {email}&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.&lt;/p&gt;\r\n\r\n'),
(12, 1, 'Commission Earned', '&lt;p style=&quot;font-size: 13px;&quot;&gt;Hi {firstname},&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have received {commission} commission!&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px;&quot;&gt;You have currently earned&amp;nbsp;a total of {total_commission} commission.&lt;/p&gt;\r\n\r\n'),
(13, 1, 'Product Return Request Complete', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Your return request (Return ID {return_id}) has been completed.&lt;/p&gt;&lt;p&gt;{comment}&lt;/p&gt;&lt;p&gt;Best regards,&lt;br /&gt;4paper - магазин чековых лент и этикеток&lt;/p&gt;'),
(14, 1, 'You have been sent a gift voucher from {from_name}', '&lt;p&gt;Hi {to_name},&lt;/p&gt;&lt;p&gt;{voucher_theme}&lt;/p&gt;&lt;p&gt;You have received a gift voucher worth {amount}.&lt;/p&gt;&lt;p&gt;Message from {from_name}:&lt;/p&gt;&lt;p&gt;{message}&lt;/p&gt;&lt;p&gt;You can redeem the gift voucher with the&amp;nbsp;code &lt;strong&gt;{code}&lt;/strong&gt; on our website.&lt;/p&gt;&lt;p&gt;Best regards,&lt;br /&gt;4paper - магазин чековых лент и этикеток&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0),
(34, 'module_quickcheckout', 'catalog/controller/checkout/checkout/before', 'extension/quickcheckout/checkout/eventPreControllerCheckoutCheckout', 1, 0),
(35, 'module_quickcheckout', 'catalog/controller/checkout/success/before', 'extension/quickcheckout/checkout/eventPreControllerCheckoutSuccess', 1, 0),
(36, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/override', 1, 499),
(37, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/render', 1, 999),
(38, 'module_template_switcher', 'catalog/controller/*/before', 'extension/module/template_switcher/before', 1, 0),
(39, 'module_template_switcher', 'admin/view/*/before', 'extension/module/template_switcher/override', 1, 0),
(40, 'module_template_switcher', 'admin/view/design/layout_form/before', 'extension/module/template_switcher/eventViewDesignLayoutFormBefore', 1, 0),
(41, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/override', 1, 499),
(42, 'module_template_switcher', 'catalog/view/*/before', 'extension/module/template_switcher/render', 1, 999),
(43, 'module_template_switcher', 'catalog/controller/*/before', 'extension/module/template_switcher/before', 1, 0),
(44, 'module_template_switcher', 'admin/view/*/before', 'extension/module/template_switcher/override', 1, 0),
(45, 'module_template_switcher', 'admin/view/design/layout_form/before', 'extension/module/template_switcher/eventViewDesignLayoutFormBefore', 1, 0),
(46, 'xshippingpro', 'catalog/view/mail/order_add/before', 'extension/shipping/xshippingpro/onOrderEmail', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(8, 'total', 'credit'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(71, 'module', 'ocfilter'),
(63, 'module', 'trade_import'),
(53, 'total', 'sub_total'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(52, 'module', 'extendedsearch'),
(54, 'module', 'quickcheckout'),
(64, 'module', 'manager'),
(65, 'module', 'account'),
(66, 'analytics', 'metrika'),
(67, 'feed', 'yandex_sitemap'),
(68, 'feed', 'yandex_market'),
(70, 'module', 'template_switcher'),
(73, 'payment', 'rbs'),
(76, 'shipping', 'xshippingpro');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(1, 0, 'svg-support.ocmod.zip', '2019-05-16 14:45:33'),
(2, 0, 'opencart-3-x-export-import-multilingual-3-20-cloud.ocmod.zip', '2019-05-16 14:45:56'),
(3, 0, 'Limit_Autocomplete_v3.3.ocmod.zip', '2019-05-16 14:46:10'),
(4, 0, 'user.group.visual.3.x.ocmod.zip', '2019-05-16 14:47:00'),
(19, 0, 'ocfilter.ocmod.zip', '2020-05-20 04:18:56'),
(8, 0, 'quickcheckout190_oc3.ocmod.zip', '2019-05-22 20:18:39'),
(7, 1253857, 'ExtendedSearch - extends the standard search functionality - extended-search3x.ocmod.zip', '2019-05-16 16:11:10'),
(13, 0, 'admin_detail_order_email.ocmod.zip', '2019-05-24 17:22:29'),
(20, 0, 'sessions_gc.ocmod.zip', '2020-05-20 04:19:03'),
(15, 0, 'localcopy_oc3.ocmod.zip', '2019-08-07 15:31:32'),
(16, 0, 'order_manager_oc3.ocmod.zip', '2019-08-07 15:31:44'),
(17, 0, 'order_manager_oc3.ocmod.zip', '2019-08-07 15:32:01'),
(18, 0, 'yandexsitemap.ocmod.zip', '2019-09-16 15:55:23'),
(21, 0, 'opencart3.0fixes.ocmod.zip', '2020-05-20 04:19:08'),
(22, 0, 'modificationmanager.ocmod.zip', '2020-05-20 04:19:14'),
(23, 0, 'option-markups_oc3x.ocmod.zip', '2020-05-28 14:18:06'),
(25, 0, 'xshippingpro-oc3-cloud.ocmod.zip', '2020-07-22 12:05:38');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension_path`
--

INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1, 2, 'system/library/export_import', '2019-05-16 14:45:57'),
(2, 2, 'admin/controller/extension/export_import.php', '2019-05-16 14:45:57'),
(3, 2, 'admin/model/extension/export_import.php', '2019-05-16 14:45:57'),
(4, 2, 'admin/view/image/export-import', '2019-05-16 14:45:57'),
(5, 2, 'admin/view/stylesheet/export_import.css', '2019-05-16 14:45:57'),
(6, 2, 'system/library/export_import/Classes', '2019-05-16 14:45:57'),
(7, 2, 'admin/language/en-gb/extension/export_import.php', '2019-05-16 14:45:57'),
(8, 2, 'admin/view/image/export-import/loading.gif', '2019-05-16 14:45:57'),
(9, 2, 'admin/view/template/extension/export_import.twig', '2019-05-16 14:45:57'),
(10, 2, 'system/library/export_import/Classes/PHPExcel', '2019-05-16 14:45:57'),
(11, 2, 'system/library/export_import/Classes/PHPExcel.php', '2019-05-16 14:45:57'),
(12, 2, 'system/library/export_import/Classes/PHPExcel/Autoloader.php', '2019-05-16 14:45:57'),
(13, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage', '2019-05-16 14:45:57'),
(14, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorageFactory.php', '2019-05-16 14:45:57'),
(15, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine', '2019-05-16 14:45:57'),
(16, 2, 'system/library/export_import/Classes/PHPExcel/Calculation', '2019-05-16 14:45:57'),
(17, 2, 'system/library/export_import/Classes/PHPExcel/Calculation.php', '2019-05-16 14:45:57'),
(18, 2, 'system/library/export_import/Classes/PHPExcel/Cell', '2019-05-16 14:45:57'),
(19, 2, 'system/library/export_import/Classes/PHPExcel/Cell.php', '2019-05-16 14:45:57'),
(20, 2, 'system/library/export_import/Classes/PHPExcel/Chart', '2019-05-16 14:45:57'),
(21, 2, 'system/library/export_import/Classes/PHPExcel/Chart.php', '2019-05-16 14:45:57'),
(22, 2, 'system/library/export_import/Classes/PHPExcel/Comment.php', '2019-05-16 14:45:57'),
(23, 2, 'system/library/export_import/Classes/PHPExcel/DocumentProperties.php', '2019-05-16 14:45:57'),
(24, 2, 'system/library/export_import/Classes/PHPExcel/DocumentSecurity.php', '2019-05-16 14:45:57'),
(25, 2, 'system/library/export_import/Classes/PHPExcel/Exception.php', '2019-05-16 14:45:57'),
(26, 2, 'system/library/export_import/Classes/PHPExcel/HashTable.php', '2019-05-16 14:45:57'),
(27, 2, 'system/library/export_import/Classes/PHPExcel/Helper', '2019-05-16 14:45:57'),
(28, 2, 'system/library/export_import/Classes/PHPExcel/IComparable.php', '2019-05-16 14:45:57'),
(29, 2, 'system/library/export_import/Classes/PHPExcel/IOFactory.php', '2019-05-16 14:45:57'),
(30, 2, 'system/library/export_import/Classes/PHPExcel/NamedRange.php', '2019-05-16 14:45:57'),
(31, 2, 'system/library/export_import/Classes/PHPExcel/Reader', '2019-05-16 14:45:57'),
(32, 2, 'system/library/export_import/Classes/PHPExcel/ReferenceHelper.php', '2019-05-16 14:45:57'),
(33, 2, 'system/library/export_import/Classes/PHPExcel/RichText', '2019-05-16 14:45:57'),
(34, 2, 'system/library/export_import/Classes/PHPExcel/RichText.php', '2019-05-16 14:45:57'),
(35, 2, 'system/library/export_import/Classes/PHPExcel/Settings.php', '2019-05-16 14:45:57'),
(36, 2, 'system/library/export_import/Classes/PHPExcel/Shared', '2019-05-16 14:45:57'),
(37, 2, 'system/library/export_import/Classes/PHPExcel/Style', '2019-05-16 14:45:57'),
(38, 2, 'system/library/export_import/Classes/PHPExcel/Style.php', '2019-05-16 14:45:57'),
(39, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet', '2019-05-16 14:45:57'),
(40, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet.php', '2019-05-16 14:45:57'),
(41, 2, 'system/library/export_import/Classes/PHPExcel/WorksheetIterator.php', '2019-05-16 14:45:57'),
(42, 2, 'system/library/export_import/Classes/PHPExcel/Writer', '2019-05-16 14:45:57'),
(43, 2, 'system/library/export_import/Classes/PHPExcel/locale', '2019-05-16 14:45:57'),
(44, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/APC.php', '2019-05-16 14:45:57'),
(45, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/CacheBase.php', '2019-05-16 14:45:57'),
(46, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/DiscISAM.php', '2019-05-16 14:45:57'),
(47, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/ICache.php', '2019-05-16 14:45:57'),
(48, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Igbinary.php', '2019-05-16 14:45:57'),
(49, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Memcache.php', '2019-05-16 14:45:57'),
(50, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Memory.php', '2019-05-16 14:45:57'),
(51, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/MemoryGZip.php', '2019-05-16 14:45:57'),
(52, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/MemorySerialized.php', '2019-05-16 14:45:57'),
(53, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/PHPTemp.php', '2019-05-16 14:45:57'),
(54, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/SQLite.php', '2019-05-16 14:45:57'),
(55, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/SQLite3.php', '2019-05-16 14:45:57'),
(56, 2, 'system/library/export_import/Classes/PHPExcel/CachedObjectStorage/Wincache.php', '2019-05-16 14:45:57'),
(57, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine/CyclicReferenceStack.php', '2019-05-16 14:45:57'),
(58, 2, 'system/library/export_import/Classes/PHPExcel/CalcEngine/Logger.php', '2019-05-16 14:45:57'),
(59, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Database.php', '2019-05-16 14:45:57'),
(60, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/DateTime.php', '2019-05-16 14:45:57'),
(61, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Engineering.php', '2019-05-16 14:45:57'),
(62, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Exception.php', '2019-05-16 14:45:57'),
(63, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/ExceptionHandler.php', '2019-05-16 14:45:57'),
(64, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Financial.php', '2019-05-16 14:45:57'),
(65, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/FormulaParser.php', '2019-05-16 14:45:57'),
(66, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/FormulaToken.php', '2019-05-16 14:45:57'),
(67, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Function.php', '2019-05-16 14:45:57'),
(68, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Functions.php', '2019-05-16 14:45:57'),
(69, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Logical.php', '2019-05-16 14:45:57'),
(70, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/LookupRef.php', '2019-05-16 14:45:57'),
(71, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/MathTrig.php', '2019-05-16 14:45:57'),
(72, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Statistical.php', '2019-05-16 14:45:57'),
(73, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/TextData.php', '2019-05-16 14:45:57'),
(74, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Token', '2019-05-16 14:45:57'),
(75, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/functionlist.txt', '2019-05-16 14:45:57'),
(76, 2, 'system/library/export_import/Classes/PHPExcel/Cell/AdvancedValueBinder.php', '2019-05-16 14:45:57'),
(77, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DataType.php', '2019-05-16 14:45:57'),
(78, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DataValidation.php', '2019-05-16 14:45:57'),
(79, 2, 'system/library/export_import/Classes/PHPExcel/Cell/DefaultValueBinder.php', '2019-05-16 14:45:57'),
(80, 2, 'system/library/export_import/Classes/PHPExcel/Cell/ExportImportValueBinder.php', '2019-05-16 14:45:57'),
(81, 2, 'system/library/export_import/Classes/PHPExcel/Cell/Hyperlink.php', '2019-05-16 14:45:57'),
(82, 2, 'system/library/export_import/Classes/PHPExcel/Cell/IValueBinder.php', '2019-05-16 14:45:57'),
(83, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Axis.php', '2019-05-16 14:45:57'),
(84, 2, 'system/library/export_import/Classes/PHPExcel/Chart/DataSeries.php', '2019-05-16 14:45:57'),
(85, 2, 'system/library/export_import/Classes/PHPExcel/Chart/DataSeriesValues.php', '2019-05-16 14:45:57'),
(86, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Exception.php', '2019-05-16 14:45:57'),
(87, 2, 'system/library/export_import/Classes/PHPExcel/Chart/GridLines.php', '2019-05-16 14:45:57'),
(88, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Layout.php', '2019-05-16 14:45:57'),
(89, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Legend.php', '2019-05-16 14:45:57'),
(90, 2, 'system/library/export_import/Classes/PHPExcel/Chart/PlotArea.php', '2019-05-16 14:45:57'),
(91, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Properties.php', '2019-05-16 14:45:57'),
(92, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer', '2019-05-16 14:45:57'),
(93, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Title.php', '2019-05-16 14:45:57'),
(94, 2, 'system/library/export_import/Classes/PHPExcel/Helper/HTML.php', '2019-05-16 14:45:57'),
(95, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Abstract.php', '2019-05-16 14:45:57'),
(96, 2, 'system/library/export_import/Classes/PHPExcel/Reader/CSV.php', '2019-05-16 14:45:57'),
(97, 2, 'system/library/export_import/Classes/PHPExcel/Reader/DefaultReadFilter.php', '2019-05-16 14:45:57'),
(98, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2003XML.php', '2019-05-16 14:45:57'),
(99, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007', '2019-05-16 14:45:57'),
(100, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007.php', '2019-05-16 14:45:57'),
(101, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5', '2019-05-16 14:45:57'),
(102, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5.php', '2019-05-16 14:45:57'),
(103, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Exception.php', '2019-05-16 14:45:57'),
(104, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Gnumeric.php', '2019-05-16 14:45:57'),
(105, 2, 'system/library/export_import/Classes/PHPExcel/Reader/HTML.php', '2019-05-16 14:45:57'),
(106, 2, 'system/library/export_import/Classes/PHPExcel/Reader/IReadFilter.php', '2019-05-16 14:45:57'),
(107, 2, 'system/library/export_import/Classes/PHPExcel/Reader/IReader.php', '2019-05-16 14:45:57'),
(108, 2, 'system/library/export_import/Classes/PHPExcel/Reader/OOCalc.php', '2019-05-16 14:45:57'),
(109, 2, 'system/library/export_import/Classes/PHPExcel/Reader/SYLK.php', '2019-05-16 14:45:57'),
(110, 2, 'system/library/export_import/Classes/PHPExcel/RichText/ITextElement.php', '2019-05-16 14:45:57'),
(111, 2, 'system/library/export_import/Classes/PHPExcel/RichText/Run.php', '2019-05-16 14:45:57'),
(112, 2, 'system/library/export_import/Classes/PHPExcel/RichText/TextElement.php', '2019-05-16 14:45:57'),
(113, 2, 'system/library/export_import/Classes/PHPExcel/Shared/CodePage.php', '2019-05-16 14:45:57'),
(114, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Date.php', '2019-05-16 14:45:57'),
(115, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Drawing.php', '2019-05-16 14:45:57'),
(116, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher', '2019-05-16 14:45:57'),
(117, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher.php', '2019-05-16 14:45:57'),
(118, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Excel5.php', '2019-05-16 14:45:57'),
(119, 2, 'system/library/export_import/Classes/PHPExcel/Shared/File.php', '2019-05-16 14:45:57'),
(120, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Font.php', '2019-05-16 14:45:57'),
(121, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA', '2019-05-16 14:45:57'),
(122, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE', '2019-05-16 14:45:57'),
(123, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE.php', '2019-05-16 14:45:57'),
(124, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLERead.php', '2019-05-16 14:45:57'),
(125, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip', '2019-05-16 14:45:57'),
(126, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PasswordHasher.php', '2019-05-16 14:45:57'),
(127, 2, 'system/library/export_import/Classes/PHPExcel/Shared/String.php', '2019-05-16 14:45:57'),
(128, 2, 'system/library/export_import/Classes/PHPExcel/Shared/TimeZone.php', '2019-05-16 14:45:57'),
(129, 2, 'system/library/export_import/Classes/PHPExcel/Shared/XMLWriter.php', '2019-05-16 14:45:57'),
(130, 2, 'system/library/export_import/Classes/PHPExcel/Shared/ZipArchive.php', '2019-05-16 14:45:57'),
(131, 2, 'system/library/export_import/Classes/PHPExcel/Shared/ZipStreamWrapper.php', '2019-05-16 14:45:57'),
(132, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend', '2019-05-16 14:45:57'),
(133, 2, 'system/library/export_import/Classes/PHPExcel/Style/Alignment.php', '2019-05-16 14:45:57'),
(134, 2, 'system/library/export_import/Classes/PHPExcel/Style/Border.php', '2019-05-16 14:45:57'),
(135, 2, 'system/library/export_import/Classes/PHPExcel/Style/Borders.php', '2019-05-16 14:45:57'),
(136, 2, 'system/library/export_import/Classes/PHPExcel/Style/Color.php', '2019-05-16 14:45:57'),
(137, 2, 'system/library/export_import/Classes/PHPExcel/Style/Conditional.php', '2019-05-16 14:45:57'),
(138, 2, 'system/library/export_import/Classes/PHPExcel/Style/Fill.php', '2019-05-16 14:45:57'),
(139, 2, 'system/library/export_import/Classes/PHPExcel/Style/Font.php', '2019-05-16 14:45:57'),
(140, 2, 'system/library/export_import/Classes/PHPExcel/Style/NumberFormat.php', '2019-05-16 14:45:57'),
(141, 2, 'system/library/export_import/Classes/PHPExcel/Style/Protection.php', '2019-05-16 14:45:57'),
(142, 2, 'system/library/export_import/Classes/PHPExcel/Style/Supervisor.php', '2019-05-16 14:45:57'),
(143, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter', '2019-05-16 14:45:57'),
(144, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter.php', '2019-05-16 14:45:57'),
(145, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/BaseDrawing.php', '2019-05-16 14:45:57'),
(146, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/CellIterator.php', '2019-05-16 14:45:57'),
(147, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Column.php', '2019-05-16 14:45:57'),
(148, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnCellIterator.php', '2019-05-16 14:45:57'),
(149, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnDimension.php', '2019-05-16 14:45:57'),
(150, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/ColumnIterator.php', '2019-05-16 14:45:57'),
(151, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Dimension.php', '2019-05-16 14:45:57'),
(152, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing', '2019-05-16 14:45:57'),
(153, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing.php', '2019-05-16 14:45:57'),
(154, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/HeaderFooter.php', '2019-05-16 14:45:57'),
(155, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/HeaderFooterDrawing.php', '2019-05-16 14:45:57'),
(156, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/MemoryDrawing.php', '2019-05-16 14:45:57'),
(157, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/PageMargins.php', '2019-05-16 14:45:57'),
(158, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/PageSetup.php', '2019-05-16 14:45:57'),
(159, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Protection.php', '2019-05-16 14:45:57'),
(160, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Row.php', '2019-05-16 14:45:57'),
(161, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowCellIterator.php', '2019-05-16 14:45:57'),
(162, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowDimension.php', '2019-05-16 14:45:57'),
(163, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/RowIterator.php', '2019-05-16 14:45:57'),
(164, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/SheetView.php', '2019-05-16 14:45:57'),
(165, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Abstract.php', '2019-05-16 14:45:57'),
(166, 2, 'system/library/export_import/Classes/PHPExcel/Writer/CSV.php', '2019-05-16 14:45:57'),
(167, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007', '2019-05-16 14:45:57'),
(168, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007.php', '2019-05-16 14:45:57'),
(169, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5', '2019-05-16 14:45:57'),
(170, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5.php', '2019-05-16 14:45:57'),
(171, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Exception.php', '2019-05-16 14:45:57'),
(172, 2, 'system/library/export_import/Classes/PHPExcel/Writer/HTML.php', '2019-05-16 14:45:57'),
(173, 2, 'system/library/export_import/Classes/PHPExcel/Writer/IWriter.php', '2019-05-16 14:45:57'),
(174, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument', '2019-05-16 14:45:57'),
(175, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument.php', '2019-05-16 14:45:57'),
(176, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF', '2019-05-16 14:45:57'),
(177, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF.php', '2019-05-16 14:45:57'),
(178, 2, 'system/library/export_import/Classes/PHPExcel/locale/bg', '2019-05-16 14:45:57'),
(179, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs', '2019-05-16 14:45:57'),
(180, 2, 'system/library/export_import/Classes/PHPExcel/locale/da', '2019-05-16 14:45:57'),
(181, 2, 'system/library/export_import/Classes/PHPExcel/locale/de', '2019-05-16 14:45:57'),
(182, 2, 'system/library/export_import/Classes/PHPExcel/locale/en', '2019-05-16 14:45:57'),
(183, 2, 'system/library/export_import/Classes/PHPExcel/locale/es', '2019-05-16 14:45:57'),
(184, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi', '2019-05-16 14:45:57'),
(185, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr', '2019-05-16 14:45:57'),
(186, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu', '2019-05-16 14:45:57'),
(187, 2, 'system/library/export_import/Classes/PHPExcel/locale/it', '2019-05-16 14:45:57'),
(188, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl', '2019-05-16 14:45:57'),
(189, 2, 'system/library/export_import/Classes/PHPExcel/locale/no', '2019-05-16 14:45:57'),
(190, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl', '2019-05-16 14:45:57'),
(191, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt', '2019-05-16 14:45:57'),
(192, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru', '2019-05-16 14:45:57'),
(193, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv', '2019-05-16 14:45:57'),
(194, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr', '2019-05-16 14:45:57'),
(195, 2, 'system/library/export_import/Classes/PHPExcel/Calculation/Token/Stack.php', '2019-05-16 14:45:57'),
(196, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer/PHP Charting Libraries.txt', '2019-05-16 14:45:57'),
(197, 2, 'system/library/export_import/Classes/PHPExcel/Chart/Renderer/jpgraph.php', '2019-05-16 14:45:57'),
(198, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007/Chart.php', '2019-05-16 14:45:57'),
(199, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel2007/Theme.php', '2019-05-16 14:45:57'),
(200, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color', '2019-05-16 14:45:57'),
(201, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color.php', '2019-05-16 14:45:57'),
(202, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/ErrorCode.php', '2019-05-16 14:45:57'),
(203, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Escher.php', '2019-05-16 14:45:57'),
(204, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/MD5.php', '2019-05-16 14:45:57'),
(205, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/RC4.php', '2019-05-16 14:45:57'),
(206, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style', '2019-05-16 14:45:57'),
(207, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer', '2019-05-16 14:45:57'),
(208, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer.php', '2019-05-16 14:45:57'),
(209, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer', '2019-05-16 14:45:57'),
(210, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer.php', '2019-05-16 14:45:57'),
(211, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/CHANGELOG.TXT', '2019-05-16 14:45:57'),
(212, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/CholeskyDecomposition.php', '2019-05-16 14:45:57'),
(213, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/EigenvalueDecomposition.php', '2019-05-16 14:45:57'),
(214, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/LUDecomposition.php', '2019-05-16 14:45:57'),
(215, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/Matrix.php', '2019-05-16 14:45:57'),
(216, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/QRDecomposition.php', '2019-05-16 14:45:57'),
(217, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/SingularValueDecomposition.php', '2019-05-16 14:45:57'),
(218, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils', '2019-05-16 14:45:57'),
(219, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/ChainedBlockStream.php', '2019-05-16 14:45:57'),
(220, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS', '2019-05-16 14:45:57'),
(221, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS.php', '2019-05-16 14:45:57'),
(222, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/gnu-lgpl.txt', '2019-05-16 14:45:57'),
(223, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/pclzip.lib.php', '2019-05-16 14:45:57'),
(224, 2, 'system/library/export_import/Classes/PHPExcel/Shared/PCLZip/readme.txt', '2019-05-16 14:45:57'),
(225, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/bestFitClass.php', '2019-05-16 14:45:57'),
(226, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/exponentialBestFitClass.php', '2019-05-16 14:45:57'),
(227, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/linearBestFitClass.php', '2019-05-16 14:45:57'),
(228, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/logarithmicBestFitClass.php', '2019-05-16 14:45:57'),
(229, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/polynomialBestFitClass.php', '2019-05-16 14:45:57'),
(230, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/powerBestFitClass.php', '2019-05-16 14:45:57'),
(231, 2, 'system/library/export_import/Classes/PHPExcel/Shared/trend/trendClass.php', '2019-05-16 14:45:57'),
(232, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column', '2019-05-16 14:45:57'),
(233, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column.php', '2019-05-16 14:45:57'),
(234, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/Drawing/Shadow.php', '2019-05-16 14:45:57'),
(235, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Chart.php', '2019-05-16 14:45:57'),
(236, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Comments.php', '2019-05-16 14:45:57'),
(237, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/ContentTypes.php', '2019-05-16 14:45:57'),
(238, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/DocProps.php', '2019-05-16 14:45:57'),
(239, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Drawing.php', '2019-05-16 14:45:57'),
(240, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Rels.php', '2019-05-16 14:45:57'),
(241, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/RelsRibbon.php', '2019-05-16 14:45:57'),
(242, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/RelsVBA.php', '2019-05-16 14:45:57'),
(243, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/StringTable.php', '2019-05-16 14:45:57'),
(244, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Style.php', '2019-05-16 14:45:57'),
(245, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Theme.php', '2019-05-16 14:45:57'),
(246, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Workbook.php', '2019-05-16 14:45:57'),
(247, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/Worksheet.php', '2019-05-16 14:45:57'),
(248, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel2007/WriterPart.php', '2019-05-16 14:45:57'),
(249, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/BIFFwriter.php', '2019-05-16 14:45:57'),
(250, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Escher.php', '2019-05-16 14:45:57'),
(251, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Font.php', '2019-05-16 14:45:57'),
(252, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Parser.php', '2019-05-16 14:45:57'),
(253, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Workbook.php', '2019-05-16 14:45:57'),
(254, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Worksheet.php', '2019-05-16 14:45:57'),
(255, 2, 'system/library/export_import/Classes/PHPExcel/Writer/Excel5/Xf.php', '2019-05-16 14:45:57'),
(256, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Cell', '2019-05-16 14:45:57'),
(257, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Content.php', '2019-05-16 14:45:57'),
(258, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Meta.php', '2019-05-16 14:45:57'),
(259, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/MetaInf.php', '2019-05-16 14:45:57'),
(260, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Mimetype.php', '2019-05-16 14:45:57'),
(261, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Settings.php', '2019-05-16 14:45:57'),
(262, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Styles.php', '2019-05-16 14:45:57'),
(263, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Thumbnails.php', '2019-05-16 14:45:57'),
(264, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/WriterPart.php', '2019-05-16 14:45:57'),
(265, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/Core.php', '2019-05-16 14:45:57'),
(266, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/DomPDF.php', '2019-05-16 14:45:57'),
(267, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/mPDF.php', '2019-05-16 14:45:57'),
(268, 2, 'system/library/export_import/Classes/PHPExcel/Writer/PDF/tcPDF.php', '2019-05-16 14:45:57'),
(269, 2, 'system/library/export_import/Classes/PHPExcel/locale/bg/config', '2019-05-16 14:45:57'),
(270, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs/config', '2019-05-16 14:45:57'),
(271, 2, 'system/library/export_import/Classes/PHPExcel/locale/cs/functions', '2019-05-16 14:45:57'),
(272, 2, 'system/library/export_import/Classes/PHPExcel/locale/da/config', '2019-05-16 14:45:57'),
(273, 2, 'system/library/export_import/Classes/PHPExcel/locale/da/functions', '2019-05-16 14:45:57'),
(274, 2, 'system/library/export_import/Classes/PHPExcel/locale/de/config', '2019-05-16 14:45:57'),
(275, 2, 'system/library/export_import/Classes/PHPExcel/locale/de/functions', '2019-05-16 14:45:57'),
(276, 2, 'system/library/export_import/Classes/PHPExcel/locale/en/uk', '2019-05-16 14:45:57'),
(277, 2, 'system/library/export_import/Classes/PHPExcel/locale/es/config', '2019-05-16 14:45:57'),
(278, 2, 'system/library/export_import/Classes/PHPExcel/locale/es/functions', '2019-05-16 14:45:57'),
(279, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi/config', '2019-05-16 14:45:57'),
(280, 2, 'system/library/export_import/Classes/PHPExcel/locale/fi/functions', '2019-05-16 14:45:57'),
(281, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr/config', '2019-05-16 14:45:57'),
(282, 2, 'system/library/export_import/Classes/PHPExcel/locale/fr/functions', '2019-05-16 14:45:57'),
(283, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu/config', '2019-05-16 14:45:57'),
(284, 2, 'system/library/export_import/Classes/PHPExcel/locale/hu/functions', '2019-05-16 14:45:57'),
(285, 2, 'system/library/export_import/Classes/PHPExcel/locale/it/config', '2019-05-16 14:45:57'),
(286, 2, 'system/library/export_import/Classes/PHPExcel/locale/it/functions', '2019-05-16 14:45:57'),
(287, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl/config', '2019-05-16 14:45:57'),
(288, 2, 'system/library/export_import/Classes/PHPExcel/locale/nl/functions', '2019-05-16 14:45:57'),
(289, 2, 'system/library/export_import/Classes/PHPExcel/locale/no/config', '2019-05-16 14:45:57'),
(290, 2, 'system/library/export_import/Classes/PHPExcel/locale/no/functions', '2019-05-16 14:45:57'),
(291, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl/config', '2019-05-16 14:45:57'),
(292, 2, 'system/library/export_import/Classes/PHPExcel/locale/pl/functions', '2019-05-16 14:45:57'),
(293, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br', '2019-05-16 14:45:57'),
(294, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/config', '2019-05-16 14:45:57'),
(295, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/functions', '2019-05-16 14:45:57'),
(296, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru/config', '2019-05-16 14:45:57'),
(297, 2, 'system/library/export_import/Classes/PHPExcel/locale/ru/functions', '2019-05-16 14:45:57'),
(298, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv/config', '2019-05-16 14:45:57'),
(299, 2, 'system/library/export_import/Classes/PHPExcel/locale/sv/functions', '2019-05-16 14:45:57'),
(300, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr/config', '2019-05-16 14:45:57'),
(301, 2, 'system/library/export_import/Classes/PHPExcel/locale/tr/functions', '2019-05-16 14:45:57'),
(302, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BIFF5.php', '2019-05-16 14:45:57'),
(303, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BIFF8.php', '2019-05-16 14:45:57'),
(304, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Color/BuiltIn.php', '2019-05-16 14:45:57'),
(305, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style/Border.php', '2019-05-16 14:45:57'),
(306, 2, 'system/library/export_import/Classes/PHPExcel/Reader/Excel5/Style/FillPattern.php', '2019-05-16 14:45:57'),
(307, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer', '2019-05-16 14:45:57'),
(308, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer.php', '2019-05-16 14:45:57'),
(309, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer', '2019-05-16 14:45:57'),
(310, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer.php', '2019-05-16 14:45:57'),
(311, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils/Error.php', '2019-05-16 14:45:57'),
(312, 2, 'system/library/export_import/Classes/PHPExcel/Shared/JAMA/utils/Maths.php', '2019-05-16 14:45:57'),
(313, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS/File.php', '2019-05-16 14:45:57'),
(314, 2, 'system/library/export_import/Classes/PHPExcel/Shared/OLE/PPS/Root.php', '2019-05-16 14:45:57'),
(315, 2, 'system/library/export_import/Classes/PHPExcel/Worksheet/AutoFilter/Column/Rule.php', '2019-05-16 14:45:57'),
(316, 2, 'system/library/export_import/Classes/PHPExcel/Writer/OpenDocument/Cell/Comment.php', '2019-05-16 14:45:57'),
(317, 2, 'system/library/export_import/Classes/PHPExcel/locale/en/uk/config', '2019-05-16 14:45:57'),
(318, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br/config', '2019-05-16 14:45:57'),
(319, 2, 'system/library/export_import/Classes/PHPExcel/locale/pt/br/functions', '2019-05-16 14:45:57'),
(320, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DgContainer/SpgrContainer/SpContainer.php', '2019-05-16 14:45:57'),
(321, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE', '2019-05-16 14:45:57'),
(322, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE.php', '2019-05-16 14:45:57'),
(323, 2, 'system/library/export_import/Classes/PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE/Blip.php', '2019-05-16 14:45:57'),
(324, 3, 'admin/view/stylesheet/autocomplete.css', '2019-05-16 14:46:10'),
(1008, 19, 'admin/language/en', '2020-05-20 04:18:57'),
(1009, 19, 'admin/language/en-us', '2020-05-20 04:18:57'),
(1010, 19, 'admin/language/english', '2020-05-20 04:18:57'),
(1011, 19, 'admin/language/ru', '2020-05-20 04:18:57'),
(1012, 19, 'admin/language/russian', '2020-05-20 04:18:57'),
(1013, 19, 'catalog/language/en', '2020-05-20 04:18:57'),
(1014, 19, 'catalog/language/en-us', '2020-05-20 04:18:57'),
(1015, 19, 'catalog/language/english', '2020-05-20 04:18:57'),
(1016, 19, 'catalog/language/ru', '2020-05-20 04:18:57'),
(704, 8, 'admin/language/uk-ua', '2019-05-22 20:18:39'),
(705, 8, 'catalog/language/uk-ua', '2019-05-22 20:18:39'),
(706, 8, 'admin/language/uk-ua/extension', '2019-05-22 20:18:39'),
(707, 8, 'admin/model/extension/module', '2019-05-22 20:18:39'),
(708, 8, 'admin/view/javascript/quickcheckout', '2019-05-22 20:18:39'),
(709, 8, 'admin/view/stylesheet/quickcheckout.css', '2019-05-22 20:18:39'),
(710, 8, 'catalog/controller/extension/quickcheckout', '2019-05-22 20:18:39'),
(711, 8, 'catalog/language/uk-ua/extension', '2019-05-22 20:18:39'),
(712, 8, 'admin/controller/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(713, 8, 'admin/language/uk-ua/extension/module', '2019-05-22 20:18:39'),
(714, 8, 'admin/model/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(715, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider', '2019-05-22 20:18:39'),
(716, 8, 'admin/view/javascript/quickcheckout/bootstrap-sortable.js', '2019-05-22 20:18:39'),
(717, 8, 'admin/view/javascript/quickcheckout/tinysort', '2019-05-22 20:18:39'),
(718, 8, 'catalog/controller/extension/quickcheckout/cart.php', '2019-05-22 20:18:39'),
(719, 8, 'catalog/controller/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(720, 8, 'catalog/controller/extension/quickcheckout/confirm.php', '2019-05-22 20:18:39'),
(721, 8, 'catalog/controller/extension/quickcheckout/guest.php', '2019-05-22 20:18:39'),
(722, 8, 'catalog/controller/extension/quickcheckout/guest_shipping.php', '2019-05-22 20:18:39'),
(723, 8, 'catalog/controller/extension/quickcheckout/login.php', '2019-05-22 20:18:39'),
(724, 8, 'catalog/controller/extension/quickcheckout/payment_address.php', '2019-05-22 20:18:39'),
(725, 8, 'catalog/controller/extension/quickcheckout/payment_method.php', '2019-05-22 20:18:39'),
(726, 8, 'catalog/controller/extension/quickcheckout/register.php', '2019-05-22 20:18:39'),
(727, 8, 'catalog/controller/extension/quickcheckout/shipping_address.php', '2019-05-22 20:18:39'),
(728, 8, 'catalog/controller/extension/quickcheckout/shipping_method.php', '2019-05-22 20:18:39'),
(729, 8, 'catalog/controller/extension/quickcheckout/terms.php', '2019-05-22 20:18:39'),
(730, 8, 'catalog/controller/extension/quickcheckout/voucher.php', '2019-05-22 20:18:39'),
(731, 8, 'catalog/language/en-gb/extension/quickcheckout', '2019-05-22 20:18:39'),
(732, 8, 'catalog/language/ru-ru/extension/quickcheckout', '2019-05-22 20:18:39'),
(733, 8, 'catalog/language/uk-ua/extension/quickcheckout', '2019-05-22 20:18:39'),
(734, 8, 'catalog/view/javascript/jquery/quickcheckout', '2019-05-22 20:18:39'),
(735, 8, 'admin/language/en-gb/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(736, 8, 'admin/language/ru-ru/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(737, 8, 'admin/language/uk-ua/extension/module/quickcheckout.php', '2019-05-22 20:18:39'),
(738, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/css', '2019-05-22 20:18:39'),
(739, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/js', '2019-05-22 20:18:39'),
(740, 8, 'admin/view/javascript/quickcheckout/tinysort/jquery.tinysort.min.js', '2019-05-22 20:18:39'),
(741, 8, 'admin/view/template/extension/module/quickcheckout.twig', '2019-05-22 20:18:39'),
(742, 8, 'catalog/language/en-gb/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(743, 8, 'catalog/language/ru-ru/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(744, 8, 'catalog/language/uk-ua/extension/quickcheckout/checkout.php', '2019-05-22 20:18:39'),
(745, 8, 'catalog/view/javascript/jquery/quickcheckout/bootstrap-datetimepicker.min.css', '2019-05-22 20:18:39'),
(746, 8, 'catalog/view/javascript/jquery/quickcheckout/bootstrap-datetimepicker.min.js', '2019-05-22 20:18:39'),
(747, 8, 'catalog/view/javascript/jquery/quickcheckout/jquery.tinysort.min.js', '2019-05-22 20:18:39'),
(748, 8, 'catalog/view/javascript/jquery/quickcheckout/moment-with-locales.min.js', '2019-05-22 20:18:39'),
(749, 8, 'catalog/view/javascript/jquery/quickcheckout/quickcheckout.block.js', '2019-05-22 20:18:39'),
(750, 8, 'catalog/view/javascript/jquery/quickcheckout/quickcheckout.js', '2019-05-22 20:18:39'),
(751, 8, 'catalog/view/theme/default/stylesheet/quickcheckout.css', '2019-05-22 20:18:39'),
(752, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_custom.css', '2019-05-22 20:18:39'),
(753, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_mobile.css', '2019-05-22 20:18:39'),
(754, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_one.css', '2019-05-22 20:18:39'),
(755, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_three.css', '2019-05-22 20:18:39'),
(756, 8, 'catalog/view/theme/default/stylesheet/quickcheckout_two.css', '2019-05-22 20:18:39'),
(757, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/css/slider.css', '2019-05-22 20:18:39'),
(758, 8, 'admin/view/javascript/quickcheckout/bootstrap-slider/js/bootstrap-slider.js', '2019-05-22 20:18:39'),
(759, 8, 'catalog/view/theme/default/template/extension/quickcheckout', '2019-05-22 20:18:39'),
(760, 8, 'catalog/view/theme/default/template/extension/quickcheckout/cart.twig', '2019-05-22 20:18:39'),
(761, 8, 'catalog/view/theme/default/template/extension/quickcheckout/checkout.twig', '2019-05-22 20:18:39'),
(762, 8, 'catalog/view/theme/default/template/extension/quickcheckout/confirm.twig', '2019-05-22 20:18:39'),
(763, 8, 'catalog/view/theme/default/template/extension/quickcheckout/guest.twig', '2019-05-22 20:18:39'),
(764, 8, 'catalog/view/theme/default/template/extension/quickcheckout/guest_shipping.twig', '2019-05-22 20:18:39'),
(765, 8, 'catalog/view/theme/default/template/extension/quickcheckout/login.twig', '2019-05-22 20:18:39'),
(766, 8, 'catalog/view/theme/default/template/extension/quickcheckout/payment_address.twig', '2019-05-22 20:18:39'),
(767, 8, 'catalog/view/theme/default/template/extension/quickcheckout/payment_method.twig', '2019-05-22 20:18:39'),
(768, 8, 'catalog/view/theme/default/template/extension/quickcheckout/register.twig', '2019-05-22 20:18:39'),
(769, 8, 'catalog/view/theme/default/template/extension/quickcheckout/shipping_address.twig', '2019-05-22 20:18:39'),
(770, 8, 'catalog/view/theme/default/template/extension/quickcheckout/shipping_method.twig', '2019-05-22 20:18:39'),
(771, 8, 'catalog/view/theme/default/template/extension/quickcheckout/terms.twig', '2019-05-22 20:18:39'),
(772, 8, 'catalog/view/theme/default/template/extension/quickcheckout/voucher.twig', '2019-05-22 20:18:39'),
(1017, 19, 'catalog/language/russian', '2020-05-20 04:18:57'),
(1018, 19, 'system/config/ocfilter.php', '2020-05-20 04:18:57'),
(1019, 19, 'system/library/ocfilter.php', '2020-05-20 04:18:57'),
(1020, 19, 'admin/language/en/extension', '2020-05-20 04:18:57'),
(1021, 19, 'admin/language/en-us/extension', '2020-05-20 04:18:57'),
(700, 7, 'admin/controller/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(701, 7, 'admin/language/en-gb/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(702, 7, 'admin/language/ru-ru/extension/module/extendedsearch.php', '2019-05-16 16:11:10'),
(703, 7, 'admin/view/template/extension/module/extendedsearch.twig', '2019-05-16 16:11:10'),
(955, 17, 'admin/controller/dashboard', '2019-08-07 15:32:01'),
(956, 17, 'admin/view/fontawesome', '2019-08-07 15:32:01'),
(957, 17, 'admin/controller/dashboard/manager.php', '2019-08-07 15:32:01'),
(958, 17, 'admin/model/sale/manager.php', '2019-08-07 15:32:01'),
(959, 17, 'admin/view/fontawesome/css', '2019-08-07 15:32:01'),
(960, 17, 'admin/view/fontawesome/fonts', '2019-08-07 15:32:01'),
(961, 17, 'admin/view/fontawesome/less', '2019-08-07 15:32:01'),
(962, 17, 'admin/view/fontawesome/scss', '2019-08-07 15:32:01'),
(963, 17, 'admin/view/stylesheet/manager.css', '2019-08-07 15:32:01'),
(964, 17, 'admin/view/template/dashboard', '2019-08-07 15:32:01'),
(965, 17, 'admin/controller/extension/module/manager.php', '2019-08-07 15:32:01'),
(966, 17, 'admin/view/fontawesome/css/font-awesome.css', '2019-08-07 15:32:01'),
(967, 17, 'admin/view/fontawesome/css/font-awesome.min.css', '2019-08-07 15:32:01'),
(968, 17, 'admin/view/fontawesome/fonts/FontAwesome.otf', '2019-08-07 15:32:01'),
(969, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.eot', '2019-08-07 15:32:01'),
(970, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.svg', '2019-08-07 15:32:01'),
(971, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.ttf', '2019-08-07 15:32:01'),
(972, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff', '2019-08-07 15:32:01'),
(973, 17, 'admin/view/fontawesome/fonts/fontawesome-webfont.woff2', '2019-08-07 15:32:01'),
(974, 17, 'admin/view/fontawesome/less/animated.less', '2019-08-07 15:32:01'),
(975, 17, 'admin/view/fontawesome/less/bordered-pulled.less', '2019-08-07 15:32:01'),
(976, 17, 'admin/view/fontawesome/less/core.less', '2019-08-07 15:32:01'),
(977, 17, 'admin/view/fontawesome/less/fixed-width.less', '2019-08-07 15:32:01'),
(978, 17, 'admin/view/fontawesome/less/font-awesome.less', '2019-08-07 15:32:01'),
(979, 17, 'admin/view/fontawesome/less/icons.less', '2019-08-07 15:32:01'),
(980, 17, 'admin/view/fontawesome/less/larger.less', '2019-08-07 15:32:01'),
(981, 17, 'admin/view/fontawesome/less/list.less', '2019-08-07 15:32:01'),
(982, 17, 'admin/view/fontawesome/less/mixins.less', '2019-08-07 15:32:01'),
(983, 17, 'admin/view/fontawesome/less/path.less', '2019-08-07 15:32:01'),
(984, 17, 'admin/view/fontawesome/less/rotated-flipped.less', '2019-08-07 15:32:01'),
(985, 17, 'admin/view/fontawesome/less/stacked.less', '2019-08-07 15:32:01'),
(986, 17, 'admin/view/fontawesome/less/variables.less', '2019-08-07 15:32:01'),
(987, 17, 'admin/view/fontawesome/scss/_animated.scss', '2019-08-07 15:32:01'),
(988, 17, 'admin/view/fontawesome/scss/_bordered-pulled.scss', '2019-08-07 15:32:01'),
(989, 17, 'admin/view/fontawesome/scss/_core.scss', '2019-08-07 15:32:01'),
(990, 17, 'admin/view/fontawesome/scss/_fixed-width.scss', '2019-08-07 15:32:01'),
(991, 17, 'admin/view/fontawesome/scss/_icons.scss', '2019-08-07 15:32:01'),
(992, 17, 'admin/view/fontawesome/scss/_larger.scss', '2019-08-07 15:32:01'),
(993, 17, 'admin/view/fontawesome/scss/_list.scss', '2019-08-07 15:32:01'),
(994, 17, 'admin/view/fontawesome/scss/_mixins.scss', '2019-08-07 15:32:01'),
(995, 17, 'admin/view/fontawesome/scss/_path.scss', '2019-08-07 15:32:01'),
(996, 17, 'admin/view/fontawesome/scss/_rotated-flipped.scss', '2019-08-07 15:32:01'),
(997, 17, 'admin/view/fontawesome/scss/_stacked.scss', '2019-08-07 15:32:01'),
(998, 17, 'admin/view/fontawesome/scss/_variables.scss', '2019-08-07 15:32:01'),
(999, 17, 'admin/view/fontawesome/scss/font-awesome.scss', '2019-08-07 15:32:01'),
(1000, 17, 'admin/view/template/dashboard/manager.twig', '2019-08-07 15:32:01'),
(1001, 17, 'admin/language/en-gb/extension/module/manager.php', '2019-08-07 15:32:01'),
(1002, 17, 'admin/language/ru-ru/extension/module/manager.php', '2019-08-07 15:32:01'),
(1003, 17, 'admin/view/template/extension/module/manager.twig', '2019-08-07 15:32:01'),
(1004, 18, 'admin/controller/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1005, 18, 'catalog/controller/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1006, 18, 'admin/language/en-gb/extension/feed/yandex_sitemap.php', '2019-09-16 15:55:23'),
(1007, 18, 'admin/view/template/extension/feed/yandex_sitemap.twig', '2019-09-16 15:55:23'),
(1022, 19, 'admin/language/english/extension', '2020-05-20 04:18:57'),
(1023, 19, 'admin/language/ru/extension', '2020-05-20 04:18:57'),
(1024, 19, 'admin/language/russian/extension', '2020-05-20 04:18:57'),
(1025, 19, 'admin/model/extension/ocfilter.php', '2020-05-20 04:18:57'),
(1026, 19, 'admin/model/extension/ocfilter_page.php', '2020-05-20 04:18:57'),
(1027, 19, 'admin/view/image/ocfilter', '2020-05-20 04:18:57'),
(1028, 19, 'admin/view/javascript/ocfilter', '2020-05-20 04:18:57'),
(1029, 19, 'admin/view/stylesheet/ocfilter', '2020-05-20 04:18:57'),
(1030, 19, 'catalog/language/en/extension', '2020-05-20 04:18:57'),
(1031, 19, 'catalog/language/en-us/extension', '2020-05-20 04:18:57'),
(1032, 19, 'catalog/language/english/extension', '2020-05-20 04:18:57'),
(1033, 19, 'catalog/language/ru/extension', '2020-05-20 04:18:57'),
(1034, 19, 'catalog/language/russian/extension', '2020-05-20 04:18:57'),
(1035, 19, 'catalog/view/javascript/ocfilter', '2020-05-20 04:18:57'),
(1036, 19, 'admin/controller/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1037, 19, 'admin/language/en/extension/module', '2020-05-20 04:18:57'),
(1038, 19, 'admin/language/en-us/extension/module', '2020-05-20 04:18:57'),
(1039, 19, 'admin/language/english/extension/module', '2020-05-20 04:18:57'),
(1040, 19, 'admin/language/ru/extension/module', '2020-05-20 04:18:57'),
(1041, 19, 'admin/language/russian/extension/module', '2020-05-20 04:18:57'),
(1042, 19, 'admin/view/image/ocfilter/delete-value.png', '2020-05-20 04:18:57'),
(1043, 19, 'admin/view/image/ocfilter/select-text.png', '2020-05-20 04:18:57'),
(1044, 19, 'admin/view/image/ocfilter/sort-handler.png', '2020-05-20 04:18:57'),
(1045, 19, 'admin/view/javascript/ocfilter/ocfilter.js', '2020-05-20 04:18:57'),
(1046, 19, 'admin/view/stylesheet/ocfilter/ocfilter.css', '2020-05-20 04:18:57'),
(1047, 19, 'catalog/controller/extension/feed/ocfilter_sitemap.php', '2020-05-20 04:18:57'),
(1048, 19, 'catalog/controller/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1049, 19, 'catalog/language/en/extension/module', '2020-05-20 04:18:57'),
(1050, 19, 'catalog/language/en-us/extension/module', '2020-05-20 04:18:57'),
(1051, 19, 'catalog/language/english/extension/module', '2020-05-20 04:18:57'),
(1052, 19, 'catalog/language/ru/extension/module', '2020-05-20 04:18:57'),
(1053, 19, 'catalog/language/russian/extension/module', '2020-05-20 04:18:57'),
(1054, 19, 'catalog/model/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1055, 19, 'catalog/view/javascript/ocfilter/nouislider.min.css', '2020-05-20 04:18:57'),
(1056, 19, 'catalog/view/javascript/ocfilter/nouislider.min.js', '2020-05-20 04:18:57'),
(1057, 19, 'catalog/view/javascript/ocfilter/ocfilter.js', '2020-05-20 04:18:57'),
(1058, 19, 'admin/language/en/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1059, 19, 'admin/language/en-gb/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1060, 19, 'admin/language/en-us/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1061, 19, 'admin/language/english/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1062, 19, 'admin/language/ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1063, 19, 'admin/language/ru-ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1064, 19, 'admin/language/russian/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1065, 19, 'admin/view/template/extension/module/ocfilter.twig', '2020-05-20 04:18:57'),
(1066, 19, 'admin/view/template/extension/module/ocfilter_form.twig', '2020-05-20 04:18:57'),
(1067, 19, 'admin/view/template/extension/module/ocfilter_list.twig', '2020-05-20 04:18:57'),
(1068, 19, 'admin/view/template/extension/module/ocfilter_page_form.twig', '2020-05-20 04:18:57'),
(1069, 19, 'admin/view/template/extension/module/ocfilter_page_list.twig', '2020-05-20 04:18:57'),
(1070, 19, 'catalog/language/en/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1071, 19, 'catalog/language/en-gb/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1072, 19, 'catalog/language/en-us/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1073, 19, 'catalog/language/english/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1074, 19, 'catalog/language/ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1075, 19, 'catalog/language/ru-ru/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1076, 19, 'catalog/language/russian/extension/module/ocfilter.php', '2020-05-20 04:18:57'),
(1077, 19, 'catalog/view/theme/default/image/ocfilter', '2020-05-20 04:18:57'),
(1078, 19, 'catalog/view/theme/default/stylesheet/ocfilter', '2020-05-20 04:18:57'),
(1079, 19, 'catalog/view/theme/default/image/ocfilter/diagram-bg-repeat.png', '2020-05-20 04:18:57'),
(1080, 19, 'catalog/view/theme/default/stylesheet/ocfilter/ocfilter.css', '2020-05-20 04:18:57'),
(1081, 19, 'catalog/view/theme/default/stylesheet/ocfilter/src', '2020-05-20 04:18:57'),
(1082, 19, 'catalog/view/theme/default/stylesheet/ocfilter/src/drop-arrow.svg', '2020-05-20 04:18:57'),
(1083, 19, 'catalog/view/theme/default/template/extension/module/ocfilter', '2020-05-20 04:18:57'),
(1084, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_item.twig', '2020-05-20 04:18:57'),
(1085, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_list.twig', '2020-05-20 04:18:57');
INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1086, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_price.twig', '2020-05-20 04:18:57'),
(1087, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/filter_slider_item.twig', '2020-05-20 04:18:57'),
(1088, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/module.twig', '2020-05-20 04:18:57'),
(1089, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/selected_filter.twig', '2020-05-20 04:18:57'),
(1090, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/value_item.twig', '2020-05-20 04:18:57'),
(1091, 19, 'catalog/view/theme/default/template/extension/module/ocfilter/value_list.twig', '2020-05-20 04:18:57'),
(1092, 21, 'admin/controller/event/compatibility.php', '2020-05-20 04:19:08'),
(1093, 21, 'catalog/controller/event/compatibility.php', '2020-05-20 04:19:08'),
(1094, 21, 'admin/controller/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1095, 21, 'catalog/controller/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1096, 21, 'admin/language/en-gb/extension/module/template_switcher.php', '2020-05-20 04:19:08'),
(1097, 21, 'admin/view/template/extension/module/template_switcher.twig', '2020-05-20 04:19:08'),
(1098, 22, 'admin/model/extension/module/modification_manager.php', '2020-05-20 04:19:15'),
(1099, 22, 'admin/view/javascript/codemirror/mode', '2020-05-20 04:19:15'),
(1100, 22, 'admin/view/javascript/codemirror/lib/codemirror.css', '2020-05-20 04:19:15'),
(1101, 22, 'admin/view/javascript/codemirror/lib/codemirror.js', '2020-05-20 04:19:15'),
(1102, 22, 'admin/view/javascript/codemirror/mode/xml', '2020-05-20 04:19:15'),
(1103, 22, 'admin/view/template/extension/module/modification_manager', '2020-05-20 04:19:15'),
(1104, 22, 'admin/view/javascript/codemirror/mode/xml/xml.js', '2020-05-20 04:19:15'),
(1105, 22, 'admin/view/template/extension/module/modification_manager/form.twig', '2020-05-20 04:19:15'),
(1106, 22, 'admin/view/template/extension/module/modification_manager/list.twig', '2020-05-20 04:19:15'),
(1114, 25, 'system/library/ocm', '2020-07-22 12:05:38'),
(1115, 25, 'admin/model/extension/shipping', '2020-07-22 12:05:38'),
(1116, 25, 'admin/view/javascript/ocm', '2020-07-22 12:05:38'),
(1117, 25, 'catalog/controller/extension/shipping', '2020-07-22 12:05:38'),
(1118, 25, 'catalog/view/javascript/xshippingpro.min.js', '2020-07-22 12:05:38'),
(1119, 25, 'system/library/ocm/back.php', '2020-07-22 12:05:38'),
(1120, 25, 'system/library/ocm/common.php', '2020-07-22 12:05:38'),
(1121, 25, 'system/library/ocm/elements', '2020-07-22 12:05:38'),
(1122, 25, 'system/library/ocm/form.php', '2020-07-22 12:05:38'),
(1123, 25, 'system/library/ocm/front.php', '2020-07-22 12:05:38'),
(1124, 25, 'system/library/ocm/misc.php', '2020-07-22 12:05:38'),
(1125, 25, 'system/library/ocm/setting.php', '2020-07-22 12:05:38'),
(1126, 25, 'system/library/ocm/url.php', '2020-07-22 12:05:38'),
(1127, 25, 'system/library/ocm/util.php', '2020-07-22 12:05:38'),
(1128, 25, 'admin/controller/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1129, 25, 'admin/model/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1130, 25, 'admin/view/javascript/ocm/ocm.css', '2020-07-22 12:05:38'),
(1131, 25, 'admin/view/javascript/ocm/ocm.js', '2020-07-22 12:05:38'),
(1132, 25, 'catalog/controller/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1133, 25, 'catalog/model/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1134, 25, 'system/library/ocm/elements/autofill.php', '2020-07-22 12:05:38'),
(1135, 25, 'system/library/ocm/elements/bare.php', '2020-07-22 12:05:38'),
(1136, 25, 'system/library/ocm/elements/base.php', '2020-07-22 12:05:38'),
(1137, 25, 'system/library/ocm/elements/checkbox.php', '2020-07-22 12:05:38'),
(1138, 25, 'system/library/ocm/elements/checkgroup.php', '2020-07-22 12:05:38'),
(1139, 25, 'system/library/ocm/elements/checkrow.php', '2020-07-22 12:05:38'),
(1140, 25, 'system/library/ocm/elements/image.php', '2020-07-22 12:05:38'),
(1141, 25, 'system/library/ocm/elements/input.php', '2020-07-22 12:05:38'),
(1142, 25, 'system/library/ocm/elements/inputgroup.php', '2020-07-22 12:05:38'),
(1143, 25, 'system/library/ocm/elements/radio.php', '2020-07-22 12:05:38'),
(1144, 25, 'system/library/ocm/elements/radiorow.php', '2020-07-22 12:05:38'),
(1145, 25, 'system/library/ocm/elements/range.php', '2020-07-22 12:05:38'),
(1146, 25, 'system/library/ocm/elements/select.php', '2020-07-22 12:05:38'),
(1147, 25, 'system/library/ocm/elements/textarea.php', '2020-07-22 12:05:38'),
(1148, 25, 'admin/language/en-gb/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38'),
(1149, 25, 'admin/view/template/extension/shipping/xshippingpro.twig', '2020-07-22 12:05:38'),
(1150, 25, 'catalog/language/en-gb/extension/shipping/xshippingpro.php', '2020-07-22 12:05:38');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT 0,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`, `noindex`) VALUES
(5, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(5, 1, 'Политика конфиденциальности и обработки персональных данных', '&lt;p&gt;Политика конфиденциальности и обработки персональных данных&lt;br&gt;&lt;/p&gt;', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных', 'Политика конфиденциальности и обработки персональных данных');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(5, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Russian', 'ru-ru', 'ru_RU.UTF-8,ru_RU,russian', 'gb.png', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Главная'),
(2, 'Товар'),
(3, 'Категория'),
(4, 'По-умолчанию'),
(5, 'Список Производителей'),
(6, 'Аккаунт'),
(7, 'Оформление заказа'),
(8, 'Контакты'),
(9, 'Карта сайта'),
(10, 'Партнерская программа'),
(11, 'Информация'),
(12, 'Сравнение'),
(13, 'Поиск'),
(14, 'Блог'),
(15, 'Категории Блога'),
(16, 'Статьи Блога'),
(17, 'Страница Производителя'),
(18, 'Custom Quick Checkout');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(97, 3, 'ocfilter', 'column_left', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(69, 3, 0, 'product/category'),
(68, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(57, 14, 0, 'blog/latest'),
(58, 15, 0, 'blog/category'),
(56, 16, 0, 'blog/article'),
(63, 17, 0, 'product/manufacturer/info'),
(65, 18, 0, 'extension/quickcheckout/checkout');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Сантиметр', 'см'),
(1, 2, 'Centimeter', 'cm'),
(2, 1, 'Миллиметр', 'мм'),
(2, 2, 'Millimeter', 'mm'),
(3, 1, 'Дюйм', 'in'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_description`
--

CREATE TABLE `oc_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `description3` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_layout`
--

CREATE TABLE `oc_manufacturer_to_layout` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, 'SVG-support', 'SVG-support', 'Ihor Chyshkala', '0.2 Beta', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<code>SVG-support</code>\n	<name>SVG-support</name>\n	<id>SVG-support</id>\n	<version>0.2 Beta</version>\n	<author>Ihor Chyshkala</author>\n\n	<file path=\"admin/controller/common/filemanager.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF,svg,SVG]]]>\n			</add>\n		</operation>\n		<operation error=\"log\">\n			<search>\n				<![CDATA[\'jpg\',]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[						\'svg\',]]>\n			</add>\n		</operation>\n				<operation error=\"log\">\n			<search>\n				<![CDATA[\'image/jpeg\',]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[						\'image/svg+xml\',]]>\n			</add>\n		</operation>\n	</file>\n	<file path=\"admin/model/tool/image.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[$extension = pathinfo($filename, PATHINFO_EXTENSION);]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[\n		if(\'svg\' == $extension) {\n            if ($this->request->server[\'HTTPS\']) {\n                return HTTPS_CATALOG . \'image/\' . $filename;\n            } else {\n                return HTTP_CATALOG . \'image/\' . $filename;\n            }\n    	}]]>\n			</add>\n		</operation>\n	</file>\n\n	<file path=\"catalog/model/tool/image.php\">\n		<operation error=\"log\">\n			<search>\n				<![CDATA[$extension = pathinfo($filename, PATHINFO_EXTENSION);]]>\n			</search>\n			<add position=\"after\">\n				<![CDATA[\n		if(\'svg\' == $extension) {\n            if ($this->request->server[\'HTTPS\']) {\n                return HTTPS_SERVER . \'image/\' . $filename;\n            } else {\n                return HTTP_SERVER . \'image/\' . $filename;\n            }\n    	}]]>\n			</add>\n		</operation>\n	</file>\n</modification>', 1, '2019-05-16 14:45:33', '2019-05-16 14:45:33'),
(2, 2, 'Export/Import Tool (V3.20) for OpenCart 3.x', 'Export/Import Tool (V3.20) for OpenCart 3.x', 'mhccorp.com', '3.x-3.20', 'https://www.mhccorp.com', '<modification>\n	<name>Export/Import Tool (V3.20) for OpenCart 3.x</name>\n	<code>Export/Import Tool (V3.20) for OpenCart 3.x</code>\n	<version>3.x-3.20</version>\n	<author>mhccorp.com</author>\n	<link>https://www.mhccorp.com</link>\n	<file path=\"admin/controller/common/column_left.php\">\n		<operation>\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'tool/upload\')) {]]></search>\n			<add position=\"before\"><![CDATA[\n			if ($this->user->hasPermission(\'access\', \'extension/export_import\')) {\n				$maintenance[] = array(\n					\'name\'	   => $this->language->get(\'text_export_import\'),\n					\'href\'     => $this->url->link(\'extension/export_import\', \'user_token=\' . $this->session->data[\'user_token\'], true),\n					\'children\' => array()		\n				);\n			}\n			]]></add>\n		</operation>\n	</file>\n	<file path=\"admin/language/*/common/column_left.php\">\n		<operation>\n			<search><![CDATA[$_[\'text_backup\']]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'text_export_import\']             = \'Export / Import\';\n			]]></add>\n		</operation>\n	</file>\n</modification>\n', 1, '2019-05-16 14:45:57', '2019-05-16 14:45:57'),
(3, 3, 'LimitAutocomplete', 'Limit Autocomplete', 'RoS / XDX / Mars1an', 'v3.3', 'http://forum.opencart-russia.ru/threads/ocmod-nastrojka-limita-avtozapolnenija-cherez-adminku.2278/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>LimitAutocomplete</name>\n  <version>v3.3</version>\n  <author>RoS / XDX / Mars1an</author>\n  <code>Limit Autocomplete</code>\n  <link>http://forum.opencart-russia.ru/threads/ocmod-nastrojka-limita-avtozapolnenija-cherez-adminku.2278/</link>\n  \n  <file path=\"admin/controller/setting/setting.php\">\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (isset($this->error[\'encryption\'])) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (isset($this->error[\'autocomplete_limit\'])) {\n		  $data[\'error_autocomplete_limit\'] = $this->error[\'autocomplete_limit\'];\n		} else {\n		  $data[\'error_autocomplete_limit\'] = \'\';\n		}\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (isset($this->request->post[\'config_product_count\'])) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (isset($this->request->post[\'config_autocomplete_limit\'])) {\n		  $data[\'config_autocomplete_limit\'] = $this->request->post[\'config_autocomplete_limit\'];\n		} else {\n		  $data[\'config_autocomplete_limit\'] = $this->config->get(\'config_autocomplete_limit\');\n		}\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">	\n	  <search><![CDATA[\n		if (!$this->request->post[\'config_limit_admin\']) {\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		if (!$this->request->post[\'config_autocomplete_limit\']) {\n		  $this->error[\'autocomplete_limit\'] = $this->language->get(\'error_limit\');\n		}\n	  ]]></add>\n	</operation>\n  </file> \n  <file path=\"admin/language/ru-ru/setting/setting.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'entry_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\" trim=\"true\"><![CDATA[\n		$_[\'entry_autocomplete_limit\']              = \'Лимит в поле Автозаполнения (Admin)\';\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'help_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'help_autocomplete_limit\']  = \'Определяет, сколько элементов отображать при автозаполении (в панели администрирования: товары, категории, Клиенты итд).\';\n	  ]]></add>\n	</operation>\n  </file>\n  <file path=\"admin/language/en-gb/setting/setting.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'entry_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'entry_autocomplete_limit\']              = \'The limit in the autocomplete field. (Admin)\';\n	  ]]></add>\n	</operation>	\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$_[\'help_limit_admin\']\n	  ]]></search>\n	  <add position=\"before\"><![CDATA[\n		$_[\'help_autocomplete_limit\']  = \'Determines how many items to display in autocomplete. (in the admin panel: Products, Categories, Clients, etc.).\';\n	  ]]></add>\n	</operation>\n  </file>\n  <file path=\"admin/view/template/common/header.twig\">\n  <operation error=\"skip\">\n	  <search><![CDATA[\n		<link type=\"text/css\" href=\"view/stylesheet/stylesheet.css\" rel=\"stylesheet\" media=\"screen\" />\n	  ]]></search>\n	  <add position=\"after\"><![CDATA[\n		<link type=\"text/css\" href=\"view/stylesheet/autocomplete.css\" rel=\"stylesheet\" media=\"screen\" />\n	  ]]></add>\n  </operation>\n  </file>\n  <file path=\"admin/view/template/setting/setting.twig\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		<legend>{{ text_review }}</legend>\n	  ]]></search>\n	  <add position=\"before\" offset=\"2\"><![CDATA[\n		<div class=\"form-group required\">\n		  <label class=\"col-sm-2 control-label\" for=\"input-autocomplete-limit\"><span data-toggle=\"tooltip\" title=\"{{ help_autocomplete_limit }}\">{{ entry_autocomplete_limit }}</span></label>\n		  <div class=\"col-sm-10\">\n			<input type=\"text\" name=\"config_autocomplete_limit\" value=\"{{ config_autocomplete_limit }}\" placeholder=\"{{ entry_autocomplete_limit }}\" id=\"input-autocomplete-limit\" class=\"form-control\" />\n			{% if (error_autocomplete_limit) %} \n			  <div class=\"text-danger\">{{ error_autocomplete_limit }}</div>\n			{% endif %} \n		  </div>\n		</div>\n	  ]]></add>\n	</operation>\n  </file>	\n  <file path=\"admin/controller/*/*.php\">\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		\'limit\'       => 5\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		\'limit\'        => $this->config->get(\'config_autocomplete_limit\')\n	  ]]></add>\n	</operation>\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		$limit = 5;\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		$limit = $this->config->get(\'config_autocomplete_limit\');\n	  ]]></add>\n	</operation>\n	<operation error=\"skip\">\n	  <search><![CDATA[\n		\'limit\'        => 5\n	  ]]></search>\n	  <add position=\"replace\" trim=\"true\"><![CDATA[\n		\'limit\'        => $this->config->get(\'config_autocomplete_limit\')\n	  ]]></add>\n	</operation>\n  </file>\n</modification>', 1, '2019-05-16 14:46:10', '2019-05-16 14:46:10'),
(4, 4, 'user_group_visual.ocmod', 'user_group_visual.ocmod', 'SlaSoft', '3.0', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>user_group_visual.ocmod</name>\r\n  <code>user_group_visual.ocmod</code>\r\n  <version>3.0</version>\r\n  <author>SlaSoft</author>\r\n  <file path=\"admin/view/template/user/user_group_form.twig\">\r\n	<operation>\r\n      <search><![CDATA[{% for permission in permissions %}]]></search>\r\n      <add position=\"replace\"><![CDATA[{% for key, permission in permissions %}]]></add>\r\n    </operation>\r\n\r\n	<operation>\r\n      <search><![CDATA[<input type=\"checkbox\" name=\"permission[modify][]\"]]></search>\r\n      <add position=\"after\" offset=\"1\"><![CDATA[&nbsp;::&nbsp;<b>{{ name_permissions[key] }}</b>]]></add>\r\n    </operation>\r\n	<operation>\r\n      <search><![CDATA[<input type=\"checkbox\" name=\"permission[access][]\"]]></search>\r\n      <add position=\"after\" offset=\"1\"><![CDATA[&nbsp;::&nbsp;<b>{{ name_permissions[key] }}</b>]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"admin/controller/user/user_permission.php\">\r\n	<operation>\r\n      <search><![CDATA[common/startup]]></search>\r\n      <add position=\"after\"><![CDATA[\'common/column_left\',]]></add>\r\n    </operation>\r\n	<operation>\r\n      <search><![CDATA[$data[\'permissions\'][] = $permission;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n				$this->load->language($permission);\r\n				$heading_title = strip_tags($this->language->get(\'heading_title\'));\r\n				if ($heading_title == $old_heading_title) { \r\n					$heading_title = \'\';\r\n				} else {\r\n					$old_heading_title = $heading_title;\r\n				}\r\n				$data[\'name_permissions\'][] = $heading_title;\r\n]]>\r\n      </add>\r\n    </operation>\r\n\r\n	<operation>\r\n      <search><![CDATA[$data[\'permissions\'] = array();]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		$data[\'name_permissions\'] = array();\r\n		$old_heading_title = \'\';\r\n]]>\r\n      </add>\r\n    </operation>\r\n  </file>\r\n</modification>\r\n', 1, '2019-05-16 14:47:01', '2019-05-16 14:47:01'),
(13, 13, 'Admin Detail Order Email', 'admin_detail_order_email_helpforsite.com', 'Volodymyr Chornovol', '1.0', 'https://helpforsite.com', '<modification>\n  <name>Admin Detail Order Email</name>\n  <code>admin_detail_order_email_helpforsite.com</code>\n	<version>1.0</version>\n	<link>https://helpforsite.com</link>\n	<author>Volodymyr Chornovol</author>\n	\n	<file path=\"catalog/controller/mail/order.php\">\n		<operation>\n			<search><![CDATA[public function alert(&$route, &$args) {]]></search>\n			<add position=\"after\"><![CDATA[\n        return false;\n      ]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$mail->send();]]></search>\n			<add position=\"after\"><![CDATA[\n	\n    // admin alert\n		if (in_array(\'order\', (array)$this->config->get(\'config_mail_alert\'))) {  \n      $this->load->language(\'mail/order_alert\');\n			$data[\'text_greeting\'] = $this->language->get(\'text_received\');\n			$data[\'text_footer\'] = \'\';\n    \n			\n			$mail->setTo($this->config->get(\'config_email\'));\n			$mail->setFrom($this->config->get(\'config_email\'));\n			$mail->setSender(html_entity_decode($order_info[\'store_name\'], ENT_QUOTES, \'UTF-8\'));\n			$mail->setSubject(html_entity_decode(sprintf($this->language->get(\'text_subject\'), $this->config->get(\'config_name\'), $order_info[\'order_id\']), ENT_QUOTES, \'UTF-8\'));\n			$mail->setHtml($this->load->view(\'mail/order_add\', $data));\n			$mail->send();\n			\n			// Send to additional alert emails\n			$emails = explode(\',\', $this->config->get(\'config_mail_alert_email\'));\n			\n			foreach ($emails as $email) {\n				if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {\n					$mail->setTo($email);\n					$mail->send();\n				}\n			}\n		}\n\n			]]></add>\n		</operation>\n	</file>	\n</modification>\n\n\n', 1, '2019-05-24 17:22:29', '2019-05-24 17:22:29'),
(9, 8, 'Упрощенный заказ [Custom Quick Checkout]', 'quickcheckout_oc3', 'opencart3x.ru', '1.9', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<modification>\n	<name>Упрощенный заказ [Custom Quick Checkout]</name>\n	<code>quickcheckout_oc3</code>\n	<version>1.9</version>\n	<author>opencart3x.ru</author>\n	<link>https://opencart3x.ru</link>\n	<file path=\"catalog/controller/checkout/cart.php\">\n		<operation>\n			<search><![CDATA[public function index() {]]></search>\n			<add position=\"after\"><![CDATA[		\n			if ($this->cart->hasProducts() && $this->config->get(\'quickcheckout_skip_cart\')){\n				$this->response->redirect($this->url->link(\'extension/quickcheckout/checkout\'));\n			}\n            ]]></add>\n		</operation>\n	</file>\n  	<file path=\"catalog/model/setting/extension.php\">\n		<operation>\n			<search trim=\"true\" index=\"0\"><![CDATA[\n				SELECT * FROM \" . DB_PREFIX . \"extension WHERE `type` =\n			]]></search>\n			<add position=\"after\" trim=\"false\" offset=\"0\"><![CDATA[\n				if ($type == \'payment\' && isset($this->session->data[\'shipping_method\'][\'code\']) && $this->config->get(\'quickcheckout_shipping_reload\')) {\n					$payments = $query->rows;\n\n					$rules = array();\n\n					foreach ($this->config->get(\'quickcheckout_payment2shipping_shippings\') as $shippings) {\n						if (strpos($this->session->data[\'shipping_method\'][\'code\'], $shippings[\'shipping\']) === 0) {\n							$rules[] = $shippings[\'payment\'];\n						}\n					}\n\n					if (!empty($rules)) {\n						foreach ($payments as $idx => $payment) {\n							if (!in_array($payment[\'code\'], $rules)) {\n								unset($payments[$idx]);\n							}\n						}\n\n						$query->rows = $payments;\n					}\n\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($rules); echo \'</pre>\';\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($this->session->data[\'shipping_method\']); echo \'</pre>\';\n					//echo \'<pre>\'.__METHOD__.\' [\'.__LINE__.\']: \'; print_r($query->rows); echo \'</pre>\';\n				}\n			]]></add>\n		</operation>\n	</file>\n</modification>', 1, '2019-05-22 20:18:39', '2019-05-22 20:18:39'),
(7, 7, 'ExtendedSearch', 'extendedsearch', 'AlexDW', '1.04', 'https://www.opencart.com/index.php?route=marketplace/extension&filter_member=AlexDW', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<modification>\r\n	<name>ExtendedSearch</name>\r\n	<version>1.04</version>\r\n	<author>AlexDW</author>\r\n	<link>https://www.opencart.com/index.php?route=marketplace/extension&amp;filter_member=AlexDW</link>\r\n	<code>extendedsearch</code>\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"abort\">\r\n		<search><![CDATA[\r\n$sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN \" . DB_PREFIX . \"product_to_store p2s\r\n			]]></search>\r\n			<add position=\"before\" ><![CDATA[\r\n// ExtendedSearch\r\n		if ((!empty($data[\'filter_name\'])) && $this->config->get(\'module_extendedsearch_status\') && $this->config->get(\'module_extendedsearch_attr\')) $sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_attribute pa ON (p.product_id = pa.product_id) \";\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"abort\">\r\n		<search><![CDATA[\r\n$implode[] = \"pd.name LIKE \'%\" . $this->db->escape($word) . \"%\'\";\r\n			]]></search>\r\n			<add position=\"replace\" ><![CDATA[\r\n// ExtendedSearch\r\n					$adw_es = \'module_extendedsearch_\';\r\n					$es = \" (LCASE(pd.name) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'model\')) $es .= \" OR LCASE(p.model) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'sku\'))$es .= \" OR LCASE(p.sku) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'upc\'))$es .= \" OR LCASE(p.upc) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'ean\'))$es .= \" OR LCASE(p.ean) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'jan\'))$es .= \" OR LCASE(p.jan) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'isbn\'))$es .= \" OR LCASE(p.isbn) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'mpn\'))$es .= \" OR LCASE(p.mpn) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'location\'))$es .= \" OR LCASE(p.location) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n				if ($this->config->get($adw_es.\'status\') && $this->config->get($adw_es.\'attr\'))$es .= \" OR LCASE(pa.text) LIKE \'%\" . $this->db->escape(utf8_strtolower($word)) . \"%\'\";\r\n					$es .= \") \";\r\n					$implode[] = $es;\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">		\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.model) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\" ><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_model\')) {\r\n				$sql .= \" OR LCASE(p.model) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.sku) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_sku\')) {\r\n				$sql .= \" OR LCASE(p.sku) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.upc) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_upc\')) {\r\n				$sql .= \" OR LCASE(p.upc) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.ean) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_ean\')) {\r\n				$sql .= \" OR LCASE(p.ean) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.jan) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_jan\')) {\r\n				$sql .= \" OR LCASE(p.jan) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.isbn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_isbn\')) {\r\n				$sql .= \" OR LCASE(p.isbn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n		<operation error=\"skip\">\r\n		<search><![CDATA[\r\n$sql .= \" OR LCASE(p.mpn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n// ExtendedSearch\r\n			if (!$this->config->get(\'module_extendedsearch_status\') && !$this->config->get(\'module_extendedsearch_mpn\')) {\r\n				$sql .= \" OR LCASE(p.mpn) = \'\" . $this->db->escape(utf8_strtolower($data[\'filter_name\'])) . \"\'\";\r\n			}\r\n// ExtendedSearch END\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2019-05-16 16:11:10', '2019-05-16 16:11:10'),
(14, 15, 'Localcopy OCMOD Install Fix', 'localcopy-oc3', 'opencart3x.ru', '1.0', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>Localcopy OCMOD Install Fix</name>\n  <code>localcopy-oc3</code>\n  <version>1.0</version>\n  <author>opencart3x.ru</author>\n  <link>https://opencart3x.ru</link>\n\n  <file path=\"admin/controller/marketplace/install.php\">\n	<operation>\n      <search>\n        <![CDATA[if ($safe) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n		    $safe = true;\n		    ]]>\n      </add>\n    </operation>\n    <operation>\n      <search>\n        <![CDATA[if (is_dir($file) && !is_dir($path)) {]]>\n      </search>\n      <add position=\"before\">\n        <![CDATA[		\n			  if ($path == \'\') {\n  				$app_root = explode(\'/\',DIR_APPLICATION);\n  				unset($app_root[count($app_root)-2]);\n  				$app_root = implode(\'/\',$app_root);\n  				$path = $app_root . $destination;\n			  }\n		    ]]>\n      </add>\n    </operation>\n  </file> \n</modification>\n', 1, '2019-08-07 15:31:32', '2019-08-07 15:31:32'),
(15, 17, 'Order Manager', 'Order-Manager', 'opencart3x.ru', '3.0', 'https://opencart3x.ru', '<modification>\r\n	<name>Order Manager</name>\r\n	<code>Order-Manager</code>\r\n	<version>3.0</version>\r\n	<author>opencart3x.ru</author>\r\n	<link>https://opencart3x.ru</link>\r\n\r\n	<file path=\"admin/controller/common/dashboard.php\">	\r\n		<operation>\r\n			<search><![CDATA[$data[\'footer\'] = $this->load->controller(\'common/footer\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			if ($this->config->get(\'module_manager_status\')) {\r\n				$data[\'manager\'] = $this->load->controller(\'dashboard/manager\');\r\n				\r\n				$data[\'module_manager_status\'] = $this->config->get(\'module_manager_status\');\r\n				$data[\'module_manager_hide_dashboard\'] = $this->config->get(\'module_manager_hide_dashboard\');\r\n\r\n				if (isset($this->session->data[\'module_manager_success\'])) {\r\n					$data[\'module_manager_success\'] = $this->session->data[\'module_manager_success\'];\r\n					unset($this->session->data[\'module_manager_success\']);\r\n				} else {\r\n					$data[\'module_manager_success\'] = \"\";\r\n				}\r\n\r\n				if (isset($this->session->data[\'module_manager_error\'])) {\r\n					$data[\'module_manager_error\'] = $this->session->data[\'module_manager_error\'];\r\n					unset($this->session->data[\'module_manager_error\']);\r\n				} else {\r\n					$data[\'module_manager_error\'] = \"\";\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"admin/view/template/common/dashboard.twig\">\r\n		<operation>\r\n			<search><![CDATA[{% if error_install %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{% if module_manager_success  and  module_manager_success %} \r\n  					<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> {{ module_manager_success }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			\r\n				{% if module_manager_error  and  module_manager_error %} \r\n  					<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> {{ module_manager_error }} \r\n      					<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n    				</div>\r\n  				{% endif %} \r\n  			{% endif %} \r\n  						\r\n			]]></add>\r\n		</operation>  				\r\n		<operation>\r\n			<search><![CDATA[{% for row in rows %}]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			{% if module_manager_status  and  module_manager_status %} \r\n				{{ manager }} \r\n			{% endif %} \r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[<div class=\"row\">]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n\r\n			<div class=\"row\" {% if module_manager_hide_dashboard  and  module_manager_hide_dashboard %} {{ \' style=\"display: none;\"\' }} {% endif %} >\r\n			\r\n			]]></add>\r\n		</operation>								\r\n	</file>\r\n\r\n	<!-- Admin: Customer -->\r\n\r\n	<file path=\"admin/controller/customer/customer.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->redirect($this->url->link(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$this->response->redirect($this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true));\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'customer/customer_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$url = \'user_token=\'.$this->session->data[\'user_token\'].\'&return=\'.$this->request->get[\'return\'];\r\n				\r\n				if (!isset($this->request->get[\'customer_id\'])) {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/add\', $url, true);\r\n				} else {\r\n					$data[\'action\'] = $this->url->link(\'customer/customer/edit\', $url.\'&customer_id=\'.$this->request->get[\'customer_id\'], true);\r\n				}\r\n\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>				\r\n	</file>\r\n\r\n	<!-- Admin: Order -->\r\n\r\n	<file path=\"admin/controller/sale/order.php\">	\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_form\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'sale/order_info\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'return\'])) {\r\n				$data[\'cancel\'] = $this->url->link(\'common/dashboard\', base64_decode($this->request->get[\'return\']), true);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/api/login.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!in_array($this->request->server[\'REMOTE_ADDR\'], $ip_data)) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$ip_data[] = $this->request->server[\'REMOTE_ADDR\'];\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<file path=\"catalog/controller/api/*.php\">	\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[if (!isset($this->session->data[\'api_id\'])) {]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			if (isset($this->session->data[\'opencart3x\'])) {\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n</modification>', 1, '2019-08-07 15:32:01', '2019-08-07 15:32:01'),
(16, 19, 'OCFilter Modification', 'ocfilter-product-filter', 'prowebber.ru', '4.7.5', 'https://prowebber.ru/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>OCFilter Modification</name>\r\n  <code>ocfilter-product-filter</code>\r\n  <version>4.7.5</version>\r\n  <author>prowebber.ru</author>\r\n  <link>https://prowebber.ru/</link>\r\n\r\n  <!-- CONTROLLER -->\r\n	<file path=\"admin/controller/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[function getForm() {]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n    // OCFilter start\r\n    $this->document->addStyle(\'view/stylesheet/ocfilter/ocfilter.css\');\r\n    $this->document->addScript(\'view/javascript/ocfilter/ocfilter.js\');\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->language->get(\'tab_general\');]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n    // OCFilter start\r\n    $data[\'tab_ocfilter\'] = $this->language->get(\'tab_ocfilter\');\r\n    $data[\'entry_values\'] = $this->language->get(\'entry_values\');\r\n    $data[\'ocfilter_select_category\'] = $this->language->get(\'ocfilter_select_category\');\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/filter\')) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			$ocfilter = array();\r\n\r\n			if ($this->user->hasPermission(\'access\', \'extension/module/ocfilter\')) {\r\n				$ocfilter[] = array(\r\n					\'name\'     => $this->language->get(\'text_ocfilter_option\'),\r\n					\'href\'     => $this->url->link(\'extension/module/ocfilter/filter\', \'user_token=\' . $this->session->data[\'user_token\'], true),\r\n					\'children\' => array()\r\n				);\r\n			}\r\n\r\n			if ($this->user->hasPermission(\'access\', \'extension/module/ocfilter\')) {\r\n				$ocfilter[] = array(\r\n					\'name\'	   => $this->language->get(\'text_ocfilter_page\'),\r\n					\'href\'     => $this->url->link(\'extension/module/ocfilter/page\', \'user_token=\' . $this->session->data[\'user_token\'], true),\r\n					\'children\' => array()\r\n				);\r\n			}\r\n\r\n			if ($ocfilter) {\r\n				$catalog[] = array(\r\n					\'name\'	   => $this->language->get(\'text_ocfilter\'),\r\n					\'href\'     => \'\',\r\n					\'children\' => $ocfilter\r\n				);\r\n			}\r\n		  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/controller/common/column_left.php -->\r\n  <!-- /CONTROLLER -->\r\n\r\n  <!-- LANGUAGE -->\r\n	<file path=\"admin/language/{en}*/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_success\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'entry_values\']          		= \'Add the values ​​for this option.\';\r\n$_[\'tab_ocfilter\']          		= \'OCFilter Options\';\r\n$_[\'ocfilter_select_category\'] 	= \'To start, select a category for this product.\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{ru}*/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_success\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'entry_values\']          		= \'Добавьте значения для этой опции.\';\r\n$_[\'tab_ocfilter\']          		= \'Опции фильтра\';\r\n$_[\'ocfilter_select_category\'] 	= \'Для начала, выберите категории для этого товара.\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{en}*/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_option\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'text_ocfilter\']                    = \'OCFilter\';\r\n$_[\'text_ocfilter_option\']             = \'Filters\';\r\n$_[\'text_ocfilter_page\']               = \'SEO Pages\';\r\n$_[\'text_ocfilter_setting\']            = \'Settings\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"admin/language/{ru}*/common/column_left.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$_[\'text_option\']]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n// OCFilter start\r\n$_[\'text_ocfilter\']                    = \'OCFilter\';\r\n$_[\'text_ocfilter_option\']             = \'Фильтры\';\r\n$_[\'text_ocfilter_page\']               = \'Страницы\';\r\n$_[\'text_ocfilter_setting\']            = \'Настройки\';\r\n// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n  <!-- /LANGUAGE -->\r\n\r\n  <!-- MODEL -->\r\n	<file path=\"admin/model/catalog/product.php\">\r\n    <operation error=\"skip\">\r\n      <search index=\"0\"><![CDATA[if (isset($data[\'product_recurring\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n		if (isset($data[\'ocfilter_product_option\'])) {\r\n			foreach ($data[\'ocfilter_product_option\'] as $option_id => $values) {\r\n				foreach ($values[\'values\'] as $value_id => $value) {\r\n					if (isset($value[\'selected\'])) {\r\n						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = \'\" . (int)$product_id . \"\', option_id = \'\" . (int)$option_id . \"\', value_id = \'\" . (string)$value_id . \"\', slide_value_min = \'\" . (isset($value[\'slide_value_min\']) ? (float)$value[\'slide_value_min\'] : 0) . \"\', slide_value_max = \'\" . (isset($value[\'slide_value_max\']) ? (float)$value[\'slide_value_max\'] : 0) . \"\'\");\r\n					}\r\n				}\r\n			}\r\n		}\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search index=\"1\"><![CDATA[if (isset($data[\'product_recurring\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    $this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = \'\" . (int)$product_id . \"\'\");\r\n\r\n		if (isset($data[\'ocfilter_product_option\'])) {\r\n			foreach ($data[\'ocfilter_product_option\'] as $option_id => $values) {\r\n				foreach ($values[\'values\'] as $value_id => $value) {\r\n					if (isset($value[\'selected\'])) {\r\n						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = \'\" . (int)$product_id . \"\', option_id = \'\" . (int)$option_id . \"\', value_id = \'\" . (string)$value_id . \"\', slide_value_min = \'\" . (isset($value[\'slide_value_min\']) ? (float)$value[\'slide_value_min\'] : 0) . \"\', slide_value_max = \'\" . (isset($value[\'slide_value_max\']) ? (float)$value[\'slide_value_max\'] : 0) . \"\'\");\r\n					}\r\n				}\r\n			}\r\n		}\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$data[\'product_attribute\'] = $this->getProductAttributes($product_id);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n 		// OCFilter start\r\n		$this->load->model(\'extension/ocfilter\');\r\n\r\n		$data[\'ocfilter_product_option\'] = $this->model_extension_ocfilter->getProductOCFilterValues($product_id);\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"product WHERE product_id = \'\" . (int)$product_id . \"\'\");]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		// OCFilter start\r\n		$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = \'\" . (int)$product_id . \"\'\");\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/model/catalog/product.php -->\r\n\r\n  <!-- /MODEL -->\r\n\r\n  <!-- VIEW -->\r\n	<file path=\"admin/view/template/catalog/product_form.twig\">\r\n    <operation error=\"skip\">\r\n      <search index=\"0\"><![CDATA[</script></div>]]></search>\r\n      <add position=\"replace\"><![CDATA[\r\n  </script>\r\n  <!-- OCFilter start -->\r\n  <script type=\"text/javascript\"><!--\r\n  ocfilter.php = {\r\n  	text_select: \'{{ text_select }}\',\r\n  	ocfilter_select_category: \'{{ ocfilter_select_category }}\',\r\n  	entry_values: \'{{ entry_values }}\',\r\n  	tab_ocfilter: \'{{ tab_ocfilter }}\'\r\n  };\r\n\r\n  ocfilter.php.languages = [];\r\n\r\n  {% for language in languages %}\r\n  ocfilter.php.languages.push({\r\n  	\'language_id\': {{ language.language_id }},\r\n  	\'name\': \'{{ language.name }}\',\r\n    \'image\': \'{{ language.image }}\'\r\n  });\r\n  {% endfor %}\r\n  //--></script>\r\n  <!-- OCFilter end -->\r\n  </div>\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /admin/view/template/catalog/product_form.twig -->\r\n  <!-- /VIEW -->\r\n\r\n  <!-- CATALOG -->\r\n\r\n  <file path=\"catalog/controller/startup/startup.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[Cart($this->registry));]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n		// OCFilter\r\n		$this->registry->set(\'ocfilter\', new OCFilter($this->registry));\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/startup/seo_url.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[$this->url->addRewrite($this, $lang_data);]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter start\r\n      if ($this->registry->has(\'ocfilter\')) {\r\n  			$this->url->addRewrite($this->ocfilter);\r\n  		}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/model/catalog/product.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$sql .= \" LEFT JOIN \" . DB_PREFIX . \"product_description]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n		// OCFilter start\r\n		if (!empty($data[\'filter_ocfilter\'])) {\r\n    	$this->load->model(\'extension/module/ocfilter\');\r\n\r\n      $ocfilter_product_sql = $this->model_extension_module_ocfilter->getSearchSQL($data[\'filter_ocfilter\']);\r\n		} else {\r\n      $ocfilter_product_sql = false;\r\n    }\r\n\r\n    if ($ocfilter_product_sql && $ocfilter_product_sql->join) {\r\n    	$sql .= $ocfilter_product_sql->join;\r\n    }\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[if (!empty($data[\'filter_manufacturer_id]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    if (!empty($ocfilter_product_sql) && $ocfilter_product_sql->where) {\r\n    	$sql .= $ocfilter_product_sql->where;\r\n    }\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/product/category.php\">\r\n    <operation error=\"abort\">\r\n      <search index=\"0\"><![CDATA[$data[\'breadcrumbs\'] = array();]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n		// OCFilter start\r\n    if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n      $filter_ocfilter = $this->request->get[\'filter_ocfilter\'];\r\n    } else {\r\n      $filter_ocfilter = \'\';\r\n    }\r\n		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <!-- Filter params to product model -->\r\n\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$product_total =]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  		// OCFilter start\r\n  		$filter_data[\'filter_ocfilter\'] = $filter_ocfilter;\r\n  		// OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <!-- Add url -->\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"2\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"3\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search index=\"4\"><![CDATA[if (isset($this->request->get[\'filter\'])) {]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n      // OCFilter start\r\n			if (isset($this->request->get[\'filter_ocfilter\'])) {\r\n				$url .= \'&filter_ocfilter=\' . $this->request->get[\'filter_ocfilter\'];\r\n			}\r\n      // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation error=\"skip\">\r\n      <search limit=\"1\"><![CDATA[$data[\'limit\'] = $limit;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n      // OCFilter Start\r\n      if ($this->ocfilter->getParams()) {\r\n        if (isset($product_total) && !$product_total) {\r\n      	  $this->response->redirect($this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\']));\r\n        }\r\n\r\n        $this->document->setTitle($this->ocfilter->getPageMetaTitle($this->document->getTitle()));\r\n			  $this->document->setDescription($this->ocfilter->getPageMetaDescription($this->document->getDescription()));\r\n        $this->document->setKeywords($this->ocfilter->getPageMetaKeywords($this->document->getKeywords()));\r\n\r\n        $data[\'heading_title\'] = $this->ocfilter->getPageHeadingTitle($data[\'heading_title\']);\r\n        $data[\'description\'] = $this->ocfilter->getPageDescription();\r\n\r\n        if (!trim(strip_tags(html_entity_decode($data[\'description\'], ENT_QUOTES, \'UTF-8\')))) {\r\n        	$data[\'thumb\'] = \'\';\r\n        }\r\n\r\n        $breadcrumb = $this->ocfilter->getPageBreadCrumb();\r\n\r\n        if ($breadcrumb) {\r\n  			  $data[\'breadcrumbs\'][] = $breadcrumb;\r\n        }\r\n\r\n        $this->document->deleteLink(\'canonical\');\r\n      }\r\n      // OCFilter End\r\n      ]]></add>\r\n    </operation>\r\n  </file><!-- /catalog/controller/product/category.php -->\r\n\r\n  <!-- Document Noindex & Canonical -->\r\n\r\n	<file path=\"system/library/document.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[public function getLinks]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  // OCFilter canonical fix start\r\n	public function deleteLink($rel) {\r\n    foreach ($this->links as $href => $link) {\r\n      if ($link[\'rel\'] == $rel) {\r\n      	unset($this->links[$href]);\r\n      }\r\n    }\r\n	}\r\n  // OCFilter canonical fix end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation>\r\n      <search><![CDATA[private $keywords;]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n  // OCFilter start\r\n  private $noindex = false;\r\n  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n\r\n    <operation>\r\n      <search><![CDATA[public function setTitle]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n  // OCFilter start\r\n  public function setNoindex($state = false) {\r\n  	$this->noindex = $state;\r\n  }\r\n\r\n	public function isNoindex() {\r\n		return $this->noindex;\r\n	}\r\n  // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n    <operation error=\"abort\">\r\n      <search><![CDATA[$data[\'scripts\'] = $this->document->getScripts]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n    // OCFilter start\r\n    $data[\'noindex\'] = $this->document->isNoindex();\r\n    // OCFilter end\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n  <file path=\"catalog/view/theme/*/template/common/header.twig\">\r\n    <operation error=\"skip\">\r\n      <search><![CDATA[</title>]]></search>\r\n      <add position=\"after\"><![CDATA[\r\n{% if noindex %}\r\n<!-- OCFilter Start -->\r\n<meta name=\"robots\" content=\"noindex,nofollow\" />\r\n<!-- OCFilter End -->\r\n{% endif %}\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n</modification>', 1, '2020-05-20 04:18:57', '2020-05-20 04:18:57');
INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(17, 20, 'GC Sessions Basic', '6629190803', 'alex cherednichenko', '3.x', 'https://www.alcher.info', '﻿<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<modification>\n	<name>GC Sessions Basic</name>\n	<code>6629190803</code>\n	<version>3.x</version>\n	<author>alex cherednichenko</author>\n	<link>https://www.alcher.info</link>\n\n	<file path=\"system/library/session.php\">\n		<operation>\n			<search>\n				<![CDATA[\n	public function close() {\n				]]>\n			</search>\n			<add position=\"replace\" offset=\"2\">\n				<![CDATA[\n	public function close() {\n		$this->adaptor->close($this->session_id, $this->data);\n	}\n				]]>\n			</add>\n		</operation>\n	</file>\n\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search>\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add position=\"before\">\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search>\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . ((int)$expire) . \" SECOND);\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n\n<!-- InnoDB -->\n<!--\nCREATE TABLE `oc_session` (\n  `session_id` varchar(32) NOT NULL,\n  `data` text NOT NULL,\n  `ip_address` varchar(255) DEFAULT NULL,\n  `user_agent` tinytext,\n  `expire` datetime NOT NULL,\n  PRIMARY KEY (`session_id`)\n) ENGINE=InnoDB DEFAULT CHARSET=utf8\n-->\n<!--\n	<file path=\"system/library/session/db.php\">\n		<operation>\n			<search position=\"before\">\n				<![CDATA[\n		public function gc($expire) {\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		public function close( $session_id, $data ) {\n			$this->write($session_id, $data);\n			$this->gc($this->expire);\n		}\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > DATE_SUB(NOW(), INTERVAL \" . $this->expire . \" SECOND);\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION READ ONLY\");\n		$query = $this->db->query(\"SELECT `data` FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\' AND expire > \" . (int)time());\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"REPLACE INTO `\" . DB_PREFIX . \"session` SET session_id = \'\" . $this->db->escape($session_id) . \"\', `data` = \'\" . $this->db->escape(json_encode($data)) . \"\', user_agent = \'\" . $this->db->escape($_SERVER[\'HTTP_USER_AGENT\']) . \"\', ip_address = \'\".  $this->db->escape($_SERVER[\'REMOTE_ADDR\']) . \"\', expire = \'\" . $this->db->escape(date(\'Y-m-d H:i:s\', time() + $this->expire)) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE session_id = \'\" . $this->db->escape($session_id) . \"\'\");\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n		<operation>\n			<search position=\"replace\">\n				<![CDATA[\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < \" . ((int)time() + $expire));\n				]]>\n			</search>\n			<add>\n				<![CDATA[\n		$this->db->query(\"START TRANSACTION\");\n		$this->db->query(\"DELETE FROM `\" . DB_PREFIX . \"session` WHERE expire < DATE_SUB(NOW(), INTERVAL \" . $expire . \" SECOND);\"); // + 86400\n		$this->db->query(\"COMMIT\");\n				]]>\n			</add>\n		</operation>\n\n	</file>\n-->\n</modification>', 1, '2020-05-20 04:19:03', '2020-05-20 04:19:03'),
(18, 21, 'OpenCart 3.0 Compatibility Fixes', 'compatibility_fixes', 'The Krotek', '3.1.0', 'https://thekrotek.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n	<name>OpenCart 3.0 Compatibility Fixes</name>\r\n	<code>compatibility_fixes</code>\r\n	<version>3.1.0</version>\r\n	<author>The Krotek</author>\r\n    <link>https://thekrotek.com</link>\r\n 			\r\n	<!-- System: Loader -->\r\n\r\n	<file path=\"system/engine/loader.php\">\r\n		<operation>\r\n			<search><![CDATA[if (!$this->registry->has(\'model_\']]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/before\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[throw new \\Exception(\'Error: Could not load model \']]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n			\r\n			$this->registry->get(\'event\')->trigger(\'model/\'.$route.\'/after\', array(&$route));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[include_once($file);]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			include_once(modification($file));\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Language -->\r\n\r\n	<file path=\"system/library/language.php\">\r\n		<operation>\r\n			<search><![CDATA[private $default = \'en-gb\';]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\r\n			public $default = \'en-gb\';\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[public function load(]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n	\r\n			public function load($filename, $key = \'\')\r\n			{\r\n				if (!$key) {\r\n					$_ = array();\r\n\r\n					$file = DIR_LANGUAGE.\'english/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.\'english/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n			\r\n					$file = DIR_LANGUAGE.$this->default.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->default.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$file = DIR_LANGUAGE.$this->directory.\'/\'.$filename.\'.php\';\r\n					$old_file = DIR_LANGUAGE.$this->directory.\'/\'.str_replace(\'extension/\', \'\', $filename).\'.php\';\r\n		\r\n					if (is_file($file)) require(modification($file));\r\n					elseif (is_file($old_file)) require(modification($old_file));\r\n								\r\n					$this->data = array_merge($this->data, $_);\r\n				} else {\r\n					$this->data[$key] = new Language($this->directory);\r\n					$this->data[$key]->load($filename);\r\n				}\r\n		\r\n				return $this->data;\r\n			}\r\n				\r\n			public function loadDefault(]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- System: Config -->\r\n	\r\n	<file path=\"system/config/admin.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n	\r\n			\'language/*/after\' => array(\r\n				\'event/compatibility/language\',\r\n				\'event/translation\'\r\n			),\r\n				\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"system/config/catalog.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'action_event\'] = array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'model/*/before\' => array(\r\n				\'event/compatibility/beforemodel\'\r\n			),\r\n			\r\n			\'model/*/after\' => array(\r\n				\'event/compatibility/aftermodel\'\r\n			),\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/before\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'controller/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			\'event/compatibility/controller\',\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'language/*/after\' => array(]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n						\r\n			\'event/compatibility/language\',\r\n					\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	   \r\n	<!-- Admin: Extensions -->\r\n\r\n	<file path=\"admin/controller/extension/extension/{analytics,captcha,dashboard,feed,fraud,menu,module,payment,report,shipping,theme,total}*.php\">\r\n		<operation>\r\n			<search><![CDATA[$this->load->controller(\'extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($this->request->get[\'extension\'])) {\r\n				$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n\r\n				if (__FUNCTION__ == \'install\') {\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'access\', $type.\'/\'.$this->request->get[\'extension\']);\r\n					$this->model_user_user_group->addPermission($this->user->getGroupId(), \'modify\', $type.\'/\'.$this->request->get[\'extension\']);\r\n				}\r\n\r\n				$this->load->controller($type.\'/\'.$this->request->get[\'extension\'].\'/\'.__FUNCTION__);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'extension/extension]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($data[\'extensions\'])) {\r\n				$data[\'extensions\'] = array_unique($data[\'extensions\'], SORT_REGULAR);\r\n			\r\n				usort($data[\'extensions\'], function($a, $b){ return strcmp($a[\"name\"], $b[\"name\"]); });\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$type = strtolower(str_replace(\'ControllerExtensionExtension\', \'\', __CLASS__));\r\n			\r\n			$files = glob(DIR_APPLICATION . \'controller/{extension/\'.$type.\',\'.$type.\'}/*.php\', GLOB_BRACE);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Admin: User Groups -->\r\n\r\n	<file path=\"admin/controller/user/user_permission.php\">\r\n		<operation>\r\n			<search><![CDATA[ControllerUserUserPermission extends Controller {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			private function checkPermissions(&$permissions)\r\n			{\r\n				$folders = array(\'analytics\', \'captcha\', \'dashboard\', \'feed\', \'fraud\', \'module\', \'payment\', \'shipping\', \'theme\', \'total\');\r\n				\r\n				foreach ($permissions as $type => $extensions) {\r\n					foreach ($extensions as $route) {\r\n						$route = explode(\'/\', $route);\r\n						\r\n						if (($route[0] != \'extension\') && in_array($route[0], $folders) && is_file(DIR_APPLICATION.\'controller/\'.$route[0].\'/\'.$route[1].\'.php\')) {\r\n							$permissions[$type][] = \'extension/\'.$route[0].\'/\'.$route[1];\r\n						}\r\n					}\r\n					\r\n					sort($permissions[$type]);\r\n				}\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->addUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->model_user_user_group->editUserGroup(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (isset($this->request->post[\'permission\'])) {\r\n				$this->checkPermissions($this->request->post[\'permission\']);\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n		\r\n	<!-- Admin: Menu -->\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/extension\')) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$this->load->language(\'marketplace/marketplace\');			\r\n			\r\n			$this->load->language(\'extension/extension/analytics\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=analytics\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/captcha\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=captcha\', true),\r\n				\'children\' => array());\r\n\r\n			$this->load->language(\'extension/extension/dashboard\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_dashboard\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=dashboard\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/feed\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_feed\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=feed\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/fraud\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'heading_title\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=fraud\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/menu\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_menu\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=menu\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/module\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_module\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=module\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/payment\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_payment\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=payment\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/report\');\r\n			\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_report\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=report\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/shipping\');\r\n										\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_shipping\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=shipping\', true),\r\n				\'children\' => array());\r\n				\r\n			$this->load->language(\'extension/extension/theme\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_theme\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=theme\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'extension/extension/total\');\r\n					\r\n			$extension[] = array(\r\n				\'name\'	   => $this->language->get(\'text_total\'),\r\n				\'href\'     => $this->url->link(\'marketplace/extension\', \'user_token=\'.$this->session->data[\'user_token\'].\'&type=total\', true),\r\n				\'children\' => array());\r\n			\r\n			$this->load->language(\'common/column_left\');\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'marketplace/modification\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			if (!empty($extension)) {\r\n				$sorted = array_values($marketplace);\r\n				$element = end($sorted);\r\n			\r\n				$element[\'href\'] = \'\';\r\n				$element[\'children\'] = $extension;\r\n				\r\n				$marketplace[count($marketplace) - 1] = $element;\r\n			}\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n	\r\n	<!-- Catalog: Checkout -->\r\n	\r\n	<file path=\"catalog/controller/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n			$files = array_merge($files, glob(DIR_APPLICATION.\'controller/total/*.php\'));\r\n			\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($files) {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			\r\n			$modules = array();\r\n			\r\n			]]></add>\r\n		</operation>			\r\n		<operation>\r\n			<search><![CDATA[if ($result) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			\r\n				$modules[] = basename($file, \'.php\');\r\n			}\r\n\r\n			foreach (array_unique($modules) as $module) {\r\n				$result = $this->load->controller(\'extension/total/\'.$module);\r\n			\r\n			]]></add>\r\n		</operation>		\r\n	</file>\r\n				\r\n</modification>', 1, '2020-05-20 04:19:08', '2020-05-20 04:19:08'),
(19, 22, 'Modification Manager', 'modification_manager', 'opencart3x.ru', '3.x', 'https://opencart3x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<name>Modification Manager</name>\n	<code>modification_manager</code>\n	<version>3.x</version>\n	<author>opencart3x.ru</author>\n	<link>https://opencart3x.ru</link>\n\n	<file path=\"admin/language/ru-ru/marketplace/modification.php\">\n		<operation>\n			<search index=\"0\"><![CDATA[<?php]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'tab_error\'] = \'Ошибки\';\n$_[\'tab_files\'] = \'Файлы\';\n\n$_[\'text_add\'] = \'Создать\';\n$_[\'text_edit\'] = \'Редактировать: %s\';\n\n$_[\'text_enabled\'] = \'Включено\';\n$_[\'text_disabled\'] = \'Отключено\';\n\n$_[\'entry_author\'] = \'Автор\';\n$_[\'entry_name\'] = \'Название\';\n$_[\'entry_xml\'] = \'XML\';\n\n$_[\'button_filter\'] = \'Фильтр\';\n$_[\'button_reset\'] = \'Сброс\';\n\n$_[\'column_date_modified\'] = \'Дата измен.\';\n\n$_[\'error_warning\'] = \'Возникла ошибка, проверьте данные и попробуйте еще раз\';\n$_[\'error_required\'] = \'Это поле обязательно\';\n$_[\'error_name\'] = \'Пропущен тег name\';\n$_[\'error_code\'] = \'Пропущен тег code\';\n$_[\'error_exists\'] = \'Модификация \\\'%s\\\' уже использует идентификатор code: %s!\';]]></add>\n		</operation>\n	</file>\n\n	<file path=\"admin/language/en-gb/marketplace/modification.php\">\n		<operation>\n			<search index=\"0\"><![CDATA[<?php]]></search>\n			<add position=\"after\"><![CDATA[\n$_[\'tab_error\'] = \'Error\';\n$_[\'tab_files\'] = \'Files\';\n\n$_[\'text_add\'] = \'Add Modification\';\n$_[\'text_edit\'] = \'Edit Modification: %s\';\n\n$_[\'text_enabled\'] = \'Enabled\';\n$_[\'text_disabled\'] = \'Disabled\';\n\n$_[\'entry_author\'] = \'Author\';\n$_[\'entry_name\'] = \'Name\';\n$_[\'entry_xml\'] = \'XML\';\n\n$_[\'button_filter\'] = \'Filter\';\n$_[\'button_reset\'] = \'Reset\';\n\n$_[\'column_date_modified\'] = \'Last Modified\';\n\n$_[\'error_warning\'] = \'There has been an error. Please check your data and try again\';\n$_[\'error_required\'] = \'This field is required\';\n$_[\'error_name\'] = \'Missing name tag\';\n$_[\'error_code\'] = \'Missing code tag\';\n$_[\'error_exists\'] = \'Modification \\\'%s\\\' is already using the same code: %s!\';]]></add>\n		</operation>\n	</file>\n\n	<file path=\"admin/controller/marketplace/modification.php\">\n	    <operation>\n			<search index=\"0\"><![CDATA[public function index() {]]></search>\n			<add position=\"after\"><![CDATA[      	$this->load->model(\'extension/module/modification_manager\');\n\n		$this->model_extension_module_modification_manager->install();\n]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$handle = fopen(DIR_LOGS . \'ocmod.log\', \'w+\');]]></search>\n			<add position=\"before\"><![CDATA[      	fclose($handle);\n			\n					$handle = fopen(DIR_LOGS . \'ocmod_error.log\', \'w+\');]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$maintenance = $this->config->get(\'config_maintenance\');]]></search>\n			<add position=\"after\"><![CDATA[\n			// Clear logs on refresh\n			$handle = fopen(DIR_LOGS . \'ocmod.log\', \'w+\');\n			fclose($handle);\n\n			$handle = fopen(DIR_LOGS . \'ocmod_error.log\', \'w+\');\n			fclose($handle);\n]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'breadcrumbs\'] = array();]]></search>\n			<add position=\"before\"><![CDATA[      	$this->load->model(\'extension/module/modification_manager\');\n\n		if (isset($this->request->get[\'filter_name\'])) {\n			$filter_name = $this->request->get[\'filter_name\'];\n		} else {\n			$filter_name = null;\n		}\n\n      	if (isset($this->request->get[\'filter_xml\'])) {\n			$filter_xml = $this->request->get[\'filter_xml\'];\n		} else {\n			$filter_xml = null;\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$filter_author = $this->request->get[\'filter_author\'];\n		} else {\n			$filter_author = null;\n		}\n\n		$url = $this->getListUrlParams();\n\n		$data[\'add\'] = $this->url->link(\'marketplace/modification/add\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true);\n		$data[\'clear_log\'] = $this->url->link(\'marketplace/modification/clearlog\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true);\n		$data[\'filter_action\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n		$data[\'reset_url\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n\n		$data[\'tab_files\'] = $this->language->get(\'tab_files\');\n		$data[\'tab_error\'] = $this->language->get(\'tab_error\');]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'sort_name\'] =]]></search>\n			<add position=\"before\"><![CDATA[      	if (isset($this->request->get[\'filter_name\'])) {\n			$url .= \'&filter_name=\' . urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$url .= \'&filter_author=\' . urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_xml\'])) {\n			$url .= \'&filter_xml=\' . urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		$data[\'sort_date_modified\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . \'&sort=date_modified\' . $url, true);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$filter_data = array(]]></search>\n			<add position=\"after\"><![CDATA[      	\'filter_name\'	  => $filter_name,\n			\'filter_author\'	  => $filter_author,\n			\'filter_xml\'	  => $filter_xml,]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$modification_total = $this->model_setting_modification->getTotalModifications();]]></search>\n			<add position=\"replace\"><![CDATA[$modification_total = $this->model_extension_module_modification_manager->getTotalModifications($filter_data);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$results = $this->model_setting_modification->getModifications($filter_data);]]></search>\n			<add position=\"replace\"><![CDATA[$results = $this->model_extension_module_modification_manager->getModifications($filter_data);]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'modifications\'][] = array(]]></search>\n			<add position=\"after\"><![CDATA[				\'date_modified\'      => isset($result[\'date_modified\']) ? (date(\'Ymd\') == date(\'Ymd\', strtotime($result[\'date_modified\'])) ? date(\'G:i\', strtotime($result[\'date_modified\'])) : date($this->language->get(\'date_format_short\'), strtotime($result[\'date_modified\']))) : null,\n				\'edit\'			     => $this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . \'&modification_id=\' . $result[\'modification_id\'] . $url, true),]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$pagination = new Pagination();]]></search>\n			<add position=\"before\"><![CDATA[      	if (isset($this->request->get[\'filter_name\'])) {\n			$url .= \'&filter_name=\' . urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_author\'])) {\n			$url .= \'&filter_author=\' . urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($this->request->get[\'filter_xml\'])) {\n			$url .= \'&filter_xml=\' . urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$data[\'clear_log\'] =]]></search>\n			<add position=\"before\"><![CDATA[		$data[\'filter_name\'] = $filter_name;\n		$data[\'filter_author\'] = $filter_author;\n		$data[\'filter_xml\'] = $filter_xml;\n\n		$data[\'modified_files\'] = array();\n\n		$modified_files = self::modifiedFiles(DIR_MODIFICATION);\n\n		$modification_files = $this->getModificationXmlFiles();\n\n		foreach($modified_files as $modified_file) {\n			if(isset($modification_files[$modified_file])){\n				$modifications = $modification_files[$modified_file];\n			} else {\n				$modifications = array();\n			}\n\n			$data[\'modified_files\'][] = array(\n				\'file\' => $modified_file,\n				\'modifications\' => $modifications\n			);\n		}\n\n		// Error log\n		$error_file = DIR_LOGS . \'ocmod_error.log\';\n\n		if (file_exists($error_file)) {\n			$data[\'error_log\'] = htmlentities(file_get_contents($error_file, FILE_USE_INCLUDE_PATH, null));\n		} else {\n			$data[\'error_log\'] = \'\';\n		}\n		]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$this->load->view(\'marketplace/modification\']]></search>\n			<add position=\"replace\"><![CDATA[$this->load->view(\'extension/module/modification_manager/list\']]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[$this->response->redirect($this->url->link(!empty($data[\'redirect\']) ? $data[\'redirect\'] : \'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));]]></search>\n			<ignoreif position=\"replace\"><![CDATA[if (!empty($data[\'redirect\'])) {]]></ignoreif>\n			<add position=\"replace\"><![CDATA[$url = $this->getListUrlParams();\n\n			if (!empty($data[\'redirect\'])) {\n				$redirect = $data[\'redirect\'];\n			} elseif (!empty($this->request->get[\'redirect\'])) {\n				$redirect = $this->request->get[\'redirect\'];\n			} else {\n				$redirect = \'marketplace/modification\';\n			}\n\n			$this->response->redirect($this->url->link($redirect, \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[if ($this->validate()) {]]></search>\n			<add position=\"after\"><![CDATA[				$error_log = array();\n\n			// Clear vqmod cache\n			$vqmod_path = substr(DIR_SYSTEM, 0, -7) . \'vqmod/\';\n\n			if (file_exists($vqmod_path)) {\n				$vqmod_cache = glob($vqmod_path.\'vqcache/vq*\');\n\n				if ($vqmod_cache) {\n					foreach ($vqmod_cache as $file) {\n						if (file_exists($file)) {\n							@unlink($file);\n						}\n					}\n				}\n\n				if (file_exists($vqmod_path.\'mods.cache\')) {\n					@unlink($vqmod_path.\'mods.cache\');\n				}\n\n				if (file_exists($vqmod_path.\'checked.cache\')) {\n					@unlink($vqmod_path.\'checked.cache\');\n				}\n			}\n]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$log[] = \'MOD:]]></search>\n			<add position=\"after\"><![CDATA[				$error_log_mod = \'MOD: \' . $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$operations = $file->getElementsByTagName(\'operation\');]]></search>\n			<add position=\"after\"><![CDATA[				\n					$file_error = $file->getAttribute(\'error\');]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$files = glob($path, GLOB_BRACE);]]></search>\n			<add position=\"after\"><![CDATA[							if (!$files) {\n								if ($file_error != \'skip\') {\n									$error_log[] = \'----------------------------------------------------------------\';\n									$error_log[] = $error_log_mod;\n									$error_log[] = \'MISSING FILE!\';\n									$error_log[] = $path;									\n								}\n							}]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[if (!$status) {]]></search>\n			<add position=\"after\"><![CDATA[											if ($error != \'skip\') {\n												$error_log[] = \"\\n\";\n												$error_log[] = $error_log_mod;\n												$error_log[] = \'NOT FOUND!\';\n												$error_log[] = \'CODE: \' . $search;\n												$error_log[] = \'FILE: \' . $key;\n											}]]></add>\n		</operation>\n	    <operation>\n			<search index=\"0\"><![CDATA[$ocmod->write(implode(\"\\n\", $log));]]></search>\n			<add position=\"after\"><![CDATA[\n			if ($error_log) {\n				$ocmod = new Log(\'ocmod_error.log\');\n				$ocmod->write(implode(\"\\n\", $error_log));\n			}]]></add>\n		</operation>\n		<operation>\n			<search index=\"0\"><![CDATA[protected function validate(]]></search>\n			<add position=\"before\"><![CDATA[\n\npublic function add() {\n		$this->load->language(\'marketplace/modification\');\n\n		$this->load->model(\'setting/modification\');\n\n		if (($this->request->server[\'REQUEST_METHOD\'] == \'POST\') && $this->validateForm()) {\n			$xml = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$data = array(\n				\'version\' => \'\',\n				\'author\' => \'\',\n				\'link\' => \'\',\n				\'status\' => 1\n			);\n\n			$data[\'xml\'] = $xml;\n\n			$data[\'name\'] = $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n\n			$data[\'code\'] = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			if ($dom->getElementsByTagName(\'version\')->length) {\n				$data[\'version\'] = $dom->getElementsByTagName(\'version\')->item(0)->textContent;\n			}\n\n			if ($dom->getElementsByTagName(\'author\')->length) {\n				$data[\'author\'] = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n			}\n\n			$this->model_setting_modification->addModification($data);\n\n			$modification_id = $this->db->getLastId();\n\n			$this->session->data[\'success\'] = $this->language->get(\'text_success\');\n\n			$this->response->redirect($this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(array(\'modification_id\' => $modification_id)), true));\n		}\n\n		$this->getForm();\n	}\n\n	public function edit() {\n		$this->load->language(\'marketplace/modification\');\n\n		$this->load->model(\'setting/modification\');\n		\n		if (($this->request->server[\'REQUEST_METHOD\'] == \'POST\') && !empty($this->request->get[\'modification_id\']) && $this->validateForm()) {\n			$modification_id = $this->request->get[\'modification_id\'];\n\n			$xml = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$data = array();\n\n			$data[\'xml\'] = $xml;\n\n			$data[\'name\'] = $dom->getElementsByTagName(\'name\')->item(0)->textContent;\n\n			$data[\'code\'] = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			if ($dom->getElementsByTagName(\'version\')->length) {\n				$data[\'version\'] = $dom->getElementsByTagName(\'version\')->item(0)->textContent;\n			} else {\n				$data[\'version\'] = \'\';\n			}\n\n			if ($dom->getElementsByTagName(\'author\')->length) {\n				$data[\'author\'] = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n			} else {\n				$data[\'author\'] = \'\';\n			}\n\n			if ($dom->getElementsByTagName(\'link\')->length) {\n				$data[\'link\'] = $dom->getElementsByTagName(\'link\')->item(0)->textContent;\n			} else {\n				$data[\'link\'] = \'\';\n			}\n\n			$this->load->model(\'extension/module/modification_manager\');\n\n			$this->model_extension_module_modification_manager->editModification($modification_id, $data);\n\n			$url = $this->getListUrlParams(array(\'modification_id\' => $modification_id));\n\n			if (isset($this->request->get[\'refresh\'])) {\n				$this->response->redirect($this->url->link(\'marketplace/modification/refresh\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));\n			}\n\n			if ($this->db->countAffected()) {\n				$this->session->data[\'success\'] = $this->language->get(\'text_success\');\n\n				$this->response->redirect($this->url->link(\'marketplace/modification/edit\', \'user_token=\' . $this->session->data[\'user_token\'] . $url, true));\n			}\n		}\n\n		$this->getForm();\n	}\n\n	public function getForm() {\n		$data[\'heading_title\'] = $this->language->get(\'heading_title\');\n\n		$data[\'text_enabled\'] = $this->language->get(\'text_enabled\');\n		$data[\'text_disabled\'] = $this->language->get(\'text_disabled\');\n\n		$data[\'button_save\'] = $this->language->get(\'button_save\');\n		$data[\'button_refresh\'] = $this->language->get(\'button_refresh\');\n		$data[\'button_cancel\'] = $this->language->get(\'button_cancel\');\n\n		if (isset($this->error[\'warning\'])) {\n			$data[\'error_warning\'] = $this->error[\'warning\'];\n		} elseif (!empty($this->error)) {\n			$data[\'error_warning\'] = $this->language->get(\'error_warning\');\n		} else {\n			$data[\'error_warning\'] = \'\';\n		}\n\n		if (isset($this->session->data[\'success\'])) {\n			$data[\'success\'] = $this->session->data[\'success\'];\n\n			unset($this->session->data[\'success\']);\n		} else {\n			$data[\'success\'] = false;\n		}\n\n		if (isset($this->error[\'xml\'])) {\n			$data[\'error_xml\'] = $this->error[\'xml\'];\n		}\n\n		$data[\'breadcrumbs\'] = array();\n\n		$data[\'breadcrumbs\'][] = array(\n			\'text\' => $this->language->get(\'text_home\'),\n			\'href\' => $this->url->link(\'common/dashboard\', \'user_token=\' . $this->session->data[\'user_token\'], true)\n		);\n\n		$data[\'breadcrumbs\'][] = array(\n			\'text\' => $this->language->get(\'heading_title\'),\n			\'href\' => $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(), true)\n		);\n\n		if (isset($this->request->get[\'modification_id\'])) {\n			$modification_info = $this->model_setting_modification->getModification($this->request->get[\'modification_id\']);\n			if (!$modification_info) exit;\n\n			$data[\'text_form\'] = sprintf($this->language->get(\'text_edit\'), $modification_info[\'name\']);\n\n			$data[\'action\'] = $this->url->link(\'marketplace/modification/edit\', \'&modification_id=\' . $modification_info[\'modification_id\'] . \'&user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$data[\'refresh\'] = $this->url->link(\'marketplace/modification/edit\', \'&modification_id=\' . $modification_info[\'modification_id\'] . \'&refresh=1&user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$this->document->setTitle($modification_info[\'name\'] . \' » \' . $data[\'heading_title\']);\n		} else {\n			$data[\'text_form\'] = $this->language->get(\'text_add\');\n\n			$data[\'refresh\'] = false;\n\n			$data[\'action\'] = $this->url->link(\'marketplace/modification/add\', \'user_token=\' . $this->session->data[\'user_token\'], true);\n\n			$this->document->setTitle($data[\'heading_title\']);\n		}\n\n		$data[\'cancel\'] = $this->url->link(\'marketplace/modification\', \'user_token=\' . $this->session->data[\'user_token\'] . $this->getListUrlParams(), true);\n\n		$data[\'modification\'] = array();\n\n		if (!empty($modification_info)) {\n			$data[\'modification\'][\'status\'] = $modification_info[\'status\'];\n		} else {\n			$data[\'modification\'][\'status\'] = 0;\n		}\n\n		if (isset($this->request->post[\'xml\'])) {\n			$data[\'modification\'][\'xml\'] = html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\');\n		} elseif (!empty($modification_info)) {\n			$data[\'modification\'][\'xml\'] = $modification_info[\'xml\'];\n		} else {\n			$data[\'modification\'][\'xml\'] = \'\';\n		}\n\n		$this->document->addStyle(\'view/javascript/codemirror/lib/codemirror.css\');\n		$this->document->addScript(\'view/javascript/codemirror/lib/codemirror.js\');\n		$this->document->addScript(\'view/javascript/codemirror/mode/xml/xml.js\');\n\n		$data[\'header\'] = $this->load->controller(\'common/header\');\n		$data[\'column_left\'] = $this->load->controller(\'common/column_left\');\n		$data[\'footer\'] = $this->load->controller(\'common/footer\');\n\n		$this->response->setOutput($this->load->view(\'extension/module/modification_manager/form\', $data));\n	}\n\n	private function validateForm() {\n		if (!$this->user->hasPermission(\'modify\', \'marketplace/modification\')) {\n			$this->error[\'warning\'] = $this->language->get(\'error_permission\');\n		}\n\n		$error = false;\n\n		// Required\n		if (empty($this->request->post[\'xml\'])) {\n			$error = $this->language->get(\'error_required\');\n		}\n\n		// 2. Validate XML\n		if (!$error) {\n			libxml_use_internal_errors(true);\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n\n			if(!$dom->loadXml(html_entity_decode($this->request->post[\'xml\'], ENT_QUOTES, \'UTF-8\'))){\n\n			    foreach (libxml_get_errors() as $error) {\n			        $msg = \'\';\n\n			        switch ($error->level) {\n			            case LIBXML_ERR_WARNING :\n			                $msg .= \"Warning $error->code: \";\n			                break;\n			            case LIBXML_ERR_ERROR :\n			                $msg .= \"Error $error->code: \";\n			                break;\n			            case LIBXML_ERR_FATAL :\n			                $msg .= \"Fatal Error $error->code: \";\n			                break;\n			        }\n\n			        $msg .= trim ( $error->message ) . \"\\nLine: $error->line\";\n\n			        $error = $msg;\n			    }\n\n			    libxml_clear_errors();\n			}\n\n			libxml_use_internal_errors(false);\n		}\n\n		// 3. Required tags\n		if (!$error && (!$dom->getElementsByTagName(\'name\') || $dom->getElementsByTagName(\'name\')->length == 0 || $dom->getElementsByTagName(\'name\')->item(0)->textContent == \'\')) {\n			$error = $this->language->get(\'error_name\');\n		}\n\n		if (!$error && (!$dom->getElementsByTagName(\'code\') || $dom->getElementsByTagName(\'code\')->length == 0 || $dom->getElementsByTagName(\'code\')->item(0)->textContent == \'\')) {\n			$error = $this->language->get(\'error_code\');\n		}\n\n		// 4. Check code isn\'t duplicate\n		if (!$error) {\n			$code = $dom->getElementsByTagName(\'code\')->item(0)->textContent;\n\n			$this->load->model(\'setting/modification\');\n			\n			$modification_info = $this->model_setting_modification->getModificationByCode($code);\n\n			if ($modification_info && (!isset($this->request->get[\'modification_id\']) || $modification_info[\'modification_id\'] != $this->request->get[\'modification_id\'])) {\n				$error = sprintf($this->language->get(\'error_exists\'), $modification_info[\'name\'], $modification_info[\'code\']);\n			}\n		}\n\n		if ($error) {\n			$this->error[\'xml\'] = $error;\n		}\n\n		return !$this->error;\n	}\n\n	static function modifiedFiles($dir, $dirLen = 0) {\n		$tree = glob(rtrim($dir, \'/\') . \'/*\');\n		if (!$dirLen) {\n			$dirLen = strlen($dir);\n		}\n		$files = array();\n\n	    if (is_array($tree)) {\n	        foreach($tree as $file) {\n	        	if ($file == $dir . \'index.html\') {\n					continue;\n				} elseif (is_file($file)) {\n	                $files[] = substr($file, $dirLen);\n	            } elseif (is_dir($file)) {\n	                $files = array_merge($files, self::modifiedFiles($file, $dirLen));\n	            }\n	        }\n	    }\n\n	    return $files;\n	}\n\n	protected function getListUrlParams(array $params = array()) {\n		if (isset($params[\'sort\'])) {\n			$params[\'sort\'] = $params[\'sort\'];\n		} elseif (isset($this->request->get[\'sort\'])) {\n			$params[\'sort\'] = $this->request->get[\'sort\'];\n		}\n\n		if (isset($params[\'order\'])) {\n			$params[\'order\'] = $params[\'order\'];\n		} elseif (isset($this->request->get[\'order\'])) {\n			$params[\'order\'] = $this->request->get[\'order\'];\n		}\n\n		if (isset($params[\'filter_name\'])) {\n			$params[\'filter_name\'] = urlencode(html_entity_decode($params[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_name\'])) {\n			$params[\'filter_name\'] = urlencode(html_entity_decode($this->request->get[\'filter_name\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'filter_author\'])) {\n			$params[\'filter_author\'] = urlencode(html_entity_decode($params[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_author\'])) {\n			$params[\'filter_author\'] = urlencode(html_entity_decode($this->request->get[\'filter_author\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'filter_xml\'])) {\n			$params[\'filter_xml\'] = urlencode(html_entity_decode($params[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		} elseif (isset($this->request->get[\'filter_xml\'])) {\n			$params[\'filter_xml\'] = urlencode(html_entity_decode($this->request->get[\'filter_xml\'], ENT_QUOTES, \'UTF-8\'));\n		}\n\n		if (isset($params[\'page\'])) {\n			$params[\'page\'] = $params[\'page\'];\n		} elseif (isset($this->request->get[\'page\'])) {\n			$params[\'page\'] = $this->request->get[\'page\'];\n		}\n\n		$paramsJoined = array();\n\n		foreach($params as $param => $value) {\n			$paramsJoined[] = \"$param=$value\";\n		}\n\n		return \'&\' . implode(\'&\', $paramsJoined);\n	}\n\n	protected function getModificationXmlFiles() {\n		$return = array();\n\n		$baseLen = strlen(substr(DIR_SYSTEM, 0, -7));\n\n		$xml = array();\n\n		$xml[] = file_get_contents(DIR_SYSTEM . \'modification.xml\');\n\n		$files = glob(DIR_SYSTEM . \'*.ocmod.xml\');\n\n		if ($files) {\n			foreach ($files as $file) {\n				$xml[] = file_get_contents($file);\n			}\n		}\n\n		$results = $this->model_setting_modification->getModifications();\n\n		foreach ($results as $result) {\n			if ($result[\'status\']) {\n				$xml[] = $result[\'xml\'];\n			}\n		}\n\n		foreach ($xml as $xml) {\n			if (empty($xml)){\n				continue;\n			}\n\n			$dom = new DOMDocument(\'1.0\', \'UTF-8\');\n			$dom->preserveWhiteSpace = false;\n			$dom->loadXml($xml);\n\n			$files = $dom->getElementsByTagName(\'modification\')->item(0)->getElementsByTagName(\'file\');\n\n			foreach ($files as $file) {\n				$operations = $file->getElementsByTagName(\'operation\');\n\n				$files = explode(\',\', $file->getAttribute(\'path\'));\n\n				foreach ($files as $file) {\n					$path = \'\';\n\n					// Get the full path of the files that are going to be used for modification\n					if (substr($file, 0, 7) == \'catalog\') {\n						$path = DIR_CATALOG . str_replace(\'../\', \'\', substr($file, 8));\n					}\n\n					if (substr($file, 0, 5) == \'admin\') {\n						$path = DIR_APPLICATION . str_replace(\'../\', \'\', substr($file, 6));\n					}\n\n					if (substr($file, 0, 6) == \'system\') {\n						$path = DIR_SYSTEM . str_replace(\'../\', \'\', substr($file, 7));\n					}\n\n					if ($path) {\n						$files = glob($path, GLOB_BRACE);\n\n						if ($files) {\n							foreach ($files as $file) {\n								$file = substr($file, $baseLen);\n\n								if (!isset($return[$file])) {\n									$return[$file] = array();\n								}\n\n								if ($dom->getElementsByTagName(\'author\')->length) {\n									$author = $dom->getElementsByTagName(\'author\')->item(0)->textContent;\n								} else {\n									$author = \'\';\n								}\n\n								$return[$file][] = array(\n									\'code\' => $dom->getElementsByTagName(\'code\')->item(0)->textContent,\n									\'name\' => $dom->getElementsByTagName(\'name\')->item(0)->textContent,\n									\'author\' => $author\n								);\n							}\n						}\n					}\n				}\n			}\n		}\n\n		return $return;\n	}\n\n]]></add>\n		</operation>\n	</file>\n</modification>', 1, '2020-05-20 04:19:15', '2020-05-20 04:19:15');
INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(20, 23, 'Расширенные наценки для опций', 'options-markups', 'https://prowebber.ru/', '2.0', 'https://prowebber.ru/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>Расширенные наценки для опций</name>\r\n  <code>options-markups</code>\r\n  <version>2.0</version>\r\n  <author>https://prowebber.ru/</author>\r\n  <link>https://prowebber.ru/</link>\r\n  \r\n  <file path=\"catalog/controller/product/product.php\">\r\n    <operation>\r\n      <search><![CDATA[$price = $this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false)]]></search>\r\n      <add position=\"replace\"><![CDATA[\r\n        if ($option_value[\'price_prefix\']==\"u\") {\r\n            $price = \'+\' . (float)$option_value[\'price\'].\'%\';\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"d\") {\r\n            $price = \'-\' . (float)$option_value[\'price\'].\'%\';\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"*\") {\r\n            $price = \'*\' . (float)$option_value[\'price\'];\r\n        }\r\n        elseif ($option_value[\'price_prefix\']==\"/\") {\r\n            $price = \'/\' . (float)$option_value[\'price\'];\r\n        }\r\n        else{\r\n            $price = $option_value[\'price_prefix\'].$this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false), $this->session->data[\'currency\']);\r\n        }\r\n        // $price = $this->currency->format($this->tax->calculate($option_value[\'price\'], $product_info[\'tax_class_id\'], $this->config->get(\'config_tax\') ? \'P\' : false)]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"catalog/view/theme/*/template/product/product.twig\">\r\n    <operation>\r\n      <search><![CDATA[{{ option_value.price_prefix }}]]></search>\r\n      <add position=\"replace\"><![CDATA[]]></add>\r\n    </operation>\r\n  </file>  \r\n  <file path=\"system/library/cart/cart.php\">\r\n    <operation>\r\n      <search><![CDATA[$option_price = 0;]]></search>\r\n      <add position=\"replace\"><![CDATA[$option_price = array();]]></add>\r\n    </operation>\r\n      <operation>\r\n        <search><![CDATA[if ($option_value_query->row[\'price_prefix\'] == \'+\') {]]></search>\r\n        <add position=\"replace\" offset=\"4\"><![CDATA[\r\n          if ($option_value_query->row[\'price_prefix\'] == \'=\') {\r\n              $sort_key=count($option_price) + 1;\r\n          } else {\r\n              $sort_key=count($option_price)+10;\r\n          }\r\n          \r\n          $option_price[$sort_key] = array(\r\n              $option_value_query->row[\'price_prefix\']=>$option_value_query->row[\'price\'],\r\n          );\r\n        ]]></add>\r\n      </operation>\r\n      <operation>\r\n        <search><![CDATA[if (!$product_query->row[\'quantity\']]]></search>\r\n        <add position=\"before\"><![CDATA[\r\n          $newprice = $price;\r\n 		  $defprice = $price;\r\n          \r\n          ksort($option_price);\r\n\r\n          foreach($option_price as $operations){\r\n              foreach($operations as $operation=>$value){\r\n                  if ($operation == \'=\') {\r\n\r\n                      if ($price!=0 ){\r\n                          $newprice = $value;\r\n                          $price=0;\r\n                      }\r\n                      else{\r\n                          $newprice += $value;\r\n                      }\r\n                      \r\n                  }\r\n                  elseif ($operation == \'+\') {\r\n                      $newprice += $value;\r\n                  }\r\n                  elseif ($operation == \'-\') {\r\n                      $newprice -= $value;\r\n                  }\r\n                  elseif ($operation == \'*\') {\r\n                      $newprice = $newprice * $value;\r\n                  }\r\n                  elseif ($operation == \'/\') {\r\n                      $newprice = $newprice / $value;\r\n                  }\r\n                  elseif ($operation == \'u\') {\r\n                      $newprice = $newprice + (( $defprice * $value ) / 100);\r\n                      }\r\n                  elseif ($operation == \'d\') {\r\n                      $newprice = $newprice - (( $defprice * $value ) / 100);\r\n                  }\r\n              }\r\n          }\r\n        ]]> \r\n      </add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[$price + $option_price]]></search>\r\n      <add replace=\"replace\"><![CDATA[$newprice]]></add>\r\n    </operation>\r\n  </file>\r\n  \r\n  <file path=\"admin/view/template/editors/product/product_form.twig\">\r\n    <operation>\r\n      <search><![CDATA[{% if product_option_value.price_prefix == \'+\' %}]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n        {% if product_option_value.price_prefix == \'=\' %} \r\n            <option value=\"=\" selected=\"selected\">=</option>\r\n        {% else %} \r\n            <option value=\"=\">=</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'*\' %} \r\n            <option value=\"*\" selected=\"selected\">*</option>\r\n        {% else %} \r\n            <option value=\"*\">*</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'/\' %} \r\n            <option value=\"/\" selected=\"selected\">/</option>\r\n        {% else %} \r\n            <option value=\"/\">/</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'u\' %} \r\n            <option value=\"u\" selected=\"selected\">+%</option>\r\n        {% else %} \r\n            <option value=\"u\">+%</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'d\' %} \r\n            <option value=\"d\" selected=\"selected\">-%</option>\r\n        {% else %} \r\n            <option value=\"d\">-%</option>\r\n        {% endif %}\r\n      ]]></add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[<select name=\"product_option[\' + option_row + \'][product_option_value][\' + option_value_row + \'][price_prefix]\"]]></search>\r\n      <add position=\"after\" offset=\"2\"><![CDATA[\r\n        html += \'      <option value=\"=\">=</option>\';\r\n        html += \'      <option value=\"*\">*</option>\';\r\n        html += \'      <option value=\"/\">/</option>\';\r\n        html += \'      <option value=\"u\">+%</option>\';\r\n        html += \'      <option value=\"d\">-%</option>\';\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path=\"admin/view/template/catalog/product_form.twig\">\r\n    <operation>\r\n      <search><![CDATA[{% if product_option_value.price_prefix == \'+\' %}]]></search>\r\n      <add position=\"before\"><![CDATA[\r\n        {% if product_option_value.price_prefix == \'=\' %} \r\n            <option value=\"=\" selected=\"selected\">=</option>\r\n        {% else %} \r\n            <option value=\"=\">=</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'*\' %} \r\n            <option value=\"*\" selected=\"selected\">*</option>\r\n        {% else %} \r\n            <option value=\"*\">*</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'/\' %} \r\n            <option value=\"/\" selected=\"selected\">/</option>\r\n        {% else %} \r\n            <option value=\"/\">/</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'u\' %} \r\n            <option value=\"u\" selected=\"selected\">+%</option>\r\n        {% else %} \r\n            <option value=\"u\">+%</option>\r\n        {% endif %} \r\n        {% if product_option_value.price_prefix == \'d\' %} \r\n            <option value=\"d\" selected=\"selected\">-%</option>\r\n        {% else %} \r\n            <option value=\"d\">-%</option>\r\n        {% endif %}\r\n      ]]></add>\r\n    </operation>\r\n    <operation>\r\n      <search><![CDATA[<select name=\"product_option[\' + option_row + \'][product_option_value][\' + option_value_row + \'][price_prefix]\"]]></search>\r\n      <add position=\"after\" offset=\"2\"><![CDATA[\r\n        html += \'      <option value=\"=\">=</option>\';\r\n        html += \'      <option value=\"*\">*</option>\';\r\n        html += \'      <option value=\"/\">/</option>\';\r\n        html += \'      <option value=\"u\">+%</option>\';\r\n        html += \'      <option value=\"d\">-%</option>\';\r\n      ]]></add>\r\n    </operation>\r\n  </file>\r\n  \r\n</modification>', 1, '2020-05-28 14:18:06', '0000-00-00 00:00:00'),
(21, 25, 'X-Shipping Pro', 'xshippingpro', 'OpenCartMart', '3.2.2', 'http://www.opencartmart.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n    <name>X-Shipping Pro</name>\n    <code>xshippingpro</code>\n    <version>3.2.2</version>\n    <author>OpenCartMart</author>\n    <link>http://www.opencartmart.com</link>\n    <file path=\"catalog/controller/extension/module/xtensions/checkout/xfooter.php\">\n        <operation error=\"skip\">\n            <search><![CDATA[ $this->load->model(\'catalog/information\'); ]]></search>\n            <add position=\"before\"><![CDATA[\n                  $ocm = ($ocm = $this->registry->get(\'ocm_front\')) ? $ocm : new OCM\\Front($this->registry);\n                  $data[\'_ocm_script\'] = $ocm->getScript();\n            ]]></add>\n        </operation>\n    </file>\n    <file path=\"catalog/view/theme/*/template/extension/module/xtensions/checkout/xfooter.twig\">\n      <operation error=\"skip\">\n        <search><![CDATA[ </body> ]]></search>\n        <add position=\"before\"><![CDATA[\n            {{ _ocm_script }}\n        ]]></add>\n      </operation>\n   </file>\n       <file path=\"catalog/controller/common/footer.php\">\n        <operation error=\"log\">\n            <ignoreif regex=\"true\"><![CDATA[ /ocm->getScript/ ]]></ignoreif>\n            <search index=\"0\"><![CDATA[ $this->load->model(\'catalog/information\'); ]]></search>\n            <add position=\"before\"><![CDATA[\n                  $ocm = ($ocm = $this->registry->get(\'ocm_front\')) ? $ocm : new OCM\\Front($this->registry);\n                  $data[\'_ocm_script\'] = $ocm->getScript();\n            ]]></add>\n        </operation>\n    </file>\n   <!--  d_twig taints OC 2.x tpl with twig  -->\n   <file path=\"catalog/view/theme/*/template/common/footer.twi*\">\n      <operation>\n        <ignoreif regex=\"true\"><![CDATA[ /_ocm_script/ ]]></ignoreif>\n        <search><![CDATA[ </body> ]]></search>\n        <add position=\"before\"><![CDATA[\n            {{_ocm_script}}\n        ]]></add>\n      </operation>\n   </file>\n</modification>', 1, '2020-07-22 12:05:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option`
--

CREATE TABLE `oc_ocfilter_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'checkbox',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `selectbox` tinyint(1) NOT NULL DEFAULT 0,
  `grouping` tinyint(2) NOT NULL DEFAULT 0,
  `color` tinyint(1) NOT NULL DEFAULT 0,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option`
--

INSERT INTO `oc_ocfilter_option` (`option_id`, `type`, `keyword`, `selectbox`, `grouping`, `color`, `image`, `status`, `sort_order`) VALUES
(5001, 'checkbox', 'strana-proishozhdenija', 0, 0, 0, 0, 1, 0),
(5002, 'checkbox', 'brend', 0, 0, 0, 0, 1, 0),
(5003, 'checkbox', 'tip-razmera', 0, 0, 0, 0, 1, 0),
(5004, 'checkbox', 'stil', 0, 0, 0, 0, 1, 0),
(5005, 'checkbox', 'pokroj-brjuk', 0, 0, 0, 0, 1, 0),
(5006, 'checkbox', 'sezonnost', 0, 0, 0, 0, 1, 0),
(5007, 'checkbox', 'razmer', 0, 0, 0, 0, 1, 0),
(5008, 'checkbox', 'model-kostjuma', 0, 0, 0, 0, 1, 0),
(5009, 'checkbox', 'tip-rukova-sorochka-', 0, 0, 0, 0, 1, 0),
(5010, 'checkbox', 'pokroj-sorochki', 0, 0, 0, 0, 1, 0),
(5011, 'checkbox', 'tip-uzora-sorochka-', 0, 0, 0, 0, 1, 0),
(5012, 'checkbox', 'sezonnost-kurtka-', 0, 0, 0, 0, 1, 0),
(5013, 'checkbox', 'tip-vorotnika-kurtka-', 0, 0, 0, 0, 1, 0),
(5014, 'checkbox', 'tip-vetrovki', 0, 0, 0, 0, 1, 0),
(5015, 'checkbox', 'vtip-meha-kurtka-', 0, 0, 0, 0, 1, 0),
(5016, 'checkbox', 'tsvet', 0, 0, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_description`
--

CREATE TABLE `oc_ocfilter_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `postfix` varchar(32) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_description`
--

INSERT INTO `oc_ocfilter_option_description` (`option_id`, `language_id`, `name`, `postfix`, `description`) VALUES
(5015, 1, 'ВТип меха (Куртка)', '', ''),
(5014, 1, 'Тип ветровки', '', ''),
(5012, 1, 'Сезонность (Куртка)', '', ''),
(5013, 1, 'Тип воротника (Куртка)', '', ''),
(5011, 1, 'Тип узора (Сорочка)', '', ''),
(5010, 1, 'Покрой сорочки', '', ''),
(5009, 1, 'Тип рукова (Сорочка)', '', ''),
(5008, 1, 'Модель костюма', '', ''),
(5007, 1, 'Размер', '', ''),
(5006, 1, 'Сезонность', '', ''),
(5005, 1, 'Покрой брюк', '', ''),
(5004, 1, 'Стиль', '', ''),
(5003, 1, 'Тип размера', '', ''),
(5002, 1, 'Бренд', '', ''),
(5001, 1, 'Страна происхождения', '', ''),
(5016, 1, 'Цвет', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_to_category`
--

CREATE TABLE `oc_ocfilter_option_to_category` (
  `option_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_to_category`
--

INSERT INTO `oc_ocfilter_option_to_category` (`option_id`, `category_id`) VALUES
(5001, 70),
(5002, 70),
(5003, 70),
(5004, 70),
(5007, 70),
(5008, 70),
(5016, 70),
(5001, 74),
(5002, 74),
(5003, 74),
(5004, 74),
(5006, 74),
(5007, 74),
(5016, 74),
(5001, 75),
(5002, 75),
(5003, 75),
(5004, 75),
(5005, 75),
(5006, 75),
(5007, 75),
(5016, 75),
(5001, 76),
(5002, 76),
(5003, 76),
(5004, 76),
(5005, 76),
(5006, 76),
(5007, 76),
(5016, 76),
(5001, 78),
(5002, 78),
(5003, 78),
(5004, 78),
(5007, 78),
(5016, 78),
(5001, 79),
(5002, 79),
(5003, 79),
(5004, 79),
(5007, 79),
(5016, 79),
(5001, 80),
(5002, 80),
(5003, 80),
(5004, 80),
(5007, 80),
(5016, 80),
(5001, 81),
(5002, 81),
(5003, 81),
(5004, 81),
(5007, 81),
(5016, 81),
(5001, 82),
(5002, 82),
(5003, 82),
(5004, 82),
(5007, 82),
(5016, 82),
(5001, 83),
(5002, 83),
(5003, 83),
(5004, 83),
(5007, 83),
(5016, 83),
(5001, 84),
(5002, 84),
(5003, 84),
(5004, 84),
(5007, 84),
(5016, 84),
(5001, 85),
(5002, 85),
(5003, 85),
(5004, 85),
(5007, 85),
(5016, 85),
(5001, 90),
(5002, 90),
(5003, 90),
(5004, 90),
(5007, 90),
(5009, 90),
(5010, 90),
(5011, 90),
(5016, 90),
(5001, 91),
(5002, 91),
(5003, 91),
(5004, 91),
(5007, 91),
(5009, 91),
(5010, 91),
(5011, 91),
(5016, 91),
(5001, 92),
(5002, 92),
(5003, 92),
(5004, 92),
(5007, 92),
(5009, 92),
(5010, 92),
(5011, 92),
(5016, 92),
(5001, 103),
(5002, 103),
(5003, 103),
(5004, 103),
(5007, 103),
(5012, 103),
(5013, 103),
(5014, 103),
(5015, 103),
(5016, 103),
(5001, 104),
(5002, 104),
(5003, 104),
(5004, 104),
(5007, 104),
(5012, 104),
(5013, 104),
(5016, 104),
(5001, 105),
(5002, 105),
(5003, 105),
(5004, 105),
(5007, 105),
(5014, 105),
(5016, 105),
(5001, 106),
(5002, 106),
(5003, 106),
(5004, 106),
(5007, 106),
(5012, 106),
(5013, 106),
(5015, 106),
(5016, 106),
(5001, 107),
(5002, 107),
(5003, 107),
(5004, 107),
(5007, 107),
(5016, 107),
(5001, 109),
(5002, 109),
(5003, 109),
(5004, 109),
(5006, 109),
(5007, 109),
(5016, 109);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_to_store`
--

CREATE TABLE `oc_ocfilter_option_to_store` (
  `option_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_to_store`
--

INSERT INTO `oc_ocfilter_option_to_store` (`option_id`, `store_id`) VALUES
(5001, 0),
(5002, 0),
(5003, 0),
(5004, 0),
(5005, 0),
(5006, 0),
(5007, 0),
(5008, 0),
(5009, 0),
(5010, 0),
(5011, 0),
(5012, 0),
(5013, 0),
(5014, 0),
(5015, 0),
(5016, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value`
--

CREATE TABLE `oc_ocfilter_option_value` (
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `color` varchar(6) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_value`
--

INSERT INTO `oc_ocfilter_option_value` (`value_id`, `option_id`, `keyword`, `color`, `image`, `sort_order`) VALUES
(10139, 5016, 'oranzhevyj', 'f36012', '', 0),
(10138, 5016, 'belyj', 'f2f2f7', '', 0),
(10137, 5016, 'sirenevyj', '8255b9', '', 0),
(10136, 5016, 'chernyj', '1c1c1e', '', 0),
(10135, 5016, 'sinij', '0080ff', '', 0),
(10134, 5016, 'seryj', 'f2f2f7', '', 0),
(10133, 5016, 'temno-sinij', '105293', '', 0),
(10132, 5016, 'zelenyj', '34c759', '', 0),
(10131, 5016, 'korichnevyj', '5f0c0c', '', 0),
(10130, 5015, 'krolik', '', '', 0),
(10129, 5015, 'volk', '', '', 0),
(10128, 5015, 'enot', '', '', 0),
(10127, 5015, 'muton', '', '', 0),
(10126, 5015, 'iskusstvennyj', '', '', 0),
(10125, 5014, 'bomber', '', '', 0),
(10124, 5014, 'odnostoronnjaja', '', '', 0),
(10123, 5013, 'kruglyj-vorotnik', '', '', 0),
(10122, 5013, 'polnyj-meh', '', '', 0),
(10121, 5013, 'anglijskij-vorotnik', '', '', 0),
(10120, 5013, 'bez-meha', '', '', 0),
(10119, 5012, 'zimnjaja', '', '', 0),
(10118, 5012, 'vesennjaja', '', '', 0),
(10117, 5011, 'odnotonnaja', '', '', 0),
(10116, 5011, 'fakturnaja', '', '', 0),
(10115, 5010, 'slimlong-udlinennaja', '', '', 0),
(10114, 5010, 'ultrafit-pritalennaja', '', '', 0),
(10113, 5010, 'classic-prjamaja', '', '', 0),
(10112, 5010, 'slimfit-polupritalennaja', '', '', 0),
(10111, 5009, 'korotkij-rukav', '', '', 0),
(10110, 5009, 'dlinnyj-rukav', '', '', 0),
(10109, 5008, 'dvojka', '', '', 0),
(10108, 5008, 'trojka', '', '', 0),
(10107, 5007, '6xl', '', '', 0),
(10106, 5007, '66', '', '', 0),
(10105, 5007, '64', '', '', 0),
(10104, 5007, '37', '', '', 0),
(10103, 5007, '42', '', '', 0),
(10102, 5007, '40', '', '', 0),
(10101, 5007, '35', '', '', 0),
(10100, 5007, '48', '', '', 0),
(10099, 5007, '46', '', '', 0),
(10098, 5007, '4xl', '', '', 0),
(10097, 5007, '5xl', '', '', 0),
(10096, 5007, 'm', '', '', 0),
(10095, 5007, '28', '', '', 0),
(10094, 5007, '29', '', '', 0),
(10093, 5007, '62', '', '', 0),
(10092, 5007, '60', '', '', 0),
(10091, 5007, 'xs', '', '', 0),
(10090, 5007, '3xl', '', '', 0),
(10089, 5007, '2xl', '', '', 0),
(10088, 5007, 'xl', '', '', 0),
(10087, 5007, 's', '', '', 0),
(10086, 5007, 'xxl', '', '', 0),
(10085, 5007, 'l', '', '', 0),
(10084, 5007, 'm', '', '', 0),
(10083, 5007, '58', '', '', 0),
(10082, 5007, '56', '', '', 0),
(10081, 5007, '54', '', '', 0),
(10080, 5007, '44', '', '', 0),
(10079, 5007, '52', '', '', 0),
(10078, 5007, '50', '', '', 0),
(10077, 5007, '36', '', '', 0),
(10076, 5007, '33', '', '', 0),
(10075, 5007, '32', '', '', 0),
(10074, 5007, '31', '', '', 0),
(10073, 5007, '30', '', '', 0),
(10072, 5007, '34', '', '', 0),
(10071, 5007, '38', '', '', 0),
(10070, 5006, 'demisezonnyj', '', '', 0),
(10069, 5006, 'bez-flisa', '', '', 0),
(10068, 5005, 'prjamye', '', '', 0),
(10067, 5005, 'zauzhennye', '', '', 0),
(10066, 5004, 'klassicheskaja', '', '', 0),
(10065, 5004, 'povsednevnaja', '', '', 0),
(10064, 5003, 'big', '', '', 0),
(10063, 5003, 'standart', '', '', 0),
(10062, 5002, 'giotelli', '', '', 0),
(10061, 5002, 'zilli', '', '', 0),
(10060, 5002, 'caporico', '', '', 0),
(10059, 5002, 'tello', '', '', 0),
(10058, 5002, 'diorise', '', '', 0),
(10057, 5002, 'monar', '', '', 0),
(10056, 5002, 'enzo', '', '', 0),
(10055, 5002, 'ausini', '', '', 0),
(10054, 5002, 'cf', '', '', 0),
(10053, 5002, 'grujinne', '', '', 0),
(10052, 5002, 'snowimage', '', '', 0),
(10051, 5002, 'vo-tarun', '', '', 0),
(10050, 5002, 'harry-bertoia', '', '', 0),
(10049, 5002, 'men-sezon', '', '', 0),
(10048, 5002, 'vivacana', '', '', 0),
(10047, 5002, 'zaka', '', '', 0),
(10046, 5002, 'shark-force', '', '', 0),
(10045, 5002, 'clasna', '', '', 0),
(10044, 5002, 'viktorio', '', '', 0),
(10043, 5002, 'talifeck', '', '', 0),
(10042, 5002, 'city-class', '', '', 0),
(10041, 5002, 'mxn', '', '', 0),
(10040, 5002, 'dcs', '', '', 0),
(10039, 5002, 'marcostar', '', '', 0),
(10038, 5002, 'giotelli', '', '', 0),
(10037, 5002, 'ers', '', '', 0),
(10036, 5002, 'turhan', '', '', 0),
(10035, 5002, 'jesse-james', '', '', 0),
(10034, 5002, 'tony-michael', '', '', 0),
(10033, 5002, 'mcr', '', '', 0),
(10032, 5002, 'lagos', '', '', 0),
(10031, 5002, 'lcr', '', '', 0),
(10030, 5002, 'pacifico', '', '', 0),
(10029, 5002, 'off', '', '', 0),
(10028, 5002, 'mcl', '', '', 0),
(10027, 5002, 'rake', '', '', 0),
(10026, 5002, 'signore', '', '', 0),
(10025, 5002, 'vinci', '', '', 0),
(10024, 5002, 'manzini', '', '', 0),
(10023, 5002, 'elita', '', '', 0),
(10022, 5002, 'vester', '', '', 0),
(10021, 5002, 'mario-maretti', '', '', 0),
(10020, 5002, 'fitmens', '', '', 0),
(10019, 5002, 'ncs', '', '', 0),
(10018, 5002, 'maschile', '', '', 0),
(10017, 5002, 'dergi', '', '', 0),
(10016, 5002, 'weaver', '', '', 0),
(10015, 5002, 'taft', '', '', 0),
(10014, 5002, 'amato', '', '', 0),
(10013, 5002, 'black-stone', '', '', 0),
(10012, 5002, 'plus90', '', '', 0),
(10011, 5002, 'boavitti', '', '', 0),
(10010, 5002, 'baurotti', '', '', 0),
(10009, 5002, 'semco', '', '', 0),
(10008, 5002, 'tony-cassano', '', '', 0),
(10007, 5002, 'antoni-zeeman', '', '', 0),
(10006, 5002, 'prodigy', '', '', 0),
(10005, 5002, '', '', '', 0),
(10004, 5002, 'cordial', '', '', 0),
(10003, 5001, 'knr', '', '', 0),
(10002, 5001, 'rossija', '', '', 0),
(10001, 5001, 'turtsija', '', '', 0),
(10140, 5016, 'temno-zelenyj', '186d2d', '', 0),
(10141, 5016, 'goluboj', '5ac8fa', '', 0),
(10142, 5016, 'bordovyj', '993d54', '', 0),
(10143, 5016, 'bezhevyj', 'e3e1c0', '', 0),
(10144, 5016, 'krasnyj', 'ff3b30', '', 0),
(10145, 5016, 'fioletovyj', 'af52de', '', 0),
(10146, 5016, 'temno-seryj', '525256', '', 0),
(10147, 5016, 'temno-fioletovyj', '6a3285', '', 0),
(10148, 5016, 'rozovyj', 'f6609c', '', 0),
(10149, 5016, 'zheltyj', 'ffcc00', '', 0),
(10150, 5016, 'persikovyj', 'f2d28c', '', 0),
(10151, 5016, 'temno-korichnevyj', '664705', '', 0),
(10152, 5016, 'haki', '446a16', '', 0),
(10153, 5016, 'sinij-seryj', '', '', 0),
(10154, 5016, 'birjuzovyj', '16adf3', '', 0),
(10155, 5016, 'antratsit', '6a5973', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_description`
--

CREATE TABLE `oc_ocfilter_option_value_description` (
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_value_description`
--

INSERT INTO `oc_ocfilter_option_value_description` (`value_id`, `option_id`, `language_id`, `name`) VALUES
(10134, 5016, 1, 'Серый'),
(10133, 5016, 1, 'Темно-синий'),
(10132, 5016, 1, 'Зеленый'),
(10131, 5016, 1, 'Коричневый'),
(10130, 5015, 1, 'Кролик'),
(10129, 5015, 1, 'Волк'),
(10127, 5015, 1, 'Мутон'),
(10128, 5015, 1, 'Енот'),
(10126, 5015, 1, 'Искусственный'),
(10125, 5014, 1, 'Бомбер'),
(10124, 5014, 1, 'Односторонняя'),
(10123, 5013, 1, 'Круглый воротник'),
(10122, 5013, 1, 'Полный мех'),
(10121, 5013, 1, 'Английский воротник'),
(10120, 5013, 1, 'Без меха'),
(10118, 5012, 1, 'Весенняя'),
(10119, 5012, 1, 'Зимняя'),
(10117, 5011, 1, 'Однотонная'),
(10115, 5010, 1, 'SlimLONG - Удлиненная'),
(10116, 5011, 1, 'Фактурная'),
(10114, 5010, 1, 'UltraFit - Приталенная'),
(10113, 5010, 1, 'Classic - Прямая'),
(10112, 5010, 1, 'SlimFit - Полуприталенная'),
(10111, 5009, 1, 'Короткий рукав'),
(10110, 5009, 1, 'Длинный рукав'),
(10109, 5008, 1, 'Двойка'),
(10108, 5008, 1, 'Тройка'),
(10107, 5007, 1, '6XL'),
(10106, 5007, 1, '66'),
(10105, 5007, 1, '64'),
(10104, 5007, 1, '37'),
(10103, 5007, 1, '42'),
(10102, 5007, 1, '40'),
(10101, 5007, 1, '35'),
(10100, 5007, 1, '48'),
(10099, 5007, 1, '46'),
(10098, 5007, 1, '4XL'),
(10096, 5007, 1, 'M'),
(10097, 5007, 1, '5XL'),
(10095, 5007, 1, '28'),
(10094, 5007, 1, '29'),
(10093, 5007, 1, '62'),
(10092, 5007, 1, '60'),
(10091, 5007, 1, 'XS'),
(10090, 5007, 1, '3XL'),
(10089, 5007, 1, '2XL'),
(10088, 5007, 1, 'XL'),
(10087, 5007, 1, 'S'),
(10086, 5007, 1, 'XXL'),
(10085, 5007, 1, 'L'),
(10084, 5007, 1, 'М'),
(10083, 5007, 1, '58'),
(10082, 5007, 1, '56'),
(10081, 5007, 1, '54'),
(10080, 5007, 1, '44'),
(10079, 5007, 1, '52'),
(10078, 5007, 1, '50'),
(10077, 5007, 1, '36'),
(10076, 5007, 1, '33'),
(10074, 5007, 1, '31'),
(10075, 5007, 1, '32'),
(10072, 5007, 1, '34'),
(10073, 5007, 1, '30'),
(10071, 5007, 1, '38'),
(10070, 5006, 1, 'Демисезонный'),
(10069, 5006, 1, 'Без Флиса'),
(10068, 5005, 1, 'Прямые'),
(10067, 5005, 1, 'Зауженные'),
(10066, 5004, 1, 'Классическая'),
(10065, 5004, 1, 'Повседневная'),
(10064, 5003, 1, 'Big'),
(10062, 5002, 1, 'Giotelli'),
(10063, 5003, 1, 'Standart'),
(10061, 5002, 1, 'Zilli'),
(10060, 5002, 1, 'Caporico'),
(10059, 5002, 1, 'Tello'),
(10058, 5002, 1, 'Diorise'),
(10057, 5002, 1, 'MONAR'),
(10056, 5002, 1, 'ENZO'),
(10055, 5002, 1, 'AUSINI'),
(10054, 5002, 1, 'CF'),
(10053, 5002, 1, 'GRUJINNE'),
(10052, 5002, 1, 'Snowimage'),
(10051, 5002, 1, 'VO-TARUN'),
(10050, 5002, 1, 'HARRY BERTOIA'),
(10049, 5002, 1, 'MEN SEZON'),
(10048, 5002, 1, 'VIVACANA'),
(10047, 5002, 1, 'ZAKA'),
(10046, 5002, 1, 'SHARK FORCE'),
(10045, 5002, 1, 'CLASNA'),
(10044, 5002, 1, 'Viktorio'),
(10043, 5002, 1, 'Talifeck'),
(10042, 5002, 1, 'City class'),
(10041, 5002, 1, 'MXN'),
(10040, 5002, 1, 'DCS'),
(10039, 5002, 1, 'Marcostar'),
(10038, 5002, 1, 'GIOTELLI'),
(10037, 5002, 1, 'ERS'),
(10036, 5002, 1, 'TURHAN'),
(10035, 5002, 1, 'Jesse James'),
(10034, 5002, 1, 'TONY MICHAEL'),
(10033, 5002, 1, 'MCR'),
(10032, 5002, 1, 'LAGOS'),
(10031, 5002, 1, 'LCR'),
(10030, 5002, 1, 'Pacifico'),
(10029, 5002, 1, 'OFF'),
(10028, 5002, 1, 'MCL'),
(10027, 5002, 1, 'RAKE'),
(10026, 5002, 1, 'SIGNORE'),
(10025, 5002, 1, 'VINCI'),
(10024, 5002, 1, 'MANZINI'),
(10023, 5002, 1, 'ELITA'),
(10022, 5002, 1, 'Vester'),
(10021, 5002, 1, 'Mario Maretti'),
(10020, 5002, 1, 'FITMENS'),
(10019, 5002, 1, 'NCS'),
(10018, 5002, 1, 'Maschile'),
(10017, 5002, 1, 'Dergi'),
(10016, 5002, 1, 'Weaver'),
(10015, 5002, 1, 'Taft'),
(10014, 5002, 1, 'Amato'),
(10013, 5002, 1, 'Black Stone'),
(10012, 5002, 1, '+90'),
(10011, 5002, 1, 'BOAVITTI'),
(10010, 5002, 1, 'BAUROTTI'),
(10009, 5002, 1, 'Semco'),
(10008, 5002, 1, 'TONY CASSANO'),
(10007, 5002, 1, 'ANTONI ZEEMAN'),
(10006, 5002, 1, 'Prodigy'),
(10005, 5002, 1, ''),
(10004, 5002, 1, 'CORDIAL'),
(10003, 5001, 1, 'КНР'),
(10002, 5001, 1, 'Россия'),
(10001, 5001, 1, 'Турция'),
(10135, 5016, 1, 'Синий'),
(10136, 5016, 1, 'Черный'),
(10137, 5016, 1, 'Сиреневый'),
(10138, 5016, 1, 'Белый'),
(10139, 5016, 1, 'Оранжевый'),
(10140, 5016, 1, 'Темно-зеленый'),
(10141, 5016, 1, 'Голубой'),
(10142, 5016, 1, 'Бордовый'),
(10143, 5016, 1, 'Бежевый'),
(10144, 5016, 1, 'Красный'),
(10145, 5016, 1, 'Фиолетовый'),
(10146, 5016, 1, 'Темно-серый'),
(10147, 5016, 1, 'Темно-фиолетовый'),
(10148, 5016, 1, 'Розовый'),
(10149, 5016, 1, 'Желтый'),
(10150, 5016, 1, 'Персиковый'),
(10151, 5016, 1, 'Темно-коричневый'),
(10152, 5016, 1, 'Хаки'),
(10153, 5016, 1, 'Синий-Серый'),
(10154, 5016, 1, 'Бирюзовый'),
(10155, 5016, 1, 'Антрацит');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_to_product`
--

CREATE TABLE `oc_ocfilter_option_value_to_product` (
  `ocfilter_option_value_to_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `slide_value_min` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `slide_value_max` decimal(15,4) NOT NULL DEFAULT 0.0000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ocfilter_option_value_to_product`
--

INSERT INTO `oc_ocfilter_option_value_to_product` (`ocfilter_option_value_to_product_id`, `product_id`, `option_id`, `value_id`, `slide_value_min`, `slide_value_max`) VALUES
(1, 50, 5001, 10001, '0.0000', '0.0000'),
(2, 50, 5002, 10004, '0.0000', '0.0000'),
(3, 50, 5002, 10005, '0.0000', '0.0000'),
(4, 50, 5003, 10063, '0.0000', '0.0000'),
(5, 50, 5004, 10065, '0.0000', '0.0000'),
(6, 50, 5005, 10067, '0.0000', '0.0000'),
(7, 50, 5006, 10069, '0.0000', '0.0000'),
(8, 50, 5007, 10071, '0.0000', '0.0000'),
(9, 50, 5007, 10072, '0.0000', '0.0000'),
(10, 50, 5007, 10073, '0.0000', '0.0000'),
(11, 50, 5007, 10074, '0.0000', '0.0000'),
(12, 50, 5007, 10075, '0.0000', '0.0000'),
(13, 50, 5007, 10076, '0.0000', '0.0000'),
(14, 50, 5007, 10077, '0.0000', '0.0000'),
(15, 50, 5016, 10131, '0.0000', '0.0000'),
(16, 50, 5016, 10132, '0.0000', '0.0000'),
(17, 50, 5016, 10133, '0.0000', '0.0000'),
(18, 51, 5001, 10001, '0.0000', '0.0000'),
(19, 51, 5002, 10005, '0.0000', '0.0000'),
(20, 51, 5002, 10006, '0.0000', '0.0000'),
(21, 51, 5003, 10063, '0.0000', '0.0000'),
(22, 51, 5004, 10065, '0.0000', '0.0000'),
(23, 51, 5005, 10067, '0.0000', '0.0000'),
(24, 51, 5006, 10069, '0.0000', '0.0000'),
(25, 51, 5007, 10073, '0.0000', '0.0000'),
(26, 51, 5007, 10075, '0.0000', '0.0000'),
(27, 51, 5016, 10136, '0.0000', '0.0000'),
(28, 52, 5001, 10001, '0.0000', '0.0000'),
(29, 52, 5002, 10005, '0.0000', '0.0000'),
(30, 52, 5002, 10008, '0.0000', '0.0000'),
(31, 52, 5003, 10063, '0.0000', '0.0000'),
(32, 52, 5004, 10065, '0.0000', '0.0000'),
(33, 52, 5007, 10079, '0.0000', '0.0000'),
(34, 52, 5007, 10080, '0.0000', '0.0000'),
(35, 52, 5007, 10081, '0.0000', '0.0000'),
(36, 52, 5007, 10082, '0.0000', '0.0000'),
(37, 52, 5007, 10083, '0.0000', '0.0000'),
(38, 52, 5016, 10135, '0.0000', '0.0000'),
(39, 52, 5016, 10136, '0.0000', '0.0000'),
(40, 53, 5001, 10001, '0.0000', '0.0000'),
(41, 53, 5002, 10005, '0.0000', '0.0000'),
(42, 53, 5002, 10009, '0.0000', '0.0000'),
(43, 53, 5003, 10063, '0.0000', '0.0000'),
(44, 53, 5004, 10065, '0.0000', '0.0000'),
(45, 53, 5007, 10078, '0.0000', '0.0000'),
(46, 53, 5007, 10082, '0.0000', '0.0000'),
(47, 53, 5016, 10135, '0.0000', '0.0000'),
(48, 54, 5001, 10001, '0.0000', '0.0000'),
(49, 54, 5002, 10005, '0.0000', '0.0000'),
(50, 54, 5002, 10010, '0.0000', '0.0000'),
(51, 54, 5003, 10063, '0.0000', '0.0000'),
(52, 54, 5004, 10066, '0.0000', '0.0000'),
(53, 54, 5007, 10084, '0.0000', '0.0000'),
(54, 54, 5007, 10085, '0.0000', '0.0000'),
(55, 54, 5009, 10110, '0.0000', '0.0000'),
(56, 54, 5010, 10112, '0.0000', '0.0000'),
(57, 54, 5011, 10116, '0.0000', '0.0000'),
(58, 54, 5016, 10138, '0.0000', '0.0000'),
(59, 55, 5001, 10001, '0.0000', '0.0000'),
(60, 55, 5002, 10005, '0.0000', '0.0000'),
(61, 55, 5002, 10010, '0.0000', '0.0000'),
(62, 55, 5003, 10063, '0.0000', '0.0000'),
(63, 55, 5004, 10066, '0.0000', '0.0000'),
(64, 55, 5007, 10084, '0.0000', '0.0000'),
(65, 55, 5007, 10085, '0.0000', '0.0000'),
(66, 55, 5007, 10086, '0.0000', '0.0000'),
(67, 55, 5007, 10087, '0.0000', '0.0000'),
(68, 55, 5009, 10110, '0.0000', '0.0000'),
(69, 55, 5010, 10112, '0.0000', '0.0000'),
(70, 55, 5011, 10116, '0.0000', '0.0000'),
(71, 55, 5016, 10133, '0.0000', '0.0000'),
(72, 55, 5016, 10136, '0.0000', '0.0000'),
(73, 56, 5001, 10001, '0.0000', '0.0000'),
(74, 56, 5002, 10005, '0.0000', '0.0000'),
(75, 56, 5002, 10010, '0.0000', '0.0000'),
(76, 56, 5003, 10063, '0.0000', '0.0000'),
(77, 56, 5004, 10066, '0.0000', '0.0000'),
(78, 56, 5007, 10084, '0.0000', '0.0000'),
(79, 56, 5007, 10085, '0.0000', '0.0000'),
(80, 56, 5007, 10086, '0.0000', '0.0000'),
(81, 56, 5007, 10087, '0.0000', '0.0000'),
(82, 56, 5007, 10088, '0.0000', '0.0000'),
(83, 56, 5009, 10110, '0.0000', '0.0000'),
(84, 56, 5010, 10112, '0.0000', '0.0000'),
(85, 56, 5011, 10117, '0.0000', '0.0000'),
(86, 56, 5016, 10139, '0.0000', '0.0000'),
(87, 57, 5001, 10001, '0.0000', '0.0000'),
(88, 57, 5002, 10005, '0.0000', '0.0000'),
(89, 57, 5002, 10010, '0.0000', '0.0000'),
(90, 57, 5003, 10063, '0.0000', '0.0000'),
(91, 57, 5004, 10066, '0.0000', '0.0000'),
(92, 57, 5007, 10084, '0.0000', '0.0000'),
(93, 57, 5007, 10085, '0.0000', '0.0000'),
(94, 57, 5007, 10086, '0.0000', '0.0000'),
(95, 57, 5007, 10087, '0.0000', '0.0000'),
(96, 57, 5007, 10088, '0.0000', '0.0000'),
(97, 57, 5009, 10110, '0.0000', '0.0000'),
(98, 57, 5010, 10112, '0.0000', '0.0000'),
(99, 57, 5011, 10117, '0.0000', '0.0000'),
(100, 57, 5016, 10136, '0.0000', '0.0000'),
(101, 58, 5001, 10001, '0.0000', '0.0000'),
(102, 58, 5002, 10005, '0.0000', '0.0000'),
(103, 58, 5002, 10011, '0.0000', '0.0000'),
(104, 58, 5003, 10063, '0.0000', '0.0000'),
(105, 58, 5004, 10066, '0.0000', '0.0000'),
(106, 58, 5007, 10084, '0.0000', '0.0000'),
(107, 58, 5007, 10088, '0.0000', '0.0000'),
(108, 58, 5009, 10110, '0.0000', '0.0000'),
(109, 58, 5010, 10112, '0.0000', '0.0000'),
(110, 58, 5011, 10116, '0.0000', '0.0000'),
(111, 58, 5016, 10140, '0.0000', '0.0000'),
(112, 59, 5001, 10001, '0.0000', '0.0000'),
(113, 59, 5002, 10005, '0.0000', '0.0000'),
(114, 59, 5002, 10012, '0.0000', '0.0000'),
(115, 59, 5003, 10063, '0.0000', '0.0000'),
(116, 59, 5004, 10065, '0.0000', '0.0000'),
(117, 59, 5007, 10084, '0.0000', '0.0000'),
(118, 59, 5007, 10085, '0.0000', '0.0000'),
(119, 59, 5007, 10086, '0.0000', '0.0000'),
(120, 59, 5007, 10087, '0.0000', '0.0000'),
(121, 59, 5007, 10088, '0.0000', '0.0000'),
(122, 59, 5007, 10089, '0.0000', '0.0000'),
(123, 59, 5009, 10110, '0.0000', '0.0000'),
(124, 59, 5010, 10112, '0.0000', '0.0000'),
(125, 59, 5011, 10117, '0.0000', '0.0000'),
(126, 59, 5016, 10133, '0.0000', '0.0000'),
(127, 59, 5016, 10136, '0.0000', '0.0000'),
(128, 59, 5016, 10138, '0.0000', '0.0000'),
(129, 59, 5016, 10141, '0.0000', '0.0000'),
(130, 59, 5016, 10142, '0.0000', '0.0000'),
(131, 60, 5001, 10001, '0.0000', '0.0000'),
(132, 60, 5002, 10005, '0.0000', '0.0000'),
(133, 60, 5002, 10013, '0.0000', '0.0000'),
(134, 60, 5003, 10063, '0.0000', '0.0000'),
(135, 60, 5004, 10065, '0.0000', '0.0000'),
(136, 60, 5007, 10084, '0.0000', '0.0000'),
(137, 60, 5007, 10085, '0.0000', '0.0000'),
(138, 60, 5007, 10086, '0.0000', '0.0000'),
(139, 60, 5007, 10087, '0.0000', '0.0000'),
(140, 60, 5007, 10088, '0.0000', '0.0000'),
(141, 60, 5009, 10111, '0.0000', '0.0000'),
(142, 60, 5010, 10112, '0.0000', '0.0000'),
(143, 60, 5011, 10117, '0.0000', '0.0000'),
(144, 60, 5016, 10133, '0.0000', '0.0000'),
(145, 60, 5016, 10138, '0.0000', '0.0000'),
(146, 61, 5001, 10001, '0.0000', '0.0000'),
(147, 61, 5002, 10005, '0.0000', '0.0000'),
(148, 61, 5002, 10013, '0.0000', '0.0000'),
(149, 61, 5003, 10063, '0.0000', '0.0000'),
(150, 61, 5004, 10065, '0.0000', '0.0000'),
(151, 61, 5007, 10084, '0.0000', '0.0000'),
(152, 61, 5007, 10085, '0.0000', '0.0000'),
(153, 61, 5007, 10086, '0.0000', '0.0000'),
(154, 61, 5007, 10088, '0.0000', '0.0000'),
(155, 61, 5009, 10111, '0.0000', '0.0000'),
(156, 61, 5010, 10112, '0.0000', '0.0000'),
(157, 61, 5011, 10116, '0.0000', '0.0000'),
(158, 61, 5016, 10133, '0.0000', '0.0000'),
(159, 61, 5016, 10143, '0.0000', '0.0000'),
(160, 62, 5001, 10001, '0.0000', '0.0000'),
(161, 62, 5002, 10005, '0.0000', '0.0000'),
(162, 62, 5002, 10012, '0.0000', '0.0000'),
(163, 62, 5003, 10063, '0.0000', '0.0000'),
(164, 62, 5004, 10065, '0.0000', '0.0000'),
(165, 62, 5007, 10084, '0.0000', '0.0000'),
(166, 62, 5007, 10085, '0.0000', '0.0000'),
(167, 62, 5009, 10110, '0.0000', '0.0000'),
(168, 62, 5010, 10112, '0.0000', '0.0000'),
(169, 62, 5011, 10116, '0.0000', '0.0000'),
(170, 62, 5016, 10142, '0.0000', '0.0000'),
(171, 63, 5001, 10001, '0.0000', '0.0000'),
(172, 63, 5002, 10012, '0.0000', '0.0000'),
(173, 63, 5003, 10063, '0.0000', '0.0000'),
(174, 63, 5004, 10065, '0.0000', '0.0000'),
(175, 63, 5007, 10084, '0.0000', '0.0000'),
(176, 63, 5007, 10085, '0.0000', '0.0000'),
(177, 63, 5007, 10088, '0.0000', '0.0000'),
(178, 63, 5009, 10110, '0.0000', '0.0000'),
(179, 63, 5010, 10112, '0.0000', '0.0000'),
(180, 63, 5011, 10117, '0.0000', '0.0000'),
(181, 63, 5016, 10133, '0.0000', '0.0000'),
(182, 64, 5001, 10001, '0.0000', '0.0000'),
(183, 64, 5002, 10005, '0.0000', '0.0000'),
(184, 64, 5002, 10012, '0.0000', '0.0000'),
(185, 64, 5003, 10063, '0.0000', '0.0000'),
(186, 64, 5004, 10065, '0.0000', '0.0000'),
(187, 64, 5007, 10085, '0.0000', '0.0000'),
(188, 64, 5007, 10087, '0.0000', '0.0000'),
(189, 64, 5007, 10088, '0.0000', '0.0000'),
(190, 64, 5007, 10089, '0.0000', '0.0000'),
(191, 64, 5009, 10110, '0.0000', '0.0000'),
(192, 64, 5010, 10112, '0.0000', '0.0000'),
(193, 64, 5011, 10116, '0.0000', '0.0000'),
(194, 64, 5016, 10133, '0.0000', '0.0000'),
(195, 65, 5001, 10001, '0.0000', '0.0000'),
(196, 65, 5002, 10005, '0.0000', '0.0000'),
(197, 65, 5002, 10014, '0.0000', '0.0000'),
(198, 65, 5003, 10063, '0.0000', '0.0000'),
(199, 65, 5004, 10065, '0.0000', '0.0000'),
(200, 65, 5007, 10085, '0.0000', '0.0000'),
(201, 65, 5007, 10088, '0.0000', '0.0000'),
(202, 65, 5009, 10110, '0.0000', '0.0000'),
(203, 65, 5010, 10112, '0.0000', '0.0000'),
(204, 65, 5011, 10116, '0.0000', '0.0000'),
(205, 65, 5016, 10133, '0.0000', '0.0000'),
(206, 66, 5001, 10001, '0.0000', '0.0000'),
(207, 66, 5002, 10005, '0.0000', '0.0000'),
(208, 66, 5002, 10014, '0.0000', '0.0000'),
(209, 66, 5003, 10063, '0.0000', '0.0000'),
(210, 66, 5004, 10065, '0.0000', '0.0000'),
(211, 66, 5007, 10084, '0.0000', '0.0000'),
(212, 66, 5007, 10085, '0.0000', '0.0000'),
(213, 66, 5007, 10086, '0.0000', '0.0000'),
(214, 66, 5007, 10088, '0.0000', '0.0000'),
(215, 66, 5007, 10089, '0.0000', '0.0000'),
(216, 66, 5009, 10110, '0.0000', '0.0000'),
(217, 66, 5010, 10112, '0.0000', '0.0000'),
(218, 66, 5011, 10116, '0.0000', '0.0000'),
(219, 66, 5016, 10134, '0.0000', '0.0000'),
(220, 66, 5016, 10135, '0.0000', '0.0000'),
(221, 66, 5016, 10142, '0.0000', '0.0000'),
(222, 67, 5001, 10001, '0.0000', '0.0000'),
(223, 67, 5002, 10005, '0.0000', '0.0000'),
(224, 67, 5002, 10014, '0.0000', '0.0000'),
(225, 67, 5003, 10063, '0.0000', '0.0000'),
(226, 67, 5004, 10065, '0.0000', '0.0000'),
(227, 67, 5007, 10085, '0.0000', '0.0000'),
(228, 67, 5007, 10088, '0.0000', '0.0000'),
(229, 67, 5009, 10110, '0.0000', '0.0000'),
(230, 67, 5010, 10112, '0.0000', '0.0000'),
(231, 67, 5011, 10116, '0.0000', '0.0000'),
(232, 67, 5016, 10135, '0.0000', '0.0000'),
(233, 68, 5001, 10001, '0.0000', '0.0000'),
(234, 68, 5002, 10014, '0.0000', '0.0000'),
(235, 68, 5003, 10063, '0.0000', '0.0000'),
(236, 68, 5004, 10065, '0.0000', '0.0000'),
(237, 68, 5007, 10085, '0.0000', '0.0000'),
(238, 68, 5007, 10086, '0.0000', '0.0000'),
(239, 68, 5007, 10088, '0.0000', '0.0000'),
(240, 68, 5009, 10110, '0.0000', '0.0000'),
(241, 68, 5010, 10112, '0.0000', '0.0000'),
(242, 68, 5011, 10116, '0.0000', '0.0000'),
(243, 68, 5016, 10144, '0.0000', '0.0000'),
(244, 69, 5001, 10001, '0.0000', '0.0000'),
(245, 69, 5002, 10005, '0.0000', '0.0000'),
(246, 69, 5002, 10014, '0.0000', '0.0000'),
(247, 69, 5003, 10063, '0.0000', '0.0000'),
(248, 69, 5004, 10065, '0.0000', '0.0000'),
(249, 69, 5007, 10085, '0.0000', '0.0000'),
(250, 69, 5007, 10086, '0.0000', '0.0000'),
(251, 69, 5007, 10088, '0.0000', '0.0000'),
(252, 69, 5009, 10110, '0.0000', '0.0000'),
(253, 69, 5010, 10112, '0.0000', '0.0000'),
(254, 69, 5011, 10116, '0.0000', '0.0000'),
(255, 69, 5016, 10135, '0.0000', '0.0000'),
(256, 69, 5016, 10144, '0.0000', '0.0000'),
(257, 69, 5016, 10145, '0.0000', '0.0000'),
(258, 70, 5001, 10001, '0.0000', '0.0000'),
(259, 70, 5002, 10005, '0.0000', '0.0000'),
(260, 70, 5002, 10014, '0.0000', '0.0000'),
(261, 70, 5003, 10063, '0.0000', '0.0000'),
(262, 70, 5004, 10065, '0.0000', '0.0000'),
(263, 70, 5007, 10086, '0.0000', '0.0000'),
(264, 70, 5007, 10088, '0.0000', '0.0000'),
(265, 70, 5009, 10111, '0.0000', '0.0000'),
(266, 70, 5010, 10112, '0.0000', '0.0000'),
(267, 70, 5011, 10116, '0.0000', '0.0000'),
(268, 70, 5016, 10132, '0.0000', '0.0000'),
(269, 70, 5016, 10141, '0.0000', '0.0000'),
(270, 71, 5001, 10001, '0.0000', '0.0000'),
(271, 71, 5002, 10014, '0.0000', '0.0000'),
(272, 71, 5003, 10063, '0.0000', '0.0000'),
(273, 71, 5004, 10065, '0.0000', '0.0000'),
(274, 71, 5007, 10085, '0.0000', '0.0000'),
(275, 71, 5009, 10111, '0.0000', '0.0000'),
(276, 71, 5010, 10112, '0.0000', '0.0000'),
(277, 71, 5011, 10116, '0.0000', '0.0000'),
(278, 71, 5016, 10133, '0.0000', '0.0000'),
(279, 72, 5001, 10001, '0.0000', '0.0000'),
(280, 72, 5002, 10005, '0.0000', '0.0000'),
(281, 72, 5002, 10014, '0.0000', '0.0000'),
(282, 72, 5003, 10063, '0.0000', '0.0000'),
(283, 72, 5004, 10065, '0.0000', '0.0000'),
(284, 72, 5007, 10084, '0.0000', '0.0000'),
(285, 72, 5007, 10088, '0.0000', '0.0000'),
(286, 72, 5009, 10110, '0.0000', '0.0000'),
(287, 72, 5010, 10112, '0.0000', '0.0000'),
(288, 72, 5011, 10116, '0.0000', '0.0000'),
(289, 72, 5016, 10134, '0.0000', '0.0000'),
(290, 72, 5016, 10135, '0.0000', '0.0000'),
(291, 73, 5001, 10001, '0.0000', '0.0000'),
(292, 73, 5002, 10005, '0.0000', '0.0000'),
(293, 73, 5002, 10015, '0.0000', '0.0000'),
(294, 73, 5003, 10063, '0.0000', '0.0000'),
(295, 73, 5004, 10065, '0.0000', '0.0000'),
(296, 73, 5007, 10084, '0.0000', '0.0000'),
(297, 73, 5007, 10085, '0.0000', '0.0000'),
(298, 73, 5007, 10086, '0.0000', '0.0000'),
(299, 73, 5009, 10110, '0.0000', '0.0000'),
(300, 73, 5010, 10112, '0.0000', '0.0000'),
(301, 73, 5011, 10116, '0.0000', '0.0000'),
(302, 73, 5016, 10141, '0.0000', '0.0000'),
(303, 73, 5016, 10144, '0.0000', '0.0000'),
(304, 73, 5016, 10145, '0.0000', '0.0000'),
(305, 73, 5016, 10146, '0.0000', '0.0000'),
(306, 74, 5001, 10001, '0.0000', '0.0000'),
(307, 74, 5002, 10005, '0.0000', '0.0000'),
(308, 74, 5002, 10016, '0.0000', '0.0000'),
(309, 74, 5003, 10063, '0.0000', '0.0000'),
(310, 74, 5004, 10065, '0.0000', '0.0000'),
(311, 74, 5007, 10084, '0.0000', '0.0000'),
(312, 74, 5009, 10111, '0.0000', '0.0000'),
(313, 74, 5010, 10112, '0.0000', '0.0000'),
(314, 74, 5011, 10116, '0.0000', '0.0000'),
(315, 74, 5016, 10135, '0.0000', '0.0000'),
(316, 75, 5001, 10001, '0.0000', '0.0000'),
(317, 75, 5002, 10005, '0.0000', '0.0000'),
(318, 75, 5002, 10016, '0.0000', '0.0000'),
(319, 75, 5003, 10063, '0.0000', '0.0000'),
(320, 75, 5004, 10065, '0.0000', '0.0000'),
(321, 75, 5007, 10084, '0.0000', '0.0000'),
(322, 75, 5007, 10086, '0.0000', '0.0000'),
(323, 75, 5007, 10088, '0.0000', '0.0000'),
(324, 75, 5009, 10111, '0.0000', '0.0000'),
(325, 75, 5010, 10112, '0.0000', '0.0000'),
(326, 75, 5011, 10117, '0.0000', '0.0000'),
(327, 75, 5016, 10133, '0.0000', '0.0000'),
(328, 76, 5001, 10001, '0.0000', '0.0000'),
(329, 76, 5002, 10005, '0.0000', '0.0000'),
(330, 76, 5002, 10017, '0.0000', '0.0000'),
(331, 76, 5003, 10063, '0.0000', '0.0000'),
(332, 76, 5004, 10065, '0.0000', '0.0000'),
(333, 76, 5007, 10084, '0.0000', '0.0000'),
(334, 76, 5009, 10111, '0.0000', '0.0000'),
(335, 76, 5010, 10112, '0.0000', '0.0000'),
(336, 76, 5011, 10116, '0.0000', '0.0000'),
(337, 76, 5016, 10141, '0.0000', '0.0000'),
(338, 77, 5001, 10001, '0.0000', '0.0000'),
(339, 77, 5002, 10005, '0.0000', '0.0000'),
(340, 77, 5002, 10018, '0.0000', '0.0000'),
(341, 77, 5003, 10063, '0.0000', '0.0000'),
(342, 77, 5004, 10065, '0.0000', '0.0000'),
(343, 77, 5007, 10084, '0.0000', '0.0000'),
(344, 77, 5009, 10110, '0.0000', '0.0000'),
(345, 77, 5010, 10112, '0.0000', '0.0000'),
(346, 77, 5011, 10116, '0.0000', '0.0000'),
(347, 77, 5016, 10133, '0.0000', '0.0000'),
(348, 78, 5001, 10001, '0.0000', '0.0000'),
(349, 78, 5002, 10005, '0.0000', '0.0000'),
(350, 78, 5002, 10019, '0.0000', '0.0000'),
(351, 78, 5003, 10063, '0.0000', '0.0000'),
(352, 78, 5004, 10065, '0.0000', '0.0000'),
(353, 78, 5007, 10085, '0.0000', '0.0000'),
(354, 78, 5007, 10087, '0.0000', '0.0000'),
(355, 78, 5009, 10110, '0.0000', '0.0000'),
(356, 78, 5010, 10112, '0.0000', '0.0000'),
(357, 78, 5011, 10116, '0.0000', '0.0000'),
(358, 78, 5016, 10135, '0.0000', '0.0000'),
(359, 79, 5001, 10001, '0.0000', '0.0000'),
(360, 79, 5002, 10005, '0.0000', '0.0000'),
(361, 79, 5002, 10019, '0.0000', '0.0000'),
(362, 79, 5003, 10063, '0.0000', '0.0000'),
(363, 79, 5004, 10065, '0.0000', '0.0000'),
(364, 79, 5007, 10084, '0.0000', '0.0000'),
(365, 79, 5007, 10087, '0.0000', '0.0000'),
(366, 79, 5009, 10110, '0.0000', '0.0000'),
(367, 79, 5010, 10112, '0.0000', '0.0000'),
(368, 79, 5011, 10116, '0.0000', '0.0000'),
(369, 79, 5016, 10147, '0.0000', '0.0000'),
(370, 80, 5001, 10001, '0.0000', '0.0000'),
(371, 80, 5002, 10005, '0.0000', '0.0000'),
(372, 80, 5002, 10009, '0.0000', '0.0000'),
(373, 80, 5003, 10063, '0.0000', '0.0000'),
(374, 80, 5004, 10065, '0.0000', '0.0000'),
(375, 80, 5007, 10084, '0.0000', '0.0000'),
(376, 80, 5007, 10085, '0.0000', '0.0000'),
(377, 80, 5007, 10086, '0.0000', '0.0000'),
(378, 80, 5007, 10088, '0.0000', '0.0000'),
(379, 80, 5009, 10111, '0.0000', '0.0000'),
(380, 80, 5010, 10112, '0.0000', '0.0000'),
(381, 80, 5011, 10116, '0.0000', '0.0000'),
(382, 80, 5016, 10141, '0.0000', '0.0000'),
(383, 81, 5001, 10001, '0.0000', '0.0000'),
(384, 81, 5002, 10005, '0.0000', '0.0000'),
(385, 81, 5002, 10009, '0.0000', '0.0000'),
(386, 81, 5003, 10063, '0.0000', '0.0000'),
(387, 81, 5004, 10065, '0.0000', '0.0000'),
(388, 81, 5007, 10084, '0.0000', '0.0000'),
(389, 81, 5007, 10085, '0.0000', '0.0000'),
(390, 81, 5007, 10088, '0.0000', '0.0000'),
(391, 81, 5009, 10111, '0.0000', '0.0000'),
(392, 81, 5010, 10112, '0.0000', '0.0000'),
(393, 81, 5011, 10116, '0.0000', '0.0000'),
(394, 81, 5016, 10138, '0.0000', '0.0000'),
(395, 82, 5001, 10001, '0.0000', '0.0000'),
(396, 82, 5002, 10005, '0.0000', '0.0000'),
(397, 82, 5002, 10009, '0.0000', '0.0000'),
(398, 82, 5003, 10063, '0.0000', '0.0000'),
(399, 82, 5004, 10065, '0.0000', '0.0000'),
(400, 82, 5007, 10084, '0.0000', '0.0000'),
(401, 82, 5007, 10085, '0.0000', '0.0000'),
(402, 82, 5007, 10086, '0.0000', '0.0000'),
(403, 82, 5007, 10088, '0.0000', '0.0000'),
(404, 82, 5009, 10110, '0.0000', '0.0000'),
(405, 82, 5010, 10112, '0.0000', '0.0000'),
(406, 82, 5011, 10116, '0.0000', '0.0000'),
(407, 82, 5016, 10137, '0.0000', '0.0000'),
(408, 83, 5001, 10001, '0.0000', '0.0000'),
(409, 83, 5002, 10005, '0.0000', '0.0000'),
(410, 83, 5002, 10009, '0.0000', '0.0000'),
(411, 83, 5003, 10063, '0.0000', '0.0000'),
(412, 83, 5004, 10065, '0.0000', '0.0000'),
(413, 83, 5007, 10084, '0.0000', '0.0000'),
(414, 83, 5007, 10085, '0.0000', '0.0000'),
(415, 83, 5007, 10086, '0.0000', '0.0000'),
(416, 83, 5007, 10088, '0.0000', '0.0000'),
(417, 83, 5009, 10110, '0.0000', '0.0000'),
(418, 83, 5010, 10112, '0.0000', '0.0000'),
(419, 83, 5011, 10116, '0.0000', '0.0000'),
(420, 83, 5016, 10133, '0.0000', '0.0000'),
(421, 84, 5001, 10001, '0.0000', '0.0000'),
(422, 84, 5002, 10005, '0.0000', '0.0000'),
(423, 84, 5002, 10009, '0.0000', '0.0000'),
(424, 84, 5003, 10063, '0.0000', '0.0000'),
(425, 84, 5004, 10065, '0.0000', '0.0000'),
(426, 84, 5007, 10084, '0.0000', '0.0000'),
(427, 84, 5007, 10085, '0.0000', '0.0000'),
(428, 84, 5007, 10086, '0.0000', '0.0000'),
(429, 84, 5007, 10088, '0.0000', '0.0000'),
(430, 84, 5009, 10110, '0.0000', '0.0000'),
(431, 84, 5010, 10112, '0.0000', '0.0000'),
(432, 84, 5011, 10116, '0.0000', '0.0000'),
(433, 84, 5016, 10142, '0.0000', '0.0000'),
(434, 85, 5001, 10001, '0.0000', '0.0000'),
(435, 85, 5002, 10005, '0.0000', '0.0000'),
(436, 85, 5002, 10009, '0.0000', '0.0000'),
(437, 85, 5003, 10063, '0.0000', '0.0000'),
(438, 85, 5004, 10065, '0.0000', '0.0000'),
(439, 85, 5007, 10084, '0.0000', '0.0000'),
(440, 85, 5009, 10110, '0.0000', '0.0000'),
(441, 85, 5010, 10112, '0.0000', '0.0000'),
(442, 85, 5011, 10116, '0.0000', '0.0000'),
(443, 85, 5016, 10134, '0.0000', '0.0000'),
(444, 86, 5001, 10001, '0.0000', '0.0000'),
(445, 86, 5002, 10005, '0.0000', '0.0000'),
(446, 86, 5002, 10012, '0.0000', '0.0000'),
(447, 86, 5003, 10063, '0.0000', '0.0000'),
(448, 86, 5004, 10065, '0.0000', '0.0000'),
(449, 86, 5007, 10084, '0.0000', '0.0000'),
(450, 86, 5007, 10085, '0.0000', '0.0000'),
(451, 86, 5007, 10086, '0.0000', '0.0000'),
(452, 86, 5007, 10087, '0.0000', '0.0000'),
(453, 86, 5007, 10088, '0.0000', '0.0000'),
(454, 86, 5009, 10110, '0.0000', '0.0000'),
(455, 86, 5010, 10112, '0.0000', '0.0000'),
(456, 86, 5011, 10116, '0.0000', '0.0000'),
(457, 86, 5016, 10133, '0.0000', '0.0000'),
(458, 86, 5016, 10136, '0.0000', '0.0000'),
(459, 86, 5016, 10138, '0.0000', '0.0000'),
(460, 87, 5001, 10001, '0.0000', '0.0000'),
(461, 87, 5002, 10005, '0.0000', '0.0000'),
(462, 87, 5002, 10014, '0.0000', '0.0000'),
(463, 87, 5003, 10063, '0.0000', '0.0000'),
(464, 87, 5004, 10065, '0.0000', '0.0000'),
(465, 87, 5007, 10084, '0.0000', '0.0000'),
(466, 87, 5007, 10085, '0.0000', '0.0000'),
(467, 87, 5007, 10088, '0.0000', '0.0000'),
(468, 87, 5007, 10089, '0.0000', '0.0000'),
(469, 87, 5009, 10110, '0.0000', '0.0000'),
(470, 87, 5010, 10112, '0.0000', '0.0000'),
(471, 87, 5011, 10116, '0.0000', '0.0000'),
(472, 87, 5016, 10135, '0.0000', '0.0000'),
(473, 88, 5001, 10001, '0.0000', '0.0000'),
(474, 88, 5002, 10005, '0.0000', '0.0000'),
(475, 88, 5002, 10009, '0.0000', '0.0000'),
(476, 88, 5003, 10063, '0.0000', '0.0000'),
(477, 88, 5004, 10065, '0.0000', '0.0000'),
(478, 88, 5007, 10088, '0.0000', '0.0000'),
(479, 88, 5009, 10110, '0.0000', '0.0000'),
(480, 88, 5010, 10112, '0.0000', '0.0000'),
(481, 88, 5011, 10116, '0.0000', '0.0000'),
(482, 88, 5016, 10145, '0.0000', '0.0000'),
(483, 89, 5001, 10001, '0.0000', '0.0000'),
(484, 89, 5002, 10005, '0.0000', '0.0000'),
(485, 89, 5002, 10019, '0.0000', '0.0000'),
(486, 89, 5003, 10063, '0.0000', '0.0000'),
(487, 89, 5004, 10065, '0.0000', '0.0000'),
(488, 89, 5007, 10085, '0.0000', '0.0000'),
(489, 89, 5007, 10086, '0.0000', '0.0000'),
(490, 89, 5007, 10087, '0.0000', '0.0000'),
(491, 89, 5009, 10110, '0.0000', '0.0000'),
(492, 89, 5010, 10112, '0.0000', '0.0000'),
(493, 89, 5011, 10116, '0.0000', '0.0000'),
(494, 89, 5016, 10133, '0.0000', '0.0000'),
(495, 89, 5016, 10146, '0.0000', '0.0000'),
(496, 90, 5001, 10001, '0.0000', '0.0000'),
(497, 90, 5002, 10005, '0.0000', '0.0000'),
(498, 90, 5002, 10009, '0.0000', '0.0000'),
(499, 90, 5003, 10063, '0.0000', '0.0000'),
(500, 90, 5004, 10065, '0.0000', '0.0000'),
(501, 90, 5007, 10088, '0.0000', '0.0000'),
(502, 90, 5009, 10110, '0.0000', '0.0000'),
(503, 90, 5010, 10112, '0.0000', '0.0000'),
(504, 90, 5011, 10116, '0.0000', '0.0000'),
(505, 90, 5016, 10135, '0.0000', '0.0000'),
(506, 91, 5001, 10001, '0.0000', '0.0000'),
(507, 91, 5002, 10005, '0.0000', '0.0000'),
(508, 91, 5002, 10009, '0.0000', '0.0000'),
(509, 91, 5003, 10063, '0.0000', '0.0000'),
(510, 91, 5004, 10065, '0.0000', '0.0000'),
(511, 91, 5007, 10088, '0.0000', '0.0000'),
(512, 91, 5009, 10111, '0.0000', '0.0000'),
(513, 91, 5010, 10112, '0.0000', '0.0000'),
(514, 91, 5011, 10116, '0.0000', '0.0000'),
(515, 91, 5016, 10141, '0.0000', '0.0000'),
(516, 92, 5001, 10001, '0.0000', '0.0000'),
(517, 92, 5002, 10005, '0.0000', '0.0000'),
(518, 92, 5002, 10009, '0.0000', '0.0000'),
(519, 92, 5003, 10063, '0.0000', '0.0000'),
(520, 92, 5004, 10065, '0.0000', '0.0000'),
(521, 92, 5007, 10084, '0.0000', '0.0000'),
(522, 92, 5007, 10085, '0.0000', '0.0000'),
(523, 92, 5007, 10088, '0.0000', '0.0000'),
(524, 92, 5009, 10111, '0.0000', '0.0000'),
(525, 92, 5010, 10112, '0.0000', '0.0000'),
(526, 92, 5011, 10116, '0.0000', '0.0000'),
(527, 92, 5016, 10144, '0.0000', '0.0000'),
(528, 93, 5001, 10001, '0.0000', '0.0000'),
(529, 93, 5002, 10005, '0.0000', '0.0000'),
(530, 93, 5002, 10016, '0.0000', '0.0000'),
(531, 93, 5003, 10063, '0.0000', '0.0000'),
(532, 93, 5004, 10065, '0.0000', '0.0000'),
(533, 93, 5007, 10084, '0.0000', '0.0000'),
(534, 93, 5007, 10087, '0.0000', '0.0000'),
(535, 93, 5009, 10111, '0.0000', '0.0000'),
(536, 93, 5010, 10112, '0.0000', '0.0000'),
(537, 93, 5011, 10116, '0.0000', '0.0000'),
(538, 93, 5016, 10133, '0.0000', '0.0000'),
(539, 94, 5001, 10001, '0.0000', '0.0000'),
(540, 94, 5002, 10005, '0.0000', '0.0000'),
(541, 94, 5002, 10016, '0.0000', '0.0000'),
(542, 94, 5003, 10063, '0.0000', '0.0000'),
(543, 94, 5004, 10065, '0.0000', '0.0000'),
(544, 94, 5007, 10088, '0.0000', '0.0000'),
(545, 94, 5009, 10110, '0.0000', '0.0000'),
(546, 94, 5010, 10112, '0.0000', '0.0000'),
(547, 94, 5011, 10116, '0.0000', '0.0000'),
(548, 94, 5016, 10136, '0.0000', '0.0000'),
(549, 95, 5001, 10001, '0.0000', '0.0000'),
(550, 95, 5002, 10005, '0.0000', '0.0000'),
(551, 95, 5002, 10014, '0.0000', '0.0000'),
(552, 95, 5003, 10063, '0.0000', '0.0000'),
(553, 95, 5004, 10065, '0.0000', '0.0000'),
(554, 95, 5007, 10084, '0.0000', '0.0000'),
(555, 95, 5009, 10111, '0.0000', '0.0000'),
(556, 95, 5010, 10112, '0.0000', '0.0000'),
(557, 95, 5011, 10116, '0.0000', '0.0000'),
(558, 95, 5016, 10144, '0.0000', '0.0000'),
(559, 96, 5001, 10001, '0.0000', '0.0000'),
(560, 96, 5002, 10014, '0.0000', '0.0000'),
(561, 96, 5003, 10063, '0.0000', '0.0000'),
(562, 96, 5004, 10065, '0.0000', '0.0000'),
(563, 96, 5007, 10084, '0.0000', '0.0000'),
(564, 96, 5007, 10085, '0.0000', '0.0000'),
(565, 96, 5009, 10111, '0.0000', '0.0000'),
(566, 96, 5010, 10112, '0.0000', '0.0000'),
(567, 96, 5016, 10135, '0.0000', '0.0000'),
(568, 96, 5016, 10144, '0.0000', '0.0000'),
(569, 96, 5016, 10145, '0.0000', '0.0000'),
(570, 97, 5001, 10001, '0.0000', '0.0000'),
(571, 97, 5002, 10005, '0.0000', '0.0000'),
(572, 97, 5002, 10009, '0.0000', '0.0000'),
(573, 97, 5003, 10063, '0.0000', '0.0000'),
(574, 97, 5004, 10065, '0.0000', '0.0000'),
(575, 97, 5007, 10084, '0.0000', '0.0000'),
(576, 97, 5007, 10085, '0.0000', '0.0000'),
(577, 97, 5009, 10111, '0.0000', '0.0000'),
(578, 97, 5010, 10112, '0.0000', '0.0000'),
(579, 97, 5011, 10116, '0.0000', '0.0000'),
(580, 97, 5016, 10135, '0.0000', '0.0000'),
(581, 98, 5001, 10001, '0.0000', '0.0000'),
(582, 98, 5002, 10005, '0.0000', '0.0000'),
(583, 98, 5002, 10009, '0.0000', '0.0000'),
(584, 98, 5003, 10063, '0.0000', '0.0000'),
(585, 98, 5004, 10065, '0.0000', '0.0000'),
(586, 98, 5007, 10085, '0.0000', '0.0000'),
(587, 98, 5009, 10111, '0.0000', '0.0000'),
(588, 98, 5010, 10112, '0.0000', '0.0000'),
(589, 98, 5011, 10116, '0.0000', '0.0000'),
(590, 98, 5016, 10135, '0.0000', '0.0000'),
(591, 99, 5001, 10001, '0.0000', '0.0000'),
(592, 99, 5002, 10005, '0.0000', '0.0000'),
(593, 99, 5002, 10010, '0.0000', '0.0000'),
(594, 99, 5003, 10063, '0.0000', '0.0000'),
(595, 99, 5004, 10066, '0.0000', '0.0000'),
(596, 99, 5007, 10085, '0.0000', '0.0000'),
(597, 99, 5009, 10110, '0.0000', '0.0000'),
(598, 99, 5010, 10112, '0.0000', '0.0000'),
(599, 99, 5011, 10116, '0.0000', '0.0000'),
(600, 99, 5016, 10133, '0.0000', '0.0000'),
(601, 100, 5001, 10001, '0.0000', '0.0000'),
(602, 100, 5002, 10010, '0.0000', '0.0000'),
(603, 100, 5003, 10063, '0.0000', '0.0000'),
(604, 100, 5004, 10066, '0.0000', '0.0000'),
(605, 100, 5007, 10084, '0.0000', '0.0000'),
(606, 100, 5007, 10085, '0.0000', '0.0000'),
(607, 100, 5009, 10110, '0.0000', '0.0000'),
(608, 100, 5010, 10112, '0.0000', '0.0000'),
(609, 100, 5011, 10116, '0.0000', '0.0000'),
(610, 100, 5016, 10134, '0.0000', '0.0000'),
(611, 101, 5001, 10001, '0.0000', '0.0000'),
(612, 101, 5002, 10005, '0.0000', '0.0000'),
(613, 101, 5002, 10011, '0.0000', '0.0000'),
(614, 101, 5003, 10063, '0.0000', '0.0000'),
(615, 101, 5004, 10066, '0.0000', '0.0000'),
(616, 101, 5007, 10088, '0.0000', '0.0000'),
(617, 101, 5009, 10110, '0.0000', '0.0000'),
(618, 101, 5010, 10112, '0.0000', '0.0000'),
(619, 101, 5011, 10116, '0.0000', '0.0000'),
(620, 101, 5016, 10133, '0.0000', '0.0000'),
(621, 102, 5001, 10001, '0.0000', '0.0000'),
(622, 102, 5002, 10005, '0.0000', '0.0000'),
(623, 102, 5002, 10011, '0.0000', '0.0000'),
(624, 102, 5003, 10063, '0.0000', '0.0000'),
(625, 102, 5004, 10066, '0.0000', '0.0000'),
(626, 102, 5007, 10084, '0.0000', '0.0000'),
(627, 102, 5007, 10087, '0.0000', '0.0000'),
(628, 102, 5009, 10110, '0.0000', '0.0000'),
(629, 102, 5010, 10112, '0.0000', '0.0000'),
(630, 102, 5011, 10116, '0.0000', '0.0000'),
(631, 102, 5016, 10144, '0.0000', '0.0000'),
(632, 103, 5001, 10001, '0.0000', '0.0000'),
(633, 103, 5002, 10005, '0.0000', '0.0000'),
(634, 103, 5002, 10011, '0.0000', '0.0000'),
(635, 103, 5003, 10063, '0.0000', '0.0000'),
(636, 103, 5004, 10066, '0.0000', '0.0000'),
(637, 103, 5007, 10084, '0.0000', '0.0000'),
(638, 103, 5007, 10085, '0.0000', '0.0000'),
(639, 103, 5007, 10087, '0.0000', '0.0000'),
(640, 103, 5009, 10110, '0.0000', '0.0000'),
(641, 103, 5010, 10112, '0.0000', '0.0000'),
(642, 103, 5011, 10117, '0.0000', '0.0000'),
(643, 103, 5016, 10142, '0.0000', '0.0000'),
(644, 104, 5001, 10001, '0.0000', '0.0000'),
(645, 104, 5002, 10011, '0.0000', '0.0000'),
(646, 104, 5003, 10063, '0.0000', '0.0000'),
(647, 104, 5004, 10066, '0.0000', '0.0000'),
(648, 104, 5007, 10085, '0.0000', '0.0000'),
(649, 104, 5007, 10088, '0.0000', '0.0000'),
(650, 104, 5009, 10110, '0.0000', '0.0000'),
(651, 104, 5010, 10112, '0.0000', '0.0000'),
(652, 104, 5011, 10116, '0.0000', '0.0000'),
(653, 104, 5016, 10142, '0.0000', '0.0000'),
(654, 105, 5001, 10001, '0.0000', '0.0000'),
(655, 105, 5002, 10005, '0.0000', '0.0000'),
(656, 105, 5002, 10010, '0.0000', '0.0000'),
(657, 105, 5003, 10063, '0.0000', '0.0000'),
(658, 105, 5004, 10066, '0.0000', '0.0000'),
(659, 105, 5007, 10086, '0.0000', '0.0000'),
(660, 105, 5007, 10088, '0.0000', '0.0000'),
(661, 105, 5009, 10110, '0.0000', '0.0000'),
(662, 105, 5010, 10112, '0.0000', '0.0000'),
(663, 105, 5011, 10116, '0.0000', '0.0000'),
(664, 105, 5016, 10133, '0.0000', '0.0000'),
(665, 106, 5001, 10001, '0.0000', '0.0000'),
(666, 106, 5002, 10005, '0.0000', '0.0000'),
(667, 106, 5002, 10010, '0.0000', '0.0000'),
(668, 106, 5003, 10063, '0.0000', '0.0000'),
(669, 106, 5004, 10066, '0.0000', '0.0000'),
(670, 106, 5007, 10088, '0.0000', '0.0000'),
(671, 106, 5009, 10110, '0.0000', '0.0000'),
(672, 106, 5010, 10112, '0.0000', '0.0000'),
(673, 106, 5011, 10116, '0.0000', '0.0000'),
(674, 106, 5016, 10142, '0.0000', '0.0000'),
(675, 107, 5001, 10001, '0.0000', '0.0000'),
(676, 107, 5002, 10005, '0.0000', '0.0000'),
(677, 107, 5002, 10010, '0.0000', '0.0000'),
(678, 107, 5003, 10063, '0.0000', '0.0000'),
(679, 107, 5004, 10066, '0.0000', '0.0000'),
(680, 107, 5007, 10084, '0.0000', '0.0000'),
(681, 107, 5009, 10110, '0.0000', '0.0000'),
(682, 107, 5010, 10112, '0.0000', '0.0000'),
(683, 107, 5011, 10116, '0.0000', '0.0000'),
(684, 107, 5016, 10131, '0.0000', '0.0000'),
(685, 108, 5001, 10001, '0.0000', '0.0000'),
(686, 108, 5002, 10005, '0.0000', '0.0000'),
(687, 108, 5002, 10011, '0.0000', '0.0000'),
(688, 108, 5003, 10063, '0.0000', '0.0000'),
(689, 108, 5004, 10066, '0.0000', '0.0000'),
(690, 108, 5007, 10084, '0.0000', '0.0000'),
(691, 108, 5007, 10085, '0.0000', '0.0000'),
(692, 108, 5007, 10086, '0.0000', '0.0000'),
(693, 108, 5007, 10087, '0.0000', '0.0000'),
(694, 108, 5007, 10088, '0.0000', '0.0000'),
(695, 108, 5009, 10110, '0.0000', '0.0000'),
(696, 108, 5010, 10112, '0.0000', '0.0000'),
(697, 108, 5011, 10117, '0.0000', '0.0000'),
(698, 108, 5016, 10131, '0.0000', '0.0000'),
(699, 109, 5001, 10001, '0.0000', '0.0000'),
(700, 109, 5002, 10005, '0.0000', '0.0000'),
(701, 109, 5002, 10011, '0.0000', '0.0000'),
(702, 109, 5003, 10063, '0.0000', '0.0000'),
(703, 109, 5004, 10066, '0.0000', '0.0000'),
(704, 109, 5007, 10085, '0.0000', '0.0000'),
(705, 109, 5007, 10087, '0.0000', '0.0000'),
(706, 109, 5007, 10088, '0.0000', '0.0000'),
(707, 109, 5009, 10110, '0.0000', '0.0000'),
(708, 109, 5010, 10112, '0.0000', '0.0000'),
(709, 109, 5011, 10116, '0.0000', '0.0000'),
(710, 109, 5016, 10131, '0.0000', '0.0000'),
(711, 110, 5001, 10001, '0.0000', '0.0000'),
(712, 110, 5002, 10005, '0.0000', '0.0000'),
(713, 110, 5002, 10011, '0.0000', '0.0000'),
(714, 110, 5003, 10063, '0.0000', '0.0000'),
(715, 110, 5004, 10066, '0.0000', '0.0000'),
(716, 110, 5007, 10084, '0.0000', '0.0000'),
(717, 110, 5007, 10085, '0.0000', '0.0000'),
(718, 110, 5009, 10110, '0.0000', '0.0000'),
(719, 110, 5010, 10112, '0.0000', '0.0000'),
(720, 110, 5011, 10116, '0.0000', '0.0000'),
(721, 110, 5016, 10135, '0.0000', '0.0000'),
(722, 111, 5001, 10001, '0.0000', '0.0000'),
(723, 111, 5002, 10005, '0.0000', '0.0000'),
(724, 111, 5002, 10011, '0.0000', '0.0000'),
(725, 111, 5003, 10063, '0.0000', '0.0000'),
(726, 111, 5004, 10066, '0.0000', '0.0000'),
(727, 111, 5007, 10084, '0.0000', '0.0000'),
(728, 111, 5009, 10110, '0.0000', '0.0000'),
(729, 111, 5010, 10112, '0.0000', '0.0000'),
(730, 111, 5011, 10116, '0.0000', '0.0000'),
(731, 111, 5016, 10135, '0.0000', '0.0000'),
(732, 112, 5001, 10001, '0.0000', '0.0000'),
(733, 112, 5002, 10005, '0.0000', '0.0000'),
(734, 112, 5002, 10011, '0.0000', '0.0000'),
(735, 112, 5003, 10063, '0.0000', '0.0000'),
(736, 112, 5004, 10066, '0.0000', '0.0000'),
(737, 112, 5007, 10084, '0.0000', '0.0000'),
(738, 112, 5009, 10110, '0.0000', '0.0000'),
(739, 112, 5010, 10112, '0.0000', '0.0000'),
(740, 112, 5011, 10116, '0.0000', '0.0000'),
(741, 112, 5016, 10135, '0.0000', '0.0000'),
(742, 113, 5001, 10001, '0.0000', '0.0000'),
(743, 113, 5002, 10005, '0.0000', '0.0000'),
(744, 113, 5002, 10011, '0.0000', '0.0000'),
(745, 113, 5003, 10063, '0.0000', '0.0000'),
(746, 113, 5004, 10066, '0.0000', '0.0000'),
(747, 113, 5007, 10084, '0.0000', '0.0000'),
(748, 113, 5007, 10085, '0.0000', '0.0000'),
(749, 113, 5009, 10110, '0.0000', '0.0000'),
(750, 113, 5010, 10112, '0.0000', '0.0000'),
(751, 113, 5011, 10116, '0.0000', '0.0000'),
(752, 113, 5016, 10134, '0.0000', '0.0000'),
(753, 114, 5001, 10001, '0.0000', '0.0000'),
(754, 114, 5002, 10005, '0.0000', '0.0000'),
(755, 114, 5002, 10011, '0.0000', '0.0000'),
(756, 114, 5003, 10063, '0.0000', '0.0000'),
(757, 114, 5004, 10066, '0.0000', '0.0000'),
(758, 114, 5007, 10085, '0.0000', '0.0000'),
(759, 114, 5007, 10088, '0.0000', '0.0000'),
(760, 114, 5009, 10110, '0.0000', '0.0000'),
(761, 114, 5010, 10112, '0.0000', '0.0000'),
(762, 114, 5011, 10116, '0.0000', '0.0000'),
(763, 114, 5016, 10135, '0.0000', '0.0000'),
(764, 115, 5001, 10001, '0.0000', '0.0000'),
(765, 115, 5002, 10005, '0.0000', '0.0000'),
(766, 115, 5002, 10011, '0.0000', '0.0000'),
(767, 115, 5003, 10063, '0.0000', '0.0000'),
(768, 115, 5004, 10066, '0.0000', '0.0000'),
(769, 115, 5007, 10084, '0.0000', '0.0000'),
(770, 115, 5007, 10085, '0.0000', '0.0000'),
(771, 115, 5007, 10086, '0.0000', '0.0000'),
(772, 115, 5007, 10087, '0.0000', '0.0000'),
(773, 115, 5007, 10088, '0.0000', '0.0000'),
(774, 115, 5009, 10110, '0.0000', '0.0000'),
(775, 115, 5010, 10112, '0.0000', '0.0000'),
(776, 115, 5011, 10117, '0.0000', '0.0000'),
(777, 115, 5016, 10135, '0.0000', '0.0000'),
(778, 116, 5001, 10001, '0.0000', '0.0000'),
(779, 116, 5002, 10005, '0.0000', '0.0000'),
(780, 116, 5002, 10011, '0.0000', '0.0000'),
(781, 116, 5003, 10063, '0.0000', '0.0000'),
(782, 116, 5004, 10066, '0.0000', '0.0000'),
(783, 116, 5007, 10084, '0.0000', '0.0000'),
(784, 116, 5007, 10085, '0.0000', '0.0000'),
(785, 116, 5007, 10086, '0.0000', '0.0000'),
(786, 116, 5007, 10088, '0.0000', '0.0000'),
(787, 116, 5009, 10110, '0.0000', '0.0000'),
(788, 116, 5010, 10112, '0.0000', '0.0000'),
(789, 116, 5011, 10116, '0.0000', '0.0000'),
(790, 116, 5016, 10135, '0.0000', '0.0000'),
(791, 117, 5001, 10001, '0.0000', '0.0000'),
(792, 117, 5002, 10005, '0.0000', '0.0000'),
(793, 117, 5002, 10010, '0.0000', '0.0000'),
(794, 117, 5003, 10063, '0.0000', '0.0000'),
(795, 117, 5004, 10066, '0.0000', '0.0000'),
(796, 117, 5007, 10084, '0.0000', '0.0000'),
(797, 117, 5007, 10086, '0.0000', '0.0000'),
(798, 117, 5009, 10110, '0.0000', '0.0000'),
(799, 117, 5010, 10112, '0.0000', '0.0000'),
(800, 117, 5011, 10116, '0.0000', '0.0000'),
(801, 117, 5016, 10135, '0.0000', '0.0000'),
(802, 118, 5001, 10001, '0.0000', '0.0000'),
(803, 118, 5002, 10005, '0.0000', '0.0000'),
(804, 118, 5002, 10011, '0.0000', '0.0000'),
(805, 118, 5003, 10063, '0.0000', '0.0000'),
(806, 118, 5004, 10066, '0.0000', '0.0000'),
(807, 118, 5007, 10085, '0.0000', '0.0000'),
(808, 118, 5007, 10088, '0.0000', '0.0000'),
(809, 118, 5009, 10110, '0.0000', '0.0000'),
(810, 118, 5010, 10112, '0.0000', '0.0000'),
(811, 118, 5011, 10116, '0.0000', '0.0000'),
(812, 118, 5016, 10141, '0.0000', '0.0000'),
(813, 119, 5001, 10001, '0.0000', '0.0000'),
(814, 119, 5002, 10005, '0.0000', '0.0000'),
(815, 119, 5002, 10010, '0.0000', '0.0000'),
(816, 119, 5003, 10063, '0.0000', '0.0000'),
(817, 119, 5004, 10066, '0.0000', '0.0000'),
(818, 119, 5007, 10084, '0.0000', '0.0000'),
(819, 119, 5009, 10110, '0.0000', '0.0000'),
(820, 119, 5010, 10112, '0.0000', '0.0000'),
(821, 119, 5011, 10116, '0.0000', '0.0000'),
(822, 119, 5016, 10141, '0.0000', '0.0000'),
(823, 120, 5001, 10001, '0.0000', '0.0000'),
(824, 120, 5002, 10005, '0.0000', '0.0000'),
(825, 120, 5002, 10020, '0.0000', '0.0000'),
(826, 120, 5003, 10063, '0.0000', '0.0000'),
(827, 120, 5004, 10066, '0.0000', '0.0000'),
(828, 120, 5007, 10084, '0.0000', '0.0000'),
(829, 120, 5009, 10110, '0.0000', '0.0000'),
(830, 120, 5010, 10113, '0.0000', '0.0000'),
(831, 120, 5011, 10116, '0.0000', '0.0000'),
(832, 120, 5016, 10135, '0.0000', '0.0000'),
(833, 121, 5001, 10001, '0.0000', '0.0000'),
(834, 121, 5002, 10005, '0.0000', '0.0000'),
(835, 121, 5002, 10011, '0.0000', '0.0000'),
(836, 121, 5003, 10063, '0.0000', '0.0000'),
(837, 121, 5004, 10066, '0.0000', '0.0000'),
(838, 121, 5007, 10084, '0.0000', '0.0000'),
(839, 121, 5007, 10085, '0.0000', '0.0000'),
(840, 121, 5007, 10086, '0.0000', '0.0000'),
(841, 121, 5007, 10088, '0.0000', '0.0000'),
(842, 121, 5009, 10110, '0.0000', '0.0000'),
(843, 121, 5010, 10112, '0.0000', '0.0000'),
(844, 121, 5011, 10117, '0.0000', '0.0000'),
(845, 121, 5016, 10144, '0.0000', '0.0000'),
(846, 122, 5001, 10001, '0.0000', '0.0000'),
(847, 122, 5002, 10010, '0.0000', '0.0000'),
(848, 122, 5003, 10063, '0.0000', '0.0000'),
(849, 122, 5004, 10066, '0.0000', '0.0000'),
(850, 122, 5007, 10085, '0.0000', '0.0000'),
(851, 122, 5009, 10110, '0.0000', '0.0000'),
(852, 122, 5010, 10114, '0.0000', '0.0000'),
(853, 122, 5011, 10117, '0.0000', '0.0000'),
(854, 122, 5016, 10148, '0.0000', '0.0000'),
(855, 123, 5001, 10001, '0.0000', '0.0000'),
(856, 123, 5002, 10005, '0.0000', '0.0000'),
(857, 123, 5002, 10011, '0.0000', '0.0000'),
(858, 123, 5003, 10063, '0.0000', '0.0000'),
(859, 123, 5004, 10066, '0.0000', '0.0000'),
(860, 123, 5007, 10084, '0.0000', '0.0000'),
(861, 123, 5007, 10085, '0.0000', '0.0000'),
(862, 123, 5007, 10086, '0.0000', '0.0000'),
(863, 123, 5007, 10088, '0.0000', '0.0000'),
(864, 123, 5009, 10110, '0.0000', '0.0000'),
(865, 123, 5010, 10112, '0.0000', '0.0000'),
(866, 123, 5011, 10117, '0.0000', '0.0000'),
(867, 123, 5016, 10148, '0.0000', '0.0000'),
(868, 124, 5001, 10001, '0.0000', '0.0000'),
(869, 124, 5002, 10011, '0.0000', '0.0000'),
(870, 124, 5003, 10063, '0.0000', '0.0000'),
(871, 124, 5004, 10066, '0.0000', '0.0000'),
(872, 124, 5007, 10085, '0.0000', '0.0000'),
(873, 124, 5007, 10086, '0.0000', '0.0000'),
(874, 124, 5007, 10088, '0.0000', '0.0000'),
(875, 124, 5009, 10110, '0.0000', '0.0000'),
(876, 124, 5010, 10112, '0.0000', '0.0000'),
(877, 124, 5011, 10117, '0.0000', '0.0000'),
(878, 124, 5016, 10148, '0.0000', '0.0000'),
(879, 125, 5001, 10001, '0.0000', '0.0000'),
(880, 125, 5002, 10005, '0.0000', '0.0000'),
(881, 125, 5002, 10010, '0.0000', '0.0000'),
(882, 125, 5003, 10063, '0.0000', '0.0000'),
(883, 125, 5004, 10066, '0.0000', '0.0000'),
(884, 125, 5007, 10085, '0.0000', '0.0000'),
(885, 125, 5007, 10088, '0.0000', '0.0000'),
(886, 125, 5009, 10110, '0.0000', '0.0000'),
(887, 125, 5010, 10112, '0.0000', '0.0000'),
(888, 125, 5011, 10117, '0.0000', '0.0000'),
(889, 125, 5016, 10148, '0.0000', '0.0000'),
(890, 126, 5001, 10001, '0.0000', '0.0000'),
(891, 126, 5002, 10011, '0.0000', '0.0000'),
(892, 126, 5003, 10063, '0.0000', '0.0000'),
(893, 126, 5004, 10066, '0.0000', '0.0000'),
(894, 126, 5007, 10084, '0.0000', '0.0000'),
(895, 126, 5007, 10085, '0.0000', '0.0000'),
(896, 126, 5007, 10086, '0.0000', '0.0000'),
(897, 126, 5007, 10087, '0.0000', '0.0000'),
(898, 126, 5007, 10088, '0.0000', '0.0000'),
(899, 126, 5009, 10110, '0.0000', '0.0000'),
(900, 126, 5010, 10112, '0.0000', '0.0000'),
(901, 126, 5011, 10117, '0.0000', '0.0000'),
(902, 126, 5016, 10145, '0.0000', '0.0000'),
(903, 127, 5001, 10001, '0.0000', '0.0000'),
(904, 127, 5002, 10005, '0.0000', '0.0000'),
(905, 127, 5002, 10010, '0.0000', '0.0000'),
(906, 127, 5003, 10063, '0.0000', '0.0000'),
(907, 127, 5004, 10066, '0.0000', '0.0000'),
(908, 127, 5007, 10085, '0.0000', '0.0000'),
(909, 127, 5007, 10086, '0.0000', '0.0000'),
(910, 127, 5007, 10087, '0.0000', '0.0000'),
(911, 127, 5009, 10110, '0.0000', '0.0000'),
(912, 127, 5010, 10112, '0.0000', '0.0000'),
(913, 127, 5011, 10117, '0.0000', '0.0000'),
(914, 127, 5016, 10139, '0.0000', '0.0000'),
(915, 128, 5001, 10001, '0.0000', '0.0000'),
(916, 128, 5002, 10010, '0.0000', '0.0000'),
(917, 128, 5003, 10063, '0.0000', '0.0000'),
(918, 128, 5004, 10066, '0.0000', '0.0000'),
(919, 128, 5007, 10085, '0.0000', '0.0000'),
(920, 128, 5007, 10086, '0.0000', '0.0000'),
(921, 128, 5009, 10110, '0.0000', '0.0000'),
(922, 128, 5010, 10112, '0.0000', '0.0000'),
(923, 128, 5011, 10117, '0.0000', '0.0000'),
(924, 128, 5016, 10139, '0.0000', '0.0000'),
(925, 129, 5001, 10001, '0.0000', '0.0000'),
(926, 129, 5002, 10005, '0.0000', '0.0000'),
(927, 129, 5002, 10011, '0.0000', '0.0000'),
(928, 129, 5003, 10063, '0.0000', '0.0000'),
(929, 129, 5004, 10066, '0.0000', '0.0000'),
(930, 129, 5007, 10084, '0.0000', '0.0000'),
(931, 129, 5007, 10085, '0.0000', '0.0000'),
(932, 129, 5007, 10086, '0.0000', '0.0000'),
(933, 129, 5007, 10087, '0.0000', '0.0000'),
(934, 129, 5007, 10088, '0.0000', '0.0000'),
(935, 129, 5009, 10110, '0.0000', '0.0000'),
(936, 129, 5010, 10112, '0.0000', '0.0000'),
(937, 129, 5011, 10116, '0.0000', '0.0000'),
(938, 129, 5016, 10135, '0.0000', '0.0000'),
(939, 130, 5001, 10001, '0.0000', '0.0000'),
(940, 130, 5002, 10005, '0.0000', '0.0000'),
(941, 130, 5002, 10011, '0.0000', '0.0000'),
(942, 130, 5003, 10063, '0.0000', '0.0000'),
(943, 130, 5004, 10066, '0.0000', '0.0000'),
(944, 130, 5007, 10084, '0.0000', '0.0000'),
(945, 130, 5007, 10087, '0.0000', '0.0000'),
(946, 130, 5009, 10110, '0.0000', '0.0000'),
(947, 130, 5010, 10112, '0.0000', '0.0000'),
(948, 130, 5011, 10116, '0.0000', '0.0000'),
(949, 130, 5016, 10135, '0.0000', '0.0000'),
(950, 131, 5001, 10001, '0.0000', '0.0000'),
(951, 131, 5002, 10005, '0.0000', '0.0000'),
(952, 131, 5002, 10010, '0.0000', '0.0000'),
(953, 131, 5003, 10063, '0.0000', '0.0000'),
(954, 131, 5004, 10066, '0.0000', '0.0000'),
(955, 131, 5007, 10085, '0.0000', '0.0000'),
(956, 131, 5009, 10110, '0.0000', '0.0000'),
(957, 131, 5010, 10112, '0.0000', '0.0000'),
(958, 131, 5011, 10116, '0.0000', '0.0000'),
(959, 131, 5016, 10131, '0.0000', '0.0000'),
(960, 132, 5001, 10001, '0.0000', '0.0000'),
(961, 132, 5002, 10005, '0.0000', '0.0000'),
(962, 132, 5002, 10011, '0.0000', '0.0000'),
(963, 132, 5003, 10063, '0.0000', '0.0000'),
(964, 132, 5004, 10066, '0.0000', '0.0000'),
(965, 132, 5007, 10085, '0.0000', '0.0000'),
(966, 132, 5009, 10110, '0.0000', '0.0000'),
(967, 132, 5010, 10112, '0.0000', '0.0000'),
(968, 132, 5011, 10116, '0.0000', '0.0000'),
(969, 132, 5016, 10135, '0.0000', '0.0000'),
(970, 133, 5001, 10001, '0.0000', '0.0000'),
(971, 133, 5002, 10005, '0.0000', '0.0000'),
(972, 133, 5002, 10011, '0.0000', '0.0000'),
(973, 133, 5003, 10063, '0.0000', '0.0000'),
(974, 133, 5004, 10066, '0.0000', '0.0000'),
(975, 133, 5007, 10084, '0.0000', '0.0000'),
(976, 133, 5007, 10085, '0.0000', '0.0000'),
(977, 133, 5007, 10086, '0.0000', '0.0000'),
(978, 133, 5007, 10087, '0.0000', '0.0000'),
(979, 133, 5007, 10088, '0.0000', '0.0000'),
(980, 133, 5009, 10110, '0.0000', '0.0000'),
(981, 133, 5010, 10112, '0.0000', '0.0000'),
(982, 133, 5011, 10117, '0.0000', '0.0000'),
(983, 133, 5016, 10131, '0.0000', '0.0000'),
(984, 134, 5001, 10001, '0.0000', '0.0000'),
(985, 134, 5002, 10005, '0.0000', '0.0000'),
(986, 134, 5002, 10010, '0.0000', '0.0000'),
(987, 134, 5003, 10063, '0.0000', '0.0000'),
(988, 134, 5004, 10066, '0.0000', '0.0000'),
(989, 134, 5007, 10084, '0.0000', '0.0000'),
(990, 134, 5007, 10087, '0.0000', '0.0000'),
(991, 134, 5009, 10110, '0.0000', '0.0000'),
(992, 134, 5010, 10112, '0.0000', '0.0000'),
(993, 134, 5011, 10116, '0.0000', '0.0000'),
(994, 134, 5016, 10141, '0.0000', '0.0000'),
(995, 135, 5001, 10001, '0.0000', '0.0000'),
(996, 135, 5002, 10005, '0.0000', '0.0000'),
(997, 135, 5002, 10011, '0.0000', '0.0000'),
(998, 135, 5003, 10063, '0.0000', '0.0000'),
(999, 135, 5004, 10066, '0.0000', '0.0000'),
(1000, 135, 5007, 10084, '0.0000', '0.0000'),
(1001, 135, 5007, 10085, '0.0000', '0.0000'),
(1002, 135, 5009, 10110, '0.0000', '0.0000'),
(1003, 135, 5010, 10112, '0.0000', '0.0000'),
(1004, 135, 5011, 10117, '0.0000', '0.0000'),
(1005, 135, 5016, 10148, '0.0000', '0.0000'),
(1006, 136, 5001, 10001, '0.0000', '0.0000'),
(1007, 136, 5002, 10005, '0.0000', '0.0000'),
(1008, 136, 5002, 10010, '0.0000', '0.0000'),
(1009, 136, 5003, 10063, '0.0000', '0.0000'),
(1010, 136, 5004, 10066, '0.0000', '0.0000'),
(1011, 136, 5007, 10084, '0.0000', '0.0000'),
(1012, 136, 5007, 10085, '0.0000', '0.0000'),
(1013, 136, 5007, 10090, '0.0000', '0.0000'),
(1014, 136, 5009, 10110, '0.0000', '0.0000'),
(1015, 136, 5010, 10112, '0.0000', '0.0000'),
(1016, 136, 5011, 10116, '0.0000', '0.0000'),
(1017, 136, 5016, 10134, '0.0000', '0.0000'),
(1018, 137, 5001, 10001, '0.0000', '0.0000'),
(1019, 137, 5002, 10005, '0.0000', '0.0000'),
(1020, 137, 5002, 10010, '0.0000', '0.0000'),
(1021, 137, 5003, 10063, '0.0000', '0.0000'),
(1022, 137, 5004, 10066, '0.0000', '0.0000'),
(1023, 137, 5007, 10084, '0.0000', '0.0000'),
(1024, 137, 5007, 10085, '0.0000', '0.0000'),
(1025, 137, 5009, 10110, '0.0000', '0.0000'),
(1026, 137, 5010, 10113, '0.0000', '0.0000'),
(1027, 137, 5011, 10117, '0.0000', '0.0000'),
(1028, 137, 5016, 10141, '0.0000', '0.0000'),
(1029, 138, 5001, 10001, '0.0000', '0.0000'),
(1030, 138, 5002, 10011, '0.0000', '0.0000'),
(1031, 138, 5003, 10063, '0.0000', '0.0000'),
(1032, 138, 5004, 10066, '0.0000', '0.0000'),
(1033, 138, 5007, 10084, '0.0000', '0.0000'),
(1034, 138, 5007, 10085, '0.0000', '0.0000'),
(1035, 138, 5007, 10086, '0.0000', '0.0000'),
(1036, 138, 5007, 10088, '0.0000', '0.0000'),
(1037, 138, 5009, 10110, '0.0000', '0.0000'),
(1038, 138, 5010, 10112, '0.0000', '0.0000'),
(1039, 138, 5011, 10116, '0.0000', '0.0000'),
(1040, 138, 5016, 10145, '0.0000', '0.0000'),
(1041, 139, 5001, 10001, '0.0000', '0.0000'),
(1042, 139, 5002, 10005, '0.0000', '0.0000'),
(1043, 139, 5002, 10011, '0.0000', '0.0000'),
(1044, 139, 5003, 10063, '0.0000', '0.0000'),
(1045, 139, 5004, 10066, '0.0000', '0.0000'),
(1046, 139, 5007, 10085, '0.0000', '0.0000'),
(1047, 139, 5007, 10086, '0.0000', '0.0000'),
(1048, 139, 5007, 10087, '0.0000', '0.0000'),
(1049, 139, 5007, 10088, '0.0000', '0.0000'),
(1050, 139, 5009, 10110, '0.0000', '0.0000'),
(1051, 139, 5010, 10112, '0.0000', '0.0000'),
(1052, 139, 5011, 10116, '0.0000', '0.0000'),
(1053, 139, 5016, 10148, '0.0000', '0.0000'),
(1054, 140, 5001, 10001, '0.0000', '0.0000'),
(1055, 140, 5002, 10005, '0.0000', '0.0000'),
(1056, 140, 5002, 10011, '0.0000', '0.0000'),
(1057, 140, 5003, 10063, '0.0000', '0.0000'),
(1058, 140, 5004, 10066, '0.0000', '0.0000'),
(1059, 140, 5007, 10085, '0.0000', '0.0000'),
(1060, 140, 5007, 10088, '0.0000', '0.0000'),
(1061, 140, 5007, 10090, '0.0000', '0.0000'),
(1062, 140, 5009, 10110, '0.0000', '0.0000'),
(1063, 140, 5010, 10112, '0.0000', '0.0000'),
(1064, 140, 5011, 10116, '0.0000', '0.0000'),
(1065, 140, 5016, 10145, '0.0000', '0.0000'),
(1066, 141, 5001, 10001, '0.0000', '0.0000'),
(1067, 141, 5002, 10005, '0.0000', '0.0000'),
(1068, 141, 5002, 10011, '0.0000', '0.0000'),
(1069, 141, 5003, 10063, '0.0000', '0.0000'),
(1070, 141, 5004, 10066, '0.0000', '0.0000'),
(1071, 141, 5007, 10084, '0.0000', '0.0000'),
(1072, 141, 5007, 10088, '0.0000', '0.0000'),
(1073, 141, 5009, 10110, '0.0000', '0.0000'),
(1074, 141, 5010, 10112, '0.0000', '0.0000'),
(1075, 141, 5011, 10116, '0.0000', '0.0000'),
(1076, 141, 5016, 10145, '0.0000', '0.0000'),
(1077, 142, 5001, 10001, '0.0000', '0.0000'),
(1078, 142, 5002, 10005, '0.0000', '0.0000'),
(1079, 142, 5002, 10011, '0.0000', '0.0000'),
(1080, 142, 5003, 10063, '0.0000', '0.0000'),
(1081, 142, 5004, 10066, '0.0000', '0.0000'),
(1082, 142, 5007, 10084, '0.0000', '0.0000'),
(1083, 142, 5009, 10110, '0.0000', '0.0000'),
(1084, 142, 5010, 10112, '0.0000', '0.0000'),
(1085, 142, 5011, 10116, '0.0000', '0.0000'),
(1086, 142, 5016, 10145, '0.0000', '0.0000'),
(1087, 143, 5001, 10001, '0.0000', '0.0000'),
(1088, 143, 5002, 10005, '0.0000', '0.0000'),
(1089, 143, 5002, 10010, '0.0000', '0.0000'),
(1090, 143, 5003, 10063, '0.0000', '0.0000'),
(1091, 143, 5004, 10066, '0.0000', '0.0000'),
(1092, 143, 5007, 10084, '0.0000', '0.0000'),
(1093, 143, 5009, 10110, '0.0000', '0.0000'),
(1094, 143, 5010, 10112, '0.0000', '0.0000'),
(1095, 143, 5011, 10116, '0.0000', '0.0000'),
(1096, 143, 5016, 10135, '0.0000', '0.0000'),
(1097, 144, 5001, 10001, '0.0000', '0.0000'),
(1098, 144, 5002, 10005, '0.0000', '0.0000'),
(1099, 144, 5002, 10011, '0.0000', '0.0000'),
(1100, 144, 5003, 10063, '0.0000', '0.0000'),
(1101, 144, 5004, 10066, '0.0000', '0.0000'),
(1102, 144, 5007, 10088, '0.0000', '0.0000'),
(1103, 144, 5009, 10110, '0.0000', '0.0000'),
(1104, 144, 5010, 10112, '0.0000', '0.0000'),
(1105, 144, 5011, 10116, '0.0000', '0.0000'),
(1106, 144, 5016, 10141, '0.0000', '0.0000'),
(1107, 145, 5001, 10001, '0.0000', '0.0000'),
(1108, 145, 5002, 10005, '0.0000', '0.0000'),
(1109, 145, 5002, 10011, '0.0000', '0.0000'),
(1110, 145, 5003, 10063, '0.0000', '0.0000'),
(1111, 145, 5004, 10066, '0.0000', '0.0000'),
(1112, 145, 5007, 10084, '0.0000', '0.0000'),
(1113, 145, 5007, 10086, '0.0000', '0.0000'),
(1114, 145, 5007, 10088, '0.0000', '0.0000'),
(1115, 145, 5009, 10110, '0.0000', '0.0000'),
(1116, 145, 5010, 10115, '0.0000', '0.0000'),
(1117, 145, 5011, 10116, '0.0000', '0.0000'),
(1118, 145, 5016, 10137, '0.0000', '0.0000'),
(1119, 146, 5001, 10001, '0.0000', '0.0000'),
(1120, 146, 5002, 10005, '0.0000', '0.0000'),
(1121, 146, 5002, 10010, '0.0000', '0.0000'),
(1122, 146, 5003, 10063, '0.0000', '0.0000'),
(1123, 146, 5004, 10066, '0.0000', '0.0000'),
(1124, 146, 5007, 10086, '0.0000', '0.0000'),
(1125, 146, 5007, 10088, '0.0000', '0.0000'),
(1126, 146, 5009, 10110, '0.0000', '0.0000'),
(1127, 146, 5010, 10112, '0.0000', '0.0000'),
(1128, 146, 5011, 10116, '0.0000', '0.0000'),
(1129, 146, 5016, 10141, '0.0000', '0.0000'),
(1130, 147, 5001, 10001, '0.0000', '0.0000'),
(1131, 147, 5002, 10005, '0.0000', '0.0000'),
(1132, 147, 5002, 10011, '0.0000', '0.0000'),
(1133, 147, 5003, 10063, '0.0000', '0.0000'),
(1134, 147, 5004, 10066, '0.0000', '0.0000'),
(1135, 147, 5007, 10085, '0.0000', '0.0000'),
(1136, 147, 5009, 10110, '0.0000', '0.0000'),
(1137, 147, 5010, 10112, '0.0000', '0.0000'),
(1138, 147, 5011, 10117, '0.0000', '0.0000'),
(1139, 147, 5016, 10137, '0.0000', '0.0000'),
(1140, 148, 5001, 10001, '0.0000', '0.0000'),
(1141, 148, 5002, 10005, '0.0000', '0.0000'),
(1142, 148, 5002, 10011, '0.0000', '0.0000'),
(1143, 148, 5003, 10063, '0.0000', '0.0000'),
(1144, 148, 5004, 10066, '0.0000', '0.0000'),
(1145, 148, 5007, 10085, '0.0000', '0.0000'),
(1146, 148, 5007, 10086, '0.0000', '0.0000'),
(1147, 148, 5007, 10087, '0.0000', '0.0000'),
(1148, 148, 5007, 10088, '0.0000', '0.0000'),
(1149, 148, 5009, 10110, '0.0000', '0.0000'),
(1150, 148, 5010, 10112, '0.0000', '0.0000'),
(1151, 148, 5011, 10117, '0.0000', '0.0000'),
(1152, 148, 5016, 10134, '0.0000', '0.0000'),
(1153, 149, 5001, 10001, '0.0000', '0.0000'),
(1154, 149, 5002, 10005, '0.0000', '0.0000'),
(1155, 149, 5002, 10011, '0.0000', '0.0000'),
(1156, 149, 5003, 10063, '0.0000', '0.0000'),
(1157, 149, 5004, 10066, '0.0000', '0.0000'),
(1158, 149, 5007, 10085, '0.0000', '0.0000'),
(1159, 149, 5009, 10110, '0.0000', '0.0000'),
(1160, 149, 5010, 10112, '0.0000', '0.0000'),
(1161, 149, 5011, 10116, '0.0000', '0.0000'),
(1162, 149, 5016, 10141, '0.0000', '0.0000'),
(1163, 150, 5001, 10001, '0.0000', '0.0000'),
(1164, 150, 5002, 10005, '0.0000', '0.0000'),
(1165, 150, 5002, 10010, '0.0000', '0.0000'),
(1166, 150, 5003, 10063, '0.0000', '0.0000'),
(1167, 150, 5004, 10066, '0.0000', '0.0000'),
(1168, 150, 5007, 10085, '0.0000', '0.0000'),
(1169, 150, 5009, 10110, '0.0000', '0.0000'),
(1170, 150, 5010, 10112, '0.0000', '0.0000'),
(1171, 150, 5011, 10116, '0.0000', '0.0000');
INSERT INTO `oc_ocfilter_option_value_to_product` (`ocfilter_option_value_to_product_id`, `product_id`, `option_id`, `value_id`, `slide_value_min`, `slide_value_max`) VALUES
(1172, 150, 5016, 10144, '0.0000', '0.0000'),
(1173, 151, 5001, 10001, '0.0000', '0.0000'),
(1174, 151, 5002, 10005, '0.0000', '0.0000'),
(1175, 151, 5002, 10011, '0.0000', '0.0000'),
(1176, 151, 5003, 10063, '0.0000', '0.0000'),
(1177, 151, 5004, 10066, '0.0000', '0.0000'),
(1178, 151, 5007, 10084, '0.0000', '0.0000'),
(1179, 151, 5007, 10086, '0.0000', '0.0000'),
(1180, 151, 5007, 10087, '0.0000', '0.0000'),
(1181, 151, 5007, 10088, '0.0000', '0.0000'),
(1182, 151, 5009, 10110, '0.0000', '0.0000'),
(1183, 151, 5010, 10112, '0.0000', '0.0000'),
(1184, 151, 5011, 10116, '0.0000', '0.0000'),
(1185, 151, 5016, 10137, '0.0000', '0.0000'),
(1186, 152, 5001, 10001, '0.0000', '0.0000'),
(1187, 152, 5002, 10005, '0.0000', '0.0000'),
(1188, 152, 5002, 10011, '0.0000', '0.0000'),
(1189, 152, 5003, 10063, '0.0000', '0.0000'),
(1190, 152, 5004, 10066, '0.0000', '0.0000'),
(1191, 152, 5007, 10084, '0.0000', '0.0000'),
(1192, 152, 5007, 10085, '0.0000', '0.0000'),
(1193, 152, 5007, 10086, '0.0000', '0.0000'),
(1194, 152, 5007, 10088, '0.0000', '0.0000'),
(1195, 152, 5009, 10110, '0.0000', '0.0000'),
(1196, 152, 5010, 10112, '0.0000', '0.0000'),
(1197, 152, 5011, 10116, '0.0000', '0.0000'),
(1198, 152, 5016, 10144, '0.0000', '0.0000'),
(1199, 153, 5001, 10001, '0.0000', '0.0000'),
(1200, 153, 5002, 10005, '0.0000', '0.0000'),
(1201, 153, 5002, 10011, '0.0000', '0.0000'),
(1202, 153, 5003, 10063, '0.0000', '0.0000'),
(1203, 153, 5004, 10066, '0.0000', '0.0000'),
(1204, 153, 5007, 10084, '0.0000', '0.0000'),
(1205, 153, 5007, 10085, '0.0000', '0.0000'),
(1206, 153, 5007, 10086, '0.0000', '0.0000'),
(1207, 153, 5007, 10088, '0.0000', '0.0000'),
(1208, 153, 5009, 10110, '0.0000', '0.0000'),
(1209, 153, 5010, 10112, '0.0000', '0.0000'),
(1210, 153, 5011, 10117, '0.0000', '0.0000'),
(1211, 153, 5016, 10148, '0.0000', '0.0000'),
(1212, 154, 5001, 10001, '0.0000', '0.0000'),
(1213, 154, 5002, 10005, '0.0000', '0.0000'),
(1214, 154, 5002, 10011, '0.0000', '0.0000'),
(1215, 154, 5003, 10063, '0.0000', '0.0000'),
(1216, 154, 5004, 10066, '0.0000', '0.0000'),
(1217, 154, 5007, 10084, '0.0000', '0.0000'),
(1218, 154, 5007, 10088, '0.0000', '0.0000'),
(1219, 154, 5009, 10110, '0.0000', '0.0000'),
(1220, 154, 5010, 10112, '0.0000', '0.0000'),
(1221, 154, 5011, 10116, '0.0000', '0.0000'),
(1222, 154, 5016, 10141, '0.0000', '0.0000'),
(1223, 155, 5001, 10001, '0.0000', '0.0000'),
(1224, 155, 5002, 10005, '0.0000', '0.0000'),
(1225, 155, 5002, 10011, '0.0000', '0.0000'),
(1226, 155, 5003, 10063, '0.0000', '0.0000'),
(1227, 155, 5004, 10066, '0.0000', '0.0000'),
(1228, 155, 5007, 10084, '0.0000', '0.0000'),
(1229, 155, 5007, 10085, '0.0000', '0.0000'),
(1230, 155, 5007, 10086, '0.0000', '0.0000'),
(1231, 155, 5007, 10087, '0.0000', '0.0000'),
(1232, 155, 5007, 10088, '0.0000', '0.0000'),
(1233, 155, 5009, 10110, '0.0000', '0.0000'),
(1234, 155, 5010, 10112, '0.0000', '0.0000'),
(1235, 155, 5011, 10117, '0.0000', '0.0000'),
(1236, 155, 5016, 10149, '0.0000', '0.0000'),
(1237, 156, 5001, 10001, '0.0000', '0.0000'),
(1238, 156, 5002, 10005, '0.0000', '0.0000'),
(1239, 156, 5002, 10011, '0.0000', '0.0000'),
(1240, 156, 5003, 10063, '0.0000', '0.0000'),
(1241, 156, 5004, 10066, '0.0000', '0.0000'),
(1242, 156, 5007, 10084, '0.0000', '0.0000'),
(1243, 156, 5007, 10085, '0.0000', '0.0000'),
(1244, 156, 5007, 10086, '0.0000', '0.0000'),
(1245, 156, 5007, 10087, '0.0000', '0.0000'),
(1246, 156, 5007, 10088, '0.0000', '0.0000'),
(1247, 156, 5009, 10110, '0.0000', '0.0000'),
(1248, 156, 5010, 10112, '0.0000', '0.0000'),
(1249, 156, 5011, 10117, '0.0000', '0.0000'),
(1250, 156, 5016, 10132, '0.0000', '0.0000'),
(1251, 157, 5001, 10001, '0.0000', '0.0000'),
(1252, 157, 5002, 10005, '0.0000', '0.0000'),
(1253, 157, 5002, 10011, '0.0000', '0.0000'),
(1254, 157, 5003, 10063, '0.0000', '0.0000'),
(1255, 157, 5004, 10066, '0.0000', '0.0000'),
(1256, 157, 5007, 10084, '0.0000', '0.0000'),
(1257, 157, 5007, 10085, '0.0000', '0.0000'),
(1258, 157, 5007, 10086, '0.0000', '0.0000'),
(1259, 157, 5007, 10088, '0.0000', '0.0000'),
(1260, 157, 5009, 10110, '0.0000', '0.0000'),
(1261, 157, 5010, 10112, '0.0000', '0.0000'),
(1262, 157, 5011, 10116, '0.0000', '0.0000'),
(1263, 157, 5016, 10143, '0.0000', '0.0000'),
(1264, 158, 5001, 10001, '0.0000', '0.0000'),
(1265, 158, 5002, 10005, '0.0000', '0.0000'),
(1266, 158, 5002, 10021, '0.0000', '0.0000'),
(1267, 158, 5003, 10063, '0.0000', '0.0000'),
(1268, 158, 5004, 10066, '0.0000', '0.0000'),
(1269, 158, 5007, 10088, '0.0000', '0.0000'),
(1270, 158, 5009, 10110, '0.0000', '0.0000'),
(1271, 158, 5010, 10112, '0.0000', '0.0000'),
(1272, 158, 5011, 10116, '0.0000', '0.0000'),
(1273, 158, 5016, 10137, '0.0000', '0.0000'),
(1274, 159, 5001, 10001, '0.0000', '0.0000'),
(1275, 159, 5002, 10005, '0.0000', '0.0000'),
(1276, 159, 5002, 10011, '0.0000', '0.0000'),
(1277, 159, 5003, 10063, '0.0000', '0.0000'),
(1278, 159, 5004, 10066, '0.0000', '0.0000'),
(1279, 159, 5007, 10084, '0.0000', '0.0000'),
(1280, 159, 5007, 10086, '0.0000', '0.0000'),
(1281, 159, 5007, 10087, '0.0000', '0.0000'),
(1282, 159, 5007, 10088, '0.0000', '0.0000'),
(1283, 159, 5009, 10110, '0.0000', '0.0000'),
(1284, 159, 5010, 10112, '0.0000', '0.0000'),
(1285, 159, 5011, 10117, '0.0000', '0.0000'),
(1286, 159, 5016, 10137, '0.0000', '0.0000'),
(1287, 160, 5001, 10001, '0.0000', '0.0000'),
(1288, 160, 5002, 10005, '0.0000', '0.0000'),
(1289, 160, 5002, 10010, '0.0000', '0.0000'),
(1290, 160, 5003, 10063, '0.0000', '0.0000'),
(1291, 160, 5004, 10066, '0.0000', '0.0000'),
(1292, 160, 5007, 10084, '0.0000', '0.0000'),
(1293, 160, 5007, 10085, '0.0000', '0.0000'),
(1294, 160, 5007, 10087, '0.0000', '0.0000'),
(1295, 160, 5007, 10088, '0.0000', '0.0000'),
(1296, 160, 5009, 10110, '0.0000', '0.0000'),
(1297, 160, 5010, 10112, '0.0000', '0.0000'),
(1298, 160, 5011, 10117, '0.0000', '0.0000'),
(1299, 160, 5016, 10132, '0.0000', '0.0000'),
(1300, 161, 5001, 10001, '0.0000', '0.0000'),
(1301, 161, 5002, 10005, '0.0000', '0.0000'),
(1302, 161, 5002, 10011, '0.0000', '0.0000'),
(1303, 161, 5003, 10063, '0.0000', '0.0000'),
(1304, 161, 5004, 10066, '0.0000', '0.0000'),
(1305, 161, 5007, 10084, '0.0000', '0.0000'),
(1306, 161, 5007, 10085, '0.0000', '0.0000'),
(1307, 161, 5007, 10086, '0.0000', '0.0000'),
(1308, 161, 5007, 10087, '0.0000', '0.0000'),
(1309, 161, 5007, 10088, '0.0000', '0.0000'),
(1310, 161, 5009, 10110, '0.0000', '0.0000'),
(1311, 161, 5010, 10112, '0.0000', '0.0000'),
(1312, 161, 5011, 10117, '0.0000', '0.0000'),
(1313, 161, 5016, 10148, '0.0000', '0.0000'),
(1314, 162, 5001, 10001, '0.0000', '0.0000'),
(1315, 162, 5002, 10005, '0.0000', '0.0000'),
(1316, 162, 5002, 10010, '0.0000', '0.0000'),
(1317, 162, 5003, 10063, '0.0000', '0.0000'),
(1318, 162, 5004, 10066, '0.0000', '0.0000'),
(1319, 162, 5007, 10085, '0.0000', '0.0000'),
(1320, 162, 5007, 10088, '0.0000', '0.0000'),
(1321, 162, 5009, 10110, '0.0000', '0.0000'),
(1322, 162, 5010, 10112, '0.0000', '0.0000'),
(1323, 162, 5011, 10116, '0.0000', '0.0000'),
(1324, 162, 5016, 10148, '0.0000', '0.0000'),
(1325, 163, 5001, 10001, '0.0000', '0.0000'),
(1326, 163, 5002, 10005, '0.0000', '0.0000'),
(1327, 163, 5002, 10011, '0.0000', '0.0000'),
(1328, 163, 5003, 10063, '0.0000', '0.0000'),
(1329, 163, 5004, 10066, '0.0000', '0.0000'),
(1330, 163, 5007, 10084, '0.0000', '0.0000'),
(1331, 163, 5007, 10085, '0.0000', '0.0000'),
(1332, 163, 5007, 10086, '0.0000', '0.0000'),
(1333, 163, 5007, 10087, '0.0000', '0.0000'),
(1334, 163, 5007, 10088, '0.0000', '0.0000'),
(1335, 163, 5009, 10110, '0.0000', '0.0000'),
(1336, 163, 5010, 10112, '0.0000', '0.0000'),
(1337, 163, 5011, 10117, '0.0000', '0.0000'),
(1338, 163, 5016, 10141, '0.0000', '0.0000'),
(1339, 165, 5001, 10001, '0.0000', '0.0000'),
(1340, 165, 5002, 10005, '0.0000', '0.0000'),
(1341, 165, 5002, 10011, '0.0000', '0.0000'),
(1342, 165, 5003, 10063, '0.0000', '0.0000'),
(1343, 165, 5004, 10066, '0.0000', '0.0000'),
(1344, 165, 5007, 10086, '0.0000', '0.0000'),
(1345, 165, 5007, 10088, '0.0000', '0.0000'),
(1346, 165, 5009, 10110, '0.0000', '0.0000'),
(1347, 165, 5010, 10112, '0.0000', '0.0000'),
(1348, 165, 5011, 10117, '0.0000', '0.0000'),
(1349, 165, 5016, 10138, '0.0000', '0.0000'),
(1350, 166, 5001, 10001, '0.0000', '0.0000'),
(1351, 166, 5002, 10005, '0.0000', '0.0000'),
(1352, 166, 5002, 10010, '0.0000', '0.0000'),
(1353, 166, 5003, 10063, '0.0000', '0.0000'),
(1354, 166, 5004, 10066, '0.0000', '0.0000'),
(1355, 166, 5007, 10085, '0.0000', '0.0000'),
(1356, 166, 5009, 10110, '0.0000', '0.0000'),
(1357, 166, 5010, 10112, '0.0000', '0.0000'),
(1358, 166, 5011, 10117, '0.0000', '0.0000'),
(1359, 166, 5016, 10148, '0.0000', '0.0000'),
(1360, 167, 5001, 10001, '0.0000', '0.0000'),
(1361, 167, 5002, 10005, '0.0000', '0.0000'),
(1362, 167, 5002, 10010, '0.0000', '0.0000'),
(1363, 167, 5003, 10063, '0.0000', '0.0000'),
(1364, 167, 5004, 10066, '0.0000', '0.0000'),
(1365, 167, 5007, 10088, '0.0000', '0.0000'),
(1366, 167, 5009, 10110, '0.0000', '0.0000'),
(1367, 167, 5010, 10112, '0.0000', '0.0000'),
(1368, 167, 5011, 10116, '0.0000', '0.0000'),
(1369, 167, 5016, 10141, '0.0000', '0.0000'),
(1370, 168, 5001, 10001, '0.0000', '0.0000'),
(1371, 168, 5002, 10005, '0.0000', '0.0000'),
(1372, 168, 5002, 10011, '0.0000', '0.0000'),
(1373, 168, 5003, 10063, '0.0000', '0.0000'),
(1374, 168, 5004, 10066, '0.0000', '0.0000'),
(1375, 168, 5007, 10084, '0.0000', '0.0000'),
(1376, 168, 5007, 10086, '0.0000', '0.0000'),
(1377, 168, 5007, 10087, '0.0000', '0.0000'),
(1378, 168, 5009, 10110, '0.0000', '0.0000'),
(1379, 168, 5010, 10112, '0.0000', '0.0000'),
(1380, 168, 5011, 10117, '0.0000', '0.0000'),
(1381, 168, 5016, 10143, '0.0000', '0.0000'),
(1382, 169, 5001, 10001, '0.0000', '0.0000'),
(1383, 169, 5002, 10005, '0.0000', '0.0000'),
(1384, 169, 5002, 10011, '0.0000', '0.0000'),
(1385, 169, 5003, 10063, '0.0000', '0.0000'),
(1386, 169, 5004, 10066, '0.0000', '0.0000'),
(1387, 169, 5007, 10088, '0.0000', '0.0000'),
(1388, 169, 5009, 10110, '0.0000', '0.0000'),
(1389, 169, 5010, 10112, '0.0000', '0.0000'),
(1390, 169, 5011, 10117, '0.0000', '0.0000'),
(1391, 169, 5016, 10138, '0.0000', '0.0000'),
(1392, 170, 5001, 10001, '0.0000', '0.0000'),
(1393, 170, 5002, 10005, '0.0000', '0.0000'),
(1394, 170, 5002, 10011, '0.0000', '0.0000'),
(1395, 170, 5003, 10063, '0.0000', '0.0000'),
(1396, 170, 5004, 10066, '0.0000', '0.0000'),
(1397, 170, 5007, 10084, '0.0000', '0.0000'),
(1398, 170, 5007, 10085, '0.0000', '0.0000'),
(1399, 170, 5007, 10086, '0.0000', '0.0000'),
(1400, 170, 5007, 10087, '0.0000', '0.0000'),
(1401, 170, 5007, 10088, '0.0000', '0.0000'),
(1402, 170, 5007, 10091, '0.0000', '0.0000'),
(1403, 170, 5009, 10110, '0.0000', '0.0000'),
(1404, 170, 5010, 10112, '0.0000', '0.0000'),
(1405, 170, 5011, 10117, '0.0000', '0.0000'),
(1406, 170, 5016, 10138, '0.0000', '0.0000'),
(1407, 171, 5001, 10001, '0.0000', '0.0000'),
(1408, 171, 5002, 10005, '0.0000', '0.0000'),
(1409, 171, 5002, 10010, '0.0000', '0.0000'),
(1410, 171, 5003, 10063, '0.0000', '0.0000'),
(1411, 171, 5004, 10066, '0.0000', '0.0000'),
(1412, 171, 5007, 10084, '0.0000', '0.0000'),
(1413, 171, 5007, 10086, '0.0000', '0.0000'),
(1414, 171, 5007, 10088, '0.0000', '0.0000'),
(1415, 171, 5009, 10111, '0.0000', '0.0000'),
(1416, 171, 5010, 10112, '0.0000', '0.0000'),
(1417, 171, 5011, 10116, '0.0000', '0.0000'),
(1418, 171, 5016, 10141, '0.0000', '0.0000'),
(1419, 172, 5001, 10001, '0.0000', '0.0000'),
(1420, 172, 5002, 10005, '0.0000', '0.0000'),
(1421, 172, 5002, 10022, '0.0000', '0.0000'),
(1422, 172, 5003, 10063, '0.0000', '0.0000'),
(1423, 172, 5004, 10066, '0.0000', '0.0000'),
(1424, 172, 5007, 10084, '0.0000', '0.0000'),
(1425, 172, 5007, 10085, '0.0000', '0.0000'),
(1426, 172, 5009, 10111, '0.0000', '0.0000'),
(1427, 172, 5010, 10112, '0.0000', '0.0000'),
(1428, 172, 5011, 10116, '0.0000', '0.0000'),
(1429, 172, 5016, 10141, '0.0000', '0.0000'),
(1430, 173, 5001, 10001, '0.0000', '0.0000'),
(1431, 173, 5002, 10005, '0.0000', '0.0000'),
(1432, 173, 5002, 10011, '0.0000', '0.0000'),
(1433, 173, 5003, 10063, '0.0000', '0.0000'),
(1434, 173, 5004, 10066, '0.0000', '0.0000'),
(1435, 173, 5007, 10088, '0.0000', '0.0000'),
(1436, 173, 5009, 10111, '0.0000', '0.0000'),
(1437, 173, 5010, 10112, '0.0000', '0.0000'),
(1438, 173, 5011, 10116, '0.0000', '0.0000'),
(1439, 173, 5016, 10141, '0.0000', '0.0000'),
(1440, 174, 5001, 10001, '0.0000', '0.0000'),
(1441, 174, 5002, 10005, '0.0000', '0.0000'),
(1442, 174, 5002, 10010, '0.0000', '0.0000'),
(1443, 174, 5003, 10063, '0.0000', '0.0000'),
(1444, 174, 5004, 10066, '0.0000', '0.0000'),
(1445, 174, 5007, 10084, '0.0000', '0.0000'),
(1446, 174, 5007, 10086, '0.0000', '0.0000'),
(1447, 174, 5009, 10111, '0.0000', '0.0000'),
(1448, 174, 5010, 10112, '0.0000', '0.0000'),
(1449, 174, 5011, 10116, '0.0000', '0.0000'),
(1450, 174, 5016, 10141, '0.0000', '0.0000'),
(1451, 175, 5001, 10001, '0.0000', '0.0000'),
(1452, 175, 5002, 10005, '0.0000', '0.0000'),
(1453, 175, 5002, 10010, '0.0000', '0.0000'),
(1454, 175, 5003, 10063, '0.0000', '0.0000'),
(1455, 175, 5004, 10066, '0.0000', '0.0000'),
(1456, 175, 5007, 10084, '0.0000', '0.0000'),
(1457, 175, 5007, 10086, '0.0000', '0.0000'),
(1458, 175, 5007, 10088, '0.0000', '0.0000'),
(1459, 175, 5009, 10111, '0.0000', '0.0000'),
(1460, 175, 5010, 10112, '0.0000', '0.0000'),
(1461, 175, 5011, 10116, '0.0000', '0.0000'),
(1462, 175, 5016, 10132, '0.0000', '0.0000'),
(1463, 176, 5001, 10001, '0.0000', '0.0000'),
(1464, 176, 5002, 10005, '0.0000', '0.0000'),
(1465, 176, 5002, 10011, '0.0000', '0.0000'),
(1466, 176, 5003, 10063, '0.0000', '0.0000'),
(1467, 176, 5004, 10066, '0.0000', '0.0000'),
(1468, 176, 5007, 10084, '0.0000', '0.0000'),
(1469, 176, 5007, 10085, '0.0000', '0.0000'),
(1470, 176, 5007, 10087, '0.0000', '0.0000'),
(1471, 176, 5007, 10088, '0.0000', '0.0000'),
(1472, 176, 5009, 10111, '0.0000', '0.0000'),
(1473, 176, 5010, 10112, '0.0000', '0.0000'),
(1474, 176, 5011, 10117, '0.0000', '0.0000'),
(1475, 176, 5016, 10136, '0.0000', '0.0000'),
(1476, 177, 5001, 10001, '0.0000', '0.0000'),
(1477, 177, 5002, 10005, '0.0000', '0.0000'),
(1478, 177, 5002, 10010, '0.0000', '0.0000'),
(1479, 177, 5003, 10063, '0.0000', '0.0000'),
(1480, 177, 5004, 10066, '0.0000', '0.0000'),
(1481, 177, 5007, 10084, '0.0000', '0.0000'),
(1482, 177, 5007, 10085, '0.0000', '0.0000'),
(1483, 177, 5007, 10086, '0.0000', '0.0000'),
(1484, 177, 5007, 10088, '0.0000', '0.0000'),
(1485, 177, 5009, 10111, '0.0000', '0.0000'),
(1486, 177, 5010, 10112, '0.0000', '0.0000'),
(1487, 177, 5011, 10116, '0.0000', '0.0000'),
(1488, 177, 5016, 10149, '0.0000', '0.0000'),
(1489, 178, 5001, 10001, '0.0000', '0.0000'),
(1490, 178, 5002, 10005, '0.0000', '0.0000'),
(1491, 178, 5002, 10011, '0.0000', '0.0000'),
(1492, 178, 5003, 10063, '0.0000', '0.0000'),
(1493, 178, 5004, 10066, '0.0000', '0.0000'),
(1494, 178, 5007, 10084, '0.0000', '0.0000'),
(1495, 178, 5007, 10085, '0.0000', '0.0000'),
(1496, 178, 5007, 10086, '0.0000', '0.0000'),
(1497, 178, 5007, 10087, '0.0000', '0.0000'),
(1498, 178, 5007, 10088, '0.0000', '0.0000'),
(1499, 178, 5009, 10111, '0.0000', '0.0000'),
(1500, 178, 5010, 10112, '0.0000', '0.0000'),
(1501, 178, 5011, 10117, '0.0000', '0.0000'),
(1502, 178, 5016, 10144, '0.0000', '0.0000'),
(1503, 179, 5001, 10001, '0.0000', '0.0000'),
(1504, 179, 5002, 10005, '0.0000', '0.0000'),
(1505, 179, 5002, 10010, '0.0000', '0.0000'),
(1506, 179, 5003, 10063, '0.0000', '0.0000'),
(1507, 179, 5004, 10066, '0.0000', '0.0000'),
(1508, 179, 5007, 10084, '0.0000', '0.0000'),
(1509, 179, 5007, 10085, '0.0000', '0.0000'),
(1510, 179, 5007, 10086, '0.0000', '0.0000'),
(1511, 179, 5009, 10111, '0.0000', '0.0000'),
(1512, 179, 5010, 10112, '0.0000', '0.0000'),
(1513, 179, 5011, 10116, '0.0000', '0.0000'),
(1514, 179, 5016, 10150, '0.0000', '0.0000'),
(1515, 180, 5001, 10002, '0.0000', '0.0000'),
(1516, 180, 5002, 10005, '0.0000', '0.0000'),
(1517, 180, 5002, 10023, '0.0000', '0.0000'),
(1518, 180, 5003, 10063, '0.0000', '0.0000'),
(1519, 180, 5004, 10066, '0.0000', '0.0000'),
(1520, 180, 5007, 10084, '0.0000', '0.0000'),
(1521, 180, 5007, 10085, '0.0000', '0.0000'),
(1522, 180, 5009, 10111, '0.0000', '0.0000'),
(1523, 180, 5010, 10112, '0.0000', '0.0000'),
(1524, 180, 5011, 10117, '0.0000', '0.0000'),
(1525, 180, 5016, 10150, '0.0000', '0.0000'),
(1526, 181, 5001, 10001, '0.0000', '0.0000'),
(1527, 181, 5002, 10005, '0.0000', '0.0000'),
(1528, 181, 5002, 10024, '0.0000', '0.0000'),
(1529, 181, 5003, 10064, '0.0000', '0.0000'),
(1530, 181, 5004, 10065, '0.0000', '0.0000'),
(1531, 181, 5007, 10082, '0.0000', '0.0000'),
(1532, 181, 5007, 10083, '0.0000', '0.0000'),
(1533, 181, 5007, 10092, '0.0000', '0.0000'),
(1534, 181, 5007, 10093, '0.0000', '0.0000'),
(1535, 181, 5016, 10141, '0.0000', '0.0000'),
(1536, 181, 5016, 10143, '0.0000', '0.0000'),
(1537, 182, 5001, 10001, '0.0000', '0.0000'),
(1538, 182, 5002, 10005, '0.0000', '0.0000'),
(1539, 182, 5002, 10024, '0.0000', '0.0000'),
(1540, 182, 5003, 10063, '0.0000', '0.0000'),
(1541, 182, 5004, 10065, '0.0000', '0.0000'),
(1542, 182, 5007, 10081, '0.0000', '0.0000'),
(1543, 182, 5007, 10082, '0.0000', '0.0000'),
(1544, 182, 5016, 10134, '0.0000', '0.0000'),
(1545, 183, 5001, 10001, '0.0000', '0.0000'),
(1546, 183, 5002, 10005, '0.0000', '0.0000'),
(1547, 183, 5002, 10025, '0.0000', '0.0000'),
(1548, 183, 5003, 10063, '0.0000', '0.0000'),
(1549, 183, 5004, 10065, '0.0000', '0.0000'),
(1550, 183, 5006, 10069, '0.0000', '0.0000'),
(1551, 183, 5007, 10072, '0.0000', '0.0000'),
(1552, 183, 5007, 10073, '0.0000', '0.0000'),
(1553, 183, 5007, 10074, '0.0000', '0.0000'),
(1554, 183, 5007, 10075, '0.0000', '0.0000'),
(1555, 183, 5007, 10076, '0.0000', '0.0000'),
(1556, 183, 5007, 10077, '0.0000', '0.0000'),
(1557, 183, 5007, 10094, '0.0000', '0.0000'),
(1558, 183, 5016, 10135, '0.0000', '0.0000'),
(1559, 184, 5001, 10001, '0.0000', '0.0000'),
(1560, 184, 5002, 10005, '0.0000', '0.0000'),
(1561, 184, 5002, 10025, '0.0000', '0.0000'),
(1562, 184, 5003, 10063, '0.0000', '0.0000'),
(1563, 184, 5004, 10065, '0.0000', '0.0000'),
(1564, 184, 5006, 10069, '0.0000', '0.0000'),
(1565, 184, 5007, 10073, '0.0000', '0.0000'),
(1566, 184, 5007, 10074, '0.0000', '0.0000'),
(1567, 184, 5007, 10075, '0.0000', '0.0000'),
(1568, 184, 5007, 10094, '0.0000', '0.0000'),
(1569, 184, 5007, 10095, '0.0000', '0.0000'),
(1570, 184, 5016, 10135, '0.0000', '0.0000'),
(1571, 185, 5001, 10001, '0.0000', '0.0000'),
(1572, 185, 5002, 10005, '0.0000', '0.0000'),
(1573, 185, 5002, 10006, '0.0000', '0.0000'),
(1574, 185, 5003, 10063, '0.0000', '0.0000'),
(1575, 185, 5004, 10065, '0.0000', '0.0000'),
(1576, 185, 5005, 10068, '0.0000', '0.0000'),
(1577, 185, 5006, 10069, '0.0000', '0.0000'),
(1578, 185, 5007, 10094, '0.0000', '0.0000'),
(1579, 185, 5016, 10136, '0.0000', '0.0000'),
(1580, 186, 5001, 10001, '0.0000', '0.0000'),
(1581, 186, 5002, 10005, '0.0000', '0.0000'),
(1582, 186, 5002, 10006, '0.0000', '0.0000'),
(1583, 186, 5003, 10063, '0.0000', '0.0000'),
(1584, 186, 5004, 10065, '0.0000', '0.0000'),
(1585, 186, 5005, 10068, '0.0000', '0.0000'),
(1586, 186, 5006, 10069, '0.0000', '0.0000'),
(1587, 186, 5007, 10073, '0.0000', '0.0000'),
(1588, 186, 5007, 10094, '0.0000', '0.0000'),
(1589, 186, 5016, 10135, '0.0000', '0.0000'),
(1590, 187, 5001, 10001, '0.0000', '0.0000'),
(1591, 187, 5002, 10005, '0.0000', '0.0000'),
(1592, 187, 5002, 10006, '0.0000', '0.0000'),
(1593, 187, 5003, 10063, '0.0000', '0.0000'),
(1594, 187, 5004, 10065, '0.0000', '0.0000'),
(1595, 187, 5005, 10068, '0.0000', '0.0000'),
(1596, 187, 5006, 10069, '0.0000', '0.0000'),
(1597, 187, 5007, 10074, '0.0000', '0.0000'),
(1598, 187, 5016, 10136, '0.0000', '0.0000'),
(1599, 188, 5001, 10001, '0.0000', '0.0000'),
(1600, 188, 5002, 10010, '0.0000', '0.0000'),
(1601, 188, 5003, 10063, '0.0000', '0.0000'),
(1602, 188, 5004, 10066, '0.0000', '0.0000'),
(1603, 188, 5007, 10084, '0.0000', '0.0000'),
(1604, 188, 5007, 10085, '0.0000', '0.0000'),
(1605, 188, 5007, 10086, '0.0000', '0.0000'),
(1606, 188, 5007, 10087, '0.0000', '0.0000'),
(1607, 188, 5007, 10088, '0.0000', '0.0000'),
(1608, 188, 5009, 10110, '0.0000', '0.0000'),
(1609, 188, 5010, 10112, '0.0000', '0.0000'),
(1610, 188, 5011, 10117, '0.0000', '0.0000'),
(1611, 188, 5016, 10138, '0.0000', '0.0000'),
(1612, 189, 5001, 10001, '0.0000', '0.0000'),
(1613, 189, 5002, 10010, '0.0000', '0.0000'),
(1614, 189, 5003, 10063, '0.0000', '0.0000'),
(1615, 189, 5004, 10066, '0.0000', '0.0000'),
(1616, 189, 5007, 10084, '0.0000', '0.0000'),
(1617, 189, 5007, 10085, '0.0000', '0.0000'),
(1618, 189, 5007, 10086, '0.0000', '0.0000'),
(1619, 189, 5007, 10087, '0.0000', '0.0000'),
(1620, 189, 5007, 10088, '0.0000', '0.0000'),
(1621, 189, 5009, 10110, '0.0000', '0.0000'),
(1622, 189, 5010, 10112, '0.0000', '0.0000'),
(1623, 189, 5011, 10117, '0.0000', '0.0000'),
(1624, 189, 5016, 10138, '0.0000', '0.0000'),
(1625, 190, 5001, 10001, '0.0000', '0.0000'),
(1626, 190, 5002, 10005, '0.0000', '0.0000'),
(1627, 190, 5002, 10026, '0.0000', '0.0000'),
(1628, 190, 5003, 10063, '0.0000', '0.0000'),
(1629, 190, 5004, 10065, '0.0000', '0.0000'),
(1630, 190, 5007, 10084, '0.0000', '0.0000'),
(1631, 190, 5016, 10134, '0.0000', '0.0000'),
(1632, 191, 5001, 10001, '0.0000', '0.0000'),
(1633, 191, 5002, 10005, '0.0000', '0.0000'),
(1634, 191, 5002, 10026, '0.0000', '0.0000'),
(1635, 191, 5003, 10063, '0.0000', '0.0000'),
(1636, 191, 5004, 10065, '0.0000', '0.0000'),
(1637, 191, 5007, 10084, '0.0000', '0.0000'),
(1638, 191, 5016, 10135, '0.0000', '0.0000'),
(1639, 192, 5001, 10001, '0.0000', '0.0000'),
(1640, 192, 5002, 10005, '0.0000', '0.0000'),
(1641, 192, 5002, 10027, '0.0000', '0.0000'),
(1642, 192, 5003, 10063, '0.0000', '0.0000'),
(1643, 192, 5004, 10065, '0.0000', '0.0000'),
(1644, 192, 5007, 10084, '0.0000', '0.0000'),
(1645, 192, 5007, 10088, '0.0000', '0.0000'),
(1646, 192, 5016, 10133, '0.0000', '0.0000'),
(1647, 192, 5016, 10138, '0.0000', '0.0000'),
(1648, 193, 5001, 10001, '0.0000', '0.0000'),
(1649, 193, 5002, 10005, '0.0000', '0.0000'),
(1650, 193, 5002, 10026, '0.0000', '0.0000'),
(1651, 193, 5003, 10063, '0.0000', '0.0000'),
(1652, 193, 5004, 10065, '0.0000', '0.0000'),
(1653, 193, 5007, 10085, '0.0000', '0.0000'),
(1654, 193, 5016, 10133, '0.0000', '0.0000'),
(1655, 194, 5001, 10001, '0.0000', '0.0000'),
(1656, 194, 5002, 10005, '0.0000', '0.0000'),
(1657, 194, 5002, 10028, '0.0000', '0.0000'),
(1658, 194, 5003, 10063, '0.0000', '0.0000'),
(1659, 194, 5004, 10065, '0.0000', '0.0000'),
(1660, 194, 5007, 10084, '0.0000', '0.0000'),
(1661, 194, 5016, 10135, '0.0000', '0.0000'),
(1662, 195, 5001, 10001, '0.0000', '0.0000'),
(1663, 195, 5002, 10005, '0.0000', '0.0000'),
(1664, 195, 5002, 10029, '0.0000', '0.0000'),
(1665, 195, 5003, 10063, '0.0000', '0.0000'),
(1666, 195, 5004, 10065, '0.0000', '0.0000'),
(1667, 195, 5007, 10086, '0.0000', '0.0000'),
(1668, 195, 5016, 10134, '0.0000', '0.0000'),
(1669, 196, 5001, 10001, '0.0000', '0.0000'),
(1670, 196, 5002, 10005, '0.0000', '0.0000'),
(1671, 196, 5002, 10030, '0.0000', '0.0000'),
(1672, 196, 5003, 10063, '0.0000', '0.0000'),
(1673, 196, 5004, 10065, '0.0000', '0.0000'),
(1674, 196, 5007, 10088, '0.0000', '0.0000'),
(1675, 196, 5016, 10134, '0.0000', '0.0000'),
(1676, 197, 5001, 10001, '0.0000', '0.0000'),
(1677, 197, 5002, 10005, '0.0000', '0.0000'),
(1678, 197, 5002, 10030, '0.0000', '0.0000'),
(1679, 197, 5003, 10063, '0.0000', '0.0000'),
(1680, 197, 5004, 10065, '0.0000', '0.0000'),
(1681, 197, 5007, 10084, '0.0000', '0.0000'),
(1682, 197, 5007, 10085, '0.0000', '0.0000'),
(1683, 197, 5007, 10088, '0.0000', '0.0000'),
(1684, 197, 5016, 10131, '0.0000', '0.0000'),
(1685, 197, 5016, 10134, '0.0000', '0.0000'),
(1686, 197, 5016, 10136, '0.0000', '0.0000'),
(1687, 198, 5001, 10001, '0.0000', '0.0000'),
(1688, 198, 5002, 10005, '0.0000', '0.0000'),
(1689, 198, 5002, 10031, '0.0000', '0.0000'),
(1690, 198, 5003, 10063, '0.0000', '0.0000'),
(1691, 198, 5004, 10065, '0.0000', '0.0000'),
(1692, 198, 5007, 10084, '0.0000', '0.0000'),
(1693, 198, 5007, 10086, '0.0000', '0.0000'),
(1694, 198, 5007, 10088, '0.0000', '0.0000'),
(1695, 198, 5016, 10136, '0.0000', '0.0000'),
(1696, 199, 5001, 10001, '0.0000', '0.0000'),
(1697, 199, 5002, 10005, '0.0000', '0.0000'),
(1698, 199, 5002, 10014, '0.0000', '0.0000'),
(1699, 199, 5003, 10063, '0.0000', '0.0000'),
(1700, 199, 5004, 10065, '0.0000', '0.0000'),
(1701, 199, 5007, 10085, '0.0000', '0.0000'),
(1702, 199, 5007, 10086, '0.0000', '0.0000'),
(1703, 199, 5007, 10088, '0.0000', '0.0000'),
(1704, 199, 5007, 10089, '0.0000', '0.0000'),
(1705, 199, 5016, 10136, '0.0000', '0.0000'),
(1706, 199, 5016, 10142, '0.0000', '0.0000'),
(1707, 200, 5001, 10001, '0.0000', '0.0000'),
(1708, 200, 5002, 10005, '0.0000', '0.0000'),
(1709, 200, 5002, 10027, '0.0000', '0.0000'),
(1710, 200, 5003, 10063, '0.0000', '0.0000'),
(1711, 200, 5004, 10065, '0.0000', '0.0000'),
(1712, 200, 5007, 10084, '0.0000', '0.0000'),
(1713, 200, 5016, 10134, '0.0000', '0.0000'),
(1714, 201, 5001, 10001, '0.0000', '0.0000'),
(1715, 201, 5002, 10005, '0.0000', '0.0000'),
(1716, 201, 5002, 10026, '0.0000', '0.0000'),
(1717, 201, 5003, 10063, '0.0000', '0.0000'),
(1718, 201, 5004, 10065, '0.0000', '0.0000'),
(1719, 201, 5007, 10085, '0.0000', '0.0000'),
(1720, 201, 5016, 10136, '0.0000', '0.0000'),
(1721, 202, 5001, 10001, '0.0000', '0.0000'),
(1722, 202, 5002, 10005, '0.0000', '0.0000'),
(1723, 202, 5002, 10035, '0.0000', '0.0000'),
(1724, 202, 5003, 10063, '0.0000', '0.0000'),
(1725, 202, 5004, 10065, '0.0000', '0.0000'),
(1726, 202, 5007, 10084, '0.0000', '0.0000'),
(1727, 202, 5007, 10085, '0.0000', '0.0000'),
(1728, 202, 5007, 10087, '0.0000', '0.0000'),
(1729, 202, 5007, 10088, '0.0000', '0.0000'),
(1730, 202, 5016, 10136, '0.0000', '0.0000'),
(1731, 202, 5016, 10138, '0.0000', '0.0000'),
(1732, 203, 5001, 10001, '0.0000', '0.0000'),
(1733, 203, 5002, 10005, '0.0000', '0.0000'),
(1734, 203, 5002, 10032, '0.0000', '0.0000'),
(1735, 203, 5003, 10063, '0.0000', '0.0000'),
(1736, 203, 5004, 10065, '0.0000', '0.0000'),
(1737, 203, 5007, 10084, '0.0000', '0.0000'),
(1738, 203, 5007, 10085, '0.0000', '0.0000'),
(1739, 203, 5007, 10087, '0.0000', '0.0000'),
(1740, 203, 5016, 10136, '0.0000', '0.0000'),
(1741, 204, 5001, 10001, '0.0000', '0.0000'),
(1742, 204, 5003, 10063, '0.0000', '0.0000'),
(1743, 204, 5004, 10065, '0.0000', '0.0000'),
(1744, 204, 5007, 10096, '0.0000', '0.0000'),
(1745, 204, 5016, 10131, '0.0000', '0.0000'),
(1746, 205, 5001, 10001, '0.0000', '0.0000'),
(1747, 205, 5003, 10063, '0.0000', '0.0000'),
(1748, 205, 5004, 10065, '0.0000', '0.0000'),
(1749, 205, 5007, 10085, '0.0000', '0.0000'),
(1750, 205, 5007, 10086, '0.0000', '0.0000'),
(1751, 205, 5016, 10133, '0.0000', '0.0000'),
(1752, 206, 5001, 10001, '0.0000', '0.0000'),
(1753, 206, 5002, 10005, '0.0000', '0.0000'),
(1754, 206, 5002, 10028, '0.0000', '0.0000'),
(1755, 206, 5003, 10063, '0.0000', '0.0000'),
(1756, 206, 5004, 10065, '0.0000', '0.0000'),
(1757, 206, 5007, 10084, '0.0000', '0.0000'),
(1758, 206, 5016, 10140, '0.0000', '0.0000'),
(1759, 207, 5001, 10001, '0.0000', '0.0000'),
(1760, 207, 5002, 10005, '0.0000', '0.0000'),
(1761, 207, 5002, 10026, '0.0000', '0.0000'),
(1762, 207, 5003, 10063, '0.0000', '0.0000'),
(1763, 207, 5004, 10065, '0.0000', '0.0000'),
(1764, 207, 5007, 10084, '0.0000', '0.0000'),
(1765, 207, 5016, 10138, '0.0000', '0.0000'),
(1766, 208, 5001, 10001, '0.0000', '0.0000'),
(1767, 208, 5002, 10005, '0.0000', '0.0000'),
(1768, 208, 5002, 10026, '0.0000', '0.0000'),
(1769, 208, 5003, 10063, '0.0000', '0.0000'),
(1770, 208, 5004, 10065, '0.0000', '0.0000'),
(1771, 208, 5007, 10084, '0.0000', '0.0000'),
(1772, 208, 5016, 10133, '0.0000', '0.0000'),
(1773, 209, 5001, 10001, '0.0000', '0.0000'),
(1774, 209, 5002, 10005, '0.0000', '0.0000'),
(1775, 209, 5002, 10014, '0.0000', '0.0000'),
(1776, 209, 5003, 10063, '0.0000', '0.0000'),
(1777, 209, 5004, 10065, '0.0000', '0.0000'),
(1778, 209, 5007, 10084, '0.0000', '0.0000'),
(1779, 209, 5007, 10085, '0.0000', '0.0000'),
(1780, 209, 5007, 10089, '0.0000', '0.0000'),
(1781, 209, 5016, 10133, '0.0000', '0.0000'),
(1782, 209, 5016, 10135, '0.0000', '0.0000'),
(1783, 209, 5016, 10141, '0.0000', '0.0000'),
(1784, 210, 5001, 10001, '0.0000', '0.0000'),
(1785, 210, 5002, 10005, '0.0000', '0.0000'),
(1786, 210, 5002, 10009, '0.0000', '0.0000'),
(1787, 210, 5003, 10063, '0.0000', '0.0000'),
(1788, 210, 5004, 10065, '0.0000', '0.0000'),
(1789, 210, 5007, 10084, '0.0000', '0.0000'),
(1790, 210, 5007, 10088, '0.0000', '0.0000'),
(1791, 210, 5016, 10135, '0.0000', '0.0000'),
(1792, 211, 5001, 10001, '0.0000', '0.0000'),
(1793, 211, 5002, 10005, '0.0000', '0.0000'),
(1794, 211, 5002, 10039, '0.0000', '0.0000'),
(1795, 211, 5003, 10063, '0.0000', '0.0000'),
(1796, 211, 5004, 10065, '0.0000', '0.0000'),
(1797, 211, 5007, 10085, '0.0000', '0.0000'),
(1798, 211, 5007, 10086, '0.0000', '0.0000'),
(1799, 211, 5007, 10088, '0.0000', '0.0000'),
(1800, 211, 5007, 10089, '0.0000', '0.0000'),
(1801, 211, 5007, 10090, '0.0000', '0.0000'),
(1802, 211, 5016, 10135, '0.0000', '0.0000'),
(1803, 211, 5016, 10141, '0.0000', '0.0000'),
(1804, 211, 5016, 10142, '0.0000', '0.0000'),
(1805, 212, 5001, 10001, '0.0000', '0.0000'),
(1806, 212, 5002, 10012, '0.0000', '0.0000'),
(1807, 212, 5003, 10063, '0.0000', '0.0000'),
(1808, 212, 5004, 10065, '0.0000', '0.0000'),
(1809, 212, 5007, 10087, '0.0000', '0.0000'),
(1810, 212, 5016, 10133, '0.0000', '0.0000'),
(1811, 213, 5001, 10001, '0.0000', '0.0000'),
(1812, 213, 5002, 10005, '0.0000', '0.0000'),
(1813, 213, 5002, 10026, '0.0000', '0.0000'),
(1814, 213, 5003, 10063, '0.0000', '0.0000'),
(1815, 213, 5004, 10065, '0.0000', '0.0000'),
(1816, 213, 5007, 10084, '0.0000', '0.0000'),
(1817, 213, 5016, 10132, '0.0000', '0.0000'),
(1818, 214, 5001, 10001, '0.0000', '0.0000'),
(1819, 214, 5002, 10005, '0.0000', '0.0000'),
(1820, 214, 5002, 10032, '0.0000', '0.0000'),
(1821, 214, 5003, 10063, '0.0000', '0.0000'),
(1822, 214, 5004, 10065, '0.0000', '0.0000'),
(1823, 214, 5007, 10087, '0.0000', '0.0000'),
(1824, 214, 5016, 10132, '0.0000', '0.0000'),
(1825, 214, 5016, 10136, '0.0000', '0.0000'),
(1826, 215, 5001, 10001, '0.0000', '0.0000'),
(1827, 215, 5002, 10005, '0.0000', '0.0000'),
(1828, 215, 5002, 10028, '0.0000', '0.0000'),
(1829, 215, 5003, 10063, '0.0000', '0.0000'),
(1830, 215, 5004, 10065, '0.0000', '0.0000'),
(1831, 215, 5007, 10084, '0.0000', '0.0000'),
(1832, 215, 5007, 10085, '0.0000', '0.0000'),
(1833, 215, 5007, 10086, '0.0000', '0.0000'),
(1834, 215, 5007, 10089, '0.0000', '0.0000'),
(1835, 215, 5016, 10131, '0.0000', '0.0000'),
(1836, 215, 5016, 10133, '0.0000', '0.0000'),
(1837, 215, 5016, 10144, '0.0000', '0.0000'),
(1838, 216, 5001, 10001, '0.0000', '0.0000'),
(1839, 216, 5002, 10005, '0.0000', '0.0000'),
(1840, 216, 5002, 10028, '0.0000', '0.0000'),
(1841, 216, 5003, 10063, '0.0000', '0.0000'),
(1842, 216, 5004, 10065, '0.0000', '0.0000'),
(1843, 216, 5007, 10084, '0.0000', '0.0000'),
(1844, 216, 5016, 10133, '0.0000', '0.0000'),
(1845, 217, 5001, 10001, '0.0000', '0.0000'),
(1846, 217, 5002, 10005, '0.0000', '0.0000'),
(1847, 217, 5002, 10028, '0.0000', '0.0000'),
(1848, 217, 5003, 10063, '0.0000', '0.0000'),
(1849, 217, 5004, 10065, '0.0000', '0.0000'),
(1850, 217, 5007, 10084, '0.0000', '0.0000'),
(1851, 217, 5016, 10135, '0.0000', '0.0000'),
(1852, 218, 5001, 10001, '0.0000', '0.0000'),
(1853, 218, 5002, 10005, '0.0000', '0.0000'),
(1854, 218, 5002, 10012, '0.0000', '0.0000'),
(1855, 218, 5003, 10063, '0.0000', '0.0000'),
(1856, 218, 5004, 10065, '0.0000', '0.0000'),
(1857, 218, 5007, 10084, '0.0000', '0.0000'),
(1858, 218, 5007, 10085, '0.0000', '0.0000'),
(1859, 218, 5007, 10086, '0.0000', '0.0000'),
(1860, 218, 5007, 10087, '0.0000', '0.0000'),
(1861, 218, 5007, 10088, '0.0000', '0.0000'),
(1862, 218, 5007, 10089, '0.0000', '0.0000'),
(1863, 218, 5016, 10131, '0.0000', '0.0000'),
(1864, 218, 5016, 10133, '0.0000', '0.0000'),
(1865, 218, 5016, 10134, '0.0000', '0.0000'),
(1866, 218, 5016, 10136, '0.0000', '0.0000'),
(1867, 218, 5016, 10141, '0.0000', '0.0000'),
(1868, 218, 5016, 10152, '0.0000', '0.0000'),
(1869, 219, 5001, 10001, '0.0000', '0.0000'),
(1870, 219, 5002, 10005, '0.0000', '0.0000'),
(1871, 219, 5002, 10040, '0.0000', '0.0000'),
(1872, 219, 5003, 10064, '0.0000', '0.0000'),
(1873, 219, 5004, 10065, '0.0000', '0.0000'),
(1874, 219, 5007, 10086, '0.0000', '0.0000'),
(1875, 219, 5007, 10087, '0.0000', '0.0000'),
(1876, 219, 5007, 10088, '0.0000', '0.0000'),
(1877, 219, 5016, 10146, '0.0000', '0.0000'),
(1878, 219, 5016, 10153, '0.0000', '0.0000'),
(1879, 220, 5001, 10001, '0.0000', '0.0000'),
(1880, 220, 5002, 10005, '0.0000', '0.0000'),
(1881, 220, 5002, 10028, '0.0000', '0.0000'),
(1882, 220, 5003, 10063, '0.0000', '0.0000'),
(1883, 220, 5004, 10065, '0.0000', '0.0000'),
(1884, 220, 5007, 10084, '0.0000', '0.0000'),
(1885, 220, 5007, 10085, '0.0000', '0.0000'),
(1886, 220, 5007, 10088, '0.0000', '0.0000'),
(1887, 220, 5016, 10132, '0.0000', '0.0000'),
(1888, 220, 5016, 10145, '0.0000', '0.0000'),
(1889, 221, 5001, 10001, '0.0000', '0.0000'),
(1890, 221, 5002, 10014, '0.0000', '0.0000'),
(1891, 221, 5003, 10063, '0.0000', '0.0000'),
(1892, 221, 5004, 10065, '0.0000', '0.0000'),
(1893, 221, 5007, 10084, '0.0000', '0.0000'),
(1894, 221, 5007, 10085, '0.0000', '0.0000'),
(1895, 221, 5007, 10086, '0.0000', '0.0000'),
(1896, 221, 5007, 10088, '0.0000', '0.0000'),
(1897, 221, 5016, 10133, '0.0000', '0.0000'),
(1898, 221, 5016, 10145, '0.0000', '0.0000'),
(1899, 221, 5016, 10154, '0.0000', '0.0000'),
(1900, 222, 5001, 10001, '0.0000', '0.0000'),
(1901, 222, 5002, 10005, '0.0000', '0.0000'),
(1902, 222, 5002, 10033, '0.0000', '0.0000'),
(1903, 222, 5003, 10063, '0.0000', '0.0000'),
(1904, 222, 5004, 10065, '0.0000', '0.0000'),
(1905, 222, 5007, 10084, '0.0000', '0.0000'),
(1906, 222, 5007, 10085, '0.0000', '0.0000'),
(1907, 222, 5007, 10086, '0.0000', '0.0000'),
(1908, 222, 5007, 10088, '0.0000', '0.0000'),
(1909, 222, 5016, 10133, '0.0000', '0.0000'),
(1910, 222, 5016, 10138, '0.0000', '0.0000'),
(1911, 223, 5001, 10001, '0.0000', '0.0000'),
(1912, 223, 5002, 10005, '0.0000', '0.0000'),
(1913, 223, 5002, 10026, '0.0000', '0.0000'),
(1914, 223, 5003, 10063, '0.0000', '0.0000'),
(1915, 223, 5004, 10065, '0.0000', '0.0000'),
(1916, 223, 5007, 10084, '0.0000', '0.0000'),
(1917, 223, 5007, 10085, '0.0000', '0.0000'),
(1918, 223, 5007, 10088, '0.0000', '0.0000'),
(1919, 223, 5016, 10131, '0.0000', '0.0000'),
(1920, 223, 5016, 10146, '0.0000', '0.0000'),
(1921, 224, 5001, 10001, '0.0000', '0.0000'),
(1922, 224, 5002, 10005, '0.0000', '0.0000'),
(1923, 224, 5002, 10033, '0.0000', '0.0000'),
(1924, 224, 5003, 10063, '0.0000', '0.0000'),
(1925, 224, 5004, 10065, '0.0000', '0.0000'),
(1926, 224, 5007, 10085, '0.0000', '0.0000'),
(1927, 224, 5007, 10086, '0.0000', '0.0000'),
(1928, 224, 5007, 10088, '0.0000', '0.0000'),
(1929, 224, 5016, 10133, '0.0000', '0.0000'),
(1930, 225, 5001, 10003, '0.0000', '0.0000'),
(1931, 225, 5002, 10005, '0.0000', '0.0000'),
(1932, 225, 5002, 10042, '0.0000', '0.0000'),
(1933, 225, 5003, 10063, '0.0000', '0.0000'),
(1934, 225, 5004, 10065, '0.0000', '0.0000'),
(1935, 225, 5007, 10078, '0.0000', '0.0000'),
(1936, 225, 5007, 10079, '0.0000', '0.0000'),
(1937, 225, 5007, 10081, '0.0000', '0.0000'),
(1938, 225, 5007, 10099, '0.0000', '0.0000'),
(1939, 225, 5007, 10100, '0.0000', '0.0000'),
(1940, 225, 5012, 10118, '0.0000', '0.0000'),
(1941, 225, 5013, 10120, '0.0000', '0.0000'),
(1942, 225, 5016, 10136, '0.0000', '0.0000'),
(1943, 226, 5001, 10003, '0.0000', '0.0000'),
(1944, 226, 5002, 10005, '0.0000', '0.0000'),
(1945, 226, 5002, 10043, '0.0000', '0.0000'),
(1946, 226, 5003, 10063, '0.0000', '0.0000'),
(1947, 226, 5004, 10065, '0.0000', '0.0000'),
(1948, 226, 5007, 10081, '0.0000', '0.0000'),
(1949, 226, 5007, 10082, '0.0000', '0.0000'),
(1950, 226, 5007, 10100, '0.0000', '0.0000'),
(1951, 226, 5014, 10124, '0.0000', '0.0000'),
(1952, 226, 5016, 10133, '0.0000', '0.0000'),
(1953, 227, 5001, 10003, '0.0000', '0.0000'),
(1954, 227, 5002, 10005, '0.0000', '0.0000'),
(1955, 227, 5002, 10042, '0.0000', '0.0000'),
(1956, 227, 5003, 10063, '0.0000', '0.0000'),
(1957, 227, 5004, 10065, '0.0000', '0.0000'),
(1958, 227, 5007, 10079, '0.0000', '0.0000'),
(1959, 227, 5007, 10092, '0.0000', '0.0000'),
(1960, 227, 5007, 10100, '0.0000', '0.0000'),
(1961, 227, 5014, 10124, '0.0000', '0.0000'),
(1962, 227, 5016, 10133, '0.0000', '0.0000'),
(1963, 227, 5016, 10140, '0.0000', '0.0000'),
(1964, 228, 5001, 10001, '0.0000', '0.0000'),
(1965, 228, 5002, 10005, '0.0000', '0.0000'),
(1966, 228, 5002, 10044, '0.0000', '0.0000'),
(1967, 228, 5003, 10063, '0.0000', '0.0000'),
(1968, 228, 5004, 10065, '0.0000', '0.0000'),
(1969, 228, 5007, 10085, '0.0000', '0.0000'),
(1970, 228, 5007, 10086, '0.0000', '0.0000'),
(1971, 228, 5007, 10088, '0.0000', '0.0000'),
(1972, 228, 5016, 10135, '0.0000', '0.0000'),
(1973, 229, 5001, 10003, '0.0000', '0.0000'),
(1974, 229, 5003, 10063, '0.0000', '0.0000'),
(1975, 229, 5004, 10065, '0.0000', '0.0000'),
(1976, 229, 5007, 10082, '0.0000', '0.0000'),
(1977, 229, 5007, 10100, '0.0000', '0.0000'),
(1978, 229, 5012, 10118, '0.0000', '0.0000'),
(1979, 229, 5013, 10120, '0.0000', '0.0000'),
(1980, 229, 5016, 10133, '0.0000', '0.0000'),
(1981, 230, 5001, 10003, '0.0000', '0.0000'),
(1982, 230, 5002, 10005, '0.0000', '0.0000'),
(1983, 230, 5002, 10046, '0.0000', '0.0000'),
(1984, 230, 5003, 10063, '0.0000', '0.0000'),
(1985, 230, 5004, 10065, '0.0000', '0.0000'),
(1986, 230, 5007, 10078, '0.0000', '0.0000'),
(1987, 230, 5007, 10100, '0.0000', '0.0000'),
(1988, 230, 5012, 10118, '0.0000', '0.0000'),
(1989, 230, 5013, 10120, '0.0000', '0.0000'),
(1990, 230, 5016, 10140, '0.0000', '0.0000'),
(1991, 231, 5001, 10003, '0.0000', '0.0000'),
(1992, 231, 5002, 10005, '0.0000', '0.0000'),
(1993, 231, 5002, 10042, '0.0000', '0.0000'),
(1994, 231, 5003, 10063, '0.0000', '0.0000'),
(1995, 231, 5004, 10065, '0.0000', '0.0000'),
(1996, 231, 5007, 10100, '0.0000', '0.0000'),
(1997, 231, 5012, 10118, '0.0000', '0.0000'),
(1998, 231, 5013, 10120, '0.0000', '0.0000'),
(1999, 231, 5016, 10135, '0.0000', '0.0000'),
(2000, 232, 5001, 10003, '0.0000', '0.0000'),
(2001, 232, 5002, 10005, '0.0000', '0.0000'),
(2002, 232, 5002, 10042, '0.0000', '0.0000'),
(2003, 232, 5003, 10063, '0.0000', '0.0000'),
(2004, 232, 5004, 10065, '0.0000', '0.0000'),
(2005, 232, 5007, 10079, '0.0000', '0.0000'),
(2006, 232, 5007, 10081, '0.0000', '0.0000'),
(2007, 232, 5007, 10082, '0.0000', '0.0000'),
(2008, 232, 5007, 10083, '0.0000', '0.0000'),
(2009, 232, 5007, 10092, '0.0000', '0.0000'),
(2010, 232, 5007, 10099, '0.0000', '0.0000'),
(2011, 232, 5007, 10100, '0.0000', '0.0000'),
(2012, 232, 5014, 10125, '0.0000', '0.0000'),
(2013, 232, 5016, 10133, '0.0000', '0.0000'),
(2014, 233, 5001, 10003, '0.0000', '0.0000'),
(2015, 233, 5002, 10005, '0.0000', '0.0000'),
(2016, 233, 5002, 10042, '0.0000', '0.0000'),
(2017, 233, 5003, 10063, '0.0000', '0.0000'),
(2018, 233, 5004, 10065, '0.0000', '0.0000'),
(2019, 233, 5007, 10079, '0.0000', '0.0000'),
(2020, 233, 5007, 10081, '0.0000', '0.0000'),
(2021, 233, 5007, 10082, '0.0000', '0.0000'),
(2022, 233, 5007, 10083, '0.0000', '0.0000'),
(2023, 233, 5007, 10092, '0.0000', '0.0000'),
(2024, 233, 5007, 10099, '0.0000', '0.0000'),
(2025, 233, 5007, 10100, '0.0000', '0.0000'),
(2026, 233, 5012, 10118, '0.0000', '0.0000'),
(2027, 233, 5013, 10120, '0.0000', '0.0000'),
(2028, 233, 5016, 10132, '0.0000', '0.0000'),
(2029, 233, 5016, 10135, '0.0000', '0.0000'),
(2030, 234, 5001, 10003, '0.0000', '0.0000'),
(2031, 234, 5002, 10005, '0.0000', '0.0000'),
(2032, 234, 5002, 10047, '0.0000', '0.0000'),
(2033, 234, 5003, 10063, '0.0000', '0.0000'),
(2034, 234, 5004, 10065, '0.0000', '0.0000'),
(2035, 234, 5007, 10079, '0.0000', '0.0000'),
(2036, 234, 5012, 10119, '0.0000', '0.0000'),
(2037, 234, 5013, 10121, '0.0000', '0.0000'),
(2038, 234, 5015, 10126, '0.0000', '0.0000'),
(2039, 234, 5016, 10133, '0.0000', '0.0000'),
(2040, 235, 5001, 10003, '0.0000', '0.0000'),
(2041, 235, 5002, 10005, '0.0000', '0.0000'),
(2042, 235, 5002, 10048, '0.0000', '0.0000'),
(2043, 235, 5003, 10063, '0.0000', '0.0000'),
(2044, 235, 5004, 10065, '0.0000', '0.0000'),
(2045, 235, 5007, 10086, '0.0000', '0.0000'),
(2046, 235, 5012, 10119, '0.0000', '0.0000'),
(2047, 235, 5013, 10120, '0.0000', '0.0000'),
(2048, 235, 5016, 10135, '0.0000', '0.0000'),
(2049, 236, 5001, 10003, '0.0000', '0.0000'),
(2050, 236, 5002, 10005, '0.0000', '0.0000'),
(2051, 236, 5002, 10049, '0.0000', '0.0000'),
(2052, 236, 5003, 10063, '0.0000', '0.0000'),
(2053, 236, 5004, 10065, '0.0000', '0.0000'),
(2054, 236, 5007, 10079, '0.0000', '0.0000'),
(2055, 236, 5007, 10083, '0.0000', '0.0000'),
(2056, 236, 5012, 10119, '0.0000', '0.0000'),
(2057, 236, 5013, 10122, '0.0000', '0.0000'),
(2058, 236, 5015, 10127, '0.0000', '0.0000'),
(2059, 236, 5016, 10136, '0.0000', '0.0000'),
(2060, 237, 5001, 10003, '0.0000', '0.0000'),
(2061, 237, 5002, 10005, '0.0000', '0.0000'),
(2062, 237, 5002, 10050, '0.0000', '0.0000'),
(2063, 237, 5003, 10063, '0.0000', '0.0000'),
(2064, 237, 5004, 10065, '0.0000', '0.0000'),
(2065, 237, 5007, 10079, '0.0000', '0.0000'),
(2066, 237, 5007, 10082, '0.0000', '0.0000'),
(2067, 237, 5012, 10119, '0.0000', '0.0000'),
(2068, 237, 5013, 10122, '0.0000', '0.0000'),
(2069, 237, 5015, 10126, '0.0000', '0.0000'),
(2070, 237, 5016, 10136, '0.0000', '0.0000'),
(2071, 238, 5001, 10003, '0.0000', '0.0000'),
(2072, 238, 5002, 10005, '0.0000', '0.0000'),
(2073, 238, 5002, 10042, '0.0000', '0.0000'),
(2074, 238, 5003, 10063, '0.0000', '0.0000'),
(2075, 238, 5004, 10065, '0.0000', '0.0000'),
(2076, 238, 5007, 10078, '0.0000', '0.0000'),
(2077, 238, 5007, 10082, '0.0000', '0.0000'),
(2078, 238, 5007, 10083, '0.0000', '0.0000'),
(2079, 238, 5012, 10119, '0.0000', '0.0000'),
(2080, 238, 5013, 10123, '0.0000', '0.0000'),
(2081, 238, 5015, 10126, '0.0000', '0.0000'),
(2082, 238, 5016, 10133, '0.0000', '0.0000'),
(2083, 239, 5001, 10003, '0.0000', '0.0000'),
(2084, 239, 5002, 10005, '0.0000', '0.0000'),
(2085, 239, 5002, 10051, '0.0000', '0.0000'),
(2086, 239, 5003, 10063, '0.0000', '0.0000'),
(2087, 239, 5004, 10065, '0.0000', '0.0000'),
(2088, 239, 5007, 10088, '0.0000', '0.0000'),
(2089, 239, 5012, 10119, '0.0000', '0.0000'),
(2090, 239, 5013, 10120, '0.0000', '0.0000'),
(2091, 239, 5016, 10136, '0.0000', '0.0000'),
(2092, 240, 5001, 10003, '0.0000', '0.0000'),
(2093, 240, 5002, 10052, '0.0000', '0.0000'),
(2094, 240, 5003, 10063, '0.0000', '0.0000'),
(2095, 240, 5004, 10065, '0.0000', '0.0000'),
(2096, 240, 5007, 10078, '0.0000', '0.0000'),
(2097, 240, 5012, 10119, '0.0000', '0.0000'),
(2098, 240, 5013, 10120, '0.0000', '0.0000'),
(2099, 240, 5016, 10141, '0.0000', '0.0000'),
(2100, 241, 5001, 10003, '0.0000', '0.0000'),
(2101, 241, 5002, 10005, '0.0000', '0.0000'),
(2102, 241, 5002, 10053, '0.0000', '0.0000'),
(2103, 241, 5003, 10063, '0.0000', '0.0000'),
(2104, 241, 5004, 10065, '0.0000', '0.0000'),
(2105, 241, 5007, 10078, '0.0000', '0.0000'),
(2106, 241, 5007, 10081, '0.0000', '0.0000'),
(2107, 241, 5007, 10099, '0.0000', '0.0000'),
(2108, 241, 5007, 10100, '0.0000', '0.0000'),
(2109, 241, 5012, 10119, '0.0000', '0.0000'),
(2110, 241, 5013, 10120, '0.0000', '0.0000'),
(2111, 241, 5016, 10134, '0.0000', '0.0000'),
(2112, 242, 5001, 10003, '0.0000', '0.0000'),
(2113, 242, 5002, 10005, '0.0000', '0.0000'),
(2114, 242, 5002, 10042, '0.0000', '0.0000'),
(2115, 242, 5003, 10063, '0.0000', '0.0000'),
(2116, 242, 5004, 10065, '0.0000', '0.0000'),
(2117, 242, 5007, 10079, '0.0000', '0.0000'),
(2118, 242, 5007, 10082, '0.0000', '0.0000'),
(2119, 242, 5012, 10119, '0.0000', '0.0000'),
(2120, 242, 5013, 10122, '0.0000', '0.0000'),
(2121, 242, 5015, 10129, '0.0000', '0.0000'),
(2122, 242, 5016, 10140, '0.0000', '0.0000'),
(2123, 243, 5001, 10003, '0.0000', '0.0000'),
(2124, 243, 5002, 10005, '0.0000', '0.0000'),
(2125, 243, 5002, 10048, '0.0000', '0.0000'),
(2126, 243, 5003, 10063, '0.0000', '0.0000'),
(2127, 243, 5004, 10065, '0.0000', '0.0000'),
(2128, 243, 5007, 10084, '0.0000', '0.0000'),
(2129, 243, 5007, 10085, '0.0000', '0.0000'),
(2130, 243, 5007, 10088, '0.0000', '0.0000'),
(2131, 243, 5007, 10090, '0.0000', '0.0000'),
(2132, 243, 5016, 10133, '0.0000', '0.0000'),
(2133, 244, 5001, 10001, '0.0000', '0.0000'),
(2134, 244, 5002, 10005, '0.0000', '0.0000'),
(2135, 244, 5002, 10006, '0.0000', '0.0000'),
(2136, 244, 5003, 10063, '0.0000', '0.0000'),
(2137, 244, 5004, 10065, '0.0000', '0.0000'),
(2138, 244, 5005, 10067, '0.0000', '0.0000'),
(2139, 244, 5006, 10069, '0.0000', '0.0000'),
(2140, 244, 5007, 10071, '0.0000', '0.0000'),
(2141, 244, 5007, 10072, '0.0000', '0.0000'),
(2142, 244, 5007, 10075, '0.0000', '0.0000'),
(2143, 244, 5007, 10076, '0.0000', '0.0000'),
(2144, 244, 5007, 10077, '0.0000', '0.0000'),
(2145, 244, 5007, 10101, '0.0000', '0.0000'),
(2146, 244, 5007, 10102, '0.0000', '0.0000'),
(2147, 244, 5016, 10136, '0.0000', '0.0000'),
(2148, 245, 5001, 10001, '0.0000', '0.0000'),
(2149, 245, 5002, 10005, '0.0000', '0.0000'),
(2150, 245, 5002, 10006, '0.0000', '0.0000'),
(2151, 245, 5003, 10063, '0.0000', '0.0000'),
(2152, 245, 5004, 10065, '0.0000', '0.0000'),
(2153, 245, 5005, 10067, '0.0000', '0.0000'),
(2154, 245, 5006, 10069, '0.0000', '0.0000'),
(2155, 245, 5007, 10071, '0.0000', '0.0000'),
(2156, 245, 5007, 10072, '0.0000', '0.0000'),
(2157, 245, 5007, 10077, '0.0000', '0.0000'),
(2158, 245, 5007, 10101, '0.0000', '0.0000'),
(2159, 245, 5007, 10102, '0.0000', '0.0000'),
(2160, 245, 5007, 10103, '0.0000', '0.0000'),
(2161, 245, 5007, 10104, '0.0000', '0.0000'),
(2162, 245, 5016, 10136, '0.0000', '0.0000'),
(2163, 246, 5001, 10001, '0.0000', '0.0000'),
(2164, 246, 5002, 10005, '0.0000', '0.0000'),
(2165, 246, 5002, 10006, '0.0000', '0.0000'),
(2166, 246, 5003, 10063, '0.0000', '0.0000'),
(2167, 246, 5004, 10065, '0.0000', '0.0000'),
(2168, 246, 5005, 10067, '0.0000', '0.0000'),
(2169, 246, 5006, 10069, '0.0000', '0.0000'),
(2170, 246, 5007, 10071, '0.0000', '0.0000'),
(2171, 246, 5007, 10073, '0.0000', '0.0000'),
(2172, 246, 5007, 10074, '0.0000', '0.0000'),
(2173, 246, 5007, 10075, '0.0000', '0.0000'),
(2174, 246, 5007, 10076, '0.0000', '0.0000'),
(2175, 246, 5007, 10077, '0.0000', '0.0000'),
(2176, 246, 5007, 10101, '0.0000', '0.0000'),
(2177, 246, 5016, 10133, '0.0000', '0.0000'),
(2178, 247, 5001, 10001, '0.0000', '0.0000'),
(2179, 247, 5002, 10005, '0.0000', '0.0000'),
(2180, 247, 5002, 10006, '0.0000', '0.0000'),
(2181, 247, 5003, 10063, '0.0000', '0.0000'),
(2182, 247, 5004, 10065, '0.0000', '0.0000'),
(2183, 247, 5005, 10067, '0.0000', '0.0000'),
(2184, 247, 5006, 10069, '0.0000', '0.0000'),
(2185, 247, 5007, 10072, '0.0000', '0.0000'),
(2186, 247, 5007, 10073, '0.0000', '0.0000'),
(2187, 247, 5007, 10074, '0.0000', '0.0000'),
(2188, 247, 5007, 10075, '0.0000', '0.0000'),
(2189, 247, 5007, 10076, '0.0000', '0.0000'),
(2190, 247, 5007, 10077, '0.0000', '0.0000'),
(2191, 247, 5007, 10094, '0.0000', '0.0000'),
(2192, 247, 5016, 10155, '0.0000', '0.0000'),
(2193, 248, 5001, 10003, '0.0000', '0.0000'),
(2194, 248, 5002, 10005, '0.0000', '0.0000'),
(2195, 248, 5002, 10046, '0.0000', '0.0000'),
(2196, 248, 5003, 10063, '0.0000', '0.0000'),
(2197, 248, 5004, 10065, '0.0000', '0.0000'),
(2198, 248, 5007, 10078, '0.0000', '0.0000'),
(2199, 248, 5007, 10079, '0.0000', '0.0000'),
(2200, 248, 5007, 10081, '0.0000', '0.0000'),
(2201, 248, 5007, 10082, '0.0000', '0.0000'),
(2202, 248, 5007, 10099, '0.0000', '0.0000'),
(2203, 248, 5007, 10100, '0.0000', '0.0000'),
(2204, 248, 5012, 10119, '0.0000', '0.0000'),
(2205, 248, 5015, 10128, '0.0000', '0.0000'),
(2206, 248, 5016, 10136, '0.0000', '0.0000'),
(2207, 249, 5001, 10003, '0.0000', '0.0000'),
(2208, 249, 5002, 10005, '0.0000', '0.0000'),
(2209, 249, 5002, 10046, '0.0000', '0.0000'),
(2210, 249, 5003, 10063, '0.0000', '0.0000'),
(2211, 249, 5004, 10065, '0.0000', '0.0000'),
(2212, 249, 5007, 10078, '0.0000', '0.0000'),
(2213, 249, 5007, 10079, '0.0000', '0.0000'),
(2214, 249, 5007, 10081, '0.0000', '0.0000'),
(2215, 249, 5007, 10082, '0.0000', '0.0000'),
(2216, 249, 5007, 10100, '0.0000', '0.0000'),
(2217, 249, 5012, 10119, '0.0000', '0.0000'),
(2218, 249, 5016, 10136, '0.0000', '0.0000'),
(2219, 250, 5001, 10003, '0.0000', '0.0000'),
(2220, 250, 5002, 10005, '0.0000', '0.0000'),
(2221, 250, 5002, 10046, '0.0000', '0.0000'),
(2222, 250, 5003, 10063, '0.0000', '0.0000'),
(2223, 250, 5004, 10065, '0.0000', '0.0000'),
(2224, 250, 5007, 10078, '0.0000', '0.0000'),
(2225, 250, 5007, 10079, '0.0000', '0.0000'),
(2226, 250, 5007, 10081, '0.0000', '0.0000'),
(2227, 250, 5007, 10082, '0.0000', '0.0000'),
(2228, 250, 5007, 10099, '0.0000', '0.0000'),
(2229, 250, 5007, 10100, '0.0000', '0.0000'),
(2230, 250, 5012, 10119, '0.0000', '0.0000'),
(2231, 250, 5013, 10120, '0.0000', '0.0000'),
(2232, 250, 5016, 10135, '0.0000', '0.0000'),
(2233, 250, 5016, 10144, '0.0000', '0.0000'),
(2234, 251, 5001, 10003, '0.0000', '0.0000'),
(2235, 251, 5002, 10005, '0.0000', '0.0000'),
(2236, 251, 5002, 10046, '0.0000', '0.0000'),
(2237, 251, 5003, 10064, '0.0000', '0.0000'),
(2238, 251, 5004, 10065, '0.0000', '0.0000'),
(2239, 251, 5007, 10082, '0.0000', '0.0000'),
(2240, 251, 5007, 10083, '0.0000', '0.0000'),
(2241, 251, 5007, 10092, '0.0000', '0.0000'),
(2242, 251, 5007, 10093, '0.0000', '0.0000'),
(2243, 251, 5007, 10105, '0.0000', '0.0000'),
(2244, 251, 5007, 10106, '0.0000', '0.0000'),
(2245, 251, 5012, 10119, '0.0000', '0.0000'),
(2246, 251, 5013, 10120, '0.0000', '0.0000'),
(2247, 251, 5016, 10133, '0.0000', '0.0000'),
(2248, 252, 5001, 10001, '0.0000', '0.0000'),
(2249, 252, 5002, 10005, '0.0000', '0.0000'),
(2250, 252, 5002, 10008, '0.0000', '0.0000'),
(2251, 252, 5003, 10063, '0.0000', '0.0000'),
(2252, 252, 5004, 10065, '0.0000', '0.0000'),
(2253, 252, 5005, 10067, '0.0000', '0.0000'),
(2254, 252, 5006, 10069, '0.0000', '0.0000'),
(2255, 252, 5007, 10071, '0.0000', '0.0000'),
(2256, 252, 5007, 10072, '0.0000', '0.0000'),
(2257, 252, 5007, 10073, '0.0000', '0.0000'),
(2258, 252, 5007, 10074, '0.0000', '0.0000'),
(2259, 252, 5007, 10075, '0.0000', '0.0000'),
(2260, 252, 5007, 10076, '0.0000', '0.0000'),
(2261, 252, 5007, 10077, '0.0000', '0.0000'),
(2262, 252, 5007, 10102, '0.0000', '0.0000'),
(2263, 252, 5007, 10103, '0.0000', '0.0000'),
(2264, 252, 5016, 10133, '0.0000', '0.0000'),
(2265, 252, 5016, 10136, '0.0000', '0.0000'),
(2266, 253, 5001, 10001, '0.0000', '0.0000'),
(2267, 253, 5002, 10005, '0.0000', '0.0000'),
(2268, 253, 5002, 10008, '0.0000', '0.0000'),
(2269, 253, 5003, 10063, '0.0000', '0.0000'),
(2270, 253, 5004, 10065, '0.0000', '0.0000'),
(2271, 253, 5005, 10067, '0.0000', '0.0000'),
(2272, 253, 5006, 10069, '0.0000', '0.0000'),
(2273, 253, 5007, 10071, '0.0000', '0.0000'),
(2274, 253, 5007, 10072, '0.0000', '0.0000'),
(2275, 253, 5007, 10073, '0.0000', '0.0000'),
(2276, 253, 5007, 10074, '0.0000', '0.0000'),
(2277, 253, 5007, 10075, '0.0000', '0.0000'),
(2278, 253, 5007, 10076, '0.0000', '0.0000'),
(2279, 253, 5007, 10077, '0.0000', '0.0000'),
(2280, 253, 5007, 10102, '0.0000', '0.0000'),
(2281, 253, 5007, 10103, '0.0000', '0.0000'),
(2282, 253, 5016, 10146, '0.0000', '0.0000'),
(2283, 254, 5001, 10001, '0.0000', '0.0000'),
(2284, 254, 5002, 10005, '0.0000', '0.0000'),
(2285, 254, 5002, 10008, '0.0000', '0.0000'),
(2286, 254, 5003, 10063, '0.0000', '0.0000'),
(2287, 254, 5004, 10065, '0.0000', '0.0000'),
(2288, 254, 5005, 10067, '0.0000', '0.0000'),
(2289, 254, 5006, 10069, '0.0000', '0.0000'),
(2290, 254, 5007, 10071, '0.0000', '0.0000'),
(2291, 254, 5007, 10072, '0.0000', '0.0000'),
(2292, 254, 5007, 10074, '0.0000', '0.0000'),
(2293, 254, 5007, 10075, '0.0000', '0.0000'),
(2294, 254, 5007, 10076, '0.0000', '0.0000'),
(2295, 254, 5007, 10077, '0.0000', '0.0000'),
(2296, 254, 5007, 10102, '0.0000', '0.0000'),
(2297, 254, 5016, 10135, '0.0000', '0.0000'),
(2298, 255, 5001, 10001, '0.0000', '0.0000'),
(2299, 255, 5002, 10005, '0.0000', '0.0000'),
(2300, 255, 5002, 10008, '0.0000', '0.0000'),
(2301, 255, 5003, 10063, '0.0000', '0.0000'),
(2302, 255, 5004, 10065, '0.0000', '0.0000'),
(2303, 255, 5005, 10067, '0.0000', '0.0000');
INSERT INTO `oc_ocfilter_option_value_to_product` (`ocfilter_option_value_to_product_id`, `product_id`, `option_id`, `value_id`, `slide_value_min`, `slide_value_max`) VALUES
(2304, 255, 5006, 10069, '0.0000', '0.0000'),
(2305, 255, 5007, 10071, '0.0000', '0.0000'),
(2306, 255, 5007, 10072, '0.0000', '0.0000'),
(2307, 255, 5007, 10074, '0.0000', '0.0000'),
(2308, 255, 5007, 10075, '0.0000', '0.0000'),
(2309, 255, 5007, 10076, '0.0000', '0.0000'),
(2310, 255, 5007, 10077, '0.0000', '0.0000'),
(2311, 255, 5016, 10136, '0.0000', '0.0000'),
(2312, 256, 5001, 10001, '0.0000', '0.0000'),
(2313, 256, 5002, 10005, '0.0000', '0.0000'),
(2314, 256, 5002, 10008, '0.0000', '0.0000'),
(2315, 256, 5003, 10063, '0.0000', '0.0000'),
(2316, 256, 5004, 10065, '0.0000', '0.0000'),
(2317, 256, 5005, 10067, '0.0000', '0.0000'),
(2318, 256, 5006, 10069, '0.0000', '0.0000'),
(2319, 256, 5007, 10071, '0.0000', '0.0000'),
(2320, 256, 5007, 10072, '0.0000', '0.0000'),
(2321, 256, 5007, 10074, '0.0000', '0.0000'),
(2322, 256, 5007, 10075, '0.0000', '0.0000'),
(2323, 256, 5007, 10076, '0.0000', '0.0000'),
(2324, 256, 5007, 10077, '0.0000', '0.0000'),
(2325, 256, 5016, 10135, '0.0000', '0.0000'),
(2326, 257, 5001, 10001, '0.0000', '0.0000'),
(2327, 257, 5002, 10005, '0.0000', '0.0000'),
(2328, 257, 5002, 10008, '0.0000', '0.0000'),
(2329, 257, 5003, 10063, '0.0000', '0.0000'),
(2330, 257, 5004, 10065, '0.0000', '0.0000'),
(2331, 257, 5005, 10067, '0.0000', '0.0000'),
(2332, 257, 5006, 10069, '0.0000', '0.0000'),
(2333, 257, 5007, 10071, '0.0000', '0.0000'),
(2334, 257, 5007, 10072, '0.0000', '0.0000'),
(2335, 257, 5007, 10074, '0.0000', '0.0000'),
(2336, 257, 5007, 10075, '0.0000', '0.0000'),
(2337, 257, 5007, 10076, '0.0000', '0.0000'),
(2338, 257, 5007, 10077, '0.0000', '0.0000'),
(2339, 257, 5016, 10146, '0.0000', '0.0000'),
(2340, 258, 5001, 10001, '0.0000', '0.0000'),
(2341, 258, 5002, 10005, '0.0000', '0.0000'),
(2342, 258, 5002, 10008, '0.0000', '0.0000'),
(2343, 258, 5003, 10063, '0.0000', '0.0000'),
(2344, 258, 5004, 10065, '0.0000', '0.0000'),
(2345, 258, 5005, 10067, '0.0000', '0.0000'),
(2346, 258, 5006, 10069, '0.0000', '0.0000'),
(2347, 258, 5007, 10071, '0.0000', '0.0000'),
(2348, 258, 5007, 10072, '0.0000', '0.0000'),
(2349, 258, 5007, 10074, '0.0000', '0.0000'),
(2350, 258, 5007, 10075, '0.0000', '0.0000'),
(2351, 258, 5007, 10076, '0.0000', '0.0000'),
(2352, 258, 5007, 10077, '0.0000', '0.0000'),
(2353, 258, 5007, 10102, '0.0000', '0.0000'),
(2354, 258, 5007, 10103, '0.0000', '0.0000'),
(2355, 258, 5016, 10136, '0.0000', '0.0000'),
(2356, 259, 5001, 10001, '0.0000', '0.0000'),
(2357, 259, 5002, 10005, '0.0000', '0.0000'),
(2358, 259, 5002, 10014, '0.0000', '0.0000'),
(2359, 259, 5003, 10063, '0.0000', '0.0000'),
(2360, 259, 5004, 10065, '0.0000', '0.0000'),
(2361, 259, 5007, 10088, '0.0000', '0.0000'),
(2362, 259, 5009, 10110, '0.0000', '0.0000'),
(2363, 259, 5010, 10112, '0.0000', '0.0000'),
(2364, 259, 5011, 10116, '0.0000', '0.0000'),
(2365, 259, 5016, 10135, '0.0000', '0.0000'),
(2366, 260, 5001, 10001, '0.0000', '0.0000'),
(2367, 260, 5002, 10005, '0.0000', '0.0000'),
(2368, 260, 5002, 10056, '0.0000', '0.0000'),
(2369, 260, 5003, 10063, '0.0000', '0.0000'),
(2370, 260, 5004, 10065, '0.0000', '0.0000'),
(2371, 260, 5006, 10070, '0.0000', '0.0000'),
(2372, 260, 5007, 10078, '0.0000', '0.0000'),
(2373, 260, 5007, 10079, '0.0000', '0.0000'),
(2374, 260, 5007, 10081, '0.0000', '0.0000'),
(2375, 260, 5007, 10082, '0.0000', '0.0000'),
(2376, 260, 5007, 10100, '0.0000', '0.0000'),
(2377, 260, 5016, 10134, '0.0000', '0.0000'),
(2378, 261, 5001, 10001, '0.0000', '0.0000'),
(2379, 261, 5002, 10005, '0.0000', '0.0000'),
(2380, 261, 5002, 10058, '0.0000', '0.0000'),
(2381, 261, 5003, 10063, '0.0000', '0.0000'),
(2382, 261, 5004, 10065, '0.0000', '0.0000'),
(2383, 261, 5007, 10084, '0.0000', '0.0000'),
(2384, 261, 5007, 10085, '0.0000', '0.0000'),
(2385, 261, 5007, 10088, '0.0000', '0.0000'),
(2386, 261, 5007, 10089, '0.0000', '0.0000'),
(2387, 261, 5016, 10131, '0.0000', '0.0000'),
(2388, 261, 5016, 10135, '0.0000', '0.0000'),
(2389, 262, 5001, 10001, '0.0000', '0.0000'),
(2390, 262, 5002, 10005, '0.0000', '0.0000'),
(2391, 262, 5002, 10058, '0.0000', '0.0000'),
(2392, 262, 5003, 10063, '0.0000', '0.0000'),
(2393, 262, 5004, 10065, '0.0000', '0.0000'),
(2394, 262, 5007, 10084, '0.0000', '0.0000'),
(2395, 262, 5007, 10085, '0.0000', '0.0000'),
(2396, 262, 5007, 10088, '0.0000', '0.0000'),
(2397, 262, 5007, 10089, '0.0000', '0.0000'),
(2398, 262, 5016, 10131, '0.0000', '0.0000'),
(2399, 262, 5016, 10132, '0.0000', '0.0000'),
(2400, 262, 5016, 10133, '0.0000', '0.0000'),
(2401, 262, 5016, 10152, '0.0000', '0.0000'),
(2402, 263, 5001, 10001, '0.0000', '0.0000'),
(2403, 263, 5002, 10005, '0.0000', '0.0000'),
(2404, 263, 5002, 10058, '0.0000', '0.0000'),
(2405, 263, 5003, 10063, '0.0000', '0.0000'),
(2406, 263, 5004, 10065, '0.0000', '0.0000'),
(2407, 263, 5007, 10084, '0.0000', '0.0000'),
(2408, 263, 5007, 10085, '0.0000', '0.0000'),
(2409, 263, 5007, 10088, '0.0000', '0.0000'),
(2410, 263, 5007, 10089, '0.0000', '0.0000'),
(2411, 263, 5016, 10135, '0.0000', '0.0000'),
(2412, 264, 5001, 10001, '0.0000', '0.0000'),
(2413, 264, 5002, 10005, '0.0000', '0.0000'),
(2414, 264, 5002, 10058, '0.0000', '0.0000'),
(2415, 264, 5003, 10063, '0.0000', '0.0000'),
(2416, 264, 5004, 10065, '0.0000', '0.0000'),
(2417, 264, 5007, 10084, '0.0000', '0.0000'),
(2418, 264, 5007, 10085, '0.0000', '0.0000'),
(2419, 264, 5007, 10088, '0.0000', '0.0000'),
(2420, 264, 5007, 10089, '0.0000', '0.0000'),
(2421, 264, 5016, 10131, '0.0000', '0.0000'),
(2422, 264, 5016, 10132, '0.0000', '0.0000'),
(2423, 265, 5001, 10001, '0.0000', '0.0000'),
(2424, 265, 5002, 10005, '0.0000', '0.0000'),
(2425, 265, 5002, 10058, '0.0000', '0.0000'),
(2426, 265, 5003, 10063, '0.0000', '0.0000'),
(2427, 265, 5004, 10065, '0.0000', '0.0000'),
(2428, 265, 5007, 10084, '0.0000', '0.0000'),
(2429, 265, 5007, 10085, '0.0000', '0.0000'),
(2430, 265, 5007, 10086, '0.0000', '0.0000'),
(2431, 265, 5007, 10088, '0.0000', '0.0000'),
(2432, 265, 5016, 10134, '0.0000', '0.0000'),
(2433, 266, 5001, 10001, '0.0000', '0.0000'),
(2434, 266, 5002, 10005, '0.0000', '0.0000'),
(2435, 266, 5002, 10058, '0.0000', '0.0000'),
(2436, 266, 5003, 10063, '0.0000', '0.0000'),
(2437, 266, 5004, 10065, '0.0000', '0.0000'),
(2438, 266, 5007, 10084, '0.0000', '0.0000'),
(2439, 266, 5007, 10085, '0.0000', '0.0000'),
(2440, 266, 5007, 10088, '0.0000', '0.0000'),
(2441, 266, 5007, 10089, '0.0000', '0.0000'),
(2442, 266, 5016, 10142, '0.0000', '0.0000'),
(2443, 267, 5001, 10001, '0.0000', '0.0000'),
(2444, 267, 5002, 10005, '0.0000', '0.0000'),
(2445, 267, 5002, 10058, '0.0000', '0.0000'),
(2446, 267, 5003, 10063, '0.0000', '0.0000'),
(2447, 267, 5004, 10065, '0.0000', '0.0000'),
(2448, 267, 5007, 10084, '0.0000', '0.0000'),
(2449, 267, 5007, 10085, '0.0000', '0.0000'),
(2450, 267, 5007, 10088, '0.0000', '0.0000'),
(2451, 267, 5007, 10089, '0.0000', '0.0000'),
(2452, 267, 5016, 10134, '0.0000', '0.0000'),
(2453, 268, 5007, 10078, '0.0000', '0.0000'),
(2454, 268, 5007, 10079, '0.0000', '0.0000'),
(2455, 268, 5007, 10081, '0.0000', '0.0000'),
(2456, 268, 5007, 10082, '0.0000', '0.0000'),
(2457, 268, 5007, 10100, '0.0000', '0.0000'),
(2458, 268, 5016, 10143, '0.0000', '0.0000'),
(2459, 269, 5001, 10001, '0.0000', '0.0000'),
(2460, 269, 5002, 10005, '0.0000', '0.0000'),
(2461, 269, 5002, 10028, '0.0000', '0.0000'),
(2462, 269, 5003, 10063, '0.0000', '0.0000'),
(2463, 269, 5004, 10065, '0.0000', '0.0000'),
(2464, 269, 5007, 10084, '0.0000', '0.0000'),
(2465, 269, 5007, 10085, '0.0000', '0.0000'),
(2466, 269, 5007, 10088, '0.0000', '0.0000'),
(2467, 269, 5007, 10089, '0.0000', '0.0000'),
(2468, 269, 5016, 10131, '0.0000', '0.0000'),
(2469, 270, 5001, 10001, '0.0000', '0.0000'),
(2470, 270, 5002, 10005, '0.0000', '0.0000'),
(2471, 270, 5002, 10056, '0.0000', '0.0000'),
(2472, 270, 5003, 10063, '0.0000', '0.0000'),
(2473, 270, 5004, 10065, '0.0000', '0.0000'),
(2474, 270, 5006, 10070, '0.0000', '0.0000'),
(2475, 270, 5007, 10078, '0.0000', '0.0000'),
(2476, 270, 5007, 10079, '0.0000', '0.0000'),
(2477, 270, 5007, 10081, '0.0000', '0.0000'),
(2478, 270, 5007, 10082, '0.0000', '0.0000'),
(2479, 270, 5007, 10100, '0.0000', '0.0000'),
(2480, 270, 5016, 10131, '0.0000', '0.0000'),
(2481, 271, 5001, 10001, '0.0000', '0.0000'),
(2482, 271, 5002, 10005, '0.0000', '0.0000'),
(2483, 271, 5002, 10014, '0.0000', '0.0000'),
(2484, 271, 5003, 10063, '0.0000', '0.0000'),
(2485, 271, 5004, 10065, '0.0000', '0.0000'),
(2486, 271, 5007, 10084, '0.0000', '0.0000'),
(2487, 271, 5007, 10085, '0.0000', '0.0000'),
(2488, 271, 5007, 10088, '0.0000', '0.0000'),
(2489, 271, 5007, 10089, '0.0000', '0.0000'),
(2490, 271, 5016, 10132, '0.0000', '0.0000'),
(2491, 272, 5001, 10001, '0.0000', '0.0000'),
(2492, 272, 5002, 10005, '0.0000', '0.0000'),
(2493, 272, 5002, 10014, '0.0000', '0.0000'),
(2494, 272, 5003, 10063, '0.0000', '0.0000'),
(2495, 272, 5004, 10065, '0.0000', '0.0000'),
(2496, 272, 5007, 10084, '0.0000', '0.0000'),
(2497, 272, 5007, 10085, '0.0000', '0.0000'),
(2498, 272, 5007, 10088, '0.0000', '0.0000'),
(2499, 272, 5007, 10089, '0.0000', '0.0000'),
(2500, 272, 5016, 10145, '0.0000', '0.0000'),
(2501, 273, 5001, 10001, '0.0000', '0.0000'),
(2502, 273, 5002, 10005, '0.0000', '0.0000'),
(2503, 273, 5002, 10014, '0.0000', '0.0000'),
(2504, 273, 5003, 10063, '0.0000', '0.0000'),
(2505, 273, 5004, 10065, '0.0000', '0.0000'),
(2506, 273, 5007, 10084, '0.0000', '0.0000'),
(2507, 273, 5007, 10085, '0.0000', '0.0000'),
(2508, 273, 5007, 10088, '0.0000', '0.0000'),
(2509, 273, 5007, 10089, '0.0000', '0.0000'),
(2510, 273, 5009, 10110, '0.0000', '0.0000'),
(2511, 273, 5010, 10112, '0.0000', '0.0000'),
(2512, 273, 5011, 10116, '0.0000', '0.0000'),
(2513, 273, 5016, 10145, '0.0000', '0.0000'),
(2514, 274, 5001, 10001, '0.0000', '0.0000'),
(2515, 274, 5002, 10005, '0.0000', '0.0000'),
(2516, 274, 5002, 10014, '0.0000', '0.0000'),
(2517, 274, 5003, 10063, '0.0000', '0.0000'),
(2518, 274, 5004, 10065, '0.0000', '0.0000'),
(2519, 274, 5007, 10085, '0.0000', '0.0000'),
(2520, 274, 5007, 10088, '0.0000', '0.0000'),
(2521, 274, 5007, 10089, '0.0000', '0.0000'),
(2522, 274, 5009, 10110, '0.0000', '0.0000'),
(2523, 274, 5010, 10112, '0.0000', '0.0000'),
(2524, 274, 5011, 10116, '0.0000', '0.0000'),
(2525, 274, 5016, 10145, '0.0000', '0.0000'),
(2526, 275, 5001, 10001, '0.0000', '0.0000'),
(2527, 275, 5002, 10005, '0.0000', '0.0000'),
(2528, 275, 5002, 10014, '0.0000', '0.0000'),
(2529, 275, 5003, 10063, '0.0000', '0.0000'),
(2530, 275, 5004, 10065, '0.0000', '0.0000'),
(2531, 275, 5007, 10085, '0.0000', '0.0000'),
(2532, 275, 5007, 10088, '0.0000', '0.0000'),
(2533, 275, 5007, 10089, '0.0000', '0.0000'),
(2534, 275, 5009, 10110, '0.0000', '0.0000'),
(2535, 275, 5010, 10112, '0.0000', '0.0000'),
(2536, 275, 5011, 10116, '0.0000', '0.0000'),
(2537, 275, 5016, 10144, '0.0000', '0.0000'),
(2538, 276, 5001, 10001, '0.0000', '0.0000'),
(2539, 276, 5002, 10005, '0.0000', '0.0000'),
(2540, 276, 5002, 10014, '0.0000', '0.0000'),
(2541, 276, 5003, 10063, '0.0000', '0.0000'),
(2542, 276, 5004, 10065, '0.0000', '0.0000'),
(2543, 276, 5007, 10085, '0.0000', '0.0000'),
(2544, 276, 5007, 10088, '0.0000', '0.0000'),
(2545, 276, 5007, 10089, '0.0000', '0.0000'),
(2546, 276, 5009, 10110, '0.0000', '0.0000'),
(2547, 276, 5010, 10112, '0.0000', '0.0000'),
(2548, 276, 5011, 10116, '0.0000', '0.0000'),
(2549, 276, 5016, 10132, '0.0000', '0.0000'),
(2550, 277, 5001, 10001, '0.0000', '0.0000'),
(2551, 277, 5002, 10005, '0.0000', '0.0000'),
(2552, 277, 5002, 10060, '0.0000', '0.0000'),
(2553, 277, 5003, 10063, '0.0000', '0.0000'),
(2554, 277, 5004, 10065, '0.0000', '0.0000'),
(2555, 277, 5007, 10084, '0.0000', '0.0000'),
(2556, 277, 5007, 10085, '0.0000', '0.0000'),
(2557, 277, 5007, 10088, '0.0000', '0.0000'),
(2558, 277, 5016, 10131, '0.0000', '0.0000'),
(2559, 278, 5001, 10001, '0.0000', '0.0000'),
(2560, 278, 5002, 10005, '0.0000', '0.0000'),
(2561, 278, 5002, 10010, '0.0000', '0.0000'),
(2562, 278, 5003, 10063, '0.0000', '0.0000'),
(2563, 278, 5004, 10066, '0.0000', '0.0000'),
(2564, 278, 5007, 10084, '0.0000', '0.0000'),
(2565, 278, 5007, 10085, '0.0000', '0.0000'),
(2566, 278, 5007, 10087, '0.0000', '0.0000'),
(2567, 278, 5007, 10088, '0.0000', '0.0000'),
(2568, 278, 5007, 10089, '0.0000', '0.0000'),
(2569, 278, 5009, 10110, '0.0000', '0.0000'),
(2570, 278, 5010, 10112, '0.0000', '0.0000'),
(2571, 278, 5011, 10117, '0.0000', '0.0000'),
(2572, 278, 5016, 10133, '0.0000', '0.0000'),
(2573, 279, 5001, 10001, '0.0000', '0.0000'),
(2574, 279, 5002, 10005, '0.0000', '0.0000'),
(2575, 279, 5002, 10010, '0.0000', '0.0000'),
(2576, 279, 5003, 10063, '0.0000', '0.0000'),
(2577, 279, 5004, 10066, '0.0000', '0.0000'),
(2578, 279, 5007, 10084, '0.0000', '0.0000'),
(2579, 279, 5007, 10085, '0.0000', '0.0000'),
(2580, 279, 5007, 10087, '0.0000', '0.0000'),
(2581, 279, 5007, 10088, '0.0000', '0.0000'),
(2582, 279, 5007, 10089, '0.0000', '0.0000'),
(2583, 279, 5009, 10110, '0.0000', '0.0000'),
(2584, 279, 5010, 10112, '0.0000', '0.0000'),
(2585, 279, 5011, 10117, '0.0000', '0.0000'),
(2586, 279, 5016, 10132, '0.0000', '0.0000'),
(2587, 280, 5001, 10001, '0.0000', '0.0000'),
(2588, 280, 5002, 10005, '0.0000', '0.0000'),
(2589, 280, 5002, 10010, '0.0000', '0.0000'),
(2590, 280, 5003, 10063, '0.0000', '0.0000'),
(2591, 280, 5004, 10066, '0.0000', '0.0000'),
(2592, 280, 5007, 10084, '0.0000', '0.0000'),
(2593, 280, 5007, 10085, '0.0000', '0.0000'),
(2594, 280, 5007, 10086, '0.0000', '0.0000'),
(2595, 280, 5007, 10087, '0.0000', '0.0000'),
(2596, 280, 5007, 10088, '0.0000', '0.0000'),
(2597, 280, 5009, 10110, '0.0000', '0.0000'),
(2598, 280, 5010, 10112, '0.0000', '0.0000'),
(2599, 280, 5011, 10116, '0.0000', '0.0000'),
(2600, 280, 5016, 10137, '0.0000', '0.0000'),
(2601, 282, 5001, 10001, '0.0000', '0.0000'),
(2602, 282, 5002, 10005, '0.0000', '0.0000'),
(2603, 282, 5002, 10010, '0.0000', '0.0000'),
(2604, 282, 5003, 10063, '0.0000', '0.0000'),
(2605, 282, 5004, 10066, '0.0000', '0.0000'),
(2606, 282, 5007, 10084, '0.0000', '0.0000'),
(2607, 282, 5007, 10085, '0.0000', '0.0000'),
(2608, 282, 5007, 10086, '0.0000', '0.0000'),
(2609, 282, 5007, 10088, '0.0000', '0.0000'),
(2610, 282, 5009, 10110, '0.0000', '0.0000'),
(2611, 282, 5010, 10112, '0.0000', '0.0000'),
(2612, 282, 5011, 10116, '0.0000', '0.0000'),
(2613, 282, 5016, 10133, '0.0000', '0.0000'),
(2614, 283, 5001, 10001, '0.0000', '0.0000'),
(2615, 283, 5002, 10005, '0.0000', '0.0000'),
(2616, 283, 5002, 10010, '0.0000', '0.0000'),
(2617, 283, 5003, 10063, '0.0000', '0.0000'),
(2618, 283, 5004, 10066, '0.0000', '0.0000'),
(2619, 283, 5007, 10084, '0.0000', '0.0000'),
(2620, 283, 5007, 10085, '0.0000', '0.0000'),
(2621, 283, 5007, 10086, '0.0000', '0.0000'),
(2622, 283, 5007, 10088, '0.0000', '0.0000'),
(2623, 283, 5007, 10090, '0.0000', '0.0000'),
(2624, 283, 5007, 10098, '0.0000', '0.0000'),
(2625, 283, 5009, 10110, '0.0000', '0.0000'),
(2626, 283, 5010, 10112, '0.0000', '0.0000'),
(2627, 283, 5011, 10116, '0.0000', '0.0000'),
(2628, 283, 5016, 10133, '0.0000', '0.0000'),
(2629, 284, 5001, 10001, '0.0000', '0.0000'),
(2630, 284, 5002, 10005, '0.0000', '0.0000'),
(2631, 284, 5002, 10010, '0.0000', '0.0000'),
(2632, 284, 5003, 10063, '0.0000', '0.0000'),
(2633, 284, 5004, 10066, '0.0000', '0.0000'),
(2634, 284, 5007, 10084, '0.0000', '0.0000'),
(2635, 284, 5007, 10085, '0.0000', '0.0000'),
(2636, 284, 5007, 10086, '0.0000', '0.0000'),
(2637, 284, 5007, 10088, '0.0000', '0.0000'),
(2638, 284, 5007, 10090, '0.0000', '0.0000'),
(2639, 284, 5007, 10098, '0.0000', '0.0000'),
(2640, 284, 5009, 10110, '0.0000', '0.0000'),
(2641, 284, 5010, 10113, '0.0000', '0.0000'),
(2642, 284, 5011, 10116, '0.0000', '0.0000'),
(2643, 284, 5016, 10133, '0.0000', '0.0000'),
(2644, 285, 5001, 10001, '0.0000', '0.0000'),
(2645, 285, 5002, 10005, '0.0000', '0.0000'),
(2646, 285, 5002, 10006, '0.0000', '0.0000'),
(2647, 285, 5003, 10063, '0.0000', '0.0000'),
(2648, 285, 5004, 10065, '0.0000', '0.0000'),
(2649, 285, 5005, 10067, '0.0000', '0.0000'),
(2650, 285, 5006, 10069, '0.0000', '0.0000'),
(2651, 285, 5007, 10073, '0.0000', '0.0000'),
(2652, 285, 5007, 10074, '0.0000', '0.0000'),
(2653, 285, 5007, 10075, '0.0000', '0.0000'),
(2654, 285, 5007, 10077, '0.0000', '0.0000'),
(2655, 285, 5016, 10134, '0.0000', '0.0000'),
(2656, 285, 5016, 10135, '0.0000', '0.0000'),
(2657, 286, 5001, 10001, '0.0000', '0.0000'),
(2658, 286, 5002, 10005, '0.0000', '0.0000'),
(2659, 286, 5002, 10011, '0.0000', '0.0000'),
(2660, 286, 5003, 10063, '0.0000', '0.0000'),
(2661, 286, 5004, 10066, '0.0000', '0.0000'),
(2662, 286, 5007, 10084, '0.0000', '0.0000'),
(2663, 286, 5007, 10085, '0.0000', '0.0000'),
(2664, 286, 5007, 10086, '0.0000', '0.0000'),
(2665, 286, 5007, 10087, '0.0000', '0.0000'),
(2666, 286, 5007, 10088, '0.0000', '0.0000'),
(2667, 286, 5009, 10110, '0.0000', '0.0000'),
(2668, 286, 5010, 10112, '0.0000', '0.0000'),
(2669, 286, 5011, 10117, '0.0000', '0.0000'),
(2670, 286, 5016, 10135, '0.0000', '0.0000'),
(2671, 288, 5001, 10001, '0.0000', '0.0000'),
(2672, 288, 5002, 10005, '0.0000', '0.0000'),
(2673, 288, 5002, 10032, '0.0000', '0.0000'),
(2674, 288, 5003, 10063, '0.0000', '0.0000'),
(2675, 288, 5004, 10065, '0.0000', '0.0000'),
(2676, 288, 5007, 10084, '0.0000', '0.0000'),
(2677, 288, 5007, 10085, '0.0000', '0.0000'),
(2678, 288, 5007, 10086, '0.0000', '0.0000'),
(2679, 288, 5007, 10088, '0.0000', '0.0000'),
(2680, 288, 5016, 10136, '0.0000', '0.0000'),
(2681, 289, 5001, 10001, '0.0000', '0.0000'),
(2682, 289, 5002, 10005, '0.0000', '0.0000'),
(2683, 289, 5002, 10032, '0.0000', '0.0000'),
(2684, 289, 5003, 10063, '0.0000', '0.0000'),
(2685, 289, 5004, 10065, '0.0000', '0.0000'),
(2686, 289, 5007, 10084, '0.0000', '0.0000'),
(2687, 289, 5007, 10085, '0.0000', '0.0000'),
(2688, 289, 5016, 10136, '0.0000', '0.0000'),
(2689, 290, 5001, 10001, '0.0000', '0.0000'),
(2690, 290, 5002, 10005, '0.0000', '0.0000'),
(2691, 290, 5002, 10026, '0.0000', '0.0000'),
(2692, 290, 5003, 10063, '0.0000', '0.0000'),
(2693, 290, 5004, 10065, '0.0000', '0.0000'),
(2694, 290, 5007, 10084, '0.0000', '0.0000'),
(2695, 290, 5007, 10085, '0.0000', '0.0000'),
(2696, 290, 5007, 10088, '0.0000', '0.0000'),
(2697, 290, 5016, 10131, '0.0000', '0.0000'),
(2698, 290, 5016, 10135, '0.0000', '0.0000'),
(2699, 291, 5001, 10001, '0.0000', '0.0000'),
(2700, 291, 5002, 10005, '0.0000', '0.0000'),
(2701, 291, 5002, 10033, '0.0000', '0.0000'),
(2702, 291, 5003, 10063, '0.0000', '0.0000'),
(2703, 291, 5004, 10065, '0.0000', '0.0000'),
(2704, 291, 5007, 10085, '0.0000', '0.0000'),
(2705, 291, 5007, 10086, '0.0000', '0.0000'),
(2706, 291, 5016, 10136, '0.0000', '0.0000'),
(2707, 292, 5001, 10001, '0.0000', '0.0000'),
(2708, 292, 5002, 10005, '0.0000', '0.0000'),
(2709, 292, 5002, 10036, '0.0000', '0.0000'),
(2710, 292, 5003, 10063, '0.0000', '0.0000'),
(2711, 292, 5004, 10065, '0.0000', '0.0000'),
(2712, 292, 5007, 10084, '0.0000', '0.0000'),
(2713, 292, 5016, 10147, '0.0000', '0.0000'),
(2714, 293, 5001, 10001, '0.0000', '0.0000'),
(2715, 293, 5002, 10005, '0.0000', '0.0000'),
(2716, 293, 5002, 10026, '0.0000', '0.0000'),
(2717, 293, 5003, 10063, '0.0000', '0.0000'),
(2718, 293, 5004, 10065, '0.0000', '0.0000'),
(2719, 293, 5007, 10084, '0.0000', '0.0000'),
(2720, 293, 5016, 10131, '0.0000', '0.0000'),
(2721, 294, 5001, 10001, '0.0000', '0.0000'),
(2722, 294, 5002, 10005, '0.0000', '0.0000'),
(2723, 294, 5002, 10028, '0.0000', '0.0000'),
(2724, 294, 5003, 10063, '0.0000', '0.0000'),
(2725, 294, 5004, 10065, '0.0000', '0.0000'),
(2726, 294, 5007, 10086, '0.0000', '0.0000'),
(2727, 294, 5016, 10136, '0.0000', '0.0000'),
(2728, 295, 5001, 10001, '0.0000', '0.0000'),
(2729, 295, 5002, 10005, '0.0000', '0.0000'),
(2730, 295, 5002, 10026, '0.0000', '0.0000'),
(2731, 295, 5003, 10063, '0.0000', '0.0000'),
(2732, 295, 5004, 10065, '0.0000', '0.0000'),
(2733, 295, 5007, 10084, '0.0000', '0.0000'),
(2734, 295, 5016, 10142, '0.0000', '0.0000'),
(2735, 296, 5001, 10001, '0.0000', '0.0000'),
(2736, 296, 5002, 10005, '0.0000', '0.0000'),
(2737, 296, 5002, 10036, '0.0000', '0.0000'),
(2738, 296, 5003, 10063, '0.0000', '0.0000'),
(2739, 296, 5004, 10065, '0.0000', '0.0000'),
(2740, 296, 5007, 10085, '0.0000', '0.0000'),
(2741, 296, 5007, 10088, '0.0000', '0.0000'),
(2742, 296, 5016, 10143, '0.0000', '0.0000'),
(2743, 297, 5001, 10001, '0.0000', '0.0000'),
(2744, 297, 5002, 10005, '0.0000', '0.0000'),
(2745, 297, 5002, 10036, '0.0000', '0.0000'),
(2746, 297, 5003, 10063, '0.0000', '0.0000'),
(2747, 297, 5004, 10065, '0.0000', '0.0000'),
(2748, 297, 5007, 10085, '0.0000', '0.0000'),
(2749, 297, 5007, 10086, '0.0000', '0.0000'),
(2750, 297, 5007, 10088, '0.0000', '0.0000'),
(2751, 297, 5016, 10134, '0.0000', '0.0000'),
(2752, 297, 5016, 10142, '0.0000', '0.0000'),
(2753, 298, 5001, 10001, '0.0000', '0.0000'),
(2754, 298, 5002, 10005, '0.0000', '0.0000'),
(2755, 298, 5002, 10028, '0.0000', '0.0000'),
(2756, 298, 5003, 10063, '0.0000', '0.0000'),
(2757, 298, 5004, 10065, '0.0000', '0.0000'),
(2758, 298, 5007, 10085, '0.0000', '0.0000'),
(2759, 298, 5007, 10086, '0.0000', '0.0000'),
(2760, 298, 5007, 10088, '0.0000', '0.0000'),
(2761, 298, 5016, 10133, '0.0000', '0.0000'),
(2762, 298, 5016, 10143, '0.0000', '0.0000'),
(2763, 299, 5001, 10001, '0.0000', '0.0000'),
(2764, 299, 5002, 10005, '0.0000', '0.0000'),
(2765, 299, 5002, 10033, '0.0000', '0.0000'),
(2766, 299, 5003, 10063, '0.0000', '0.0000'),
(2767, 299, 5004, 10065, '0.0000', '0.0000'),
(2768, 299, 5007, 10084, '0.0000', '0.0000'),
(2769, 299, 5007, 10085, '0.0000', '0.0000'),
(2770, 299, 5007, 10086, '0.0000', '0.0000'),
(2771, 299, 5007, 10088, '0.0000', '0.0000'),
(2772, 299, 5016, 10136, '0.0000', '0.0000'),
(2773, 300, 5001, 10001, '0.0000', '0.0000'),
(2774, 300, 5002, 10005, '0.0000', '0.0000'),
(2775, 300, 5002, 10041, '0.0000', '0.0000'),
(2776, 300, 5003, 10063, '0.0000', '0.0000'),
(2777, 300, 5004, 10065, '0.0000', '0.0000'),
(2778, 300, 5007, 10084, '0.0000', '0.0000'),
(2779, 300, 5007, 10087, '0.0000', '0.0000'),
(2780, 300, 5016, 10138, '0.0000', '0.0000'),
(2781, 301, 5001, 10003, '0.0000', '0.0000'),
(2782, 301, 5002, 10005, '0.0000', '0.0000'),
(2783, 301, 5002, 10048, '0.0000', '0.0000'),
(2784, 301, 5003, 10063, '0.0000', '0.0000'),
(2785, 301, 5004, 10065, '0.0000', '0.0000'),
(2786, 301, 5007, 10084, '0.0000', '0.0000'),
(2787, 301, 5012, 10119, '0.0000', '0.0000'),
(2788, 301, 5013, 10120, '0.0000', '0.0000'),
(2789, 301, 5016, 10136, '0.0000', '0.0000'),
(2790, 302, 5001, 10003, '0.0000', '0.0000'),
(2791, 302, 5002, 10005, '0.0000', '0.0000'),
(2792, 302, 5002, 10046, '0.0000', '0.0000'),
(2793, 302, 5003, 10063, '0.0000', '0.0000'),
(2794, 302, 5004, 10065, '0.0000', '0.0000'),
(2795, 302, 5007, 10078, '0.0000', '0.0000'),
(2796, 302, 5007, 10079, '0.0000', '0.0000'),
(2797, 302, 5007, 10081, '0.0000', '0.0000'),
(2798, 302, 5007, 10082, '0.0000', '0.0000'),
(2799, 302, 5007, 10100, '0.0000', '0.0000'),
(2800, 302, 5012, 10119, '0.0000', '0.0000'),
(2801, 302, 5015, 10128, '0.0000', '0.0000'),
(2802, 302, 5016, 10144, '0.0000', '0.0000'),
(2803, 303, 5001, 10001, '0.0000', '0.0000'),
(2804, 303, 5002, 10005, '0.0000', '0.0000'),
(2805, 303, 5002, 10034, '0.0000', '0.0000'),
(2806, 303, 5003, 10063, '0.0000', '0.0000'),
(2807, 303, 5004, 10065, '0.0000', '0.0000'),
(2808, 303, 5007, 10085, '0.0000', '0.0000'),
(2809, 303, 5007, 10088, '0.0000', '0.0000'),
(2810, 303, 5007, 10089, '0.0000', '0.0000'),
(2811, 303, 5007, 10090, '0.0000', '0.0000'),
(2812, 303, 5016, 10131, '0.0000', '0.0000'),
(2813, 303, 5016, 10132, '0.0000', '0.0000'),
(2814, 303, 5016, 10136, '0.0000', '0.0000'),
(2815, 303, 5016, 10149, '0.0000', '0.0000'),
(2816, 304, 5001, 10001, '0.0000', '0.0000'),
(2817, 304, 5002, 10005, '0.0000', '0.0000'),
(2818, 304, 5002, 10007, '0.0000', '0.0000'),
(2819, 304, 5003, 10063, '0.0000', '0.0000'),
(2820, 304, 5004, 10066, '0.0000', '0.0000'),
(2821, 304, 5007, 10078, '0.0000', '0.0000'),
(2822, 304, 5007, 10079, '0.0000', '0.0000'),
(2823, 304, 5008, 10108, '0.0000', '0.0000'),
(2824, 304, 5016, 10132, '0.0000', '0.0000'),
(2825, 304, 5016, 10137, '0.0000', '0.0000'),
(2826, 305, 5001, 10001, '0.0000', '0.0000'),
(2827, 305, 5002, 10005, '0.0000', '0.0000'),
(2828, 305, 5002, 10034, '0.0000', '0.0000'),
(2829, 305, 5003, 10063, '0.0000', '0.0000'),
(2830, 305, 5004, 10065, '0.0000', '0.0000'),
(2831, 305, 5007, 10084, '0.0000', '0.0000'),
(2832, 305, 5007, 10085, '0.0000', '0.0000'),
(2833, 305, 5007, 10086, '0.0000', '0.0000'),
(2834, 305, 5007, 10088, '0.0000', '0.0000'),
(2835, 305, 5007, 10089, '0.0000', '0.0000'),
(2836, 305, 5007, 10090, '0.0000', '0.0000'),
(2837, 305, 5016, 10144, '0.0000', '0.0000'),
(2838, 305, 5016, 10149, '0.0000', '0.0000'),
(2839, 306, 5001, 10001, '0.0000', '0.0000'),
(2840, 306, 5002, 10005, '0.0000', '0.0000'),
(2841, 306, 5002, 10037, '0.0000', '0.0000'),
(2842, 306, 5003, 10063, '0.0000', '0.0000'),
(2843, 306, 5004, 10065, '0.0000', '0.0000'),
(2844, 306, 5007, 10084, '0.0000', '0.0000'),
(2845, 306, 5007, 10085, '0.0000', '0.0000'),
(2846, 306, 5007, 10086, '0.0000', '0.0000'),
(2847, 306, 5007, 10087, '0.0000', '0.0000'),
(2848, 306, 5007, 10088, '0.0000', '0.0000'),
(2849, 306, 5007, 10089, '0.0000', '0.0000'),
(2850, 306, 5016, 10132, '0.0000', '0.0000'),
(2851, 306, 5016, 10134, '0.0000', '0.0000'),
(2852, 306, 5016, 10136, '0.0000', '0.0000'),
(2853, 307, 5001, 10001, '0.0000', '0.0000'),
(2854, 307, 5002, 10005, '0.0000', '0.0000'),
(2855, 307, 5002, 10038, '0.0000', '0.0000'),
(2856, 307, 5003, 10063, '0.0000', '0.0000'),
(2857, 307, 5004, 10066, '0.0000', '0.0000'),
(2858, 307, 5007, 10078, '0.0000', '0.0000'),
(2859, 307, 5007, 10079, '0.0000', '0.0000'),
(2860, 307, 5007, 10081, '0.0000', '0.0000'),
(2861, 307, 5007, 10082, '0.0000', '0.0000'),
(2862, 307, 5007, 10083, '0.0000', '0.0000'),
(2863, 307, 5007, 10092, '0.0000', '0.0000'),
(2864, 307, 5008, 10109, '0.0000', '0.0000'),
(2865, 307, 5016, 10135, '0.0000', '0.0000'),
(2866, 307, 5016, 10151, '0.0000', '0.0000'),
(2867, 308, 5001, 10001, '0.0000', '0.0000'),
(2868, 308, 5002, 10005, '0.0000', '0.0000'),
(2869, 308, 5002, 10028, '0.0000', '0.0000'),
(2870, 308, 5003, 10063, '0.0000', '0.0000'),
(2871, 308, 5004, 10065, '0.0000', '0.0000'),
(2872, 308, 5007, 10090, '0.0000', '0.0000'),
(2873, 308, 5007, 10097, '0.0000', '0.0000'),
(2874, 308, 5007, 10098, '0.0000', '0.0000'),
(2875, 308, 5016, 10133, '0.0000', '0.0000'),
(2876, 308, 5016, 10142, '0.0000', '0.0000'),
(2877, 309, 5001, 10001, '0.0000', '0.0000'),
(2878, 309, 5002, 10005, '0.0000', '0.0000'),
(2879, 309, 5002, 10026, '0.0000', '0.0000'),
(2880, 309, 5003, 10063, '0.0000', '0.0000'),
(2881, 309, 5004, 10065, '0.0000', '0.0000'),
(2882, 309, 5007, 10084, '0.0000', '0.0000'),
(2883, 309, 5007, 10085, '0.0000', '0.0000'),
(2884, 309, 5007, 10088, '0.0000', '0.0000'),
(2885, 309, 5016, 10131, '0.0000', '0.0000'),
(2886, 309, 5016, 10134, '0.0000', '0.0000'),
(2887, 309, 5016, 10135, '0.0000', '0.0000'),
(2888, 310, 5001, 10001, '0.0000', '0.0000'),
(2889, 310, 5002, 10005, '0.0000', '0.0000'),
(2890, 310, 5002, 10007, '0.0000', '0.0000'),
(2891, 310, 5003, 10063, '0.0000', '0.0000'),
(2892, 310, 5004, 10066, '0.0000', '0.0000'),
(2893, 310, 5007, 10078, '0.0000', '0.0000'),
(2894, 310, 5007, 10079, '0.0000', '0.0000'),
(2895, 310, 5007, 10080, '0.0000', '0.0000'),
(2896, 310, 5007, 10081, '0.0000', '0.0000'),
(2897, 310, 5007, 10099, '0.0000', '0.0000'),
(2898, 310, 5007, 10100, '0.0000', '0.0000'),
(2899, 310, 5008, 10109, '0.0000', '0.0000'),
(2900, 310, 5016, 10135, '0.0000', '0.0000'),
(2901, 311, 5001, 10001, '0.0000', '0.0000'),
(2902, 311, 5002, 10005, '0.0000', '0.0000'),
(2903, 311, 5002, 10014, '0.0000', '0.0000'),
(2904, 311, 5003, 10064, '0.0000', '0.0000'),
(2905, 311, 5004, 10065, '0.0000', '0.0000'),
(2906, 311, 5007, 10090, '0.0000', '0.0000'),
(2907, 311, 5007, 10097, '0.0000', '0.0000'),
(2908, 311, 5007, 10098, '0.0000', '0.0000'),
(2909, 311, 5016, 10133, '0.0000', '0.0000'),
(2910, 312, 5001, 10001, '0.0000', '0.0000'),
(2911, 312, 5002, 10005, '0.0000', '0.0000'),
(2912, 312, 5002, 10014, '0.0000', '0.0000'),
(2913, 312, 5003, 10064, '0.0000', '0.0000'),
(2914, 312, 5004, 10065, '0.0000', '0.0000'),
(2915, 312, 5007, 10090, '0.0000', '0.0000'),
(2916, 312, 5007, 10097, '0.0000', '0.0000'),
(2917, 312, 5007, 10098, '0.0000', '0.0000'),
(2918, 312, 5016, 10135, '0.0000', '0.0000'),
(2919, 312, 5016, 10142, '0.0000', '0.0000'),
(2920, 313, 5001, 10001, '0.0000', '0.0000'),
(2921, 313, 5002, 10005, '0.0000', '0.0000'),
(2922, 313, 5002, 10014, '0.0000', '0.0000'),
(2923, 313, 5003, 10064, '0.0000', '0.0000'),
(2924, 313, 5004, 10065, '0.0000', '0.0000'),
(2925, 313, 5007, 10090, '0.0000', '0.0000'),
(2926, 313, 5007, 10097, '0.0000', '0.0000'),
(2927, 313, 5007, 10098, '0.0000', '0.0000'),
(2928, 313, 5016, 10135, '0.0000', '0.0000'),
(2929, 313, 5016, 10144, '0.0000', '0.0000'),
(2930, 313, 5016, 10145, '0.0000', '0.0000'),
(2931, 314, 5001, 10001, '0.0000', '0.0000'),
(2932, 314, 5002, 10005, '0.0000', '0.0000'),
(2933, 314, 5002, 10026, '0.0000', '0.0000'),
(2934, 314, 5003, 10064, '0.0000', '0.0000'),
(2935, 314, 5004, 10065, '0.0000', '0.0000'),
(2936, 314, 5007, 10090, '0.0000', '0.0000'),
(2937, 314, 5016, 10133, '0.0000', '0.0000'),
(2938, 315, 5001, 10003, '0.0000', '0.0000'),
(2939, 315, 5002, 10005, '0.0000', '0.0000'),
(2940, 315, 5002, 10045, '0.0000', '0.0000'),
(2941, 315, 5003, 10063, '0.0000', '0.0000'),
(2942, 315, 5004, 10065, '0.0000', '0.0000'),
(2943, 315, 5007, 10079, '0.0000', '0.0000'),
(2944, 315, 5007, 10081, '0.0000', '0.0000'),
(2945, 315, 5007, 10099, '0.0000', '0.0000'),
(2946, 315, 5007, 10100, '0.0000', '0.0000'),
(2947, 315, 5012, 10118, '0.0000', '0.0000'),
(2948, 315, 5013, 10120, '0.0000', '0.0000'),
(2949, 315, 5016, 10146, '0.0000', '0.0000'),
(2950, 316, 5001, 10003, '0.0000', '0.0000'),
(2951, 316, 5002, 10005, '0.0000', '0.0000'),
(2952, 316, 5002, 10042, '0.0000', '0.0000'),
(2953, 316, 5003, 10063, '0.0000', '0.0000'),
(2954, 316, 5004, 10065, '0.0000', '0.0000'),
(2955, 316, 5007, 10078, '0.0000', '0.0000'),
(2956, 316, 5007, 10081, '0.0000', '0.0000'),
(2957, 316, 5007, 10082, '0.0000', '0.0000'),
(2958, 316, 5007, 10083, '0.0000', '0.0000'),
(2959, 316, 5012, 10119, '0.0000', '0.0000'),
(2960, 316, 5013, 10123, '0.0000', '0.0000'),
(2961, 316, 5015, 10128, '0.0000', '0.0000'),
(2962, 316, 5016, 10133, '0.0000', '0.0000'),
(2963, 317, 5001, 10001, '0.0000', '0.0000'),
(2964, 317, 5002, 10005, '0.0000', '0.0000'),
(2965, 317, 5002, 10006, '0.0000', '0.0000'),
(2966, 317, 5003, 10063, '0.0000', '0.0000'),
(2967, 317, 5004, 10065, '0.0000', '0.0000'),
(2968, 317, 5005, 10068, '0.0000', '0.0000'),
(2969, 317, 5006, 10069, '0.0000', '0.0000'),
(2970, 317, 5007, 10072, '0.0000', '0.0000'),
(2971, 317, 5007, 10073, '0.0000', '0.0000'),
(2972, 317, 5007, 10074, '0.0000', '0.0000'),
(2973, 317, 5007, 10075, '0.0000', '0.0000'),
(2974, 317, 5007, 10076, '0.0000', '0.0000'),
(2975, 317, 5007, 10077, '0.0000', '0.0000'),
(2976, 317, 5007, 10094, '0.0000', '0.0000'),
(2977, 317, 5016, 10133, '0.0000', '0.0000'),
(2978, 318, 5001, 10003, '0.0000', '0.0000'),
(2979, 318, 5002, 10005, '0.0000', '0.0000'),
(2980, 318, 5002, 10042, '0.0000', '0.0000'),
(2981, 318, 5003, 10063, '0.0000', '0.0000'),
(2982, 318, 5004, 10065, '0.0000', '0.0000'),
(2983, 318, 5007, 10078, '0.0000', '0.0000'),
(2984, 318, 5007, 10079, '0.0000', '0.0000'),
(2985, 318, 5007, 10081, '0.0000', '0.0000'),
(2986, 318, 5007, 10082, '0.0000', '0.0000'),
(2987, 318, 5007, 10083, '0.0000', '0.0000'),
(2988, 318, 5007, 10092, '0.0000', '0.0000'),
(2989, 318, 5007, 10100, '0.0000', '0.0000'),
(2990, 318, 5012, 10119, '0.0000', '0.0000'),
(2991, 318, 5015, 10130, '0.0000', '0.0000'),
(2992, 318, 5016, 10133, '0.0000', '0.0000'),
(2993, 319, 5001, 10003, '0.0000', '0.0000'),
(2994, 319, 5002, 10005, '0.0000', '0.0000'),
(2995, 319, 5002, 10054, '0.0000', '0.0000'),
(2996, 319, 5003, 10063, '0.0000', '0.0000'),
(2997, 319, 5004, 10065, '0.0000', '0.0000'),
(2998, 319, 5007, 10081, '0.0000', '0.0000'),
(2999, 319, 5016, 10134, '0.0000', '0.0000'),
(3000, 320, 5001, 10003, '0.0000', '0.0000'),
(3001, 320, 5002, 10005, '0.0000', '0.0000'),
(3002, 320, 5002, 10055, '0.0000', '0.0000'),
(3003, 320, 5003, 10063, '0.0000', '0.0000'),
(3004, 320, 5004, 10065, '0.0000', '0.0000'),
(3005, 320, 5007, 10078, '0.0000', '0.0000'),
(3006, 320, 5007, 10079, '0.0000', '0.0000'),
(3007, 320, 5007, 10082, '0.0000', '0.0000'),
(3008, 320, 5016, 10143, '0.0000', '0.0000'),
(3009, 321, 5001, 10003, '0.0000', '0.0000'),
(3010, 321, 5002, 10005, '0.0000', '0.0000'),
(3011, 321, 5002, 10054, '0.0000', '0.0000'),
(3012, 321, 5003, 10063, '0.0000', '0.0000'),
(3013, 321, 5004, 10065, '0.0000', '0.0000'),
(3014, 321, 5007, 10081, '0.0000', '0.0000'),
(3015, 321, 5007, 10082, '0.0000', '0.0000'),
(3016, 321, 5007, 10099, '0.0000', '0.0000'),
(3017, 321, 5007, 10100, '0.0000', '0.0000'),
(3018, 321, 5016, 10146, '0.0000', '0.0000'),
(3019, 322, 5001, 10001, '0.0000', '0.0000'),
(3020, 322, 5002, 10005, '0.0000', '0.0000'),
(3021, 322, 5002, 10056, '0.0000', '0.0000'),
(3022, 322, 5003, 10063, '0.0000', '0.0000'),
(3023, 322, 5004, 10065, '0.0000', '0.0000'),
(3024, 322, 5006, 10070, '0.0000', '0.0000'),
(3025, 322, 5007, 10078, '0.0000', '0.0000'),
(3026, 322, 5007, 10079, '0.0000', '0.0000'),
(3027, 322, 5007, 10081, '0.0000', '0.0000'),
(3028, 322, 5007, 10082, '0.0000', '0.0000'),
(3029, 322, 5007, 10083, '0.0000', '0.0000'),
(3030, 322, 5007, 10100, '0.0000', '0.0000'),
(3031, 322, 5016, 10142, '0.0000', '0.0000'),
(3032, 323, 5001, 10001, '0.0000', '0.0000'),
(3033, 323, 5002, 10005, '0.0000', '0.0000'),
(3034, 323, 5002, 10056, '0.0000', '0.0000'),
(3035, 323, 5003, 10063, '0.0000', '0.0000'),
(3036, 323, 5004, 10065, '0.0000', '0.0000'),
(3037, 323, 5006, 10070, '0.0000', '0.0000'),
(3038, 323, 5007, 10078, '0.0000', '0.0000'),
(3039, 323, 5007, 10079, '0.0000', '0.0000'),
(3040, 323, 5007, 10081, '0.0000', '0.0000'),
(3041, 323, 5007, 10082, '0.0000', '0.0000'),
(3042, 323, 5007, 10100, '0.0000', '0.0000'),
(3043, 323, 5016, 10131, '0.0000', '0.0000'),
(3044, 324, 5001, 10001, '0.0000', '0.0000'),
(3045, 324, 5002, 10005, '0.0000', '0.0000'),
(3046, 324, 5002, 10057, '0.0000', '0.0000'),
(3047, 324, 5003, 10063, '0.0000', '0.0000'),
(3048, 324, 5004, 10065, '0.0000', '0.0000'),
(3049, 324, 5005, 10067, '0.0000', '0.0000'),
(3050, 324, 5006, 10069, '0.0000', '0.0000'),
(3051, 324, 5007, 10071, '0.0000', '0.0000'),
(3052, 324, 5007, 10072, '0.0000', '0.0000'),
(3053, 324, 5007, 10073, '0.0000', '0.0000'),
(3054, 324, 5007, 10074, '0.0000', '0.0000'),
(3055, 324, 5007, 10075, '0.0000', '0.0000'),
(3056, 324, 5007, 10076, '0.0000', '0.0000'),
(3057, 324, 5007, 10077, '0.0000', '0.0000'),
(3058, 324, 5007, 10102, '0.0000', '0.0000'),
(3059, 324, 5016, 10131, '0.0000', '0.0000'),
(3060, 325, 5001, 10001, '0.0000', '0.0000'),
(3061, 325, 5002, 10005, '0.0000', '0.0000'),
(3062, 325, 5002, 10032, '0.0000', '0.0000'),
(3063, 325, 5003, 10063, '0.0000', '0.0000'),
(3064, 325, 5004, 10065, '0.0000', '0.0000'),
(3065, 325, 5007, 10084, '0.0000', '0.0000'),
(3066, 325, 5007, 10085, '0.0000', '0.0000'),
(3067, 325, 5007, 10086, '0.0000', '0.0000'),
(3068, 325, 5007, 10088, '0.0000', '0.0000'),
(3069, 325, 5007, 10089, '0.0000', '0.0000'),
(3070, 325, 5016, 10131, '0.0000', '0.0000'),
(3071, 325, 5016, 10143, '0.0000', '0.0000'),
(3072, 326, 5001, 10001, '0.0000', '0.0000'),
(3073, 326, 5002, 10005, '0.0000', '0.0000'),
(3074, 326, 5002, 10032, '0.0000', '0.0000'),
(3075, 326, 5003, 10063, '0.0000', '0.0000'),
(3076, 326, 5004, 10065, '0.0000', '0.0000'),
(3077, 326, 5007, 10084, '0.0000', '0.0000'),
(3078, 326, 5007, 10085, '0.0000', '0.0000'),
(3079, 326, 5007, 10086, '0.0000', '0.0000'),
(3080, 326, 5007, 10088, '0.0000', '0.0000'),
(3081, 326, 5007, 10089, '0.0000', '0.0000'),
(3082, 326, 5016, 10136, '0.0000', '0.0000'),
(3083, 326, 5016, 10149, '0.0000', '0.0000'),
(3084, 327, 5001, 10001, '0.0000', '0.0000'),
(3085, 327, 5002, 10005, '0.0000', '0.0000'),
(3086, 327, 5002, 10032, '0.0000', '0.0000'),
(3087, 327, 5003, 10063, '0.0000', '0.0000'),
(3088, 327, 5004, 10065, '0.0000', '0.0000'),
(3089, 327, 5007, 10084, '0.0000', '0.0000'),
(3090, 327, 5007, 10085, '0.0000', '0.0000'),
(3091, 327, 5007, 10088, '0.0000', '0.0000'),
(3092, 327, 5007, 10089, '0.0000', '0.0000'),
(3093, 327, 5016, 10134, '0.0000', '0.0000'),
(3094, 327, 5016, 10135, '0.0000', '0.0000'),
(3095, 328, 5001, 10001, '0.0000', '0.0000'),
(3096, 328, 5002, 10005, '0.0000', '0.0000'),
(3097, 328, 5002, 10037, '0.0000', '0.0000'),
(3098, 328, 5003, 10063, '0.0000', '0.0000'),
(3099, 328, 5004, 10065, '0.0000', '0.0000'),
(3100, 328, 5007, 10084, '0.0000', '0.0000'),
(3101, 328, 5007, 10085, '0.0000', '0.0000'),
(3102, 328, 5007, 10087, '0.0000', '0.0000'),
(3103, 328, 5007, 10088, '0.0000', '0.0000'),
(3104, 328, 5007, 10089, '0.0000', '0.0000'),
(3105, 328, 5007, 10091, '0.0000', '0.0000'),
(3106, 328, 5016, 10131, '0.0000', '0.0000'),
(3107, 328, 5016, 10135, '0.0000', '0.0000'),
(3108, 328, 5016, 10152, '0.0000', '0.0000'),
(3109, 329, 5001, 10001, '0.0000', '0.0000'),
(3110, 329, 5002, 10005, '0.0000', '0.0000'),
(3111, 329, 5002, 10059, '0.0000', '0.0000'),
(3112, 329, 5003, 10063, '0.0000', '0.0000'),
(3113, 329, 5004, 10065, '0.0000', '0.0000'),
(3114, 329, 5005, 10067, '0.0000', '0.0000'),
(3115, 329, 5006, 10069, '0.0000', '0.0000'),
(3116, 329, 5007, 10071, '0.0000', '0.0000'),
(3117, 329, 5007, 10072, '0.0000', '0.0000'),
(3118, 329, 5007, 10074, '0.0000', '0.0000'),
(3119, 329, 5007, 10075, '0.0000', '0.0000'),
(3120, 329, 5007, 10076, '0.0000', '0.0000'),
(3121, 329, 5007, 10077, '0.0000', '0.0000'),
(3122, 329, 5016, 10131, '0.0000', '0.0000'),
(3123, 330, 5001, 10001, '0.0000', '0.0000'),
(3124, 330, 5002, 10005, '0.0000', '0.0000'),
(3125, 330, 5002, 10028, '0.0000', '0.0000'),
(3126, 330, 5003, 10063, '0.0000', '0.0000'),
(3127, 330, 5004, 10065, '0.0000', '0.0000'),
(3128, 330, 5007, 10084, '0.0000', '0.0000'),
(3129, 330, 5007, 10085, '0.0000', '0.0000'),
(3130, 330, 5007, 10088, '0.0000', '0.0000'),
(3131, 330, 5007, 10089, '0.0000', '0.0000'),
(3132, 330, 5016, 10135, '0.0000', '0.0000'),
(3133, 331, 5001, 10001, '0.0000', '0.0000'),
(3134, 331, 5002, 10005, '0.0000', '0.0000'),
(3135, 331, 5002, 10028, '0.0000', '0.0000'),
(3136, 331, 5003, 10063, '0.0000', '0.0000'),
(3137, 331, 5004, 10065, '0.0000', '0.0000'),
(3138, 331, 5007, 10084, '0.0000', '0.0000'),
(3139, 331, 5007, 10085, '0.0000', '0.0000'),
(3140, 331, 5007, 10088, '0.0000', '0.0000'),
(3141, 331, 5007, 10089, '0.0000', '0.0000'),
(3142, 331, 5016, 10135, '0.0000', '0.0000'),
(3143, 332, 5001, 10001, '0.0000', '0.0000'),
(3144, 332, 5002, 10005, '0.0000', '0.0000'),
(3145, 332, 5002, 10028, '0.0000', '0.0000'),
(3146, 332, 5003, 10063, '0.0000', '0.0000'),
(3147, 332, 5004, 10065, '0.0000', '0.0000'),
(3148, 332, 5007, 10084, '0.0000', '0.0000'),
(3149, 332, 5007, 10085, '0.0000', '0.0000'),
(3150, 332, 5007, 10088, '0.0000', '0.0000'),
(3151, 332, 5007, 10089, '0.0000', '0.0000'),
(3152, 332, 5016, 10131, '0.0000', '0.0000'),
(3153, 332, 5016, 10132, '0.0000', '0.0000'),
(3154, 333, 5001, 10001, '0.0000', '0.0000'),
(3155, 333, 5002, 10005, '0.0000', '0.0000'),
(3156, 333, 5002, 10056, '0.0000', '0.0000'),
(3157, 333, 5003, 10063, '0.0000', '0.0000'),
(3158, 333, 5004, 10065, '0.0000', '0.0000'),
(3159, 333, 5006, 10070, '0.0000', '0.0000'),
(3160, 333, 5007, 10078, '0.0000', '0.0000'),
(3161, 333, 5007, 10079, '0.0000', '0.0000'),
(3162, 333, 5007, 10081, '0.0000', '0.0000'),
(3163, 333, 5007, 10082, '0.0000', '0.0000'),
(3164, 333, 5007, 10083, '0.0000', '0.0000'),
(3165, 333, 5007, 10100, '0.0000', '0.0000'),
(3166, 333, 5016, 10135, '0.0000', '0.0000'),
(3167, 334, 5001, 10001, '0.0000', '0.0000'),
(3168, 334, 5002, 10005, '0.0000', '0.0000'),
(3169, 334, 5002, 10032, '0.0000', '0.0000'),
(3170, 334, 5003, 10063, '0.0000', '0.0000'),
(3171, 334, 5004, 10065, '0.0000', '0.0000'),
(3172, 334, 5007, 10084, '0.0000', '0.0000'),
(3173, 334, 5007, 10085, '0.0000', '0.0000'),
(3174, 334, 5007, 10088, '0.0000', '0.0000'),
(3175, 334, 5007, 10089, '0.0000', '0.0000'),
(3176, 334, 5016, 10136, '0.0000', '0.0000'),
(3177, 335, 5001, 10001, '0.0000', '0.0000'),
(3178, 335, 5002, 10005, '0.0000', '0.0000'),
(3179, 335, 5002, 10032, '0.0000', '0.0000'),
(3180, 335, 5003, 10063, '0.0000', '0.0000'),
(3181, 335, 5004, 10065, '0.0000', '0.0000'),
(3182, 335, 5007, 10084, '0.0000', '0.0000'),
(3183, 335, 5007, 10085, '0.0000', '0.0000'),
(3184, 335, 5007, 10088, '0.0000', '0.0000'),
(3185, 335, 5007, 10089, '0.0000', '0.0000'),
(3186, 335, 5016, 10136, '0.0000', '0.0000'),
(3187, 336, 5001, 10001, '0.0000', '0.0000'),
(3188, 336, 5002, 10005, '0.0000', '0.0000'),
(3189, 336, 5002, 10032, '0.0000', '0.0000'),
(3190, 336, 5003, 10063, '0.0000', '0.0000'),
(3191, 336, 5004, 10065, '0.0000', '0.0000'),
(3192, 336, 5007, 10084, '0.0000', '0.0000'),
(3193, 336, 5007, 10085, '0.0000', '0.0000'),
(3194, 336, 5007, 10088, '0.0000', '0.0000'),
(3195, 336, 5007, 10089, '0.0000', '0.0000'),
(3196, 336, 5016, 10136, '0.0000', '0.0000'),
(3197, 337, 5001, 10001, '0.0000', '0.0000'),
(3198, 337, 5002, 10005, '0.0000', '0.0000'),
(3199, 337, 5002, 10028, '0.0000', '0.0000'),
(3200, 337, 5003, 10063, '0.0000', '0.0000'),
(3201, 337, 5004, 10065, '0.0000', '0.0000'),
(3202, 337, 5007, 10084, '0.0000', '0.0000'),
(3203, 337, 5007, 10085, '0.0000', '0.0000'),
(3204, 337, 5007, 10086, '0.0000', '0.0000'),
(3205, 337, 5007, 10088, '0.0000', '0.0000'),
(3206, 337, 5016, 10131, '0.0000', '0.0000'),
(3207, 337, 5016, 10135, '0.0000', '0.0000'),
(3208, 337, 5016, 10142, '0.0000', '0.0000'),
(3209, 338, 5001, 10001, '0.0000', '0.0000'),
(3210, 338, 5002, 10005, '0.0000', '0.0000'),
(3211, 338, 5002, 10015, '0.0000', '0.0000'),
(3212, 338, 5003, 10063, '0.0000', '0.0000'),
(3213, 338, 5004, 10065, '0.0000', '0.0000'),
(3214, 338, 5007, 10084, '0.0000', '0.0000'),
(3215, 338, 5007, 10085, '0.0000', '0.0000'),
(3216, 338, 5007, 10088, '0.0000', '0.0000'),
(3217, 338, 5007, 10089, '0.0000', '0.0000'),
(3218, 338, 5009, 10110, '0.0000', '0.0000'),
(3219, 338, 5010, 10112, '0.0000', '0.0000'),
(3220, 338, 5011, 10116, '0.0000', '0.0000'),
(3221, 338, 5016, 10134, '0.0000', '0.0000'),
(3222, 339, 5001, 10001, '0.0000', '0.0000'),
(3223, 339, 5002, 10005, '0.0000', '0.0000'),
(3224, 339, 5002, 10015, '0.0000', '0.0000'),
(3225, 339, 5003, 10063, '0.0000', '0.0000'),
(3226, 339, 5004, 10065, '0.0000', '0.0000'),
(3227, 339, 5007, 10084, '0.0000', '0.0000'),
(3228, 339, 5007, 10085, '0.0000', '0.0000'),
(3229, 339, 5007, 10088, '0.0000', '0.0000'),
(3230, 339, 5007, 10089, '0.0000', '0.0000'),
(3231, 339, 5009, 10110, '0.0000', '0.0000'),
(3232, 339, 5010, 10112, '0.0000', '0.0000'),
(3233, 339, 5011, 10116, '0.0000', '0.0000'),
(3234, 339, 5016, 10146, '0.0000', '0.0000'),
(3235, 340, 5001, 10001, '0.0000', '0.0000'),
(3236, 340, 5002, 10005, '0.0000', '0.0000'),
(3237, 340, 5002, 10015, '0.0000', '0.0000'),
(3238, 340, 5003, 10063, '0.0000', '0.0000'),
(3239, 340, 5004, 10065, '0.0000', '0.0000'),
(3240, 340, 5007, 10085, '0.0000', '0.0000'),
(3241, 340, 5007, 10088, '0.0000', '0.0000'),
(3242, 340, 5007, 10089, '0.0000', '0.0000'),
(3243, 340, 5009, 10110, '0.0000', '0.0000'),
(3244, 340, 5010, 10112, '0.0000', '0.0000'),
(3245, 340, 5011, 10116, '0.0000', '0.0000'),
(3246, 340, 5016, 10145, '0.0000', '0.0000'),
(3247, 341, 5001, 10001, '0.0000', '0.0000'),
(3248, 341, 5002, 10005, '0.0000', '0.0000'),
(3249, 341, 5002, 10009, '0.0000', '0.0000'),
(3250, 341, 5003, 10063, '0.0000', '0.0000'),
(3251, 341, 5004, 10065, '0.0000', '0.0000'),
(3252, 341, 5007, 10084, '0.0000', '0.0000'),
(3253, 341, 5007, 10085, '0.0000', '0.0000'),
(3254, 341, 5007, 10088, '0.0000', '0.0000'),
(3255, 341, 5007, 10089, '0.0000', '0.0000'),
(3256, 341, 5009, 10110, '0.0000', '0.0000'),
(3257, 341, 5010, 10112, '0.0000', '0.0000'),
(3258, 341, 5011, 10116, '0.0000', '0.0000'),
(3259, 341, 5016, 10135, '0.0000', '0.0000'),
(3260, 342, 5001, 10001, '0.0000', '0.0000'),
(3261, 342, 5002, 10005, '0.0000', '0.0000'),
(3262, 342, 5002, 10009, '0.0000', '0.0000'),
(3263, 342, 5003, 10063, '0.0000', '0.0000'),
(3264, 342, 5004, 10065, '0.0000', '0.0000'),
(3265, 342, 5007, 10084, '0.0000', '0.0000'),
(3266, 342, 5007, 10085, '0.0000', '0.0000'),
(3267, 342, 5007, 10088, '0.0000', '0.0000'),
(3268, 342, 5007, 10089, '0.0000', '0.0000'),
(3269, 342, 5007, 10090, '0.0000', '0.0000'),
(3270, 342, 5007, 10097, '0.0000', '0.0000'),
(3271, 342, 5007, 10098, '0.0000', '0.0000'),
(3272, 342, 5007, 10107, '0.0000', '0.0000'),
(3273, 342, 5009, 10110, '0.0000', '0.0000'),
(3274, 342, 5010, 10112, '0.0000', '0.0000'),
(3275, 342, 5011, 10116, '0.0000', '0.0000'),
(3276, 342, 5016, 10145, '0.0000', '0.0000'),
(3277, 343, 5001, 10001, '0.0000', '0.0000'),
(3278, 343, 5002, 10005, '0.0000', '0.0000'),
(3279, 343, 5002, 10009, '0.0000', '0.0000'),
(3280, 343, 5003, 10063, '0.0000', '0.0000'),
(3281, 343, 5004, 10065, '0.0000', '0.0000'),
(3282, 343, 5007, 10084, '0.0000', '0.0000'),
(3283, 343, 5007, 10085, '0.0000', '0.0000'),
(3284, 343, 5007, 10088, '0.0000', '0.0000'),
(3285, 343, 5007, 10089, '0.0000', '0.0000'),
(3286, 343, 5009, 10110, '0.0000', '0.0000'),
(3287, 343, 5010, 10112, '0.0000', '0.0000'),
(3288, 343, 5011, 10116, '0.0000', '0.0000'),
(3289, 343, 5016, 10135, '0.0000', '0.0000'),
(3290, 344, 5001, 10001, '0.0000', '0.0000'),
(3291, 344, 5002, 10005, '0.0000', '0.0000'),
(3292, 344, 5002, 10009, '0.0000', '0.0000'),
(3293, 344, 5003, 10063, '0.0000', '0.0000'),
(3294, 344, 5004, 10065, '0.0000', '0.0000'),
(3295, 344, 5007, 10084, '0.0000', '0.0000'),
(3296, 344, 5007, 10085, '0.0000', '0.0000'),
(3297, 344, 5007, 10087, '0.0000', '0.0000'),
(3298, 344, 5007, 10088, '0.0000', '0.0000'),
(3299, 344, 5007, 10089, '0.0000', '0.0000'),
(3300, 344, 5009, 10110, '0.0000', '0.0000'),
(3301, 344, 5010, 10112, '0.0000', '0.0000'),
(3302, 344, 5011, 10116, '0.0000', '0.0000'),
(3303, 344, 5016, 10138, '0.0000', '0.0000'),
(3304, 345, 5001, 10001, '0.0000', '0.0000'),
(3305, 345, 5002, 10005, '0.0000', '0.0000'),
(3306, 345, 5002, 10009, '0.0000', '0.0000'),
(3307, 345, 5003, 10063, '0.0000', '0.0000'),
(3308, 345, 5004, 10065, '0.0000', '0.0000'),
(3309, 345, 5007, 10084, '0.0000', '0.0000'),
(3310, 345, 5007, 10085, '0.0000', '0.0000'),
(3311, 345, 5007, 10088, '0.0000', '0.0000'),
(3312, 345, 5007, 10089, '0.0000', '0.0000'),
(3313, 345, 5009, 10110, '0.0000', '0.0000'),
(3314, 345, 5010, 10112, '0.0000', '0.0000'),
(3315, 345, 5011, 10116, '0.0000', '0.0000'),
(3316, 345, 5016, 10141, '0.0000', '0.0000'),
(3317, 346, 5001, 10001, '0.0000', '0.0000'),
(3318, 346, 5002, 10005, '0.0000', '0.0000'),
(3319, 346, 5002, 10015, '0.0000', '0.0000'),
(3320, 346, 5003, 10063, '0.0000', '0.0000'),
(3321, 346, 5004, 10065, '0.0000', '0.0000'),
(3322, 346, 5007, 10084, '0.0000', '0.0000'),
(3323, 346, 5007, 10085, '0.0000', '0.0000'),
(3324, 346, 5007, 10088, '0.0000', '0.0000'),
(3325, 346, 5007, 10089, '0.0000', '0.0000'),
(3326, 346, 5009, 10110, '0.0000', '0.0000'),
(3327, 346, 5010, 10112, '0.0000', '0.0000'),
(3328, 346, 5011, 10116, '0.0000', '0.0000'),
(3329, 346, 5016, 10145, '0.0000', '0.0000'),
(3330, 347, 5001, 10001, '0.0000', '0.0000'),
(3331, 347, 5002, 10005, '0.0000', '0.0000'),
(3332, 347, 5002, 10009, '0.0000', '0.0000'),
(3333, 347, 5003, 10063, '0.0000', '0.0000'),
(3334, 347, 5004, 10065, '0.0000', '0.0000'),
(3335, 347, 5007, 10090, '0.0000', '0.0000'),
(3336, 347, 5007, 10097, '0.0000', '0.0000'),
(3337, 347, 5007, 10098, '0.0000', '0.0000'),
(3338, 347, 5007, 10107, '0.0000', '0.0000'),
(3339, 347, 5009, 10110, '0.0000', '0.0000'),
(3340, 347, 5010, 10112, '0.0000', '0.0000'),
(3341, 347, 5011, 10116, '0.0000', '0.0000'),
(3342, 347, 5016, 10135, '0.0000', '0.0000'),
(3343, 348, 5001, 10001, '0.0000', '0.0000'),
(3344, 348, 5002, 10005, '0.0000', '0.0000'),
(3345, 348, 5002, 10009, '0.0000', '0.0000'),
(3346, 348, 5003, 10064, '0.0000', '0.0000'),
(3347, 348, 5004, 10065, '0.0000', '0.0000'),
(3348, 348, 5007, 10090, '0.0000', '0.0000'),
(3349, 348, 5007, 10097, '0.0000', '0.0000'),
(3350, 348, 5007, 10098, '0.0000', '0.0000'),
(3351, 348, 5007, 10107, '0.0000', '0.0000'),
(3352, 348, 5009, 10110, '0.0000', '0.0000'),
(3353, 348, 5010, 10112, '0.0000', '0.0000'),
(3354, 348, 5011, 10116, '0.0000', '0.0000'),
(3355, 348, 5016, 10135, '0.0000', '0.0000'),
(3356, 349, 5001, 10001, '0.0000', '0.0000'),
(3357, 349, 5002, 10005, '0.0000', '0.0000'),
(3358, 349, 5002, 10009, '0.0000', '0.0000'),
(3359, 349, 5003, 10064, '0.0000', '0.0000'),
(3360, 349, 5004, 10065, '0.0000', '0.0000'),
(3361, 349, 5007, 10090, '0.0000', '0.0000'),
(3362, 349, 5007, 10097, '0.0000', '0.0000'),
(3363, 349, 5007, 10098, '0.0000', '0.0000'),
(3364, 349, 5007, 10107, '0.0000', '0.0000'),
(3365, 349, 5009, 10110, '0.0000', '0.0000'),
(3366, 349, 5010, 10112, '0.0000', '0.0000'),
(3367, 349, 5011, 10116, '0.0000', '0.0000'),
(3368, 349, 5016, 10141, '0.0000', '0.0000'),
(3369, 350, 5001, 10001, '0.0000', '0.0000'),
(3370, 350, 5002, 10005, '0.0000', '0.0000'),
(3371, 350, 5002, 10036, '0.0000', '0.0000'),
(3372, 350, 5003, 10063, '0.0000', '0.0000'),
(3373, 350, 5004, 10065, '0.0000', '0.0000'),
(3374, 350, 5007, 10084, '0.0000', '0.0000'),
(3375, 350, 5007, 10085, '0.0000', '0.0000'),
(3376, 350, 5007, 10088, '0.0000', '0.0000'),
(3377, 350, 5007, 10089, '0.0000', '0.0000'),
(3378, 350, 5016, 10134, '0.0000', '0.0000'),
(3379, 350, 5016, 10135, '0.0000', '0.0000'),
(3380, 350, 5016, 10142, '0.0000', '0.0000'),
(3381, 351, 5001, 10001, '0.0000', '0.0000'),
(3382, 351, 5002, 10005, '0.0000', '0.0000'),
(3383, 351, 5002, 10061, '0.0000', '0.0000'),
(3384, 351, 5003, 10063, '0.0000', '0.0000'),
(3385, 351, 5004, 10065, '0.0000', '0.0000'),
(3386, 351, 5007, 10084, '0.0000', '0.0000'),
(3387, 351, 5007, 10085, '0.0000', '0.0000'),
(3388, 351, 5007, 10088, '0.0000', '0.0000'),
(3389, 351, 5007, 10089, '0.0000', '0.0000'),
(3390, 351, 5007, 10090, '0.0000', '0.0000'),
(3391, 351, 5016, 10133, '0.0000', '0.0000'),
(3392, 351, 5016, 10134, '0.0000', '0.0000'),
(3393, 351, 5016, 10135, '0.0000', '0.0000'),
(3394, 352, 5001, 10001, '0.0000', '0.0000'),
(3395, 352, 5002, 10005, '0.0000', '0.0000'),
(3396, 352, 5002, 10025, '0.0000', '0.0000'),
(3397, 352, 5003, 10063, '0.0000', '0.0000'),
(3398, 352, 5004, 10065, '0.0000', '0.0000'),
(3399, 352, 5006, 10069, '0.0000', '0.0000'),
(3400, 352, 5007, 10071, '0.0000', '0.0000'),
(3401, 352, 5007, 10072, '0.0000', '0.0000'),
(3402, 352, 5007, 10075, '0.0000', '0.0000'),
(3403, 352, 5007, 10076, '0.0000', '0.0000'),
(3404, 352, 5007, 10077, '0.0000', '0.0000'),
(3405, 352, 5007, 10102, '0.0000', '0.0000'),
(3406, 352, 5016, 10135, '0.0000', '0.0000'),
(3407, 353, 5001, 10001, '0.0000', '0.0000'),
(3408, 353, 5002, 10005, '0.0000', '0.0000'),
(3409, 353, 5002, 10026, '0.0000', '0.0000'),
(3410, 353, 5003, 10063, '0.0000', '0.0000'),
(3411, 353, 5004, 10065, '0.0000', '0.0000'),
(3412, 353, 5007, 10084, '0.0000', '0.0000'),
(3413, 353, 5007, 10085, '0.0000', '0.0000'),
(3414, 353, 5007, 10088, '0.0000', '0.0000'),
(3415, 353, 5016, 10131, '0.0000', '0.0000'),
(3416, 353, 5016, 10134, '0.0000', '0.0000'),
(3417, 353, 5016, 10135, '0.0000', '0.0000'),
(3418, 353, 5016, 10136, '0.0000', '0.0000'),
(3419, 354, 5001, 10001, '0.0000', '0.0000'),
(3420, 354, 5002, 10005, '0.0000', '0.0000'),
(3421, 354, 5002, 10026, '0.0000', '0.0000'),
(3422, 354, 5003, 10063, '0.0000', '0.0000'),
(3423, 354, 5004, 10065, '0.0000', '0.0000'),
(3424, 354, 5007, 10084, '0.0000', '0.0000'),
(3425, 354, 5007, 10085, '0.0000', '0.0000'),
(3426, 354, 5007, 10088, '0.0000', '0.0000'),
(3427, 354, 5016, 10134, '0.0000', '0.0000'),
(3428, 354, 5016, 10135, '0.0000', '0.0000'),
(3429, 355, 5001, 10001, '0.0000', '0.0000'),
(3430, 355, 5002, 10005, '0.0000', '0.0000'),
(3431, 355, 5002, 10028, '0.0000', '0.0000'),
(3432, 355, 5003, 10063, '0.0000', '0.0000'),
(3433, 355, 5004, 10065, '0.0000', '0.0000'),
(3434, 355, 5007, 10084, '0.0000', '0.0000'),
(3435, 355, 5007, 10085, '0.0000', '0.0000');
INSERT INTO `oc_ocfilter_option_value_to_product` (`ocfilter_option_value_to_product_id`, `product_id`, `option_id`, `value_id`, `slide_value_min`, `slide_value_max`) VALUES
(3436, 355, 5007, 10088, '0.0000', '0.0000'),
(3437, 355, 5007, 10089, '0.0000', '0.0000'),
(3438, 355, 5016, 10134, '0.0000', '0.0000'),
(3439, 355, 5016, 10135, '0.0000', '0.0000'),
(3440, 356, 5001, 10001, '0.0000', '0.0000'),
(3441, 356, 5002, 10005, '0.0000', '0.0000'),
(3442, 356, 5002, 10062, '0.0000', '0.0000'),
(3443, 356, 5003, 10063, '0.0000', '0.0000'),
(3444, 356, 5004, 10066, '0.0000', '0.0000'),
(3445, 356, 5007, 10078, '0.0000', '0.0000'),
(3446, 356, 5007, 10079, '0.0000', '0.0000'),
(3447, 356, 5007, 10081, '0.0000', '0.0000'),
(3448, 356, 5007, 10082, '0.0000', '0.0000'),
(3449, 356, 5007, 10083, '0.0000', '0.0000'),
(3450, 356, 5007, 10092, '0.0000', '0.0000'),
(3451, 356, 5008, 10109, '0.0000', '0.0000'),
(3452, 356, 5016, 10131, '0.0000', '0.0000'),
(3453, 357, 5001, 10001, '0.0000', '0.0000'),
(3454, 357, 5002, 10005, '0.0000', '0.0000'),
(3455, 357, 5002, 10038, '0.0000', '0.0000'),
(3456, 357, 5003, 10063, '0.0000', '0.0000'),
(3457, 357, 5004, 10066, '0.0000', '0.0000'),
(3458, 357, 5007, 10078, '0.0000', '0.0000'),
(3459, 357, 5007, 10079, '0.0000', '0.0000'),
(3460, 357, 5007, 10081, '0.0000', '0.0000'),
(3461, 357, 5007, 10082, '0.0000', '0.0000'),
(3462, 357, 5007, 10083, '0.0000', '0.0000'),
(3463, 357, 5007, 10099, '0.0000', '0.0000'),
(3464, 357, 5007, 10100, '0.0000', '0.0000'),
(3465, 357, 5008, 10108, '0.0000', '0.0000'),
(3466, 357, 5016, 10132, '0.0000', '0.0000'),
(3467, 357, 5016, 10135, '0.0000', '0.0000'),
(3468, 358, 5001, 10001, '0.0000', '0.0000'),
(3469, 358, 5002, 10005, '0.0000', '0.0000'),
(3470, 358, 5002, 10038, '0.0000', '0.0000'),
(3471, 358, 5003, 10063, '0.0000', '0.0000'),
(3472, 358, 5004, 10066, '0.0000', '0.0000'),
(3473, 358, 5007, 10078, '0.0000', '0.0000'),
(3474, 358, 5007, 10079, '0.0000', '0.0000'),
(3475, 358, 5007, 10081, '0.0000', '0.0000'),
(3476, 358, 5007, 10082, '0.0000', '0.0000'),
(3477, 358, 5007, 10083, '0.0000', '0.0000'),
(3478, 358, 5007, 10100, '0.0000', '0.0000'),
(3479, 358, 5008, 10108, '0.0000', '0.0000'),
(3480, 358, 5016, 10146, '0.0000', '0.0000'),
(3481, 359, 5001, 10001, '0.0000', '0.0000'),
(3482, 359, 5002, 10005, '0.0000', '0.0000'),
(3483, 359, 5002, 10038, '0.0000', '0.0000'),
(3484, 359, 5003, 10063, '0.0000', '0.0000'),
(3485, 359, 5004, 10066, '0.0000', '0.0000'),
(3486, 359, 5007, 10078, '0.0000', '0.0000'),
(3487, 359, 5007, 10079, '0.0000', '0.0000'),
(3488, 359, 5007, 10081, '0.0000', '0.0000'),
(3489, 359, 5007, 10082, '0.0000', '0.0000'),
(3490, 359, 5007, 10083, '0.0000', '0.0000'),
(3491, 359, 5007, 10099, '0.0000', '0.0000'),
(3492, 359, 5007, 10100, '0.0000', '0.0000'),
(3493, 359, 5008, 10108, '0.0000', '0.0000'),
(3494, 359, 5016, 10145, '0.0000', '0.0000'),
(3495, 360, 5001, 10001, '0.0000', '0.0000'),
(3496, 360, 5002, 10005, '0.0000', '0.0000'),
(3497, 360, 5002, 10038, '0.0000', '0.0000'),
(3498, 360, 5003, 10063, '0.0000', '0.0000'),
(3499, 360, 5004, 10066, '0.0000', '0.0000'),
(3500, 360, 5007, 10078, '0.0000', '0.0000'),
(3501, 360, 5007, 10079, '0.0000', '0.0000'),
(3502, 360, 5007, 10081, '0.0000', '0.0000'),
(3503, 360, 5007, 10082, '0.0000', '0.0000'),
(3504, 360, 5007, 10099, '0.0000', '0.0000'),
(3505, 360, 5007, 10100, '0.0000', '0.0000'),
(3506, 360, 5008, 10108, '0.0000', '0.0000'),
(3507, 360, 5016, 10145, '0.0000', '0.0000'),
(3508, 361, 5001, 10001, '0.0000', '0.0000'),
(3509, 361, 5002, 10005, '0.0000', '0.0000'),
(3510, 361, 5002, 10062, '0.0000', '0.0000'),
(3511, 361, 5003, 10063, '0.0000', '0.0000'),
(3512, 361, 5004, 10066, '0.0000', '0.0000'),
(3513, 361, 5007, 10078, '0.0000', '0.0000'),
(3514, 361, 5007, 10079, '0.0000', '0.0000'),
(3515, 361, 5007, 10081, '0.0000', '0.0000'),
(3516, 361, 5007, 10082, '0.0000', '0.0000'),
(3517, 361, 5007, 10083, '0.0000', '0.0000'),
(3518, 361, 5007, 10099, '0.0000', '0.0000'),
(3519, 361, 5007, 10100, '0.0000', '0.0000'),
(3520, 361, 5008, 10108, '0.0000', '0.0000'),
(3521, 361, 5016, 10131, '0.0000', '0.0000'),
(3522, 362, 5001, 10001, '0.0000', '0.0000'),
(3523, 362, 5002, 10005, '0.0000', '0.0000'),
(3524, 362, 5002, 10062, '0.0000', '0.0000'),
(3525, 362, 5003, 10063, '0.0000', '0.0000'),
(3526, 362, 5004, 10066, '0.0000', '0.0000'),
(3527, 362, 5007, 10078, '0.0000', '0.0000'),
(3528, 362, 5007, 10079, '0.0000', '0.0000'),
(3529, 362, 5007, 10081, '0.0000', '0.0000'),
(3530, 362, 5007, 10082, '0.0000', '0.0000'),
(3531, 362, 5007, 10083, '0.0000', '0.0000'),
(3532, 362, 5007, 10099, '0.0000', '0.0000'),
(3533, 362, 5007, 10100, '0.0000', '0.0000'),
(3534, 362, 5008, 10108, '0.0000', '0.0000'),
(3535, 362, 5016, 10134, '0.0000', '0.0000'),
(3536, 363, 5001, 10001, '0.0000', '0.0000'),
(3537, 363, 5002, 10005, '0.0000', '0.0000'),
(3538, 363, 5002, 10025, '0.0000', '0.0000'),
(3539, 363, 5003, 10063, '0.0000', '0.0000'),
(3540, 363, 5004, 10065, '0.0000', '0.0000'),
(3541, 363, 5006, 10069, '0.0000', '0.0000'),
(3542, 363, 5007, 10072, '0.0000', '0.0000'),
(3543, 363, 5007, 10073, '0.0000', '0.0000'),
(3544, 363, 5007, 10074, '0.0000', '0.0000'),
(3545, 363, 5007, 10075, '0.0000', '0.0000'),
(3546, 363, 5007, 10076, '0.0000', '0.0000'),
(3547, 363, 5007, 10077, '0.0000', '0.0000'),
(3548, 363, 5007, 10094, '0.0000', '0.0000'),
(3549, 363, 5016, 10133, '0.0000', '0.0000'),
(3550, 364, 5001, 10001, '0.0000', '0.0000'),
(3551, 364, 5002, 10005, '0.0000', '0.0000'),
(3552, 364, 5002, 10025, '0.0000', '0.0000'),
(3553, 364, 5003, 10063, '0.0000', '0.0000'),
(3554, 364, 5004, 10065, '0.0000', '0.0000'),
(3555, 364, 5005, 10067, '0.0000', '0.0000'),
(3556, 364, 5006, 10069, '0.0000', '0.0000'),
(3557, 364, 5007, 10071, '0.0000', '0.0000'),
(3558, 364, 5007, 10072, '0.0000', '0.0000'),
(3559, 364, 5007, 10074, '0.0000', '0.0000'),
(3560, 364, 5007, 10075, '0.0000', '0.0000'),
(3561, 364, 5007, 10076, '0.0000', '0.0000'),
(3562, 364, 5007, 10077, '0.0000', '0.0000'),
(3563, 364, 5007, 10102, '0.0000', '0.0000'),
(3564, 364, 5007, 10103, '0.0000', '0.0000'),
(3565, 364, 5016, 10133, '0.0000', '0.0000'),
(3566, 364, 5016, 10134, '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_option_value_to_product_description`
--

CREATE TABLE `oc_ocfilter_option_value_to_product_description` (
  `product_id` int(11) NOT NULL,
  `value_id` bigint(20) NOT NULL,
  `option_id` int(11) NOT NULL,
  `language_id` tinyint(2) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_page`
--

CREATE TABLE `oc_ocfilter_page` (
  `ocfilter_page_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `keyword` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `over` set('domain','category') NOT NULL DEFAULT 'category',
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ocfilter_page_description`
--

CREATE TABLE `oc_ocfilter_page_description` (
  `ocfilter_page_id` int(11) NOT NULL DEFAULT 0,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_characteristic`
--

CREATE TABLE `oc_option_characteristic` (
  `characteristic_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_characteristic_description`
--

CREATE TABLE `oc_option_characteristic_description` (
  `characteristic_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_characteristic`
--

CREATE TABLE `oc_option_value_characteristic` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `characteristic_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT 0,
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `order_status_id` int(11) NOT NULL DEFAULT 0,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT 1.00000000,
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT 0,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `tax` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'В обработке'),
(3, 1, 'Доставлено'),
(7, 1, 'Отменено'),
(5, 1, 'Сделка завершена'),
(8, 1, 'Возврат'),
(9, 1, 'Отмена и аннулирование'),
(10, 1, 'Неудавшийся'),
(11, 1, 'Возмещенный'),
(12, 1, 'Полностью измененный'),
(13, 1, 'Полный возврат'),
(1, 1, 'Ожидание'),
(15, 1, 'Обработано'),
(14, 1, 'Истекло'),
(2, 2, 'Processing'),
(8, 2, 'Denied'),
(11, 2, 'Refunded'),
(3, 2, 'Shipped'),
(10, 2, 'Failed'),
(1, 2, 'Pending'),
(9, 2, 'Canceled Reversal'),
(7, 2, 'Canceled'),
(12, 2, 'Reversed'),
(13, 2, 'Chargeback'),
(5, 2, 'Complete'),
(14, 2, 'Expired'),
(16, 1, 'Анулированный'),
(16, 2, 'Voided'),
(15, 2, 'Processed');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `points` int(8) NOT NULL DEFAULT 0,
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `weight_class_id` int(11) NOT NULL DEFAULT 0,
  `length` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `width` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `height` decimal(15,8) NOT NULL DEFAULT 0.00000000,
  `length_class_id` int(11) NOT NULL DEFAULT 0,
  `measure_name` varchar(255) NOT NULL DEFAULT '',
  `subtract` tinyint(1) NOT NULL DEFAULT 1,
  `minimum` int(11) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `viewed` int(5) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description_composition`
--

CREATE TABLE `oc_product_description_composition` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `composition` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT 0,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `price_old` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_article`
--

CREATE TABLE `oc_product_related_article` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_mn`
--

CREATE TABLE `oc_product_related_mn` (
  `product_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related_wb`
--

CREATE TABLE `oc_product_related_wb` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `customer_group_id` int(11) NOT NULL DEFAULT 0,
  `points` int(8) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1,
  `price` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Возмещенный'),
(2, 1, 'Возврат средств'),
(3, 1, 'Отправлена замена'),
(1, 2, 'Refunded'),
(3, 2, 'Replacement Sent'),
(2, 2, 'Credit Issued');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Получен неисправным (сломанным)'),
(1, 2, 'Dead On Arrival'),
(2, 1, 'Получен не тот (ошибочный) товар'),
(2, 2, 'Received Wrong Item'),
(3, 1, 'Заказан по ошибке'),
(3, 2, 'Order Error'),
(4, 1, 'Неисправен, пожалуйста укажите/приложите подробности'),
(4, 2, 'Faulty, please supply details'),
(5, 1, 'Другое (другая причина), пожалуйста укажите/приложите подробности'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'В ожидании'),
(3, 1, 'Выполнен'),
(2, 1, 'Ожидание товара'),
(1, 2, 'Pending'),
(2, 2, 'Awaiting Products'),
(3, 2, 'Complete');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review_article`
--

CREATE TABLE `oc_review_article` (
  `review_article_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(1, 0, 1, 'common/home', ''),
(2, 0, 2, 'common/home', 'en'),
(3, 0, 1, 'information_id=5', 'policy'),
(4, 0, 1, 'extension/quickcheckout/checkout', 'checkout'),
(5, 0, 1, 'account/register', 'register'),
(6, 0, 1, 'information/contact', 'contacts'),
(7, 0, 1, 'product/search', 'search'),
(8, 0, 1, 'checkout/cart', 'cart'),
(9, 0, 1, 'checkout/checkout', 'checkout'),
(10, 0, 1, 'account/login', 'login');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` longtext NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('095f05ab8f0bfb36ece3b4e65f', '{\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2020-12-12 14:42:39'),
('1984f0ffef5f1d77970f8305f8', '{\"language\":\"ru-ru\",\"currency\":\"RUB\",\"user_id\":\"1\",\"user_token\":\"6l3BxBvL2j2It0SmYyvDKSEIXsJ8TrL0\",\"api_token\":\"72afdb9f727038240877d805c4\"}', '2020-12-12 15:00:56'),
('642b571452ac73b4ecc6581eeb', '{\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2020-12-13 10:56:16'),
('72afdb9f727038240877d805c4', '{\"api_id\":\"1\"}', '2020-12-12 14:41:44'),
('77a264a207724424ec0343293f', '{\"language\":\"ru-ru\",\"currency\":\"RUB\",\"user_id\":\"1\",\"user_token\":\"Soa0KbaIs4KTR5tLcV4ROKXLw07EgI5R\",\"api_token\":\"85dd0cda033fce33e3106b1cfc\"}', '2020-12-16 02:30:13'),
('85dd0cda033fce33e3106b1cfc', '{\"api_id\":\"1\"}', '2020-12-13 10:20:24'),
('e702ac82b8f96f102264d2c195', '{\"language\":\"ru-ru\",\"currency\":\"RUB\"}', '2020-12-13 11:35:15');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(7405, 0, 'module_manager', 'module_manager_date_format', '', 0),
(7406, 0, 'module_manager', 'module_manager_addips', '', 0),
(7407, 0, 'module_manager', 'module_manager_status', '1', 0),
(7403, 0, 'module_manager', 'module_manager_name_format', 'firstname', 0),
(7404, 0, 'module_manager', 'module_manager_address_format', '&lt;b&gt;{name}&lt;/b&gt;\r\nМагазин: {store}\r\n{company}\r\n&lt;a href=\'skype:+{telephone}?call\'&gt;{telephone}&lt;/a&gt;\r\n&lt;a href=\'mailto:{email}\'&gt;{email}&lt;/a&gt;\r\n{address}\r\n{country}, {city}, {zone}\r\n{postcode}\'\'\'\'\'', 0),
(7402, 0, 'module_manager', 'module_manager_default_order', 'DESC', 0),
(7401, 0, 'module_manager', 'module_manager_default_sort', 'o.order_id', 0),
(7400, 0, 'module_manager', 'module_manager_default_links', '', 0),
(7399, 0, 'module_manager', 'module_manager_default_limit', '10', 0),
(7398, 0, 'module_manager', 'module_manager_notify', '1', 0),
(7397, 0, 'module_manager', 'module_manager_filters', '', 0),
(7396, 0, 'module_manager', 'module_manager_hide_dashboard', '', 0),
(7395, 0, 'module_manager', 'module_manager_notice', '', 0),
(7392, 0, 'module_manager', 'module_manager_payments', '{\"cod\":{\"color\":\"\"},\"free_checkout\":{\"color\":\"\"}}', 1),
(7393, 0, 'module_manager', 'module_manager_shippings', '{\"free\":{\"color\":\"\"}}', 1),
(7394, 0, 'module_manager', 'module_manager_mode', 'full', 0),
(7389, 0, 'module_manager', 'module_manager_buttons', '[\"history\",\"invoice\",\"shipping\",\"delete\",\"create\",\"minimize\",\"toggle\",\"filter\",\"clear\",\"edit_customer\",\"view_order\",\"edit_order\"]', 1),
(7390, 0, 'module_manager', 'module_manager_columns', '[\"select\",\"order_id\",\"order_status_id\",\"customer\",\"recipient\",\"date_added\",\"date_modified\",\"products\",\"payment\",\"shipping\",\"subtotal\",\"total\",\"actions\"]', 1),
(7391, 0, 'module_manager', 'module_manager_statuses', '{\"16\":{\"checked\":\"1\",\"color\":\"\"},\"2\":{\"checked\":\"1\",\"color\":\"\"},\"8\":{\"checked\":\"1\",\"color\":\"\"},\"11\":{\"checked\":\"1\",\"color\":\"\"},\"3\":{\"checked\":\"1\",\"color\":\"\"},\"14\":{\"checked\":\"1\",\"color\":\"\"},\"10\":{\"checked\":\"1\",\"color\":\"\"},\"15\":{\"checked\":\"1\",\"color\":\"\"},\"1\":{\"checked\":\"1\",\"color\":\"\"},\"9\":{\"checked\":\"1\",\"color\":\"\"},\"7\":{\"checked\":\"1\",\"color\":\"\"},\"12\":{\"checked\":\"1\",\"color\":\"\"},\"13\":{\"checked\":\"1\",\"color\":\"\"},\"5\":{\"checked\":\"1\",\"color\":\"\"},\"0\":{\"checked\":\"1\",\"color\":\"\"}}', 1),
(13873, 0, 'module_trade_import', 'module_trade_import_round_price', 'off', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(1192, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(1181, 0, 'total_total', 'total_total_sort_order', '9', 0),
(1180, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(1191, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(1190, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(1189, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(11492, 0, 'theme_default', 'theme_default_image_wishlist_width', '64', 0),
(11491, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(11490, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(11489, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(11488, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(11487, 0, 'theme_default', 'theme_default_image_additional_height', '1000', 0),
(11486, 0, 'theme_default', 'theme_default_image_additional_width', '1000', 0),
(11485, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(11484, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(11483, 0, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(11482, 0, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(11481, 0, 'theme_default', 'theme_default_image_thumb_height', '1000', 0),
(11480, 0, 'theme_default', 'theme_default_image_thumb_width', '1000', 0),
(11479, 0, 'theme_default', 'theme_default_image_manufacturer_height', '80', 0),
(11478, 0, 'theme_default', 'theme_default_image_manufacturer_width', '80', 0),
(11477, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(151, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(152, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(153, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(154, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(155, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(156, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(157, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(158, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(159, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(160, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(161, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(162, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(163, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(164, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(165, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(166, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(167, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(168, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(169, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(170, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(171, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(173, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(174, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(175, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(176, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(177, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(178, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(179, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(180, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(181, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(182, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(183, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(184, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(185, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(186, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(187, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(188, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(189, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(190, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(191, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(192, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(193, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(194, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(195, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(196, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(197, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(198, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(199, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(200, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(321, 0, 'developer', 'developer_theme', '0', 0),
(203, 0, 'configblog', 'configblog_name', 'Блог', 0),
(204, 0, 'configblog', 'configblog_html_h1', 'Блог для интернет магазина на OpenCart', 0),
(205, 0, 'configblog', 'configblog_meta_title', 'Блог для интернет магазина на OpenCart', 0),
(206, 0, 'configblog', 'configblog_meta_description', 'Блог для интернет магазина на OpenCart', 0),
(207, 0, 'configblog', 'configblog_meta_keyword', 'Блог для интернет магазина на OpenCart', 0),
(208, 0, 'configblog', 'configblog_article_count', '1', 0),
(209, 0, 'configblog', 'configblog_article_limit', '20', 0),
(210, 0, 'configblog', 'configblog_article_description_length', '200', 0),
(211, 0, 'configblog', 'configblog_limit_admin', '20', 0),
(212, 0, 'configblog', 'configblog_blog_menu', '1', 0),
(213, 0, 'configblog', 'configblog_article_download', '1', 0),
(214, 0, 'configblog', 'configblog_review_status', '1', 0),
(215, 0, 'configblog', 'configblog_review_guest', '1', 0),
(216, 0, 'configblog', 'configblog_review_mail', '1', 0),
(217, 0, 'configblog', 'configblog_image_category_width', '50', 0),
(218, 0, 'configblog', 'configblog_image_category_height', '50', 0),
(219, 0, 'configblog', 'configblog_image_article_width', '150', 0),
(220, 0, 'configblog', 'configblog_image_article_height', '150', 0),
(221, 0, 'configblog', 'configblog_image_related_width', '200', 0),
(222, 0, 'configblog', 'configblog_image_related_height', '200', 0),
(322, 0, 'developer', 'developer_sass', '1', 0),
(418, 0, 'opencart', 'opencart_username', 'demonized', 0),
(419, 0, 'opencart', 'opencart_secret', 'KcQpnhGLfh3N8HEI5G5zDC0tCxGDLXI57VR8v2DWb5SB9xyhZ3BhrKwyngphxlZUa0JBq61uLeh1J6G7scXrBgHXFCbXg30qe6rgcpbb9VMXtufSq0bnItYrED2GaJmhEvVDaucFEb17j0upKNuWDW4FY3j1YbVtLgkpqJwdUCaS1CnJX3j4JI2gn5dZVYMhxqpaPxz3OMUTqBINzQjcI0JAQ8Iuuj1FBBoqrGdGrbeBAxQ2xahaPAyOgiTsa7JeoAAIHhu4Yjyr6rym150bIcC2nNUboDViOol5GWx7TRp34ZlbMnAvhtMtRGtWzb7lDT1qNNWLywWJVMiGijS0Z5J4RTD1wqYwOywSAVtFGBFwhFa3oER220CQpDAIPdkLknhUWXfFru0prcX9zA3Cvh4Q80G4PjNsjoTlwndic0Zh8smVVkSwUnTpXIiIbpodCTG9XGaiGT7pR3dkPVuXszifWsI6fMpEbnMaCCMH3KgolwnuhIo7xuU8gp5D6SZT', 0),
(542, 0, 'd_quickcheckout', 'd_quickcheckout_status', '1', 0),
(543, 0, 'd_quickcheckout', 'd_quickcheckout_trigger', '#button-confirm, .button, .btn, .button_oc, input[type=submit]', 0),
(544, 0, 'd_quickcheckout', 'd_quickcheckout_debug', '0', 0),
(545, 0, 'd_quickcheckout', 'd_quickcheckout_setting', '{\"name\":\"05\\/16\\/2019 05:45:04 am\",\"general\":{\"clear_session\":\"0\",\"login_refresh\":\"0\",\"analytics_event\":\"0\",\"update_mini_cart\":\"1\",\"compress\":\"1\",\"min_order\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a sum more then %s to make an order       \",\"2\":\"The minimum order is %s      \"}},\"min_quantity\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a quantity more then %s to make an order    \",\"2\":\"The minimum quantity is %s   \"}},\"config\":\"d_quickcheckout\"}}', 1),
(541, 0, 'd_quickcheckout', 'd_quickcheckout_setting_cycle', '{\"1\":\"1  \"}', 1),
(9755, 0, 'module_ocfilter', 'module_ocfilter_copy_type', 'checkbox', 0),
(6793, 0, 'analytics_metrika', 'analytics_metrika_status', '1', 0),
(11497, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(11496, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(531, 0, 'module_extendedsearch', 'module_extendedsearch_status', '1', 0),
(532, 0, 'module_extendedsearch', 'module_extendedsearch_model', '1', 0),
(533, 0, 'module_extendedsearch', 'module_extendedsearch_sku', '1', 0),
(534, 0, 'module_extendedsearch', 'module_extendedsearch_upc', '1', 0),
(535, 0, 'module_extendedsearch', 'module_extendedsearch_ean', '1', 0),
(536, 0, 'module_extendedsearch', 'module_extendedsearch_jan', '1', 0),
(537, 0, 'module_extendedsearch', 'module_extendedsearch_isbn', '1', 0),
(538, 0, 'module_extendedsearch', 'module_extendedsearch_mpn', '1', 0),
(539, 0, 'module_extendedsearch', 'module_extendedsearch_location', '1', 0),
(540, 0, 'module_extendedsearch', 'module_extendedsearch_attr', '1', 0),
(546, 0, 'd_quickcheckout', 'd_quickcheckout_debug_file', 'd_quickcheckout.log', 0),
(1193, 0, 'module_quickcheckout', 'module_quickcheckout_status', '1', 0),
(11476, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(11475, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(11474, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(11473, 0, 'theme_default', 'theme_default_status', '1', 0),
(11472, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(6792, 0, 'analytics_metrika', 'analytics_metrika_code', '&lt;!-- Yandex.Metrika counter --&gt;\r\n&lt;script type=&quot;text/javascript&quot; &gt;\r\n   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};\r\n   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})\r\n   (window, document, &quot;script&quot;, &quot;https://mc.yandex.ru/metrika/tag.js&quot;, &quot;ym&quot;);\r\n\r\n   ym(55343608, &quot;init&quot;, {\r\n        clickmap:true,\r\n        trackLinks:true,\r\n        accurateTrackBounce:true\r\n   });\r\n&lt;/script&gt;\r\n&lt;noscript&gt;&lt;div&gt;&lt;img src=&quot;https://mc.yandex.ru/watch/55343608&quot; style=&quot;position:absolute; left:-9999px;&quot; alt=&quot;&quot; /&gt;&lt;/div&gt;&lt;/noscript&gt;\r\n&lt;!-- /Yandex.Metrika counter --&gt;', 0),
(6066, 0, 'module_account', 'module_account_status', '1', 0),
(13872, 0, 'module_trade_import', 'module_trade_import_hide_empty_product', '0', 0),
(13871, 0, 'module_trade_import', 'module_trade_import_hide_product', '0', 0),
(11470, 0, 'quickcheckout', 'quickcheckout_delivery_max_hour', '17', 0),
(11469, 0, 'quickcheckout', 'quickcheckout_delivery_min_hour', '09', 0),
(11468, 0, 'quickcheckout', 'quickcheckout_delivery_max', '30', 0),
(11467, 0, 'quickcheckout', 'quickcheckout_delivery_min', '1', 0),
(11464, 0, 'quickcheckout', 'quickcheckout_delivery_time', '0', 0),
(11465, 0, 'quickcheckout', 'quickcheckout_delivery_required', '0', 0),
(11466, 0, 'quickcheckout', 'quickcheckout_delivery_unavailable', '&quot;2017-10-31&quot;, &quot;2017-08-11&quot;, &quot;2017-12-25&quot;', 0),
(11462, 0, 'quickcheckout', 'quickcheckout_survey_type', '0', 0),
(11463, 0, 'quickcheckout', 'quickcheckout_delivery', '0', 0),
(11461, 0, 'quickcheckout', 'quickcheckout_survey_text', '{\"1\":\"\"}', 1),
(11460, 0, 'quickcheckout', 'quickcheckout_survey_required', '0', 0),
(11459, 0, 'quickcheckout', 'quickcheckout_survey', '0', 0),
(11458, 0, 'quickcheckout', 'quickcheckout_shipping_logo', '{\"flat\":\"\",\"free\":\"\",\"xshippingpro\":\"\"}', 1),
(11457, 0, 'quickcheckout', 'quickcheckout_shipping_default', 'xshippingpro', 0),
(11456, 0, 'quickcheckout', 'quickcheckout_shipping', '1', 0),
(11454, 0, 'quickcheckout', 'quickcheckout_shipping_module', '0', 0),
(11455, 0, 'quickcheckout', 'quickcheckout_shipping_title_display', '1', 0),
(11453, 0, 'quickcheckout', 'quickcheckout_payment_logo', '{\"cod\":\"\",\"rbs\":\"\"}', 1),
(11452, 0, 'quickcheckout', 'quickcheckout_shipping_reload', '0', 0),
(11451, 0, 'quickcheckout', 'quickcheckout_payment_default', 'rbs', 0),
(11450, 0, 'quickcheckout', 'quickcheckout_payment', '1', 0),
(11449, 0, 'quickcheckout', 'quickcheckout_payment_reload', '0', 0),
(11448, 0, 'quickcheckout', 'quickcheckout_payment_module', '1', 0),
(11447, 0, 'quickcheckout', 'quickcheckout_html_footer', '{\"1\":\"\"}', 1),
(11446, 0, 'quickcheckout', 'quickcheckout_html_header', '{\"1\":\"\"}', 1),
(11445, 0, 'quickcheckout', 'quickcheckout_reward', '0', 0),
(11444, 0, 'quickcheckout', 'quickcheckout_voucher', '0', 0),
(11443, 0, 'quickcheckout', 'quickcheckout_coupon', '0', 0),
(11441, 0, 'quickcheckout', 'quickcheckout_field_rules', '{\"display\":\"on\",\"required\":\"on\",\"sort_order\":\"\"}', 1),
(11442, 0, 'quickcheckout', 'quickcheckout_field_comment', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"}}', 1),
(11440, 0, 'quickcheckout', 'quickcheckout_field_shipping', '{\"required\":\"on\",\"default\":\"on\",\"sort_order\":\"\"}', 1),
(11435, 0, 'quickcheckout', 'quickcheckout_field_postcode', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"11\"}', 1),
(11438, 0, 'quickcheckout', 'quickcheckout_field_newsletter', '{\"sort_order\":\"\"}', 1),
(11439, 0, 'quickcheckout', 'quickcheckout_field_register', '{\"sort_order\":\"\"}', 1),
(11437, 0, 'quickcheckout', 'quickcheckout_field_zone', '{\"required\":\"on\",\"default\":\"2748\",\"sort_order\":\"0\"}', 1),
(11436, 0, 'quickcheckout', 'quickcheckout_field_country', '{\"default\":\"176\",\"sort_order\":\"12\"}', 1),
(11434, 0, 'quickcheckout', 'quickcheckout_field_city', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"10\"}', 1),
(6794, 0, 'feed_yandex_sitemap', 'feed_yandex_sitemap_status', '1', 0),
(13870, 0, 'module_trade_import', 'module_trade_import_delete_product', '1', 0),
(13869, 0, 'module_trade_import', 'module_trade_import_add_product', '1', 0),
(13868, 0, 'module_trade_import', 'module_trade_import_sub_filters', '0', 0),
(13867, 0, 'module_trade_import', 'module_trade_import_hide_category', '0', 0),
(13866, 0, 'module_trade_import', 'module_trade_import_delete_category', '1', 0),
(13865, 0, 'module_trade_import', 'module_trade_import_add_category', '1', 0),
(11433, 0, 'quickcheckout', 'quickcheckout_field_address_2', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"9\"}', 1),
(11429, 0, 'quickcheckout', 'quickcheckout_field_telephone', '{\"display\":\"on\",\"required\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u041c\\u043e\\u0431\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\"},\"sort_order\":\"4\"}', 1),
(11431, 0, 'quickcheckout', 'quickcheckout_field_customer_group', '{\"display\":\"on\",\"sort_order\":\"0\"}', 1),
(11432, 0, 'quickcheckout', 'quickcheckout_field_address_1', '{\"display\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\"},\"sort_order\":\"1\"}', 1),
(11430, 0, 'quickcheckout', 'quickcheckout_field_company', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0418\\u041d\\u041d \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438\"},\"sort_order\":\"5\"}', 1),
(11428, 0, 'quickcheckout', 'quickcheckout_field_email', '{\"display\":\"on\",\"required\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u0430\\u044f \\u043f\\u043e\\u0447\\u0442\\u0430\"},\"sort_order\":\"2\"}', 1),
(11427, 0, 'quickcheckout', 'quickcheckout_field_lastname', '{\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\"},\"sort_order\":\"2\"}', 1),
(11426, 0, 'quickcheckout', 'quickcheckout_field_firstname', '{\"display\":\"on\",\"default\":{\"1\":\"\"},\"placeholder\":{\"1\":\"\\u0418\\u043c\\u044f \\u0438 \\u0444\\u0430\\u043c\\u0438\\u043b\\u0438\\u044f\"},\"sort_order\":\"3\"}', 1),
(11425, 0, 'quickcheckout', 'quickcheckout_custom_css', '', 0),
(11420, 0, 'quickcheckout', 'quickcheckout_cart', '1', 0),
(11421, 0, 'quickcheckout', 'quickcheckout_show_shipping_address', '0', 0),
(11422, 0, 'quickcheckout', 'quickcheckout_slide_effect', '0', 0),
(11423, 0, 'quickcheckout', 'quickcheckout_load_screen', '0', 0),
(11424, 0, 'quickcheckout', 'quickcheckout_loading_display', '0', 0),
(11419, 0, 'quickcheckout', 'quickcheckout_login_module', '0', 0),
(11418, 0, 'quickcheckout', 'quickcheckout_step', '{\"cart\":{\"column\":\"1\",\"row\":\"0\"},\"login\":{\"column\":\"2\",\"row\":\"0\"},\"shipping_method\":{\"column\":\"4\",\"row\":\"0\"},\"payment_address\":{\"column\":\"4\",\"row\":\"1\"},\"shipping_address\":{\"column\":\"4\",\"row\":\"2\"},\"payment_method\":{\"column\":\"4\",\"row\":\"3\"},\"coupons\":{\"column\":\"4\",\"row\":\"4\"},\"confirm\":{\"column\":\"4\",\"row\":\"6\"}}', 1),
(13864, 0, 'module_trade_import', 'module_trade_import_save_json', '1', 0),
(13842, 0, 'config', 'config_watermark_size_y', '50', 0),
(13841, 0, 'config', 'config_watermark_size_x', '187', 0),
(13840, 0, 'config', 'config_watermark_product_in_cart', '0', 0),
(13839, 0, 'config', 'config_watermark_product_in_wish_list', '0', 0),
(13838, 0, 'config', 'config_watermark_product_in_compare', '0', 0),
(13837, 0, 'config', 'config_watermark_product_related', '0', 0),
(13836, 0, 'config', 'config_watermark_product_additional', '0', 0),
(13835, 0, 'config', 'config_watermark_product_list', '0', 0),
(13834, 0, 'config', 'config_watermark_product_popup', '1', 0),
(13830, 0, 'config', 'config_watermark_pos_y', '0', 0),
(13831, 0, 'config', 'config_watermark_opacity', '1.0', 0),
(13832, 0, 'config', 'config_watermark_category_image', '0', 0),
(13833, 0, 'config', 'config_watermark_product_thumb', '0', 0),
(13829, 0, 'config', 'config_watermark_pos_y_center', '1', 0),
(13828, 0, 'config', 'config_watermark_pos_x', '16', 0),
(13827, 0, 'config', 'config_watermark_pos_x_center', '0', 0),
(13826, 0, 'config', 'config_watermark_zoom', '0.6', 0),
(13825, 0, 'config', 'config_watermark_image', 'catalog/opencart-logo.png', 0),
(13824, 0, 'config', 'config_watermark_resize_first', '1', 0),
(13823, 0, 'config', 'config_watermark_hide_real_path', '0', 0),
(13822, 0, 'config', 'config_watermark_status', '1', 0),
(13821, 0, 'config', 'config_valide_params', 'tracking\r\nutm_source\r\nutm_campaign\r\nutm_medium\r\ntype\r\nsource\r\nblock\r\nposition\r\nkeyword\r\nyclid\r\ngclid', 0),
(13820, 0, 'config', 'config_valide_param_flag', '1', 0),
(13819, 0, 'config', 'config_page_postfix', '', 0),
(13818, 0, 'config', 'config_seopro_lowercase', '1', 0),
(13817, 0, 'config', 'config_seopro_addslash', '1', 0),
(13816, 0, 'config', 'config_seo_url_cache', '0', 0),
(13815, 0, 'config', 'config_seo_url_short_category_path', '0', 0),
(13814, 0, 'config', 'config_seo_url_include_path', '1', 0),
(13811, 0, 'config', 'config_error_log', '1', 0),
(13807, 0, 'config', 'config_file_max_size', '300000', 0),
(13808, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(13809, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(13810, 0, 'config', 'config_error_display', '1', 0),
(13863, 0, 'module_trade_import', 'module_trade_import_local_json', '0', 0),
(13862, 0, 'module_trade_import', 'module_trade_import_time_zone', 'Asia/Vladivostok', 0),
(13861, 0, 'module_trade_import', 'module_trade_import_sync_period', '10_minutes', 0),
(13812, 0, 'config', 'config_error_filename', 'error.log', 0),
(13813, 0, 'config', 'config_seo_pro', '1', 0),
(13802, 0, 'config', 'config_compression', '5', 0),
(13803, 0, 'config', 'config_secure', '1', 0),
(13804, 0, 'config', 'config_password', '1', 0),
(13805, 0, 'config', 'config_shared', '0', 0),
(13806, 0, 'config', 'config_encryption', 'qR6NRhiKJ3Vp016e5sDTTcjJ1r2XSOjaBtkzlCdY2pJKEKuWFAMrsNDGUic3Kmt6CUeI1hQ3cHInhDJ8tguyIvQlLCG3qJ1jG3gl6LrsclfHif9ganwNs1WCp1wuG2eBBGx9wZPfMHnqVaziZ8HHeGe43LM4Pj2GgeIoA8sraG1oekSa8U359W4V1ZrffA2uqn4S5ZkGRiqcS4IbDJT73nbdQw7yHeFLXz0VA3EIR9DvjoJ3qkjrXRyfJeyn2FRUpqN4ffUeSBHx2tOZOHZ2K4E4KJun5qJHkdV7SiFvu7ricD9EY8AMAcjkK0KBbjvM6FeAaTCq0UAyBnA0gxgfcHuU7NYmnpw1IdsckfgMjp1cqBMQdOp3D22Yh6bcytBBSR0hFqxSuEjUkNudD97LNB3b2ZlcaGDdqJV1uULIS2HbcG25kGJPZQs9jUJqnhuCdMfyRC5tvn2GZjbK0sRNP7gav3YJrkvWDQfj9JJ37vFpw0I81ltm28PsUpIuGbA6PzsCAdA5kV3EbwJEq7r6LE0ufvhmyEnHMLObaeZWISkx4NEejsh3onuUrBpEAHeSopREWr4ZPzUW5wPfW3dkCI8bJNwnH6y9f2fAGchymXleacCZYEoFkFnPQDOifvMbLfNYbmHKO3ow6UuV87TfD6hlruxiCayCJvCK70BCdZpSaro2RD7OuYfqDtWt8wAOIgaDUTT4NIbOniE3U2mWs48rQkmIRo9v12Aev89v4cxSGpjn4ol4OfdMn72zCdvmTqrdhUxIqUr8CgyYuzswqHwBIWlCOPIswX95Wr5YGhrZT8Ywh6Rzxvq3yEdi4W0yTVWvqmCLXZjM1ZjRQHCNchINtn0vg6enVP6CmwxcHgXsNleDRWO034fq50EbYXCp0bqA6sXyKSQ1iLZJPeKBKuABT01GAEjK560hmFCgGk5RnsiXsgwOQIqID9kaoDxarCZZWnI74CwdEeQhimUueqy4mSdRgZ0oBT7zWGuUbmb48vA8PUaVizqKYxVV8iE2', 0),
(13860, 0, 'module_trade_import', 'module_trade_import_enable_sync', '0', 0),
(13859, 0, 'module_trade_import', 'module_trade_import_add_properties_to_filters', 'Размер', 0),
(13858, 0, 'module_trade_import', 'module_trade_import_ignore_filter', '', 0),
(13857, 0, 'module_trade_import', 'module_trade_import_ignore_category', '', 0),
(9417, 0, 'module_template_switcher', 'module_template_switcher_status', '1', 0),
(13856, 0, 'module_trade_import', 'module_trade_import_parent_id', '', 0),
(13855, 0, 'module_trade_import', 'module_trade_import_price_map', '', 0),
(13854, 0, 'module_trade_import', 'module_trade_import_top_category', '', 0),
(13853, 0, 'module_trade_import', 'module_trade_import_price', '22e6c41e-592f-4769-a402-ca85c577feac', 0),
(9754, 0, 'module_ocfilter', 'module_ocfilter_hide_empty_values', '1', 0),
(9752, 0, 'module_ocfilter', 'module_ocfilter_show_options_limit', '0', 0),
(9753, 0, 'module_ocfilter', 'module_ocfilter_show_values_limit', '10', 0),
(9751, 0, 'module_ocfilter', 'module_ocfilter_consider_option', '0', 0),
(9750, 0, 'module_ocfilter', 'module_ocfilter_consider_special', '0', 0),
(9749, 0, 'module_ocfilter', 'module_ocfilter_consider_discount', '0', 0),
(9748, 0, 'module_ocfilter', 'module_ocfilter_manual_price', '1', 0),
(9747, 0, 'module_ocfilter', 'module_ocfilter_stock_out_value', '1', 0),
(9746, 0, 'module_ocfilter', 'module_ocfilter_stock_status_type', 'checkbox', 0),
(9745, 0, 'module_ocfilter', 'module_ocfilter_stock_status_method', 'quantity', 0),
(9744, 0, 'module_ocfilter', 'module_ocfilter_stock_status', '1', 0),
(9743, 0, 'module_ocfilter', 'module_ocfilter_manufacturer_type', 'checkbox', 0),
(9742, 0, 'module_ocfilter', 'module_ocfilter_manufacturer', '0', 0),
(9741, 0, 'module_ocfilter', 'module_ocfilter_show_counter', '1', 0),
(9740, 0, 'module_ocfilter', 'module_ocfilter_show_price', '1', 0),
(9739, 0, 'module_ocfilter', 'module_ocfilter_show_selected', '1', 0),
(9738, 0, 'module_ocfilter', 'module_ocfilter_search_button', '1', 0),
(9737, 0, 'module_ocfilter', 'module_ocfilter_sitemap_link', 'http://neko.neko.ru/index.php?route=extension/feed/ocfilter_sitemap', 0),
(9736, 0, 'module_ocfilter', 'module_ocfilter_sitemap_status', '1', 0),
(9735, 0, 'module_ocfilter', 'module_ocfilter_sub_category', '0', 0),
(9734, 0, 'module_ocfilter', 'module_ocfilter_status', '1', 0),
(9756, 0, 'module_ocfilter', 'module_ocfilter_copy_status', '1', 0),
(9757, 0, 'module_ocfilter', 'module_ocfilter_copy_attribute', '0', 0),
(9758, 0, 'module_ocfilter', 'module_ocfilter_copy_filter', '1', 0),
(9759, 0, 'module_ocfilter', 'module_ocfilter_copy_option', '0', 0),
(9760, 0, 'module_ocfilter', 'module_ocfilter_copy_truncate', '1', 0),
(9761, 0, 'module_ocfilter', 'module_ocfilter_copy_category', '1', 0),
(13852, 0, 'module_trade_import', 'module_trade_import_order_token', '', 0),
(13851, 0, 'module_trade_import', 'module_trade_import_order_address', 'https://4ait.4ait.ru/api/v1/orders', 0),
(13850, 0, 'module_trade_import', 'module_trade_import_enable_order', '1', 0),
(13849, 0, 'module_trade_import', 'module_trade_import_old_api_token', '', 0),
(13799, 0, 'config', 'config_maintenance', '0', 0),
(13800, 0, 'config', 'config_seo_url', '1', 0),
(13801, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(13798, 0, 'config', 'config_mail_alert_email', '', 0),
(13797, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(13796, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(10042, 0, 'payment_rbs', 'payment_rbs_taxType', '0', 0),
(10041, 0, 'payment_rbs', 'payment_rbs_taxSystem', '3', 0),
(10040, 0, 'payment_rbs', 'payment_rbs_ofd_status', '1', 0),
(10039, 0, 'payment_rbs', 'payment_rbs_currency', '0', 0),
(10038, 0, 'payment_rbs', 'payment_rbs_logging', '1', 0),
(10037, 0, 'payment_rbs', 'payment_rbs_sort_order', '', 0),
(10036, 0, 'payment_rbs', 'payment_rbs_order_status_id', '15', 0),
(10035, 0, 'payment_rbs', 'payment_rbs_stage', 'one', 0),
(10034, 0, 'payment_rbs', 'payment_rbs_mode', 'test', 0),
(10033, 0, 'payment_rbs', 'payment_rbs_merchantPassword', '', 0),
(10032, 0, 'payment_rbs', 'payment_rbs_merchantLogin', '', 0),
(10031, 0, 'payment_rbs', 'payment_rbs_status', '1', 0),
(10043, 0, 'payment_rbs', 'payment_rbs_ffdVersion', 'v10', 0),
(10044, 0, 'payment_rbs', 'payment_rbs_paymentMethodType', '1', 0),
(10045, 0, 'payment_rbs', 'payment_rbs_paymentObjectType', '1', 0),
(11417, 0, 'quickcheckout', 'quickcheckout_column', '{\"1\":\"8\",\"2\":\"4\",\"3\":\"0\",\"4\":\"4\"}', 1),
(11416, 0, 'quickcheckout', 'quickcheckout_responsive', '1', 0),
(11415, 0, 'quickcheckout', 'quickcheckout_layout', '4', 0),
(11414, 0, 'quickcheckout', 'quickcheckout_keyword', '{\"1\":\"checkout\"}', 1),
(11413, 0, 'quickcheckout', 'quickcheckout_proceed_button_text', '{\"1\":\"\\u041f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0434\\u0438\\u0442\\u044c\"}', 1),
(11404, 0, 'quickcheckout', 'quickcheckout_confirmation_page', '1', 0),
(11405, 0, 'quickcheckout', 'quickcheckout_save_data', '1', 0),
(11406, 0, 'quickcheckout', 'quickcheckout_edit_cart', '1', 0),
(11407, 0, 'quickcheckout', 'quickcheckout_highlight_error', '1', 0),
(11408, 0, 'quickcheckout', 'quickcheckout_text_error', '1', 0),
(11409, 0, 'quickcheckout', 'quickcheckout_auto_submit', '0', 0),
(11410, 0, 'quickcheckout', 'quickcheckout_payment_target', '#button-confirm, .button, .btn', 0),
(11411, 0, 'quickcheckout', 'quickcheckout_skip_cart', '1', 0),
(11412, 0, 'quickcheckout', 'quickcheckout_force_bootstrap', '0', 0),
(11403, 0, 'quickcheckout', 'quickcheckout_debug', '0', 0),
(11402, 0, 'quickcheckout', 'quickcheckout_minimum_order', '0', 0),
(11401, 0, 'quickcheckout', 'quickcheckout_status', '1', 0),
(13795, 0, 'config', 'config_mail_smtp_port', '25', 0),
(13848, 0, 'module_trade_import', 'module_trade_import_old_api_address', '', 0),
(13847, 0, 'module_trade_import', 'module_trade_import_enable_old_api', '0', 0),
(11495, 0, 'theme_default', 'theme_default_image_cart_height', '200', 0),
(11494, 0, 'theme_default', 'theme_default_image_cart_width', '200', 0),
(11493, 0, 'theme_default', 'theme_default_image_wishlist_height', '64', 0),
(11398, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_name', '{\"1\":\"\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":\"\",\"6\":\"\",\"7\":\"\",\"8\":\"\",\"9\":\"\",\"10\":\"\"}', 1),
(11397, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_limit', '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"4\":\"1\",\"5\":\"1\",\"6\":\"1\",\"7\":\"1\",\"8\":\"1\",\"9\":\"1\",\"10\":\"1\"}', 1),
(11396, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group', '{\"1\":\"no_group\",\"2\":\"no_group\",\"3\":\"no_group\",\"4\":\"no_group\",\"5\":\"no_group\",\"6\":\"no_group\",\"7\":\"no_group\",\"8\":\"no_group\",\"9\":\"no_group\",\"10\":\"no_group\"}', 1),
(11395, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_debug', '0', 0),
(11394, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_desc_mail', '', 0),
(11392, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sorting', '1', 0),
(11393, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_heading', '{\"1\":\"\\u041e\\u043f\\u0446\\u0438\\u0438 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\"}', 1),
(11390, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_group', 'no_group', 0),
(11391, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_group_limit', '1', 0),
(11389, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_map_api', '', 0),
(11387, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_status', '1', 0),
(11388, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sort_order', '', 0),
(11399, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_sub_group_desc', '{\"1\":\"\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":\"\",\"6\":\"\",\"7\":\"\",\"8\":\"\",\"9\":\"\",\"10\":\"\"}', 1),
(11400, 0, 'shipping_xshippingpro', 'shipping_xshippingpro_estimator', '{\"type\":\"method\",\"selector\":\"#product\",\"css\":\"\"}', 1),
(11471, 0, 'quickcheckout', 'quickcheckout_delivery_days_of_week', '', 0),
(13846, 0, 'module_trade_import', 'module_trade_import_token', '3f879ee5e028e8aca11482e6bae32d7f432bf642', 0),
(13845, 0, 'module_trade_import', 'module_trade_import_nomenclature', 'https://4ait.4ait.ru/api/v1/products', 0),
(13844, 0, 'module_trade_import', 'module_trade_import_code', 'https://4ait.4ait.ru/api/v1/auth', 0),
(13843, 0, 'module_trade_import', 'module_trade_import_server', 'https://4ait.4ait.ru', 0),
(13794, 0, 'config', 'config_mail_smtp_password', 'PaSs2019dv1', 0),
(13793, 0, 'config', 'config_mail_smtp_username', 'postservice@4ait.ru', 0),
(13792, 0, 'config', 'config_mail_smtp_hostname', 'ssl://smtp.yandex.ru', 0),
(13791, 0, 'config', 'config_mail_parameter', '-O DeliveryMode=b', 0),
(13789, 0, 'config', 'config_icon', '', 0),
(13790, 0, 'config', 'config_mail_engine', 'mail', 0),
(13788, 0, 'config', 'config_logo_footer', 'catalog/Group 91.svg', 0),
(13787, 0, 'config', 'config_logo', 'catalog/Group 9.svg', 0),
(13786, 0, 'config', 'config_captcha_page', '[\"guest\",\"review\",\"return\",\"contact\"]', 1),
(13784, 0, 'config', 'config_return_status_id', '2', 0),
(13785, 0, 'config', 'config_captcha', '', 0),
(13783, 0, 'config', 'config_return_id', '0', 0),
(13782, 0, 'config', 'config_affiliate_id', '0', 0),
(13781, 0, 'config', 'config_affiliate_commission', '5', 0),
(13780, 0, 'config', 'config_affiliate_auto', '0', 0),
(13778, 0, 'config', 'config_affiliate_group_id', '1', 0),
(13779, 0, 'config', 'config_affiliate_approval', '0', 0),
(13777, 0, 'config', 'config_stock_checkout', '1', 0),
(13776, 0, 'config', 'config_stock_warning', '0', 0),
(13775, 0, 'config', 'config_stock_display', '0', 0),
(13774, 0, 'config', 'config_api_id', '1', 0),
(13773, 0, 'config', 'config_fraud_status_id', '16', 0),
(13772, 0, 'config', 'config_complete_status', '[\"3\",\"5\"]', 1),
(13771, 0, 'config', 'config_processing_status', '[\"2\",\"3\",\"1\",\"12\",\"5\"]', 1),
(13770, 0, 'config', 'config_order_status_id', '1', 0),
(13769, 0, 'config', 'config_checkout_id', '5', 0),
(13768, 0, 'config', 'config_checkout_guest', '1', 0),
(13767, 0, 'config', 'config_cart_weight', '0', 0),
(13766, 0, 'config', 'config_invoice_prefix', 'INV-2019-00', 0),
(13765, 0, 'config', 'config_account_id', '0', 0),
(13764, 0, 'config', 'config_login_attempts', '5', 0),
(13763, 0, 'config', 'config_customer_price', '0', 0),
(13762, 0, 'config', 'config_customer_group_display', '[\"1\",\"4\"]', 1),
(13761, 0, 'config', 'config_customer_group_id', '1', 0),
(13760, 0, 'config', 'config_customer_search', '0', 0),
(13759, 0, 'config', 'config_customer_activity', '1', 0),
(13758, 0, 'config', 'config_customer_online', '1', 0),
(13757, 0, 'config', 'config_tax_customer', 'shipping', 0),
(13756, 0, 'config', 'config_tax_default', 'shipping', 0),
(13755, 0, 'config', 'config_tax', '0', 0),
(13754, 0, 'config', 'config_voucher_max', '1000', 0),
(13753, 0, 'config', 'config_voucher_min', '1', 0),
(13752, 0, 'config', 'config_review_guest', '0', 0),
(13751, 0, 'config', 'config_review_status', '0', 0),
(13750, 0, 'config', 'config_autocomplete_limit', '20', 0),
(13749, 0, 'config', 'config_limit_admin', '20', 0),
(13748, 0, 'config', 'config_product_count', '0', 0),
(13747, 0, 'config', 'config_weight_class_id', '1', 0),
(13746, 0, 'config', 'config_length_class_id', '1', 0),
(13745, 0, 'config', 'config_currency_auto', '1', 0),
(13743, 0, 'config', 'config_admin_language', 'ru-ru', 0),
(13744, 0, 'config', 'config_currency', 'RUB', 0),
(13742, 0, 'config', 'config_language', 'ru-ru', 0),
(13741, 0, 'config', 'config_zone_id', '2748', 0),
(13740, 0, 'config', 'config_country_id', '176', 0),
(13739, 0, 'config', 'config_schema_address', '', 0),
(13738, 0, 'config', 'config_comment', '', 0),
(13737, 0, 'config', 'config_open', 'c 10:00 до 05:00', 0),
(13736, 0, 'config', 'config_image', '', 0),
(13735, 0, 'config', 'config_fax', '', 0),
(13734, 0, 'config', 'config_telephone2', '', 0),
(13733, 0, 'config', 'config_telephone', '+7 (4212) 12-34-56', 0),
(13732, 0, 'config', 'config_email', 'ecos@4ait.ru', 0),
(13731, 0, 'config', 'config_geocode', '', 0),
(13730, 0, 'config', 'config_address', 'ECOS', 0),
(13729, 0, 'config', 'config_owner', 'ECOS', 0),
(13728, 0, 'config', 'config_name', 'ECOS', 0),
(13727, 0, 'config', 'config_layout_id', '4', 0),
(13726, 0, 'config', 'config_theme', 'default', 0),
(13725, 0, 'config', 'config_meta_keyword', 'ECOS', 0),
(13724, 0, 'config', 'config_meta_description', 'ECOS', 0),
(13723, 0, 'config', 'config_meta_title', 'ECOS', 0),
(13874, 0, 'module_trade_import', 'module_trade_import_add_separate_products', '0', 0),
(13875, 0, 'module_trade_import', 'module_trade_import_ocfilter', '1', 0),
(13876, 0, 'module_trade_import', 'module_trade_import_keep_names', '1', 0),
(13877, 0, 'module_trade_import', 'module_trade_import_keep_product_description', '1', 0),
(13878, 0, 'module_trade_import', 'module_trade_import_keep_meta', '1', 0),
(13879, 0, 'module_trade_import', 'module_trade_import_short_url', '0', 0),
(13880, 0, 'module_trade_import', 'module_trade_import_full_path_url', '0', 0),
(13881, 0, 'module_trade_import', 'module_trade_import_add_one_product', '', 0),
(13882, 0, 'module_trade_import', 'module_trade_import_sync_schedule', '2020-12-07 17:54:00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '72317.2400'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '0.0000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks`
--

CREATE TABLE `oc_stocks` (
  `stocks_id` int(11) NOT NULL,
  `start_at` date NOT NULL,
  `end_at` date NOT NULL,
  `discount` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks_description`
--

CREATE TABLE `oc_stocks_description` (
  `stocks_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_h1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stocks_product`
--

CREATE TABLE `oc_stocks_product` (
  `stocks_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'В наличии'),
(8, 1, 'Предзаказ'),
(5, 1, 'Нет в наличии'),
(6, 1, 'Ожидание 2-3 дня'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT 0.0000,
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import`
--

CREATE TABLE `oc_trade_import` (
  `operation_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `json_timestamp` timestamp NULL DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_category_codes`
--

CREATE TABLE `oc_trade_import_category_codes` (
  `category_id` int(11) NOT NULL,
  `group_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_filter_group_codes`
--

CREATE TABLE `oc_trade_import_filter_group_codes` (
  `filter_group_id` int(11) NOT NULL,
  `filter_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_characteristic_codes`
--

CREATE TABLE `oc_trade_import_option_characteristic_codes` (
  `characteristic_id` int(11) NOT NULL,
  `property_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_codes`
--

CREATE TABLE `oc_trade_import_option_codes` (
  `option_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_option_value_codes`
--

CREATE TABLE `oc_trade_import_option_value_codes` (
  `option_value_id` int(11) NOT NULL,
  `characteristic_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_product_codes`
--

CREATE TABLE `oc_trade_import_product_codes` (
  `product_id` int(11) NOT NULL,
  `nomenclature_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_stocks_codes`
--

CREATE TABLE `oc_trade_import_stocks_codes` (
  `stocks_id` int(11) NOT NULL,
  `stocks_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_import_warehouse_codes`
--

CREATE TABLE `oc_trade_import_warehouse_codes` (
  `warehouse_id` int(11) NOT NULL,
  `storage_uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_trade_orders`
--

CREATE TABLE `oc_trade_orders` (
  `operation_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', 'ba28daf3ce4691a19dca3ae2d93cf613739bef8f', 'OdKRjcQVP', 'John', 'Doe', 'demonized@4ait.ru', '', '', '127.0.0.1', 1, '2019-05-16 14:35:22');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Администратор', '{\"access\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/stocks\",\"catalog\\/warehouse\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/analytics\\/metrika\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/manager\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/export_import\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_sitemap\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/extendedsearch\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/ocfilter\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/quickcheckout\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/template_switcher\",\"extension\\/module\\/trade_import\",\"extension\\/module\\/vqmod_manager\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/rbs\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/shipping\\/xshippingpro\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/ecos\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"blog\\/article\",\"blog\\/category\",\"blog\\/review\",\"blog\\/setting\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"catalog\\/stocks\",\"catalog\\/warehouse\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"dashboard\\/manager\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/analytics\\/metrika\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/manager\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/export_import\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/feed\\/unisender\",\"extension\\/feed\\/yandex_market\",\"extension\\/feed\\/yandex_sitemap\",\"extension\\/feed\\/yandex_turbo\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blog_category\",\"extension\\/module\\/blog_featured\",\"extension\\/module\\/blog_latest\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/extendedsearch\",\"extension\\/module\\/featured\",\"extension\\/module\\/featured_article\",\"extension\\/module\\/featured_product\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/manager\",\"extension\\/module\\/ocfilter\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/quickcheckout\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/template_switcher\",\"extension\\/module\\/trade_import\",\"extension\\/module\\/vqmod_manager\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/ocstore_w1\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/rbs\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/webmoney_wmb\",\"extension\\/payment\\/webmoney_wme\",\"extension\\/payment\\/webmoney_wmk\",\"extension\\/payment\\/webmoney_wmr\",\"extension\\/payment\\/webmoney_wmu\",\"extension\\/payment\\/webmoney_wmv\",\"extension\\/payment\\/webmoney_wmz\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/shipping\\/xshippingpro\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"marketplace\\/opencartforum\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"search\\/search\",\"setting\\/ecos\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse`
--

CREATE TABLE `oc_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `working_hours` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse_description`
--

CREATE TABLE `oc_warehouse_description` (
  `warehouse_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_warehouse_product`
--

CREATE TABLE `oc_warehouse_product` (
  `warehouse_product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_value_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT 0.00000000
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Килограммы', 'кг'),
(1, 2, 'Kilogram', 'kg'),
(2, 1, 'Граммы', 'г'),
(2, 2, 'Gram', 'g'),
(5, 1, 'Фунты', 'lb'),
(5, 2, 'Pound', 'lb'),
(6, 1, 'Унции', 'oz'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_xshippingpro`
--

CREATE TABLE `oc_xshippingpro` (
  `id` int(8) NOT NULL,
  `method_data` mediumtext DEFAULT NULL,
  `tab_id` int(8) DEFAULT NULL,
  `sort_order` int(8) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_xshippingpro`
--

INSERT INTO `oc_xshippingpro` (`id`, `method_data`, `tab_id`, `sort_order`) VALUES
(1, '{\"display\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e \\u0425\\u0430\\u0431\\u0430\\u0440\\u043e\\u0432\\u0441\\u043a\\u0443\",\"name\":{\"1\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e \\u0425\\u0430\\u0431\\u0430\\u0440\\u043e\\u0432\\u0441\\u043a\\u0443\"},\"mask\":{\"1\":\"\"},\"desc\":{\"1\":\"\"},\"error\":{\"1\":\"\"},\"tax_class_id\":\"0\",\"logo\":\"\",\"sort_order\":\"\",\"status\":\"1\",\"group\":\"0\",\"store_all\":\"1\",\"geo_zone_all\":\"1\",\"customer_group_all\":\"1\",\"country_all\":\"1\",\"zone_all\":\"1\",\"currency_all\":\"1\",\"payment_all\":\"1\",\"customer\":\"1\",\"customer_rule\":\"inclusive\",\"city_all\":\"1\",\"city\":\"\",\"city_rule\":\"inclusive\",\"postal_all\":\"1\",\"postal\":\"\",\"postal_rule\":\"inclusive\",\"coupon_all\":\"1\",\"coupon\":\"\",\"coupon_rule\":\"inclusive\",\"days_all\":\"1\",\"time_start\":\"\",\"time_end\":\"\",\"date_start\":\"\",\"date_end\":\"\",\"category\":\"\",\"product\":\"\",\"option\":\"\",\"manufacturer_rule\":\"\",\"location_rule\":\"\",\"product_or\":\"0\",\"rate_type\":\"grand_shipping\",\"dimensional_factor\":\"500\",\"cost\":\"\",\"ranges\":[{\"product_id\":\"\",\"start\":\"0\",\"end\":\"2500\",\"cost\":\"250\",\"block\":\"0\",\"partial\":\"0\",\"type\":\"quantity\"},{\"product_id\":\"\",\"start\":\"2500\",\"end\":\"9999999999999999999\",\"cost\":\"0\",\"block\":\"0\",\"partial\":\"0\",\"type\":\"quantity\"}],\"additional\":\"\",\"additional_per\":\"\",\"additional_limit\":\"\",\"order_total_start\":\"\",\"order_total_end\":\"\",\"weight_start\":\"\",\"weight_end\":\"\",\"quantity_start\":\"\",\"quantity_end\":\"\",\"max_length\":\"\",\"max_width\":\"\",\"max_height\":\"\",\"cart_adjust\":\"0\",\"rate_final\":\"single\",\"rate_percent\":\"sub\",\"rate_min\":\"\",\"rate_max\":\"\",\"rate_add\":\"\",\"equation\":\"\"}', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Арагацотн', 'AGT', 1),
(181, 11, 'Арарат', 'ARR', 1),
(182, 11, 'Армавир', 'ARM', 1),
(183, 11, 'Гегаркуник', 'GEG', 1),
(184, 11, 'Котайк', 'KOT', 1),
(185, 11, 'Лори', 'LOR', 1),
(186, 11, 'Ширак', 'SHI', 1),
(187, 11, 'Сюник', 'SYU', 1),
(188, 11, 'Тавуш', 'TAV', 1),
(189, 11, 'Вайоц Дзор', 'VAY', 1),
(190, 11, 'Ереван', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Брест', 'BR', 1),
(338, 20, 'Гомель', 'HO', 1),
(339, 20, 'Минск', 'HM', 1),
(340, 20, 'Гродно', 'HR', 1),
(341, 20, 'Могилев', 'MA', 1),
(342, 20, 'Минская область', 'MI', 1),
(343, 20, 'Витебск', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Алматинская область', 'AL', 1),
(1717, 109, 'Алматы - город республ-го значения', 'AC', 1),
(1718, 109, 'Акмолинская область', 'AM', 1),
(1719, 109, 'Актюбинская область', 'AQ', 1),
(1720, 109, 'Астана - город республ-го значения', 'AS', 1),
(1721, 109, 'Атырауская область', 'AT', 1),
(1722, 109, 'Западно-Казахстанская область', 'BA', 1),
(1723, 109, 'Байконур - город республ-го значения', 'BY', 1),
(1724, 109, 'Мангистауская область', 'MA', 1),
(1725, 109, 'Южно-Казахстанская область', 'ON', 1),
(1726, 109, 'Павлодарская область', 'PA', 1),
(1727, 109, 'Карагандинская область', 'QA', 1),
(1728, 109, 'Костанайская область', 'QO', 1),
(1729, 109, 'Кызылординская область', 'QY', 1),
(1730, 109, 'Восточно-Казахстанская область', 'SH', 1),
(1731, 109, 'Северо-Казахстанская область', 'SO', 1),
(1732, 109, 'Жамбылская область', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Республика Хакасия', 'KK', 1),
(2722, 176, 'Забайкальский край', 'ZAB', 1),
(2723, 176, 'Чукотский АО', 'CHU', 1),
(2724, 176, 'Архангельская область', 'ARK', 1),
(2725, 176, 'Астраханская область', 'AST', 1),
(2726, 176, 'Алтайский край', 'ALT', 1),
(2727, 176, 'Белгородская область', 'BEL', 1),
(2728, 176, 'Еврейская АО', 'YEV', 1),
(2729, 176, 'Амурская область', 'AMU', 1),
(2730, 176, 'Брянская область', 'BRY', 1),
(2731, 176, 'Чувашская Республика', 'CU', 1),
(2732, 176, 'Челябинская область', 'CHE', 1),
(2733, 176, 'Карачаево-Черкесия', 'KC', 1),
(2735, 176, 'Таймырский АО', 'TDN', 1),
(2736, 176, 'Республика Калмыкия', 'KL', 1),
(2738, 176, 'Республика Алтай', 'AL', 1),
(2739, 176, 'Чеченская Республика', 'CE', 1),
(2740, 176, 'Иркутская область', 'IRK', 1),
(2741, 176, 'Ивановская область', 'IVA', 1),
(2742, 176, 'Удмуртская Республика', 'UD', 1),
(2743, 176, 'Калининградская область', 'KGD', 1),
(2744, 176, 'Калужская область', 'KLU', 1),
(2745, 176, 'Краснодарский край', 'KDA', 1),
(2746, 176, 'Республика Татарстан', 'TA', 1),
(2747, 176, 'Кемеровская область', 'KEM', 1),
(2748, 176, 'Хабаровский край', 'KHA', 1),
(2749, 176, 'Ханты-Мансийский АО - Югра', 'KHM', 1),
(2750, 176, 'Костромская область', 'KOS', 1),
(2751, 176, 'Московская область', 'MOS', 1),
(2752, 176, 'Красноярский край', 'KYA', 1),
(2753, 176, 'Коми-Пермяцкий АО', 'KOP', 1),
(2754, 176, 'Курганская область', 'KGN', 1),
(2755, 176, 'Курская область', 'KRS', 1),
(2756, 176, 'Республика Тыва', 'TY', 1),
(2757, 176, 'Липецкая область', 'LIP', 1),
(2758, 176, 'Магаданская область', 'MAG', 1),
(2759, 176, 'Республика Дагестан', 'DA', 1),
(2760, 176, 'Республика Адыгея', 'AD', 1),
(2761, 176, 'Москва', 'MOW', 1),
(2762, 176, 'Мурманская область', 'MUR', 1),
(2763, 176, 'Республика Кабардино-Балкария', 'KB', 1),
(2764, 176, 'Ненецкий АО', 'NEN', 1),
(2765, 176, 'Республика Ингушетия', 'IN', 1),
(2766, 176, 'Нижегородская область', 'NIZ', 1),
(2767, 176, 'Новгородская область', 'NGR', 1),
(2768, 176, 'Новосибирская область', 'NVS', 1),
(2769, 176, 'Омская область', 'OMS', 1),
(2770, 176, 'Орловская область', 'ORL', 1),
(2771, 176, 'Оренбургская область', 'ORE', 1),
(2772, 176, 'Корякский АО', 'KOR', 1),
(2773, 176, 'Пензенская область', 'PNZ', 1),
(2774, 176, 'Пермский край', 'PER', 1),
(2775, 176, 'Камчатский край', 'KAM', 1),
(2776, 176, 'Республика Карелия', 'KR', 1),
(2777, 176, 'Псковская область', 'PSK', 1),
(2778, 176, 'Ростовская область', 'ROS', 1),
(2779, 176, 'Рязанская область', 'RYA', 1),
(2780, 176, 'Ямало-Ненецкий АО', 'YAN', 1),
(2781, 176, 'Самарская область', 'SAM', 1),
(2782, 176, 'Республика Мордовия', 'MO', 1),
(2783, 176, 'Саратовская область', 'SAR', 1),
(2784, 176, 'Смоленская область', 'SMO', 1),
(2785, 176, 'Санкт-Петербург', 'SPE', 1),
(2786, 176, 'Ставропольский край', 'STA', 1),
(2787, 176, 'Республика Коми', 'KO', 1),
(2788, 176, 'Тамбовская область', 'TAM', 1),
(2789, 176, 'Томская область', 'TOM', 1),
(2790, 176, 'Тульская область', 'TUL', 1),
(2791, 176, 'Ленинградская область', 'LEN', 1),
(2792, 176, 'Тверская область', 'TVE', 1),
(2793, 176, 'Тюменская область', 'TYU', 1),
(2794, 176, 'Республика Башкортостан', 'BA', 1),
(2795, 176, 'Ульяновская область', 'ULY', 1),
(2796, 176, 'Республика Бурятия', 'BU', 1),
(2798, 176, 'Республика Северная Осетия', 'SE', 1),
(2799, 176, 'Владимирская область', 'VLA', 1),
(2800, 176, 'Приморский край', 'PRI', 1),
(2801, 176, 'Волгоградская область', 'VGG', 1),
(2802, 176, 'Вологодская область', 'VLG', 1),
(2803, 176, 'Воронежская область', 'VOR', 1),
(2804, 176, 'Кировская область', 'KIR', 1),
(2805, 176, 'Республика  Саха / Якутия', 'SA', 1),
(2806, 176, 'Ярославская область', 'YAR', 1),
(2807, 176, 'Свердловская область', 'SVE', 1),
(2808, 176, 'Республика Марий Эл', 'ME', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Черкасская область', '71', 1),
(3481, 220, 'Черниговская область', '74', 1),
(3482, 220, 'Черновицкая область', '77', 1),
(3483, 220, 'Крым', '43', 1),
(3484, 220, 'Днепропетровская область', '12', 1),
(3485, 220, 'Донецкая область', '14', 1),
(3486, 220, 'Ивано-Франковская область', '26', 1),
(3487, 220, 'Херсонская область', '65', 1),
(3488, 220, 'Хмельницкая область', '68', 1),
(3489, 220, 'Кировоградская область', '35', 1),
(3490, 220, 'Киев', '30', 1),
(3491, 220, 'Киевская область', '32', 1),
(3492, 220, 'Луганская область', '09', 1),
(3493, 220, 'Львовская область', '46', 1),
(3494, 220, 'Николаевская область', '48', 1),
(3495, 220, 'Одесская область', '51', 1),
(3496, 220, 'Полтавская область', '53', 1),
(3497, 220, 'Ровненская область', '56', 1),
(3498, 220, 'Севастополь', '40', 1),
(3499, 220, 'Сумская область', '59', 1),
(3500, 220, 'Тернопольская область', '61', 1),
(3501, 220, 'Винницкая область', '05', 1),
(3502, 220, 'Волынская область', '07', 1),
(3503, 220, 'Закарпатская область', '21', 1),
(3504, 220, 'Запорожская область', '23', 1),
(3505, 220, 'Житомирская область', '18', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Харьковская область', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT 0,
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Индексы таблицы `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Индексы таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Индексы таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Индексы таблицы `oc_article`
--
ALTER TABLE `oc_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Индексы таблицы `oc_article_description`
--
ALTER TABLE `oc_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  ADD PRIMARY KEY (`article_image_id`);

--
-- Индексы таблицы `oc_article_related`
--
ALTER TABLE `oc_article_related`
  ADD PRIMARY KEY (`article_id`,`related_id`);

--
-- Индексы таблицы `oc_article_related_mn`
--
ALTER TABLE `oc_article_related_mn`
  ADD PRIMARY KEY (`article_id`,`manufacturer_id`);

--
-- Индексы таблицы `oc_article_related_product`
--
ALTER TABLE `oc_article_related_product`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_article_related_wb`
--
ALTER TABLE `oc_article_related_wb`
  ADD PRIMARY KEY (`article_id`,`category_id`);

--
-- Индексы таблицы `oc_article_to_blog_category`
--
ALTER TABLE `oc_article_to_blog_category`
  ADD PRIMARY KEY (`article_id`,`blog_category_id`);

--
-- Индексы таблицы `oc_article_to_download`
--
ALTER TABLE `oc_article_to_download`
  ADD PRIMARY KEY (`article_id`,`download_id`);

--
-- Индексы таблицы `oc_article_to_layout`
--
ALTER TABLE `oc_article_to_layout`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_article_to_store`
--
ALTER TABLE `oc_article_to_store`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Индексы таблицы `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Индексы таблицы `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Индексы таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Индексы таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Индексы таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  ADD PRIMARY KEY (`blog_category_id`);

--
-- Индексы таблицы `oc_blog_category_description`
--
ALTER TABLE `oc_blog_category_description`
  ADD PRIMARY KEY (`blog_category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_blog_category_path`
--
ALTER TABLE `oc_blog_category_path`
  ADD PRIMARY KEY (`blog_category_id`,`path_id`);

--
-- Индексы таблицы `oc_blog_category_to_layout`
--
ALTER TABLE `oc_blog_category_to_layout`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_blog_category_to_store`
--
ALTER TABLE `oc_blog_category_to_store`
  ADD PRIMARY KEY (`blog_category_id`,`store_id`);

--
-- Индексы таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Индексы таблицы `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Индексы таблицы `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`),
  ADD KEY `path_id` (`path_id`);

--
-- Индексы таблицы `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Индексы таблицы `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Индексы таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Индексы таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Индексы таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Индексы таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Индексы таблицы `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Индексы таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Индексы таблицы `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Индексы таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Индексы таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Индексы таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Индексы таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Индексы таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Индексы таблицы `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Индексы таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Индексы таблицы `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Индексы таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Индексы таблицы `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Индексы таблицы `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Индексы таблицы `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Индексы таблицы `oc_email_template`
--
ALTER TABLE `oc_email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Индексы таблицы `oc_email_template_description`
--
ALTER TABLE `oc_email_template_description`
  ADD PRIMARY KEY (`email_template_id`,`language_id`);

--
-- Индексы таблицы `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Индексы таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Индексы таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Индексы таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Индексы таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`),
  ADD KEY `filter_group_id` (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`),
  ADD KEY `filter_group_id` (`filter_group_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Индексы таблицы `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Индексы таблицы `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Индексы таблицы `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Индексы таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Индексы таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Индексы таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Индексы таблицы `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Индексы таблицы `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Индексы таблицы `oc_manufacturer_to_layout`
--
ALTER TABLE `oc_manufacturer_to_layout`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Индексы таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Индексы таблицы `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Индексы таблицы `oc_ocfilter_option`
--
ALTER TABLE `oc_ocfilter_option`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `keyword` (`keyword`),
  ADD KEY `sort_order` (`sort_order`);

--
-- Индексы таблицы `oc_ocfilter_option_description`
--
ALTER TABLE `oc_ocfilter_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Индексы таблицы `oc_ocfilter_option_to_category`
--
ALTER TABLE `oc_ocfilter_option_to_category`
  ADD PRIMARY KEY (`category_id`,`option_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_ocfilter_option_to_store`
--
ALTER TABLE `oc_ocfilter_option_to_store`
  ADD PRIMARY KEY (`store_id`,`option_id`);

--
-- Индексы таблицы `oc_ocfilter_option_value`
--
ALTER TABLE `oc_ocfilter_option_value`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_ocfilter_option_value_description`
--
ALTER TABLE `oc_ocfilter_option_value_description`
  ADD PRIMARY KEY (`value_id`,`language_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_ocfilter_option_value_to_product`
--
ALTER TABLE `oc_ocfilter_option_value_to_product`
  ADD PRIMARY KEY (`ocfilter_option_value_to_product_id`),
  ADD UNIQUE KEY `option_id_value_id_product_id` (`option_id`,`value_id`,`product_id`),
  ADD KEY `slide_value_min_slide_value_max` (`slide_value_min`,`slide_value_max`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_ocfilter_option_value_to_product_description`
--
ALTER TABLE `oc_ocfilter_option_value_to_product_description`
  ADD PRIMARY KEY (`product_id`,`value_id`,`option_id`,`language_id`);

--
-- Индексы таблицы `oc_ocfilter_page`
--
ALTER TABLE `oc_ocfilter_page`
  ADD PRIMARY KEY (`ocfilter_page_id`),
  ADD KEY `keyword` (`keyword`),
  ADD KEY `category_id_params` (`category_id`,`params`);

--
-- Индексы таблицы `oc_ocfilter_page_description`
--
ALTER TABLE `oc_ocfilter_page_description`
  ADD PRIMARY KEY (`ocfilter_page_id`,`language_id`);

--
-- Индексы таблицы `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Индексы таблицы `oc_option_characteristic`
--
ALTER TABLE `oc_option_characteristic`
  ADD PRIMARY KEY (`characteristic_id`);

--
-- Индексы таблицы `oc_option_characteristic_description`
--
ALTER TABLE `oc_option_characteristic_description`
  ADD PRIMARY KEY (`characteristic_id`,`language_id`);

--
-- Индексы таблицы `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Индексы таблицы `oc_option_value_characteristic`
--
ALTER TABLE `oc_option_value_characteristic`
  ADD PRIMARY KEY (`option_value_id`,`option_id`,`characteristic_id`);

--
-- Индексы таблицы `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Индексы таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Индексы таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Индексы таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Индексы таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Индексы таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Индексы таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Индексы таблицы `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `model` (`model`),
  ADD KEY `sku` (`sku`),
  ADD KEY `upc` (`upc`),
  ADD KEY `ean` (`ean`),
  ADD KEY `jan` (`jan`),
  ADD KEY `isbn` (`isbn`),
  ADD KEY `mpn` (`mpn`),
  ADD KEY `stock_status_id` (`stock_status_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `price` (`price`),
  ADD KEY `quantity` (`quantity`),
  ADD KEY `shipping` (`shipping`),
  ADD KEY `date_available` (`date_available`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`),
  ADD KEY `text` (`text`(10));

--
-- Индексы таблицы `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`),
  ADD KEY `tag` (`tag`(10));

--
-- Индексы таблицы `oc_product_description_composition`
--
ALTER TABLE `oc_product_description_composition`
  ADD PRIMARY KEY (`product_id`,`language_id`);

--
-- Индексы таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Индексы таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `value` (`value`(10));

--
-- Индексы таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_option_id` (`product_option_id`),
  ADD KEY `option_value_id` (`option_value_id`),
  ADD KEY `price` (`price`),
  ADD KEY `quantity` (`quantity`);

--
-- Индексы таблицы `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Индексы таблицы `oc_product_related_article`
--
ALTER TABLE `oc_product_related_article`
  ADD PRIMARY KEY (`article_id`,`product_id`);

--
-- Индексы таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Индексы таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Индексы таблицы `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Индексы таблицы `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Индексы таблицы `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Индексы таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Индексы таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Индексы таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Индексы таблицы `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  ADD PRIMARY KEY (`review_article_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Индексы таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Индексы таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Индексы таблицы `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Индексы таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Индексы таблицы `oc_stocks`
--
ALTER TABLE `oc_stocks`
  ADD PRIMARY KEY (`stocks_id`);

--
-- Индексы таблицы `oc_stocks_description`
--
ALTER TABLE `oc_stocks_description`
  ADD PRIMARY KEY (`stocks_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_stocks_product`
--
ALTER TABLE `oc_stocks_product`
  ADD PRIMARY KEY (`stocks_id`,`product_id`);

--
-- Индексы таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Индексы таблицы `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Индексы таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Индексы таблицы `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Индексы таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Индексы таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD PRIMARY KEY (`group_uuid`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_trade_import_filter_group_codes`
--
ALTER TABLE `oc_trade_import_filter_group_codes`
  ADD PRIMARY KEY (`filter_uuid`),
  ADD KEY `filter_group_id` (`filter_group_id`);

--
-- Индексы таблицы `oc_trade_import_option_characteristic_codes`
--
ALTER TABLE `oc_trade_import_option_characteristic_codes`
  ADD PRIMARY KEY (`property_uuid`),
  ADD KEY `characteristic_id` (`characteristic_id`);

--
-- Индексы таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `option_id` (`option_id`);

--
-- Индексы таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD PRIMARY KEY (`characteristic_uuid`),
  ADD KEY `option_value_id` (`option_value_id`);

--
-- Индексы таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD PRIMARY KEY (`nomenclature_uuid`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_trade_import_stocks_codes`
--
ALTER TABLE `oc_trade_import_stocks_codes`
  ADD PRIMARY KEY (`stocks_uuid`),
  ADD KEY `stocks_id` (`stocks_id`);

--
-- Индексы таблицы `oc_trade_import_warehouse_codes`
--
ALTER TABLE `oc_trade_import_warehouse_codes`
  ADD PRIMARY KEY (`storage_uuid`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Индексы таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Индексы таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Индексы таблицы `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Индексы таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Индексы таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Индексы таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Индексы таблицы `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Индексы таблицы `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Индексы таблицы `oc_warehouse_description`
--
ALTER TABLE `oc_warehouse_description`
  ADD PRIMARY KEY (`warehouse_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_warehouse_product`
--
ALTER TABLE `oc_warehouse_product`
  ADD PRIMARY KEY (`warehouse_product_id`),
  ADD UNIQUE KEY `unique_warehouse_product` (`warehouse_id`,`product_id`,`option_value_id`);

--
-- Индексы таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Индексы таблицы `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Индексы таблицы `oc_xshippingpro`
--
ALTER TABLE `oc_xshippingpro`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Индексы таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT для таблицы `oc_article`
--
ALTER TABLE `oc_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT для таблицы `oc_article_image`
--
ALTER TABLE `oc_article_image`
  MODIFY `article_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT для таблицы `oc_blog_category`
--
ALTER TABLE `oc_blog_category`
  MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT для таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT для таблицы `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT для таблицы `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT для таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT для таблицы `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_email_template`
--
ALTER TABLE `oc_email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT для таблицы `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1151;

--
-- AUTO_INCREMENT для таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT для таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT для таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option`
--
ALTER TABLE `oc_ocfilter_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5017;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option_value`
--
ALTER TABLE `oc_ocfilter_option_value`
  MODIFY `value_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10156;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_option_value_to_product`
--
ALTER TABLE `oc_ocfilter_option_value_to_product`
  MODIFY `ocfilter_option_value_to_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3567;

--
-- AUTO_INCREMENT для таблицы `oc_ocfilter_page`
--
ALTER TABLE `oc_ocfilter_page`
  MODIFY `ocfilter_page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option_characteristic`
--
ALTER TABLE `oc_option_characteristic`
  MODIFY `characteristic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT для таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=292;

--
-- AUTO_INCREMENT для таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=618;

--
-- AUTO_INCREMENT для таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT для таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_review_article`
--
ALTER TABLE `oc_review_article`
  MODIFY `review_article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13883;

--
-- AUTO_INCREMENT для таблицы `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `oc_stocks`
--
ALTER TABLE `oc_stocks`
  MODIFY `stocks_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT для таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT для таблицы `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_import`
--
ALTER TABLE `oc_trade_import`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_trade_orders`
--
ALTER TABLE `oc_trade_orders`
  MODIFY `operation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `oc_warehouse`
--
ALTER TABLE `oc_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_warehouse_product`
--
ALTER TABLE `oc_warehouse_product`
  MODIFY `warehouse_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `oc_xshippingpro`
--
ALTER TABLE `oc_xshippingpro`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4232;

--
-- AUTO_INCREMENT для таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_category_codes`
--
ALTER TABLE `oc_trade_import_category_codes`
  ADD CONSTRAINT `oc_trade_import_category_codes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `oc_category` (`category_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_filter_group_codes`
--
ALTER TABLE `oc_trade_import_filter_group_codes`
  ADD CONSTRAINT `oc_trade_import_filter_group_codes_ibfk_1` FOREIGN KEY (`filter_group_id`) REFERENCES `oc_filter_group` (`filter_group_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_characteristic_codes`
--
ALTER TABLE `oc_trade_import_option_characteristic_codes`
  ADD CONSTRAINT `oc_trade_import_option_characteristic_codes_ibfk_1` FOREIGN KEY (`characteristic_id`) REFERENCES `oc_option_characteristic` (`characteristic_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_codes`
--
ALTER TABLE `oc_trade_import_option_codes`
  ADD CONSTRAINT `oc_trade_import_option_codes_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `oc_option` (`option_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_option_value_codes`
--
ALTER TABLE `oc_trade_import_option_value_codes`
  ADD CONSTRAINT `oc_trade_import_option_value_codes_ibfk_1` FOREIGN KEY (`option_value_id`) REFERENCES `oc_option_value` (`option_value_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_product_codes`
--
ALTER TABLE `oc_trade_import_product_codes`
  ADD CONSTRAINT `oc_trade_import_product_codes_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `oc_product` (`product_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_stocks_codes`
--
ALTER TABLE `oc_trade_import_stocks_codes`
  ADD CONSTRAINT `oc_trade_import_stocks_codes_ibfk_1` FOREIGN KEY (`stocks_id`) REFERENCES `oc_stocks` (`stocks_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oc_trade_import_warehouse_codes`
--
ALTER TABLE `oc_trade_import_warehouse_codes`
  ADD CONSTRAINT `oc_trade_import_warehouse_codes_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `oc_warehouse` (`warehouse_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
