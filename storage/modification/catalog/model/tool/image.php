<?php
class ModelToolImage extends Model {

	public function watermark_get_setting($store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = 'config' AND `key` LIKE 'config_watermark%'");

		foreach ($query->rows as $result) {
			$setting_data[$result['key']] = $result['value'];
		}

		return $setting_data;
	}

	public function resize($filename, $width, $height, $mode = 0, $enable_watermark = true) {
		if (!is_file(DIR_IMAGE . $filename) || substr(str_replace('\\', '/', realpath(DIR_IMAGE . $filename)), 0, strlen(DIR_IMAGE)) != str_replace('\\', '/', DIR_IMAGE)) {
			return;
		}

		$enable_watermark = $this->config->get('config_watermark_status');

		if ($enable_watermark) {
			$watermark = new Image(DIR_IMAGE . $this->config->get('config_watermark_image'));
			imagealphablending($watermark->getImage(), false);
			imagesavealpha($watermark->getImage(), true);
			imagefilter($watermark->getImage(), IMG_FILTER_COLORIZE, 0, 0, 0, (int) 127 * (1 - $this->config->get('config_watermark_opacity')));
			$pos_x = $this->config->get('config_watermark_pos_x');
			$pos_y = $this->config->get('config_watermark_pos_y');
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		if('svg' == $extension) {
            if ($this->request->server['HTTPS']) {
                return HTTPS_SERVER . 'image/' . $filename;
            } else {
                return HTTP_SERVER . 'image/' . $filename;
            }
    	}

		$image_old = $filename;
		$image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $image_new) || (filemtime(DIR_IMAGE . $image_old) > filemtime(DIR_IMAGE . $image_new))) {
			list($width_orig, $height_orig, $image_type) = getimagesize(DIR_IMAGE . $image_old);
				 
			if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) { 
				return DIR_IMAGE . $image_old;
			}
						
			$path = '';

			$directories = explode('/', dirname($image_new));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $image_old);
				$image->resize($width, $height, '', $mode);
				if ($enable_watermark) {
					if ($this->config->get('config_watermark_resize_first')) {
						$image->watermark_resized($watermark, $this->config->get('config_watermark_pos_y_center'), abs($pos_x), $pos_y);
					} else {
						$image->watermark($watermark, $this->watermark_get_setting());
					}
				}
				$image->save(DIR_IMAGE . $image_new);
			} else {
				copy(DIR_IMAGE . $image_old, DIR_IMAGE . $image_new);
			}
		}
		
		$image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
		
		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $image_new;
		} else {
			return $this->config->get('config_url') . 'image/' . $image_new;
		}
	}

	public function get_image($filename, $enable_watermark = false) {
		if (!is_file(DIR_IMAGE . $filename) || substr(str_replace('\\', '/', realpath(DIR_IMAGE . $filename)), 0, strlen(DIR_IMAGE)) != str_replace('\\', '/', DIR_IMAGE)) {
			return;
		}

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $filename;
		} else {
			return $this->config->get('config_url') . 'image/' . $filename;
		}
	}
}
