<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');
		
		if ( isset($this->session->data['order_id']) && ( ! empty($this->session->data['order_id']))  ) {
			$this->session->data['last_order_id'] = $this->session->data['order_id'];
		}

		if (isset($this->session->data['order_id'])) {
			if ($this->config->get('module_trade_import_enable_order')) {
				//Connect to Trade and send order
				$time = time();
		    	$date = date('c', $time);
				$this->load->model('extension/module/trade_import');
				$this->load->model('account/customer');
		    	$order_data = array();
				if ($this->customer->isLogged()) {
					$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
					$order_data['firstname'] = empty($customer_info['firstname']) ? 'null' : $customer_info['firstname'];
					$order_data['email'] = empty($customer_info['email']) ? 'null' : $customer_info['email'];
					$order_data['telephone'] = empty($customer_info['telephone']) ? 'null' : $customer_info['telephone'];
				} elseif (isset($this->session->data['guest'])) {
					$order_data['firstname'] = empty($this->session->data['guest']['firstname']) ? 'null' : $this->session->data['guest']['firstname'];
					$order_data['email'] = empty($this->session->data['guest']['email']) ? 'null' : $this->session->data['guest']['email'];
					$order_data['telephone'] = empty($this->session->data['guest']['telephone']) ? 'null' : $this->session->data['guest']['telephone'];
				}
				$order_data['address'] = $this->model_extension_module_trade_import->get_order_address($this->session->data['order_id']);
				$order_data['address'] = empty($order_data['address']) ? 'null' : $order_data['address'];
		    	$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
				$order_data['store_id'] = $this->config->get('config_store_id');
				if ($order_data['store_id']) {
					$order_data['store_url'] = parse_url($this->config->get('config_url'), PHP_URL_HOST);
				} else {
					$order_data['store_url'] = parse_url(HTTP_SERVER, PHP_URL_HOST);
				}

		    	$data = array();
		    	$data['orders'] = array();
		    	$data['orders'][] = array(
		    		'contractor'	=> array(
		    			'phone'		=> $order_data['telephone'],
		    			'email'		=> $order_data['email'],
		    			'name'		=> $order_data['firstname'],
		    			'address'	=> $order_data['address'],
		    		),
		    		'date' 			=> $date,
		    		'description'	=> $order_data['store_url'] . " - " . $order_data['invoice_prefix'] . "-" . $this->session->data['order_id'],
		    	);
		    	$data['orders'][0]['goods'] = array();
		    	foreach ($this->cart->getProducts() as $product) {
					foreach ($product['option'] as $option) {
						if ($option['option_id'] == $this->model_extension_module_trade_import->get_optionid_by_code($this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']))) {
							$characteristic_uuid = $this->model_extension_module_trade_import->get_option_value_code_by_id($option['option_value_id']);
							break;
						}
					}

		    		$data['orders'][0]['goods'][] = array(
		    			'nomenclature_uuid'		=> $this->model_extension_module_trade_import->get_product_code_by_id($product['product_id']),
		    			'characteristic_uuid'	=> isset($characteristic_uuid) ? $characteristic_uuid : NULL,
		    			'shipment_uuid'			=> NULL,
		    			'quantity'				=> (double)$product['quantity'],
		    			'price'					=> (double)$product['price'],
		    			'discount'				=> (double)0.00,
		    			'total'					=> (double)$product['total']
		    		);
		    	}
		    	if (!file_exists("trade_import")) {
		    		mkdir("trade_import", 0777, true);
		    	}
		    	$order_url = $this->config->get('module_trade_import_order_address');
		    	$url = $this->config->get('module_trade_import_code');
		    	$token = $this->config->get('module_trade_import_token');
		    	$old_api_token = $this->config->get('module_trade_import_order_token');
		    	if (!$this->config->get('module_trade_import_enable_old_api')) {
			    	$data_string = json_encode(array('token' => $token));
			    	$ch = curl_init();
					$header = array();
					$header[] = "Content-Type: application/json";
					$header[] = "UUID: " . $token;
					$header[] = "Timestamp: " . $date;
					$header[] = "Authorization: " . hash("sha512", $token . $time);
					$header[] = "Content-Length: " . strlen($data_string);
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    	$response = curl_exec($ch);
			    	if ($response === false) {
						echo 'Curl error: ', curl_error($ch), "\n";
						file_put_contents("trade_import/error_" . $date . ".log", curl_error($ch) . "\n" . $response);
						$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
			        	curl_close($ch);
			        	return 0;
			        } else {
			        	$response_decoded = json_decode($response, true);
		                $access_token = $response_decoded['access_token'];
		                curl_close($ch);
		                $data_string = json_encode($data);
		                $ch = curl_init();
		                $header = array();
		                $header[] = "Content-Type: application/json";
		                $header[] = "Authorization: Bearer " . $access_token;
		                curl_setopt($ch, CURLOPT_URL, $order_url);
		                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		                $response = curl_exec($ch);
		                curl_close($ch);
			        }
			    } else {
			    	$data_string = json_encode($data);
			    	$ch = curl_init();
					$header = array();
					$header[] = "Content-Type: application/json";
					$header[] = "UUID: " . $old_api_token;
					$header[] = "Timestamp: " . $date;
					$header[] = "Authorization: " . hash("sha512", $old_api_token . $time);
					$header[] = "Content-Length: " . strlen($data_string);
					curl_setopt($ch, CURLOPT_URL, $order_url);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    	$response = curl_exec($ch);
			    	curl_close($ch);
			    }
		        if (json_decode($response) !== NULL) {
		        	$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], $response);
		        } else {
		        	$this->model_extension_module_trade_import->add_order($data_string, $this->session->data['order_id'], 'ERROR: ' . $date . ".log");
		        	file_put_contents("trade_import/error_" . $date . ".log", $response);
		        }
			}

			$this->cart->clear();

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		if (! empty($this->session->data['last_order_id']) ) {
			$this->document->setTitle(sprintf($this->language->get('heading_title_customer'), $this->session->data['last_order_id']));
			$this->document->setRobots('noindex,follow');
		} else {
			$this->document->setTitle($this->language->get('heading_title'));
			$this->document->setRobots('noindex,follow');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);
		
		if (! empty($this->session->data['last_order_id']) ) {
			$data['heading_title'] = sprintf($this->language->get('heading_title_customer'), $this->session->data['last_order_id']);
		} else {
			$data['heading_title'] = $this->language->get('heading_title');
		}

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/order/info&order_id=' . $this->session->data['last_order_id'], '', true), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('information/contact'), $this->url->link('product/special'), $this->session->data['last_order_id'], $this->url->link('account/download', '', true));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'), $this->session->data['last_order_id']);
		}

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}