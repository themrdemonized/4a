<?php
class ModelCatalogOptionValueCharacteristic extends Model {
	public function getOptionValueCharacteristic($option_value_id) {
		$data = array();

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_value_characteristic` ovc LEFT JOIN " . DB_PREFIX . "option_characteristic_description ocd ON (ovc.characteristic_id = ocd.characteristic_id) WHERE ocd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND ovc.option_value_id = '" . (int)$option_value_id . "' ORDER BY ocd.name ASC, ovc.value ASC");

		foreach ($query->rows as $result) {
			$data[$result['name']][] = $result['value'];
		}

		return $data;
	}

	public function getOptionValueCharacteristics($option_value_ids = array()) {
		$data = array();

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_value_characteristic` " . (!empty($option_value_ids) ? ("WHERE option_value_id IN (" . $this->db->escape(implode(",", $option_value_ids)) . ")") : NULL));

		foreach ($query->rows as $result) {
			$data[$result['option_value_id']][$result['characteristic_id']] = $result['value'];
		}

		return $data;
	}

	public function getOptionCharacteristics($option_value_ids = array()) {
		$data = array();

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_characteristic` oc LEFT JOIN " . DB_PREFIX . "option_value_characteristic ovc ON (oc.characteristic_id = ovc.characteristic_id) LEFT JOIN " . DB_PREFIX . "option_characteristic_description ocd ON (oc.characteristic_id = ocd.characteristic_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov ON (ovc.option_value_id = pov.option_value_id) WHERE ocd.language_id = '" . (int)$this->config->get('config_language_id') . "'" . (!empty($option_value_ids) ? (" AND ovc.option_value_id IN (" . $this->db->escape(implode(",", $option_value_ids)) . ")") : NULL) . " GROUP BY oc.characteristic_id, ovc.value ORDER BY ocd.name ASC, pov.price ASC, ovc.value ASC");

		foreach ($query->rows as $result) {
			$data[$result['characteristic_id']]['characteristic_id'] = $result['characteristic_id'];
			$data[$result['characteristic_id']]['name'] = $result['name'];
			$data[$result['characteristic_id']]['type'] = $result['type'];
			$data[$result['characteristic_id']]['characteristic'][] = array(
				'option_value_id' => $result['option_value_id'],
				'value' => $result['serialized'] ? json_decode($result['value'], true) : $result['value'],
			);
		}

		usort($data, function($a, $b) {
			return strcmp($a['name'], $b['name']);
		});

		return $data;
	}

	public function getAllOptionCharacteristics($option_value_ids = array()) {
		$data = array();

		$data['grouped'] = $this->getOptionCharacteristics($option_value_ids);

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_characteristic` oc LEFT JOIN " . DB_PREFIX . "option_value_characteristic ovc ON (oc.characteristic_id = ovc.characteristic_id) LEFT JOIN " . DB_PREFIX . "option_characteristic_description ocd ON (oc.characteristic_id = ocd.characteristic_id) LEFT JOIN " . DB_PREFIX . "product_option_value pov ON (ovc.option_value_id = pov.option_value_id) WHERE ocd.language_id = '" . (int)$this->config->get('config_language_id') . "'" . (!empty($option_value_ids) ? (" AND ovc.option_value_id IN (" . $this->db->escape(implode(",", $option_value_ids)) . ")") : NULL) . " ORDER BY ocd.name ASC, pov.price ASC, ovc.value ASC");

		$data['raw'] = $query->rows;

		return $data;
	}
}