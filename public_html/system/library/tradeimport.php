<?php

class TradeImport {

    private $registry;

    private $categories = array();
    private $products = array();
    private $filters = array();
    private $filters_color = array();
    private $discounts = array();
    private $stocks = array();
    private $warehouses = array();
    private $option_characteristic = array();
    private $color_codes = array();
    private $stock_checkout;
    private $access_token;
    private $debug = false;

    private static $instance;
   
    /**
    * @param  object  $registry  Registry Object
    */
    public static function get_instance($registry) {

        if (is_null(static::$instance)) {
          static::$instance = new static($registry);
        }

        return static::$instance;
    }

    public function __construct($registry, $debug = false) {
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '1024M'); 
        $this->registry = $registry;
        $this->debug = $debug;
        $this->load->model('setting/setting');
        $this->load->model('extension/module/trade_import');
        $this->load->language('extension/module/trade_import');
    }

    public function __get($name) {
        return $this->registry->get($name);
    }

    //JSON Process
    protected function open_path($path) {
        $folder_path = dirname($path);
        if (!is_dir($folder_path)) {
            mkdir($folder_path, 0777, true);
        }
        return fopen($path, 'w+');
    }

    protected function replace_between($str, $needle_start, $needle_end, $replacement) {
        $pos = strpos($str, $needle_start);
        $start = $pos === false ? 0 : $pos + strlen($needle_start);
        $pos = strpos($str, $needle_end, $start);
        $end = $pos === false ? strlen($str) : $pos;
        return substr_replace($str, $replacement, $start, $end - $start);
    }

    protected function array_flatten($array) {
        $flat = array(); // initialize return array
        $stack = array_values($array); // initialize stack
        while($stack) // process stack until done
        {
            $value = array_shift($stack);
            if (is_array($value)) // a value to further process
            {
                $stack = array_merge(array_values($value), $stack);
            }
            else // a value to take
            {
               $flat[] = $value;
            }
        }
        return $flat;
    }

    protected function get_groups($response) {
        $string = $this->replace_between($response, '{', '"groups"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }   

    protected function organize_groups($arr, $parent_key = '', $path = '', $real_path = '') {
        $branch = array();
        foreach ($arr as $element) {
            $p = $path . "/" . $element[0];
            $rp = $real_path . "/" . $element[1];
            if ($element[3] == $parent_key) {
                $element[5] = $p;
                $element[6] = $rp;
                $children = $this->organize_groups($arr, $element[0], $p, $rp);
                if ($children) {
                    $element[7] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }   

    protected function get_nomenclatures($response) {
        $strings = array();
        $string = $this->replace_between($response, '{', '"nomenclatures"', '');
        $string = $this->replace_between($string, '"deleted"', chr(NULL), '');
        $string = str_replace(']],"deleted"', ']]', $string);
        $string = substr_replace($string, "", 0, strrpos($string, '"values":[[') + strlen('"values":'));
        //return [$string];
        $string = substr($string, 1, -1);
        $level = 0;
        $key = 0;
        $size = 0;
        for ($pos = 0; $pos < strlen($string); $pos++) {
            $size++;
            if ($string[$pos] === '[') {
                $level++;
            } else if ($string[$pos] === ']') {
                $level--;
            }
            if (($size > 1000000) && ($level == 0)) {
                $strings[] = "[" . substr($string, $key, $pos - $key + 1) . "]";
                $size = 0;
                if (isset($string[$pos + 1]) && ($string[$pos + 1] === ',')) {
                    $pos++;
                }
                $key = $pos + 1;
            }
        }
        if (empty($strings)) {
            $strings[] = "[" . $string . "]";
        } else {
            if ($key < strlen($string)) {
                $strings[] = "[" . substr($string, $key) . "]";
            }
        }
        return $strings;
    }

    protected function get_prices($response) {
        $string = $this->replace_between($response, '{', '"price_types"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    protected function get_stocks($response) {
        $string = $this->replace_between($response, '{', '"stocks"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    protected function get_warehouses($response) {
        $string = $this->replace_between($response, '{', '"storages"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    protected function get_property_types($response) {
        $string = $this->replace_between($response, '{', '"property_types"', '');
        $string = $this->replace_between($string, ']],"deleted"', chr(NULL), '}}');
        $string = str_replace(']],"deleted"', ']]', $string);
        return $string;
    }

    protected function get_timestamp($response) {
        $string = $this->replace_between($response, '{', '"timestamp"', '');
        $timestamp = json_decode($string, true);
        return $timestamp['timestamp'];
    }

    protected function get_seo($string, $full_path_url = false) {
        $cyr = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];
        $lat = ['a','b','v','g','d','e','io','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya','a','b','v','g','d','e','io','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya'];
        $symb = ['_','(',')','\\',' ','.',',','<','>','"',"'",'«','»','+',"&nbsp;&nbsp;&gt;&nbsp;&nbsp;","&quot;", "*", ";", "?", "!", "@", "&", "#", "$", "%", "^", "=", "|", "~", "№", ":", "[", "]"];
        if (!$full_path_url) {
            $symb[] = '/';
        }
        $seo = strtolower(str_replace($cyr, $lat, $string));
        $seo = str_replace($symb, '-', $seo);
        $seo = preg_replace('/^\-+|\-+$|\-+(?=\-)/', '', $seo);
        return $seo;
    }

    protected function get_category_name($name) {
        $category_name = explode('&nbsp;&nbsp;&gt;&nbsp;&nbsp;', $name);
        return end($category_name);
    }

    protected function get_category_path($name) {
        $category_path = str_replace('&nbsp;&nbsp;&gt;&nbsp;&nbsp;', '/', $name); 
        return $category_path;
    }

    protected function get_categories() {
        $categories = array();
        foreach ($this->model_extension_module_trade_import->getCategories(array()) as $category) {
            $categories[] = array(
                'category_id'   => $category['category_id'],
                'name'          => $this->get_category_name($category['name']),
                'parent_id'     => $category['parent_id'],
                'path'          => $this->get_category_path($category['name']),
                'original_path' => $category['name'],
            );
        }
        return $categories;
    }

    protected function organize_categories($arr, $parent_key = 0) {
        $branch = array();
        foreach ($arr as $element) {
            if ($element['parent_id'] == $parent_key) {
                $children = $this->organize_categories($arr, $element['category_id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

//Categories
    protected function set_category_template() {
        $data = array(
            'parent_category_id' => $this->config->get('module_trade_import_parent_id') !== NULL ? $this->config->get('module_trade_import_parent_id') : 0,
            'parent_category_code' => array_filter(explode(",", $this->config->get('module_trade_import_top_category'))),
            'store' => $this->model_extension_module_trade_import->get_store_name(),
            'keep_names' => $this->config->get('module_trade_import_keep_names'),
            'keep_meta' => $this->config->get('module_trade_import_keep_meta'),
            'short_url' => $this->config->get('module_trade_import_short_url'),
            'full_path_url' => $this->config->get('module_trade_import_full_path_url'),
            'category_description' => array(
                1 => array(
                    'name'              => '',
                    'meta_h1'           => '',
                    'description'       => '',
                    'meta_title'        => '',
                    'meta_description'  => '',
                    'meta_keyword'      => ''
                )
            ),
            'category_store' => array(
                0 => 0
            ),
            'column'        => 1,
            'sort_order'    => 0,
            'status'        => 1,
            'noindex'       => 1,
            'category_seo_url' => array(
                0 => array(
                    1 => ''
                )
            ),
            'parent_id' => 0,
            'top' => 0,
            'code' => '',
            'path' => ''
        );
        return $data;
    }

    protected function set_category($arr, &$data) {
        $data['category_description'][1]['name'] = $arr[1];
        $data['category_description'][1]['meta_h1'] = $arr[1];
        $data['category_description'][1]['description'] = $arr[1];
        $data['category_description'][1]['meta_title'] = 'Купить ' . $arr[1] . ' в ' . $data['store'] . ' по лучшей цене';
        $data['category_description'][1]['meta_description'] = 'Покупайте ' . $arr[1] . ' в магазине ' . $data['store'] . ' по лучшей цене';
        $data['category_description'][1]['meta_keyword'] = str_replace('/',',',$arr[4]) . ','. $data['store'];
        if ($data['full_path_url']) {
            $data['category_seo_url'][0][1] = $this->get_seo(substr($arr[6], 1), true);
        } else {
            $data['category_seo_url'][0][1] = $data['short_url'] ? $this->get_seo($arr[1]) : $this->get_seo(substr($arr[6], 1));
        }
        $data['code'] = $arr[0];
        $data['path'] = substr($arr[5], 1);
        $data['raw_path'] = substr($arr[6], 1);
        $this->categories[$arr[0]] = $data;
        if (isset($arr[7])) {
            foreach ($arr[7] as $sub_category) {
                $this->set_category($sub_category, $data);
            }
        }
    }

    protected function hide_category($arr) {
        if (isset($arr['children'])) {
            foreach ($arr['children'] as $children) {
                if ($this->hide_category($children) == 1) {
                    $not_empty = 1;
                }
            }
        }
        $data = $this->model_extension_module_trade_import->getProductsByCategoryId($arr['category_id']);
        foreach ($data as $product) {
            if ($product['status']) {
                $this->model_extension_module_trade_import->show_category($arr['category_id']);
                return 1;
            }
        }
        if (!isset($not_empty)) {
            if ($this->debug) {
                echo 'Category ', $arr['path'], ' is empty. Hiding.', "\n";
            }
            $this->model_extension_module_trade_import->hide_category($arr['category_id']);
        } else {
            $this->model_extension_module_trade_import->show_category($arr['category_id']);
            return 1;
        }
    }

    protected function hide_categories() {
        foreach ($this->organize_categories($this->get_categories()) as $category) {
            $this->hide_category($category);
        }
    }

//Products
    protected function set_price($method, $price) {
        switch ($method) {
            case 'off': return $price; break;
            case 'normal': return round($price); break;
            case 'up': return ceil($price); break;
            case 'down': return floor($price); break;
            case 'middle': return round($price * 2, 0) / 2; break;
            default: return $price; break;
        }
    }

    protected function set_product_template() {
        $price_map = array();
        $maps = array_filter(explode(";", $this->config->get('module_trade_import_price_map')));
        if (!empty($maps)) {
            foreach ($maps as $map) {
                $t = explode(':', $map);
                $price_map[$t[0]] = explode(',', $t[1]);
            }
        }
        $data = array(
            'store'     => $this->model_extension_module_trade_import->get_store_name(),
            'parent_category_id' => $this->config->get('module_trade_import_parent_id'),
            'parent_category_code' => array_filter(explode(",", $this->config->get('module_trade_import_top_category'))),
            'price_uuid'=> $this->config->get('module_trade_import_price'),
            'price_map' => $price_map,
            'hide_product' => $this->config->get('module_trade_import_hide_product') || $this->config->get('module_trade_import_hide_empty_product'),
            'hide_noprice_product' => $this->config->get('module_trade_import_hide_product'),
            'hide_empty_product' => $this->config->get('module_trade_import_hide_empty_product'),
            'server'    => $this->config->get('module_trade_import_server'),
            'separate'  => $this->config->get('module_trade_import_add_separate_products'),
            'round_price' => $this->config->get('module_trade_import_round_price'),
            'customer_groups' => array_column($this->model_extension_module_trade_import->getCustomerGroups(), 'customer_group_id'),
            'ignore_filter' => array_filter(explode(",", $this->config->get('module_trade_import_ignore_filter'))),
            'add_properties_to_filters' => array_filter(explode(",", $this->config->get('module_trade_import_add_properties_to_filters'))),
            'keep_names' => $this->config->get('module_trade_import_keep_names'),
            'keep_product_description' => $this->config->get('module_trade_import_keep_product_description'),
            'keep_meta' => $this->config->get('module_trade_import_keep_meta'),
            'full_path_url' => $this->config->get('module_trade_import_full_path_url'),
            'model'     => '',
            'sku'       => '',
            'jan'       => NULL,
            'isbn'      => NULL,
            'mpn'       => NULL,
            'upc'       => NULL,
            'location'  => NULL,
            'manufacturer_id'   => NULL,
            'points'    => NULL,
            'weight'    => NULL,
            'weight_class_id'   => NULL,
            'length'    => NULL,
            'width'     => NULL,
            'height'    => NULL,
            'length_class_id'   => NULL,
            'tax_class_id'      => NULL,
            'image'     => NULL,
            'additional_image' => array(),
            'minimum'   => 1,
            'subtract'  => $this->stock_checkout,
            'stock_status_id'   => 5,
            'shipping'  => 1,
            'noindex'   => 1,
            'date_available'    => date("Y-m-d"),
            'product_category' => array(),
            'product_store' => array(
                0   => 0
            ),
            'product_image' => array(),
            'sort_order' => 0,
            'product_description' => array(
                1 => array(
                    'name'  => '',
                    'description' => '',
                    'meta_h1' => '',
                    'meta_title' => '',
                    'meta_description' => '',
                    'meta_keyword' => '',
                    'tag' => ''
                )
            ),
            'product_description_composition' => array(),
            'quantity'  => 0,
            'price'     => 0,
            'ean'       => NULL,
            'product_seo_url' => array(),
            'product_special' => array(),
            'status'    => 0,
            'product_option' => array(),
            'code' => '',
            'option_data' => array(),
        );
        return $data;
    }

    protected function get_image($data, $id, $rewrite = false) {
        $image_file = "catalog/trade_import/" . $id . ".png";
        if (!file_exists(DIR_IMAGE . $image_file) || $rewrite) {
            if ($rewrite) {
                unlink(DIR_IMAGE . $image_file);
            }
            $image_server = $data['server'] . '/api/v1/attachments/' . $id;
            $fp = $this->open_path(DIR_IMAGE . $image_file);
            $ch = curl_init();
            $header = array();
            $header[] = "Authorization: Bearer " . $this->access_token;
            curl_setopt($ch, CURLOPT_URL, $image_server);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $r = curl_exec($ch);
            curl_close($ch);
            if (isset(json_decode($r, true)['error'])) {
                $url = $this->config->get('module_trade_import_code');
                $token = $this->config->get('module_trade_import_token');
                $time = time();
                $date = date('c', $time);
                $ch = curl_init();
                $header = array();
                $data_string = json_encode(array('token' => $token));
                $header[] = "Content-Type: application/json";
                $header[] = "UUID: " . $token;
                $header[] = "Timestamp: " . $date;
                $header[] = "Authorization: " . hash("sha512", $token . $time);
                $header[] = "Content-Length: " . strlen($data_string);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                $response = curl_exec($ch);
                if ($response === false) {
                    if ($this->debug) {
                        echo 'Curl error: ', curl_error($ch), "\n";
                        echo $response;
                    }
                    curl_close($ch);
                    $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
                    return 0;
                }
                if ($this->debug) {
                    echo $url, " connection successful.", "\n";
                }
                $response_decoded = json_decode($response, true);
                $this->access_token = $response_decoded['access_token'];
                curl_close($ch);
                fclose($fp);
                $this->get_image($data, $id, true);
            }
            fwrite($fp, $r);
            fclose($fp);
        }
        return $image_file;
    }

    protected function set_products($arr, &$data, $category_codes, $force_add = false) {
        if (isset($category_codes[$arr[5]]) || $force_add || $data['parent_category_id']) {
            $data['model'] = isset($arr[2]) ? $arr[2] : $arr[3];
            $data['sku'] = $arr[2];
            $data['image'] = NULL;
            $data['additional_image'] = array();
            if (isset($arr[11])) {
                foreach (array_filter(explode(",", substr($arr[11], 1, -1))) as $key => $image) {
                    if ($key == 0) {
                        $data['image'] = $this->get_image($data, $image);
                    } else {
                        $data['additional_image'][] = $this->get_image($data, $image);
                    }
                }  
            }
            $data['code'] =  $arr[0];
            $data['group_code'] = $arr[5];
            $data['measure_name'] = $arr[6];
            $data['product_category'] = isset($this->categories[$arr[5]]['path']) ? $this->categories[$arr[5]]['path'] : NULL;
            $data['product_description'][1]['name'] = $arr[3];
            $data['product_description'][1]['description'] = $arr[4];
            $data['product_description'][1]['meta_h1'] = $arr[3];
            $data['product_description'][1]['meta_title'] = 'Купить ' . $arr[3] . ' в ' . $data['store'] . ' по лучшей цене';
            $data['product_description'][1]['meta_description'] = 'Покупайте ' . $arr[3] . ' в магазине ' . $data['store'] . ' по лучшей цене';
            $data['product_description'][1]['meta_keyword'] = $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL) . ',' . $data['store'];
            $data['product_description'][1]['tag'] = $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL);
            $data['product_description_composition'] = array();
            if (!empty($arr[12])) {
                $data['product_description_composition'][1]['composition'] = $arr[12];
            }

            $data['quantity'] = 0;
            if (isset($arr[16])) {
                foreach ($arr[16] as $quantity) {
                    $data['quantity'] += (int)$quantity[0];
                }
            }
            $data['product_special'] = NULL;
            $data['price'] = 0;
            if (isset($arr[17])) {
                foreach ($arr[17] as $price) {
                    if ($data['price_uuid']) {
                        if ($price[2] == $data['price_uuid']) {
                            $data['price'] = $this->set_price($data['round_price'], $price[0]);
                        }
                        if (isset($data['price_map'][$price[2]])) {
                            foreach ($data['price_map'][$price[2]] as $key => $customer) {
                                $data['product_special'][$key]['customer_group_id'] = $customer;
                                $data['product_special'][$key]['priority'] = 1;
                                $data['product_special'][$key]['price'] = $this->set_price($data['round_price'], $price[0]);
                                $data['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                                $data['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                            }
                        }
                    } else {
                        if ($data['price'] == 0) {
                            $data['price'] = $this->set_price($data['round_price'], $price[0]);
                        }
                        if (isset($data['price_map'][$price[2]])) {
                            foreach ($data['price_map'][$price[2]] as $key => $customer) {
                                $data['product_special'][$key]['customer_group_id'] = $customer;
                                $data['product_special'][$key]['priority'] = 1;
                                $data['product_special'][$key]['price'] = $this->set_price($data['round_price'], $price[0]);
                                $data['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                                $data['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                            }
                        }
                    }
                }
            }
            $data['ean'] = NULL;
            if (isset($arr[15])) {
                $data['ean'] = $arr[15][0][0];
            }
            if (isset($arr[16])) {
                foreach ($arr[16] as $warehouse) {
                    if (isset($this->warehouses[$warehouse[2]])) {
                        $this->warehouses[$warehouse[2]]['products'][] = array(
                            'nomenclature_uuid' => $arr[0],
                            'characteristic_uuid' => $warehouse[1],
                            'quantity' => $warehouse[0],
                            'name' => $arr[3]
                        );
                    }
                }
            }
            if (isset($arr[19])) {
                foreach ($arr[19] as $filter) {
                    if (!in_array($filter[1], $data['ignore_filter'])) {
                        $this->filters[$filter[1]]['filter_group_name'] = $filter[1];
                        foreach ($this->array_flatten($filter[2]) as $filter_name) {
                            if (substr($filter_name, 0, 1) === "{" && substr($filter_name, -1) === "}") {
                                $filters = json_decode(str_replace("=>", ": ", $filter_name));
                            } else {
                                $filters = (array) $filter_name;
                            }
                            if (isset($data['product_category'])) {
                                foreach (explode("/", $data['product_category']) as $category) {
                                    if (isset($this->categories[$category])) {
                                        foreach ($filters as $f) {
                                            $this->filters[$filter[1]]['filters'][$f]['categories'][$category] = $this->categories[$category]['category_description'][1]['name'];
                                        }
                                    }
                                }
                            }
                            foreach ($filters as $f) {
                                $this->filters[$filter[1]]['filters'][$f]['products'][$data['code']] = $arr[3];
                            } 
                        }
                    }
                }
            }
            if ($data['full_path_url'] && isset($this->categories[$arr[5]]['path'])) {
                $data['product_seo_url'][0][1] = $this->get_seo($this->categories[$arr[5]]['raw_path'] . "/" . $arr[3], true);
            } else {
                $data['product_seo_url'][0][1] = $this->get_seo($arr[3]);
            }
            $data['status'] = 1;
            if ($data['hide_product']) {
                if ($data['hide_noprice_product']) {
                    if ($data['price'] <= 0) {
                        $data['status'] = 0;
                        if ($this->debug) {
                            echo 'Product ', $arr[3], ' is null price. Hiding.', "\n";
                        }
                    }
                }
                if ($data['hide_empty_product']) {
                    if ($data['quantity'] <= 0) {
                        $data['status'] = 0;
                        if ($this->debug) {
                            echo 'Product ', $arr[3], ' is empty. Hiding.', "\n";
                        }
                    }
                }
            }
            $data['option_data'] = array();
            $data['product_option'] = array();
            if (isset($arr[13])) {
                $data['option_data'][0] = array(
                    'option_description'    => array(
                        1   => array(
                            'name' => $arr[3]
                        )
                    ),
                    'type'  => 'select',
                    'sort_order'    => 0
                );
                $data['product_option'][0]['name'] = $arr[3];
                $data['product_option'][0]['type'] = 'select';
                $data['product_option'][0]['required'] = 1;
                foreach ($arr[13] as $key => $attr) {
                    $data['option_data'][0]['option_value'][$attr[0]]['image'] = NULL;
                    $data['option_data'][0]['option_value'][$attr[0]]['sort_order'] = $key;
                    $data['option_data'][0]['option_value'][$attr[0]]['code'] = $attr[0];
                    $data['option_data'][0]['option_value'][$attr[0]]['option_value_description'][1]['name'] = $attr[1];
                    if (isset($attr[2])) {
                        foreach ($attr[2] as $property) {
                            $this->option_characteristic[$property[0]]['code'] = $property[0];
                            $this->option_characteristic[$property[0]]['type'] = isset($this->color_codes[$property[0]]) ? 'colors' : 'select';
                            if (!in_array($property[1], $data['ignore_filter']) && ($this->option_characteristic[$property[0]]['type'] == 'colors' || in_array($property[1], $data['add_properties_to_filters']))) {
                                if ($this->option_characteristic[$property[0]]['type'] == 'colors') {
                                    $this->filters_color[$property[1]]['filter_group_name'] = $property[1];
                                    foreach ($this->array_flatten($property[2]) as $p) {
                                        $filters = isset($this->color_codes[$property[0]][$p]) ? array($p => $this->color_codes[$property[0]][$p]) : array($p => "");
                                        $filters = json_encode($filters, JSON_UNESCAPED_UNICODE);
                                        if (isset($data['product_category'])) {
                                            foreach (explode("/", $data['product_category']) as $category) {
                                                if (isset($this->categories[$category])) {
                                                    $this->filters_color[$property[1]]['filters'][$filters]['categories'][$category] = $this->categories[$category]['category_description'][1]['name'];
                                                }
                                            }
                                        }
                                        $this->filters_color[$property[1]]['filters'][$filters]['products'][$data['code']] = $arr[3]; 
                                    }
                                } else {
                                    $this->filters[$property[1]]['filter_group_name'] = $property[1];
                                    foreach ($this->array_flatten($property[2]) as $p) {
                                        $filters = $p;
                                        if (isset($data['product_category'])) {
                                            foreach (explode("/", $data['product_category']) as $category) {
                                                if (isset($this->categories[$category])) {
                                                    $this->filters[$property[1]]['filters'][$filters]['categories'][$category] = $this->categories[$category]['category_description'][1]['name'];
                                                }
                                            }
                                        }
                                        $this->filters[$property[1]]['filters'][$filters]['products'][$data['code']] = $arr[3]; 
                                    }
                                }
                            }
                            $this->option_characteristic[$property[0]]['name'] = $property[1];
                            if (isset($property[2])) {
                                $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]] = array();
                                foreach ($this->array_flatten($property[2]) as $p) {
                                    if ($this->option_characteristic[$property[0]]['type'] == 'colors') {
                                        $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]][] = isset($this->color_codes[$property[0]][$p]) ? array($p => $this->color_codes[$property[0]][$p]) : array($p => "");
                                    } else {
                                        $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]][] = $p;
                                    }
                                }
                            }
                        }
                    }
                    if ($data['separate']) {
                        $data['option_data'][0]['option_value'][$attr[0]]['name'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$attr[0]]['quantity'] = 0;
                        $data['option_data'][0]['option_value'][$attr[0]]['price'] = $data['price'];
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['name'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['description'] = $arr[4];
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['meta_h1'] = $arr[3] . " " . $attr[1];
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['meta_title'] = 'Купить ' . $arr[3] . " " . $attr[1] . ' в ' . $data['store'] . ' по лучшей цене';
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['meta_description'] = 'Покупайте ' . $arr[3] . " " . $attr[1] . ' в магазине ' . $data['store'] . ' по лучшей цене';
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['meta_keyword'] = $arr[3] . " " . $attr[1] . "," . $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL) . ',' . $data['store'];
                        $data['option_data'][0]['option_value'][$attr[0]]['product_description'][1]['tag'] = $arr[3] . " " . $attr[1] . "," . $arr[3] . (isset($arr[2]) ? ',' . $arr[2] : NULL);
                        $data['option_data'][0]['option_value'][$attr[0]]['product_seo_url'][0][1] = $this->get_seo($arr[3] . " " . $attr[1]);
                        if (isset($arr[19])) {
                            foreach ($arr[19] as $filter) {
                                if (!in_array($filter[1], $data['ignore_filter'])) {
                                    foreach ($this->array_flatten($filter[2]) as $filter_name) {
                                        if (substr($filter_name, 0, 1) === "{" && substr($filter_name, -1) === "}") {
                                            $filters = json_decode(str_replace("=>", ": ", $filter_name));
                                        } else {
                                            $filters = (array) $filter_name;
                                        }
                                        foreach ($filters as $f) {
                                            $this->filters[$filter[1]]['filters'][$f]['products'][$attr[0]] = $arr[3] . " " . $attr[1];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $data['product_option'][0]['product_option_value'][$attr[0]]['code'] = $attr[0];
                    $data['product_option'][0]['product_option_value'][$attr[0]]['subtract'] = $this->stock_checkout;
                    $data['product_option'][0]['product_option_value'][$attr[0]]['price_prefix'] = '=';
                    $data['product_option'][0]['product_option_value'][$attr[0]]['points_prefix'] = '+';
                    $data['product_option'][0]['product_option_value'][$attr[0]]['weight_prefix'] = '+';
                    $data['product_option'][0]['product_option_value'][$attr[0]]['points'] = NULL;
                    $data['product_option'][0]['product_option_value'][$attr[0]]['weight'] = NULL;
                    $data['product_option'][0]['product_option_value'][$attr[0]]['quantity'] = 0;
                    $data['product_option'][0]['product_option_value'][$attr[0]]['price'] = $data['product_option'][0]['product_option_value'][$attr[0]]['price_old'] = $data['price'];
                    if (isset($arr[17])) {
                        foreach ($arr[17] as $price) {
                            if ($price[1] == $attr[0]) {
                                if ($data['price_uuid']) {
                                    if ($price[2] == $data['price_uuid']) {
                                        $data['option_data'][0]['option_value'][$attr[0]]['price'] = $data['separate'] ? $this->set_price($data['round_price'], $price[0]) : NULL;
                                        $data['product_option'][0]['product_option_value'][$attr[0]]['price'] = $data['product_option'][0]['product_option_value'][$attr[0]]['price_old'] = $this->set_price($data['round_price'], $price[0]);
                                        if ($price[0] < $data['price']) {
                                            $data['price'] = $this->set_price($data['round_price'], $price[0]);
                                        }
                                    }
                                    if (isset($data['price_map'][$price[2]]) && $data['separate']) {
                                        foreach ($data['price_map'][$price[2]] as $key => $customer) {
                                            $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['customer_group_id'] = $customer;
                                            $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['priority'] = 1;
                                            $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['price'] = $this->set_price($data['round_price'], $price[0]);
                                            $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                                            $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                                        }
                                    }
                                } else {
                                    $data['option_data'][0]['option_value'][$attr[0]]['price'] = $data['separate'] ? $this->set_price($data['round_price'], $price[0]) : NULL;
                                    $data['product_option'][0]['product_option_value'][$attr[0]]['price'] = $data['product_option'][0]['product_option_value'][$attr[0]]['price_old'] = $this->set_price($data['round_price'], $price[0]);
                                    if ($price[0] < $data['price']) {
                                        $data['price'] = $this->set_price($data['round_price'], $price[0]);
                                    }
                                    // if (isset($data['price_map'][$price[2]]) && $data['separate']) {
                                    //     foreach ($data['price_map'][$price[2]] as $key => $customer) {
                                    //         $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['customer_group_id'] = $customer;
                                    //         $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['priority'] = 1;
                                    //         $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['price'] = $this->set_price($data['round_price'], $price[0]);
                                    //         $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['date_start'] = date("Y-m-d H:i:s");
                                    //         $data['option_data'][0]['option_value'][$attr[0]]['product_special'][$key]['date_end'] = date("Y-m-d H:i:s", time() + 604800);
                                    //     }
                                    // }
                                    break;
                                }
                            }
                        }
                    }
                    if (isset($arr[16])) {
                        foreach ($arr[16] as $quantity) {
                            if ($quantity[1] == $attr[0]) {
                                if ($data['separate']) {
                                    $data['option_data'][0]['option_value'][$attr[0]]['quantity'] += $quantity[0];
                                }
                                $data['product_option'][0]['product_option_value'][$attr[0]]['quantity'] += $quantity[0];
                            }
                        }
                    }
                    $data['option_data'][0]['option_value'][$attr[0]]['status'] = 1;
                    if ($data['separate']) {
                        if ($data['hide_product']) {
                            if ($data['hide_noprice_product']) {
                                if ($data['option_data'][0]['option_value'][$attr[0]]['price'] <= 0) {
                                    $data['option_data'][0]['option_value'][$attr[0]]['status'] = 0;
                                    if ($this->debug) {
                                        echo 'Product ', $data['option_data'][0]['option_value'][$attr[0]]['name'], ' is null price. Hiding.', "\n";
                                    }
                                }
                            }
                            if ($data['hide_empty_product']) {
                                if ($data['option_data'][0]['option_value'][$attr[0]]['quantity'] <= 0) {
                                    $data['option_data'][0]['option_value'][$attr[0]]['status'] = 0;
                                    if ($this->debug) {
                                        echo 'Product ', $data['option_data'][0]['option_value'][$attr[0]]['name'], ' is empty. Hiding.', "\n";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (isset($this->discounts[$arr[0]])) {
                foreach ($this->discounts[$arr[0]] as $key => $special) {
                    foreach ($data['customer_groups'] as $group) {
                        $data['product_special'][] = array(
                            'customer_group_id' => $group,
                            'priority' => 1,
                            'price' => $this->set_price($data['round_price'], $special['special']),
                            'date_start' =>  $special['date_start'],
                            'date_end' => $special['date_end']
                        );
                    }
                    if (!empty($special['characteristic_uuid']) && isset($data['product_option'][0]['product_option_value'][$special['characteristic_uuid']]['price'])) {
                        if ($this->debug) {
                            echo 'Adding stock for ' . $special['characteristic_uuid'] . "\n";
                        }
                        $data['option_data'][0]['option_value'][$special['characteristic_uuid']]['price'] = $data['separate'] ? $this->set_price($data['round_price'], $special['special']) : NULL;
                        $data['product_option'][0]['product_option_value'][$special['characteristic_uuid']]['price'] = $this->set_price($data['round_price'], $special['special']);
                    }
                }
            }
            $this->products[$arr[0]] = $data;
        }
    }

//Filters
    protected function set_filters($arr) {
        if (isset($arr[19])) {
            foreach ($arr[19] as $filter) {
                $this->filters[$filter[1]]['filter_group_name'] = $filter[1];
                foreach ($this->array_flatten($filter[2]) as $filter_name) {
                    if (isset($this->categories[$arr[5]]['path'])) {
                        foreach (explode("/", $this->categories[$arr[5]]['path']) as $category) {
                            $this->filters[$filter[1]]['filters'][$filter_name]['categories'][$category] = $this->categories[$category]['category_description'][1]['name'];
                        }
                    }
                    $this->filters[$filter[1]]['filters'][$filter_name]['products'][$arr[0]] = $arr[3];
                }
            }
        }
    }

//Color codes
    protected function set_color_codes($arr) {
        if (!empty($arr)) {
            foreach ($arr as $property_type) {
                if (isset($property_type[3])) {
                    if ($property_type[3]['type'] == 'colors') {
                        $this->color_codes[$property_type[0]] = $property_type[3]['colors'];
                    }
                }
            }
        }
    }

//Characteristic Properties
    protected function set_option_characteristic($arr) {
        if (isset($arr[13])) {
            foreach ($arr[13] as $key => $attr) {
                if (isset($attr[2])) {
                    foreach ($attr[2] as $property) {
                        $this->option_characteristic[$property[0]]['code'] = $property[0];
                        $this->option_characteristic[$property[0]]['type'] = isset($this->color_codes[$property[0]]) ? 'colors' : 'select';
                        $this->option_characteristic[$property[0]]['name'] = $property[1];
                        if (isset($property[2])) {
                            $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]] = array();
                            foreach ($this->array_flatten($property[2]) as $p) {
                                if ($this->option_characteristic[$property[0]]['type'] == 'colors') {
                                    $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]][] = isset($this->color_codes[$property[0]][$p]) ? array($p => $this->color_codes[$property[0]][$p]) : array($p => "");
                                } else {
                                    $this->option_characteristic[$property[0]]['option_value'][$arr[0]][$attr[0]][] = $p;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

//Discounts
    protected function set_discounts($arr) {
        if (!empty($arr)) {
            foreach ($arr as $stock) {
                if (isset($stock[19]) && (empty($stock[15]) || $stock[15] == "ONLINE" || $stock[15] == "EVERYWHERE")) {
                    foreach ($stock[19] as $product) {
                        if ($product[3] > 0) {
                            $this->discounts[$product[0]][] = array(
                                'characteristic_uuid' => $product[1],
                                'price' => $product[2],
                                'discount' => $product[3],
                                'special' => $product[4],
                                'date_start' => $stock[1],
                                'date_end' => $stock[2]
                            );
                        }
                    }
                }
            }
        }
    }

//Stocks
    protected function set_stocks_template() {
        $data = array(
            'server'    => $this->config->get('module_trade_import_server'),
            'store' => $this->model_extension_module_trade_import->get_store_name(),
            'keep_names' => $this->config->get('module_trade_import_keep_names'),
            'keep_meta' => $this->config->get('module_trade_import_keep_meta')
        );
        return $data;
    }

    protected function set_stocks($arr, $data) {
        if (!empty($arr)) {
            foreach ($arr as $stock) {
                if (isset($stock[19]) && (empty($stock[15]) || $stock[15] == "ONLINE" || $stock[15] == "EVERYWHERE") && ($stock[6] > 0)) {
                    $this->stocks[$stock[0]] = array(
                        'stocks_uuid' => $stock[0],
                        'start_at' => $stock[1],
                        'end_at' => $stock[2],
                        'discount' => $stock[6],
                        'image' => isset($stock[18]) ? $this->get_image($data, array_filter(explode(",", substr($stock[18], 1, -1)))[0]) : '',
                        'sort_order' => 0,
                        'name' => $stock[3],
                        'description' => $stock[16],
                        'requirements' => '',
                        'meta_title' => $stock[3],
                        'meta_description' => $stock[16],
                        'meta_keyword' => $stock[3] . "," . $data['store'],
                        'stocks_seo_url' => $this->get_seo($stock[3]),
                        'keep_names' => $data['keep_names'],
                        'keep_meta' => $data['keep_meta']
                    );
                    foreach ($stock[19] as $product) {
                        $this->stocks[$stock[0]]['products'][$product[0]] = array(
                            'nomenclature_uuid' => $product[0]
                        );
                    }
                }
            }
        }
    }

//Warehouses
    protected function set_warehouses($arr) {
        if (!empty($arr)) {
            foreach ($arr as $wd) {
                $working_hours = $wd[4];
                foreach ($working_hours as $k => $wh) {
                    $time = date_create($wh['starting_time'], timezone_open('Europe/London'));
                    date_timezone_set($time, timezone_open($this->config->get('module_trade_import_time_zone')));
                    $working_hours[$k]['starting_time'] = date_format($time, 'H:i:s');

                    $time = date_create($wh['ending_time'], timezone_open('Europe/London'));
                    date_timezone_set($time, timezone_open($this->config->get('module_trade_import_time_zone')));
                    $working_hours[$k]['ending_time'] = date_format($time, 'H:i:s');
                }
                $this->warehouses[$wd[0]] = array(
                    'storage_uuid' => $wd[0],
                    'name' => $wd[2],
                    'address' => $wd[3],
                    'working_hours' => json_encode($working_hours, JSON_UNESCAPED_UNICODE)
                );
            }
        }
    }

//Connect to Trade
    protected function connect() {   
        if ($this->config->get('module_trade_import_local_json')) {
            $response = file_get_contents('records.json');
            if ($response === false) {
                if ($this->debug) {
                    echo 'Unable to open file.';
                }
                $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
                return 0;
            } else {
                if ($this->debug) {
                    echo 'File opened.', "\n";
                }
            }
        } else {
            if (!$this->config->get('module_trade_import_enable_old_api')) {
                $url = $this->config->get('module_trade_import_code');
                $nomenclature_url = $this->config->get('module_trade_import_nomenclature');
                $token = $this->config->get('module_trade_import_token');
                $time = time();
                $date = date('c', $time);
                $ch = curl_init();
                $header = array();
                $data_string = json_encode(array('token' => $token));
                $header[] = "Content-Type: application/json";
                $header[] = "UUID: " . $token;
                $header[] = "Timestamp: " . $date;
                $header[] = "Authorization: " . hash("sha512", $token . $time);
                $header[] = "Content-Length: " . strlen($data_string);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                $response = curl_exec($ch);
                if ($response === false) {
                    if ($this->debug) {
                        echo 'Curl error: ', curl_error($ch), "\n";
                        echo $response;
                    }
                    curl_close($ch);
                    $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
                    return 0;
                } else {
                    if ($this->debug) {
                        echo $url, " connection successful.", "\n";
                    }
                    $response_decoded = json_decode($response, true);
                    $this->access_token = $response_decoded['access_token'];
                    curl_close($ch);
                    $ch = curl_init();
                    $header = array();
                    $header[] = "Content-Type: application/json";
                    $header[] = "Authorization: Bearer " . $this->access_token;
                    curl_setopt($ch, CURLOPT_URL, $nomenclature_url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
            } else {
                $url = $this->config->get('module_trade_import_old_api_address');
                $token = $this->config->get('module_trade_import_old_api_token');
                $ch = curl_init();
                $header = array();
                $header[] = "Content-Type: application/json";
                $header[] = "Authorization: " . $token;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                if ($response === false) {
                    if ($this->debug) {
                        echo 'Curl error: ', curl_error($ch), "\n";
                        echo $response;
                    }
                    curl_close($ch);
                    $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
                    return 0;
                } else {
                    if ($this->debug) {
                        echo $url, " connection successful. Getting file.", "\n";
                    }
                }
                curl_close($ch);
            }
        }
        return $response;
    }

//Main function. URL for check: http://sys.sm27.ru/easykkm/api/v1/records.json  
    public function get_json() {
        date_default_timezone_set($this->config->get('module_trade_import_time_zone'));
        $this->model_extension_module_trade_import->editSettingValue('module_trade_import', 'module_trade_import_sync_schedule', date("Y-m-d H:i:00", strtotime("+" . str_replace("_", " ",  $this->config->get('module_trade_import_sync_period')), time())));
        $response = $this->connect();
        if (!$response) {
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            if ($this->debug) {
                echo 'Connection error.', "\n";
            }
            return 0;
        }
        if ($this->config->get('module_trade_import_save_json')) {
            file_put_contents('records.json', $response);
        }
        $groups_unsorted = json_decode($this->get_groups($response), true);
        if (!$groups_unsorted) {
            if ($this->debug) {
                echo 'Invalid JSON, unable to get groups.', "\n";
                echo $response;
            }
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        }
        $nomenclatures = $this->get_nomenclatures($response);
        if (!json_decode($nomenclatures[0])) {
            if ($this->debug) {
                echo 'Invalid JSON, unable to get nomenclatures.', "\n";
                echo $response;
            }
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        }
        $groups = $this->organize_groups($groups_unsorted['groups']['values']);
        $timestamp = $this->get_timestamp($response);
        $stocks = json_decode($this->get_stocks($response), true);
        if ($stocks) {
            $this->set_discounts($stocks['stocks']['values']);
            $stocks_template = $this->set_stocks_template();
            $this->set_stocks($stocks['stocks']['values'], $stocks_template);
        }
        $warehouses = json_decode($this->get_warehouses($response), true);
        if ($warehouses) {
            $this->set_warehouses($warehouses['storages']['values']);
        }
        $property_types = json_decode($this->get_property_types($response), true);
        if ($property_types) {
            foreach ($property_types['property_types']['values'] as $key => $value) {
                $property_types['property_types']['values'][$key][3] = json_decode(str_replace("trade_import_replace", "\\\"", preg_replace('/[\\\\]+/', '', str_replace("\\\\\\\"", "trade_import_replace", substr($value[3], 2, -2)))), true);
            }
            $this->set_color_codes($property_types['property_types']['values']);
        }
        unset($response);
        if ($this->debug) {
            echo memory_get_usage(true), ' bytes.', "\n";
        }
        if (memory_get_usage(true) > 100000000) {
            if ($this->debug) {
                echo "Memory usage is too high, disabling debug", "\n";
            }
            $this->debug = false;
        }
        $category_template = $this->set_category_template();
        $top_group_uuid = array_filter(explode(",", $this->config->get('module_trade_import_top_category')));
        $ignore_category = array_flip(array_filter(explode(",", $this->config->get('module_trade_import_ignore_category'))));
        $this->stock_checkout = !$this->config->get('config_stock_checkout');
        if ($this->config->get('module_trade_import_add_category')) {
            if (empty($top_group_uuid)) {
                foreach ($groups as $group) {
                    if (!isset($ignore_category[$group[0]])) {
                        $this->set_category($group, $category_template);
                    }
                }
            } else {
                foreach ($groups as $group) {
                    if (in_array($group[0], $top_group_uuid)) {
                        foreach ($group[7] as $g) {
                            if (!isset($ignore_category[$g[0]])) {
                                $this->set_category($g, $category_template);
                            }
                        }
                    }
                }
            }
            $this->model_extension_module_trade_import->add_multiple_categories($this->categories, $this->debug);
        }
        $category_codes = $this->model_extension_module_trade_import->get_category_codes();
        $product_codes = array();
        $product_template = $this->set_product_template();
        if ($this->config->get('module_trade_import_add_product')) {
            foreach ($nomenclatures as $nomenclature) {
                $x = json_decode($nomenclature);
                foreach ($x as $n) {
                    $this->set_products($n, $product_template, $category_codes);
                }
                $x = NULL;
                if ($this->debug) {
                    echo memory_get_usage(true), ' bytes.', "\n";
                }
                if (!empty($this->products)) {
                    $this->model_extension_module_trade_import->add_multiple_products($this->products, $this->config->get('module_trade_import_add_separate_products'), $this->debug);
                }
                $product_codes += array_flip(array_keys($this->products));
                $this->products = NULL;
                $this->products = array();
                gc_collect_cycles();
            }
            if ($this->debug) {
                echo memory_get_usage(true), ' bytes.', "\n";
            }
            $this->model_extension_module_trade_import->add_multiple_filters($this->filters, $this->config->get('module_trade_import_parent_id'), $this->debug);
            if (!empty($this->filters_color)) {
                $this->model_extension_module_trade_import->add_multiple_filters_color($this->filters_color, $this->config->get('module_trade_import_parent_id'), $this->debug);
            }
            //OCFilter Integration
            if ($this->config->get('module_trade_import_ocfilter')) {
                $this->model_extension_module_trade_import->OCFilterCopyFilters(array(
                    'copy_type' => 'checkbox',
                    'copy_status' => 1,
                    'copy_attribute' => 0,
                    'attribute_separator' => '',
                    'copy_filter' => 1,
                    'copy_option' => 0,
                    'copy_truncate' => 1,
                    'copy_category' => 1
                ));
                if (!empty($this->filters_color)) {
                    $this->model_extension_module_trade_import->OCFilterSetFilterColors($this->filters_color, $this->debug);
                }
            }
            if (!empty($this->option_characteristic)) {
                $this->model_extension_module_trade_import->add_multiple_option_characteristic($this->option_characteristic, $this->debug);
            }
            if (!empty($this->warehouses)) {
                $this->model_extension_module_trade_import->add_multiple_warehouses($this->warehouses, $this->debug);
            }
            if (!empty($this->stocks)) {
                $this->model_extension_module_trade_import->add_multiple_stocks($this->stocks, $this->debug);
            }
        }
        if ($this->config->get('module_trade_import_delete_product')) {
            $this->model_extension_module_trade_import->delete_multiple_products($product_codes, $this->debug);
        }
        if ($this->config->get('module_trade_import_hide_product') && !$this->config->get('module_trade_import_add_product')) {
            $this->model_extension_module_trade_import->hide_products();
        } else if (!$this->config->get('module_trade_import_hide_product') && !$this->config->get('module_trade_import_add_product')) {
            $this->model_extension_module_trade_import->show_products();
        }
        if ($this->config->get('module_trade_import_delete_category')) {
            $this->model_extension_module_trade_import->delete_multiple_categories(array_flip(array_column($groups_unsorted['groups']['values'], 0)), true);
        }
        if ($this->config->get('module_trade_import_hide_category')) {
            $this->hide_categories();
        }
        if ($this->debug) {
            echo "Cleaning cache: \n";
            print_r(glob(DIR_CACHE . "cache.trade_import*"));
        }
        array_map('unlink', glob(DIR_CACHE . "cache.trade_import*"));
        $this->model_extension_module_trade_import->add_operation($timestamp, 1);
    }

    //Debug functions
    public function show_groups() {
        $response = $this->connect();
        $groups_unsorted = json_decode($this->get_groups($response), true);
        if (!$groups_unsorted) {
            echo 'Invalid JSON, unable to get groups.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
        } else {
            $groups = $this->organize_groups($groups_unsorted['groups']['values']);
            echo "JSON groups:", "\n";
            print_r($groups);
            $category_template = $this->set_category_template();
            foreach ($groups as $group) {
                $this->set_category($group, $category_template);
            }
            print_r($this->categories);
            $ignore_category = array_flip(array_filter(explode(",", $this->config->get('module_trade_import_ignore_category'))));
            print_r($ignore_category);
        }
    }

    public function show_nomenclatures() {
        $response = $this->connect();
        $nomenclatures = $this->get_nomenclatures($response);
        $groups_unsorted = json_decode($this->get_groups($response), true);
        $groups = $this->organize_groups($groups_unsorted['groups']['values']);
        if (!json_decode($nomenclatures[0])) {
            echo 'Invalid JSON, unable to get nomenclatures.', "\n";
            print_r($this->get_nomenclatures($response));
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            echo "JSON nomenclatures:", "\n";
            $this->stock_checkout = !$this->config->get('config_stock_checkout');
            $category_template = $this->set_category_template();
            foreach ($groups as $group) {
                $this->set_category($group, $category_template);
            }
            $product_template = $this->set_product_template();
            $category_codes = $this->model_extension_module_trade_import->get_category_codes();
            foreach ($nomenclatures as $key => $nomenclature) {
                echo $key . "\n";
                //echo $nomenclature . "\n";
                if (!json_decode($nomenclature)) {
                    echo "Unable to decode part" . "\n" . $nomenclature . "\n";
                } else {
                    foreach (json_decode($nomenclature) as $n) {
                        print_r($n);
                    }
                }
                if ($this->debug) {
                    echo memory_get_usage(true), ' bytes.', "\n";
                }
            }
            // print_r($nomenclatures);
            // if ($this->debug) {
            //     echo memory_get_usage(true), ' bytes.', "\n";
            // }
        }
    }

    public function show_stocks() {
        $response = $this->connect();
        $stocks = json_decode($this->get_stocks($response), true);
        if (!$stocks) {
            echo 'Invalid JSON, unable to get stocks.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
        } else {
            echo "JSON stocks:", "\n";
            $this->set_discounts($stocks['stocks']['values']);
            $this->set_stocks($stocks['stocks']['values'], $this->set_stocks_template());
            print_r($stocks);
            print_r($this->discounts);
        }
    }

    public function show_filters() {
        $response = $this->connect();
        $nomenclatures = $this->get_nomenclatures($response);
        $groups_unsorted = json_decode($this->get_groups($response), true);
        $groups = $this->organize_groups($groups_unsorted['groups']['values']);
        if (!json_decode($nomenclatures[0])) {
            echo 'Invalid JSON, unable to get filters.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            echo "JSON filters: ", "\n";
            $this->stock_checkout = !$this->config->get('config_stock_checkout');
            $category_template = $this->set_category_template();
            foreach ($groups as $group) {
                $this->set_category($group, $category_template);
            }
            foreach ($nomenclatures as $nomenclature) {
                foreach (json_decode($nomenclature) as $n) {
                    $this->set_filters($n);
                }
                if ($this->debug) {
                    echo memory_get_usage(true), ' bytes.', "\n";
                }
            }
            print_r($this->filters);
        }
    }

    public function show_option_characteristic() {
        $response = $this->connect();
        $property_types = json_decode($this->get_property_types($response), true);
        if (!$property_types) {
            echo 'Invalid JSON, unable to property types.', "\n";
            echo $this->get_property_types($response), "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            echo "JSON property types:", "\n";
            foreach ($property_types['property_types']['values'] as $key => $value) {
                $property_types['property_types']['values'][$key][3] = json_decode(str_replace("trade_import_replace", "\\\"", preg_replace('/[\\\\]+/', '', str_replace("\\\\\\\"", "trade_import_replace", substr($value[3], 2, -2)))), true);
            }
            $this->set_color_codes($property_types['property_types']['values']);
            print_r($property_types);
            print_r($this->color_codes);
            $nomenclatures = $this->get_nomenclatures($response);
                echo "JSON option characteristics: ", "\n";
                foreach ($nomenclatures as $nomenclature) {
                foreach (json_decode($nomenclature) as $n) {
                    $this->set_option_characteristic($n);
                }
                if ($this->debug) {
                    echo memory_get_usage(true), ' bytes.', "\n";
                }
            }
            print_r($this->option_characteristic);
        }
    }

    public function show_prices() {
        $response = $this->connect();
        $prices = json_decode($this->get_prices($response), true);
        if (!$prices) {
            echo 'Invalid JSON, unable to get prices.', "\n";
            echo $this->get_prices($response), "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            echo "JSON prices:", "\n";
            print_r($prices);
        }
    }

    public function show_warehouses() {
        $response = $this->connect();
        $warehouses = json_decode($this->get_warehouses($response), true);
        if (!$warehouses) {
            echo 'Invalid JSON, unable to get prices.', "\n";
            echo $this->get_warehouses($response), "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            echo "JSON warehouses:", "\n";
            print_r($warehouses);
        }
    }

    public function add_one_product() {
        $response = $this->connect();
        $nomenclatures = $this->get_nomenclatures($response);
        $product_template = $this->set_product_template();
        $one_product_uuid = $this->config->get('module_trade_import_add_one_product');
        if (!json_decode($nomenclatures[0])) {
            echo 'Invalid JSON, unable to get nomenclatures.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            foreach ($nomenclatures as $nomenclature) {
                foreach (json_decode($nomenclature) as $n) {
                    if ($n[0] == $one_product_uuid) {
                        print_r($n);
                        $this->import_products($n, $product_template, array(), $this->model_extension_module_trade_import->get_product_codes(), $this->model_extension_module_trade_import->get_option_codes(), true);
                        return;
                    }
                }
            }
        }
    }

    public function add_one_separate_product() {
        $response = $this->connect();
        $nomenclatures = $this->get_nomenclatures($response);
        $product_template = $this->set_product_template();
        $one_product_uuid = $this->config->get('module_trade_import_add_one_product');
        if (!json_decode($nomenclatures[0])) {
            echo 'Invalid JSON, unable to get nomenclatures.', "\n";
            echo $response;
            $this->model_extension_module_trade_import->add_operation(NULL, 0, $response);
            return 0;
        } else {
            foreach ($nomenclatures as $nomenclature) {
                foreach (json_decode($nomenclature) as $n) {
                    if ($n[0] == $one_product_uuid) {
                        print_r($n);
                        $this->import_separate_products($n, $product_template, array(), $this->model_extension_module_trade_import->get_product_codes(), true);
                        return;
                    }
                }
            }
        }
    }

    public function add_indexes() {
        $this->model_extension_module_trade_import->add_indexes();
        echo 'Indexes added';
    }

    public function cache_images() {
        ini_set('max_execution_time', 0);
        $this->model_extension_module_trade_import->cache_images($this->debug);
    }

    public function clean_orders() {
        $this->model_extension_module_trade_import->clean_orders();
        echo 'Trade Orders tables cleaned';
    }

    public function clean_tables() {
        $this->model_extension_module_trade_import->clean_tables();
        echo 'Trade tables cleaned';
    }

    public function clean_all() {
        $this->model_extension_module_trade_import->clean_all($this->config->get('module_trade_import_parent_id'), $this->debug);
        echo 'All tables cleaned';
    }

    public function get_customer_groups() {
        echo "Customer Groups: \n";
        print_r($this->model_extension_module_trade_import->getCustomerGroups());
        echo "\nPrice Map: \n";
        $price_map = array();
        $maps = array_filter(explode(";", $this->config->get('module_trade_import_price_map')));
        if (!empty($maps)) {
            foreach ($maps as $map) {
                $t = explode(':', $map);
                $price_map[$t[0]] = explode(',', $t[1]);
            }
        }
        print_r($price_map);
    }

    public function get_orders() {
        $orders = $this->model_extension_module_trade_import->get_orders();
        echo "Trade orders: \n";
        print_r($orders);
    }
}