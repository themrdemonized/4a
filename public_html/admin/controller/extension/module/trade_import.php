<?php
class ControllerExtensionModuleTradeImport extends Controller {

    public function index() {
        $this->load->language('extension/module/trade_import');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');
        $this->load->model('extension/module/trade_import');
        $this->model_extension_module_trade_import->set_tables();
        if ($this->config->get('module_trade_import_time_zone') !== null) {
           date_default_timezone_set($this->config->get('module_trade_import_time_zone'));
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            date_default_timezone_set($this->request->post['module_trade_import_time_zone']);
            $this->request->post['module_trade_import_sync_schedule'] = date("Y-m-d H:i:00", strtotime("+" . str_replace("_", " ",  $this->request->post['module_trade_import_sync_period']), time()));
            if ($this->request->post['module_trade_import_enable_sync']) {
                $cron_action = 'add';
            } else {
                $cron_action = 'remove';
            }
            $this->cron_setting($cron_action);
            $data['next_schedule'] = $this->request->post['module_trade_import_sync_schedule'];
            $this->model_setting_setting->editSetting('module_trade_import', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['code'])) {
            $data['error_code'] = $this->error['code'];
        } else {
            $data['error_code'] = '';
        }
    
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/trade_import', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/trade_import', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        if (isset($this->request->post['module_trade_import_server'])) {
            $data['module_trade_import_server'] = $this->request->post['module_trade_import_server'];
        } else {
            $data['module_trade_import_server'] = $this->config->get('module_trade_import_server');
        }

        if (isset($this->request->post['module_trade_import_code'])) {
            $data['module_trade_import_code'] = $this->request->post['module_trade_import_code'];
        } else {
            $data['module_trade_import_code'] = $this->config->get('module_trade_import_code');
        }

        if (isset($this->request->post['module_trade_import_nomenclature'])) {
            $data['module_trade_import_nomenclature'] = $this->request->post['module_trade_import_nomenclature'];
        } else {
            $data['module_trade_import_nomenclature'] = $this->config->get('module_trade_import_nomenclature');
        }
    
        if (isset($this->request->post['module_trade_import_token'])) {
            $data['module_trade_import_token'] = $this->request->post['module_trade_import_token'];
        } else {
            $data['module_trade_import_token'] = $this->config->get('module_trade_import_token');
        }

        if (isset($this->request->post['module_trade_import_enable_old_api'])) {
            $data['module_trade_import_enable_old_api'] = $this->request->post['module_trade_import_enable_old_api'];
        } else {
            $data['module_trade_import_enable_old_api'] = $this->config->get('module_trade_import_enable_old_api');
        }

        if (isset($this->request->post['module_trade_import_old_api_address'])) {
            $data['module_trade_import_old_api_address'] = $this->request->post['module_trade_import_old_api_address'];
        } else {
            $data['module_trade_import_old_api_address'] = $this->config->get('module_trade_import_old_api_address');
        }

        if (isset($this->request->post['module_trade_import_old_api_token'])) {
            $data['module_trade_import_old_api_token'] = $this->request->post['module_trade_import_old_api_token'];
        } else {
            $data['module_trade_import_old_api_token'] = $this->config->get('module_trade_import_old_api_token');
        }

        if (isset($this->request->post['module_trade_import_enable_order'])) {
            $data['module_trade_import_enable_order'] = $this->request->post['module_trade_import_enable_order'];
        } else {
            $data['module_trade_import_enable_order'] = $this->config->get('module_trade_import_enable_order');
        }

        if (isset($this->request->post['module_trade_import_order_address'])) {
            $data['module_trade_import_order_address'] = $this->request->post['module_trade_import_order_address'];
        } else {
            $data['module_trade_import_order_address'] = $this->config->get('module_trade_import_order_address');
        }

        if (isset($this->request->post['module_trade_import_order_token'])) {
            $data['module_trade_import_order_token'] = $this->request->post['module_trade_import_order_token'];
        } else {
            $data['module_trade_import_order_token'] = $this->config->get('module_trade_import_order_token');
        }

        if (isset($this->request->post['module_trade_import_price'])) {
            $data['module_trade_import_price'] = $this->request->post['module_trade_import_price'];
        } else {
            $data['module_trade_import_price'] = $this->config->get('module_trade_import_price');
        }

        if (isset($this->request->post['module_trade_import_price_map'])) {
            $data['module_trade_import_price_map'] = $this->request->post['module_trade_import_price_map'];
        } else {
            $data['module_trade_import_price_map'] = $this->config->get('module_trade_import_price_map');
        }

        if (isset($this->request->post['module_trade_import_parent_id'])) {
            $data['module_trade_import_parent_id'] = $this->request->post['module_trade_import_parent_id'];
        } else {
            $data['module_trade_import_parent_id'] = $this->config->get('module_trade_import_parent_id');
        }

        if (isset($this->request->post['module_trade_import_top_category'])) {
            $data['module_trade_import_top_category'] = $this->request->post['module_trade_import_top_category'];
        } else {
            $data['module_trade_import_top_category'] = $this->config->get('module_trade_import_top_category');
        }

        if (isset($this->request->post['module_trade_import_ignore_category'])) {
            $data['module_trade_import_ignore_category'] = $this->request->post['module_trade_import_ignore_category'];
        } else {
            $data['module_trade_import_ignore_category'] = $this->config->get('module_trade_import_ignore_category');
        }

        if (isset($this->request->post['module_trade_import_ignore_filter'])) {
            $data['module_trade_import_ignore_filter'] = $this->request->post['module_trade_import_ignore_filter'];
        } else {
            $data['module_trade_import_ignore_filter'] = $this->config->get('module_trade_import_ignore_filter');
        }
        
        if (isset($this->request->post['module_trade_import_add_properties_to_filters'])) {
            $data['module_trade_import_add_properties_to_filters'] = $this->request->post['module_trade_import_add_properties_to_filters'];
        } else {
            $data['module_trade_import_add_properties_to_filters'] = $this->config->get('module_trade_import_add_properties_to_filters');
        }
    
        if (isset($this->request->post['module_trade_import_enable_sync'])) {
            $data['module_trade_import_enable_sync'] = $this->request->post['module_trade_import_enable_sync'];
        } else {
            $data['module_trade_import_enable_sync'] = $this->config->get('module_trade_import_enable_sync');
        }
    
        if (isset($this->request->post['module_trade_import_sync_period'])) {
            $data['module_trade_import_sync_period'] = $this->request->post['module_trade_import_sync_period'];
        } else {
            $data['module_trade_import_sync_period'] = $this->config->get('module_trade_import_sync_period');
        }
    
        if (isset($this->request->post['module_trade_import_time_zone'])) {
            $data['module_trade_import_time_zone'] = $this->request->post['module_trade_import_time_zone'];
        } else {
            $data['module_trade_import_time_zone'] = $this->config->get('module_trade_import_time_zone');
        }
    
        if (isset($this->request->post['module_trade_import_local_json'])) {
            $data['module_trade_import_local_json'] = $this->request->post['module_trade_import_local_json'];
        } else {
            $data['module_trade_import_local_json'] = $this->config->get('module_trade_import_local_json');
        }
    
        if (isset($this->request->post['module_trade_import_save_json'])) {
            $data['module_trade_import_save_json'] = $this->request->post['module_trade_import_save_json'];
        } else {
            $data['module_trade_import_save_json'] = $this->config->get('module_trade_import_save_json');
        }
    
        if (isset($this->request->post['module_trade_import_add_category'])) {
            $data['module_trade_import_add_category'] = $this->request->post['module_trade_import_add_category'];
        } else {
            $data['module_trade_import_add_category'] = $this->config->get('module_trade_import_add_category');
        }
    
        if (isset($this->request->post['module_trade_import_delete_category'])) {
            $data['module_trade_import_delete_category'] = $this->request->post['module_trade_import_delete_category'];
        } else {
            $data['module_trade_import_delete_category'] = $this->config->get('module_trade_import_delete_category');
        }
    
        if (isset($this->request->post['module_trade_import_hide_category'])) {
            $data['module_trade_import_hide_category'] = $this->request->post['module_trade_import_hide_category'];
        } else {
            $data['module_trade_import_hide_category'] = $this->config->get('module_trade_import_hide_category');
        }

        if (isset($this->request->post['module_trade_import_sub_filters'])) {
            $data['module_trade_import_sub_filters'] = $this->request->post['module_trade_import_sub_filters'];
        } else {
            $data['module_trade_import_sub_filters'] = $this->config->get('module_trade_import_sub_filters');
        }
    
        if (isset($this->request->post['module_trade_import_add_product'])) {
            $data['module_trade_import_add_product'] = $this->request->post['module_trade_import_add_product'];
        } else {
            $data['module_trade_import_add_product'] = $this->config->get('module_trade_import_add_product');
        }
    
        if (isset($this->request->post['module_trade_import_delete_product'])) {
            $data['module_trade_import_delete_product'] = $this->request->post['module_trade_import_delete_product'];
        } else {
            $data['module_trade_import_delete_product'] = $this->config->get('module_trade_import_delete_product');
        }
    
        if (isset($this->request->post['module_trade_import_hide_product'])) {
            $data['module_trade_import_hide_product'] = $this->request->post['module_trade_import_hide_product'];
        } else {
            $data['module_trade_import_hide_product'] = $this->config->get('module_trade_import_hide_product');
        }

        if (isset($this->request->post['module_trade_import_hide_empty_product'])) {
            $data['module_trade_import_hide_empty_product'] = $this->request->post['module_trade_import_hide_empty_product'];
        } else {
            $data['module_trade_import_hide_empty_product'] = $this->config->get('module_trade_import_hide_empty_product');
        }

        if (isset($this->request->post['module_trade_import_add_separate_products'])) {
            $data['module_trade_import_add_separate_products'] = $this->request->post['module_trade_import_add_separate_products'];
        } else {
            $data['module_trade_import_add_separate_products'] = $this->config->get('module_trade_import_add_separate_products');
        }

         $data['round_price_options'] = array(
            array('text' => $this->language->get('text_round_price_off'), 'value' => 'off'),
            array('text' => $this->language->get('text_round_price_normal'), 'value' => 'normal'),
            array('text' => $this->language->get('text_round_price_up'), 'value' => 'up'),
            array('text' => $this->language->get('text_round_price_down'), 'value' => 'down'),
            array('text' => $this->language->get('text_round_price_middle'), 'value' => 'middle'),
        );

        if (isset($this->request->post['module_trade_import_round_price'])) {
            $data['module_trade_import_round_price'] = $this->request->post['module_trade_import_round_price'];
        } else {
            $data['module_trade_import_round_price'] = $this->config->get('module_trade_import_round_price');
        }

        if (isset($this->request->post['module_trade_import_ocfilter'])) {
            $data['module_trade_import_ocfilter'] = $this->request->post['module_trade_import_ocfilter'];
        } else {
            $data['module_trade_import_ocfilter'] = $this->config->get('module_trade_import_ocfilter');
        }

        if (isset($this->request->post['module_trade_import_keep_names'])) {
            $data['module_trade_import_keep_names'] = $this->request->post['module_trade_import_keep_names'];
        } else {
            $data['module_trade_import_keep_names'] = $this->config->get('module_trade_import_keep_names');
        }

        if (isset($this->request->post['module_trade_import_keep_product_description'])) {
            $data['module_trade_import_keep_product_description'] = $this->request->post['module_trade_import_keep_product_description'];
        } else {
            $data['module_trade_import_keep_product_description'] = $this->config->get('module_trade_import_keep_product_description');
        }

        if (isset($this->request->post['module_trade_import_keep_meta'])) {
            $data['module_trade_import_keep_meta'] = $this->request->post['module_trade_import_keep_meta'];
        } else {
            $data['module_trade_import_keep_meta'] = $this->config->get('module_trade_import_keep_meta');
        }

        if (isset($this->request->post['module_trade_import_short_url'])) {
            $data['module_trade_import_short_url'] = $this->request->post['module_trade_import_short_url'];
        } else {
            $data['module_trade_import_short_url'] = $this->config->get('module_trade_import_short_url');
        }

        if (isset($this->request->post['module_trade_import_full_path_url'])) {
            $data['module_trade_import_full_path_url'] = $this->request->post['module_trade_import_full_path_url'];
        } else {
            $data['module_trade_import_full_path_url'] = $this->config->get('module_trade_import_full_path_url');
        }

        if (isset($this->request->post['module_trade_import_add_one_product'])) {
            $data['module_trade_import_add_one_product'] = $this->request->post['module_trade_import_add_one_product'];
        } else {
            $data['module_trade_import_add_one_product'] = $this->config->get('module_trade_import_add_one_product');
        }
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['userToken'] = $this->session->data['user_token'];
        if (!isset($data['next_schedule'])) {
            $data['next_schedule'] = $this->config->get('module_trade_import_sync_schedule');
        }
        $latest_operation = $this->model_extension_module_trade_import->get_latest_operation();
        if ($latest_operation) {
            $data['latest_id'] = $latest_operation['operation_id'];
            $data['latest_timestamp'] = $latest_operation['timestamp'];
            $data['latest_json_timestamp'] = $latest_operation['json_timestamp'];
            $data['latest_success'] = $latest_operation['success'];
        }
        $data['sync_options'] = array(
            array('text' => $this->language->get('text_2_min'), 'value' => '2_minutes'),
            array('text' => $this->language->get('text_10_min'), 'value' => '10_minutes'),
            array('text' => $this->language->get('text_30_min'), 'value' => '30_minutes'),
            array('text' => $this->language->get('text_1_hour'), 'value' => '1_hour'),
            array('text' => $this->language->get('text_2_hour'), 'value' => '2_hours'),
            array('text' => $this->language->get('text_3_hour'), 'value' => '3_hours'),
            array('text' => $this->language->get('text_6_hour'), 'value' => '6_hours'),
            array('text' => $this->language->get('text_12_hour'), 'value' => '12_hours'),
            array('text' => $this->language->get('text_1_day'), 'value' => '1_day'),
            array('text' => $this->language->get('text_2_days'), 'value' => '2_days'),
            array('text' => $this->language->get('text_3_days'), 'value' => '3_days'),
            array('text' => $this->language->get('text_1_week'), 'value' => '1_week')
        );
        $data['timezone_options'] = \DateTimeZone::listIdentifiers(DateTimeZone::ALL);
    
        $this->response->setOutput($this->load->view('extension/module/trade_import', $data));
    }

    protected function cron_setting($cron_action = 'add')
    {
        $trade_import_dir = DIR_CONFIG . 'trade_import' . '/';
        $command = '* * * * * php ' . $trade_import_dir . 'trade_import.php > /dev/null 2>&1';
        $jobs = explode(PHP_EOL, shell_exec('crontab -l'));
        foreach ($jobs as $key => $job) {
            if (($job == $command) || empty($job)) {
                unset($jobs[$key]);
            }
        }
        $jobs = implode(PHP_EOL, $jobs);
        $command = ($cron_action == 'add') ? $command . PHP_EOL : '';
        file_put_contents($trade_import_dir . 'cron.txt', $jobs . PHP_EOL . $command);
        exec('crontab -r');
        exec('crontab ' . $trade_import_dir . 'cron.txt');
    }

    public function get_json() {
        $this->trade_import->get_json(); 
    }

//Debug functions
    public function show_groups() {
        $this->trade_import->show_groups();
    }

    public function show_nomenclatures() {
        $this->trade_import->show_nomenclatures();
    }

    public function show_option_characteristic() {
        $this->trade_import->show_option_characteristic();
    }

    public function show_stocks() {
        $this->trade_import->show_stocks();
    }

    public function show_filters() {
        $this->trade_import->show_filters();
    }

    public function show_prices() {
        $this->trade_import->show_prices();
    }

    public function show_warehouses() {
        $this->trade_import->show_warehouses();
    }

    public function add_indexes() {
        $this->trade_import->add_indexes();
    }

    public function cache_images() {
        $this->trade_import->cache_images();
    }

    public function add_one_product() {
        $this->trade_import->add_one_product();
    }

    public function add_one_separate_product() {
        $this->trade_import->add_one_separate_product();
    }

    public function clean_orders() {
        $this->trade_import->clean_orders();
    }

    public function clean_tables() {
        $this->trade_import->clean_tables();
    }

    public function clean_all() {
        $this->trade_import->clean_all();
    }

    public function get_customer_groups() {
        $this->trade_import->get_customer_groups();
    }

    public function get_orders() {
        $this->trade_import->get_orders();
    }

    public function save_settings() {
        $json = array();
        $this->load->language('extension/module/trade_import');
        $this->load->model('setting/setting');
        if ($this->validate()) {
            date_default_timezone_set($this->request->post['module_trade_import_time_zone']);
            $this->request->post['module_trade_import_sync_schedule'] = date("Y-m-d H:i:00", strtotime("+" . str_replace("_", " ",  $this->request->post['module_trade_import_sync_period']), time()));
            if ($this->request->post['module_trade_import_enable_sync']) {
                $cron_action = 'add';
            } else {
                $cron_action = 'remove';
            }
            $this->cron_setting($cron_action);
            $data['next_schedule'] = $this->request->post['module_trade_import_sync_schedule'];
            $this->model_setting_setting->editSetting('module_trade_import', $this->request->post);
            $json['success'] = $this->language->get('text_success');
            $json['next_schedule'] = $this->request->post['module_trade_import_sync_schedule'];
        } else {
            $json['error_code'] = $this->error['code'];
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

//Validation
    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/trade_import')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['module_trade_import_server']) {
            $this->error['code'] = $this->language->get('error_code');
        }

        if (!$this->request->post['module_trade_import_code']) {
            $this->error['code'] = $this->language->get('error_code');
        }

        if (!$this->request->post['module_trade_import_nomenclature']) {
            $this->error['code'] = $this->language->get('error_code');
        }

        return !$this->error;
    }
}